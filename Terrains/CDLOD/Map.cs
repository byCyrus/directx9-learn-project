﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace Engine.Cdlod
{
    /// <summary>
    /// </summary>
    public interface IHeightMap
    {
        /// <summary> the Width of image</summary>
        int SizeX { get; }
        /// <summary> the Height of image</summary>
        int SizeY { get; }
        /// <summary>
        /// Get the height in [0.0 - 1.0] range
        /// </summary>
        /// <remarks>
        /// the image are oriented to match with XZ plane
        /// <para>[-x,+z]---[+x,+z]</para>
        /// <para>   |         |</para>
        /// <para>   |         |</para>
        /// <para>[-x.-z]---[+x,-z]</para>
        /// </remarks>
        float this[float x, float z] { get; set; }
        /// <summary>
        /// i'm using <code>for (int x = startx; x &lt; endx; x++)</code>
        /// </summary>
        void GetMinMax(int startx, int starty, int endx, int endy, out float Min, out float Max);
    }

    /// <summary>
    /// </summary>
    public class HeightMap : IHeightMap
    {
        public int SizeX { get; private set; }
        public int SizeY { get; private set; }

        byte[,] map;

        public HeightMap(int sizeX, int sizeY)
        {
            this.SizeX = sizeX;
            this.SizeY = sizeY;
            map = new byte[sizeX, sizeY];
        }

        float clamp(float f) { return f < 0 ? 0 : f > 1 ? 1 : f; }

        /// <summary>
        /// use [0.0 - 1.0] range to extract height
        /// </summary>
        public float this[float x, float z]
        {
            get { return map[(int)(clamp(x) * SizeX), (int)(clamp(z) * SizeY)] * 255.0f; }
            set { map[(int)(clamp(x) * SizeX), (int)(clamp(z) * SizeY)] = (byte)(value * 255.0f); }
        }
        /// <summary>
        /// use [0 - SizeX-1] range to extract height
        /// </summary>
        public float this[int x, int y]
        {
            get { return map[x, y] * 255.0f; }
            set { map[x, y] = (byte)(value * 255.0f); }
        }

        /// <summary>
        /// </summary>
        public void GetMinMax(int startx, int starty, int endx, int endy, out float Min, out float Max)
        {
            byte min = 255;
            byte max = 0;

            for (int x = startx; x < endx; x++)
                for (int y = starty; y < endy; y++)
                {
                    byte b = map[x, y];
                    if (b < min) min = b;
                    if (b > max) max = b;
                }
            Min = min / 255.0f;
            Max = max / 255.0f;
        }


        public static HeightMap FromBitmap(Bitmap bmp)
        {
            HeightMap tab = new HeightMap(bmp.Width, bmp.Height);
            // remember to swap V
            for (int x = 0; x < bmp.Width; x++)
                for (int y = 0; y < bmp.Height; y++)
                    tab.map[x, y] = (byte)(bmp.GetPixel(x, bmp.Height - y - 1).GetBrightness() * 255.0f);

            return tab;
        }

        /// <summary>
        /// To debug
        /// </summary>
        public static HeightMap WaveMap(int sizeX, int sizeY)
        {
            HeightMap tab = new HeightMap(sizeX, sizeY);

            float dx = (float)Math.PI * 2 / sizeX;
            float dy = (float)Math.PI * 2 / sizeY;

            for (int x = 0; x < sizeX; x++)
                for (int y = 0; y < sizeX; y++)
                {
                    float s = (float)Math.Sin(x * dx);
                    float c = (float)Math.Cos(y * dy);
                    tab[x, y] = s + c + 0.5f;
                }
            return tab;
        }
    }
}
