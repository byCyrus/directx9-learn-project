﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Maths;

namespace Debugger
{
    public class DX9Form : Form
    {
        bool SHADERMODE = true;

        D3D.Effect terraineffect;
        D3D.Device device;

        bool mousing = false;
        Point ptLastMousePosit;
        Point ptCurrentMousePosit;
        int spinX;
        int spinY;
        
        D3D.Font text;
        D3D.VertexBuffer terrainVerts;
        D3D.VertexBuffer terrainBase;
        D3D.VertexBuffer terrainHeight;
        D3D.VertexDeclaration multiStreamDeclaration;
        D3D.VertexDeclaration oneStreamDeclaration;

        Matrix4 world;
        Matrix4 view;
        Matrix4 proj;


        static float elapsedTime;
        static DateTime currentTime;
        static DateTime lastTime;

        #region Form
        static void Main()
        {
            using (DX9Form frm = new DX9Form())
            {
                frm.Show();
                frm.Init();
                Application.Run(frm);
            }
        }
        public DX9Form()
        {
            this.ClientSize = new Size(640, 480);
            this.Text = "Direct3D (DX9/C#) - Multiple Vertex Buffers";
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, true);
        }
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            currentTime = DateTime.Now;
            TimeSpan elapsedTimeSpan = currentTime.Subtract(lastTime);
            elapsedTime = (float)elapsedTimeSpan.Milliseconds * 0.001f;
            lastTime = currentTime;

            this.Render();
            this.Invalidate();
        }
        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            ptLastMousePosit = ptCurrentMousePosit = PointToScreen(new Point(e.X, e.Y));
            mousing = true;
        }
        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            mousing = false;
        }
        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            ptCurrentMousePosit = PointToScreen(new Point(e.X, e.Y));

            if (mousing)
            {
                spinX -= (ptCurrentMousePosit.X - ptLastMousePosit.X);
                spinY -= (ptCurrentMousePosit.Y - ptLastMousePosit.Y);
            }

            ptLastMousePosit = ptCurrentMousePosit;
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        #endregion


        /// <summary>
        /// This method basically creates and initialize the Direct3D device and
        /// anything else that doens't need to be recreated after a device 
        /// reset.
        /// </summary>
        private void Init()
        {
            D3D.Format backbufferFormat = D3D.Format.X8R8G8B8;
            D3D.Format depthstencilFormat = D3D.Format.D24S8;
            D3D.Direct3D d3d = new D3D.Direct3D();
            
            // Store the default adapter
            int ordinal = d3d.Adapters[0].Adapter;
            int adapterOrdinal = 0;

            //Debug.Assert(device.Direct3D.CheckDeviceType(adapterOrdinal, D3D.DeviceType.Hardware, backbufferFormat, backbufferFormat, true), backbufferFormat.ToString() + " not compatible");
            //Debug.Assert(device.Direct3D.CheckDepthStencilMatch(0, D3D.DeviceType.Hardware, backbufferFormat, backbufferFormat, depthstencilFormat), depthstencilFormat.ToString() + " not compatible");


            //// setting for a correct windows directX application
            D3D.PresentParameters m_param = new D3D.PresentParameters();
            m_param.Windowed = true;
            m_param.DeviceWindowHandle = this.Handle;
            m_param.BackBufferFormat = backbufferFormat;
            m_param.EnableAutoDepthStencil = true;
            m_param.PresentationInterval = D3D.PresentInterval.Immediate;
            m_param.SwapEffect = D3D.SwapEffect.Discard;
            m_param.BackBufferCount = 1;
            m_param.BackBufferHeight = this.ClientSize.Height;
            m_param.BackBufferWidth = this.ClientSize.Width;
            m_param.AutoDepthStencilFormat = depthstencilFormat;

            // Check to see if we can use a pure hardware device
            D3D.Capabilities caps = d3d.GetDeviceCaps(adapterOrdinal, D3D.DeviceType.Hardware);
            D3D.CreateFlags flags = D3D.CreateFlags.SoftwareVertexProcessing;
            

            if ((caps.DeviceCaps & D3D.DeviceCaps.HWTransformAndLight) != 0)  //SupportsHardwareTransformAndLight)
            {
                flags = D3D.CreateFlags.HardwareVertexProcessing;
            }
            if ((caps.DeviceCaps & D3D.DeviceCaps.PureDevice) != 0)// SupportsPureDevice)
            {
                flags |= D3D.CreateFlags.PureDevice;
            }
            
            Trace.WriteLine("> Device Caps : " + flags.ToString());
            Trace.WriteLine("> Device pixel shader version : " + caps.PixelShaderVersion.ToString());
            Trace.WriteLine("> Device pixel shader version : " + caps.VertexShaderVersion.ToString());

            int pxShader = caps.PixelShaderVersion.Major;
            int vxShader = caps.VertexShaderVersion.Major;

            device = new D3D.Device(d3d, adapterOrdinal, D3D.DeviceType.Hardware, this.Handle, flags, m_param);
            device.Direct3D.Disposing += new EventHandler<EventArgs>(OnResetDevice);

            // clean the viewport
            this.Clear(this.BackColor); 

            OnResetDevice(device, null);

            terraineffect = D3D.Effect.FromString(device,Properties.Resources.Terrain, D3D.ShaderFlags.None);
            terraineffect.Technique = "Terrain";

        }
        public void Clear(Color color)
        {
            D3D.ClearFlags flags = D3D.ClearFlags.Target;
            flags |= D3D.ClearFlags.ZBuffer;
            flags |= D3D.ClearFlags.Stencil;
            device.Clear(flags, DX.ColorBGRA.FromBgra(color.ToArgb()), 1.0f, 0);
        }

        /// <summary>
        /// This event-handler is a good place to create and initialize any 
        /// Direct3D related objects, which may become invalid during a 
        /// device reset.
        /// </summary>
        public void OnResetDevice(object sender, EventArgs e)
        {
            D3D.Device m_device = (D3D.Device)sender;

            System.Drawing.Font systemfont = new System.Drawing.Font("Arial", 12f, FontStyle.Regular);
            text = new D3D.Font(device, systemfont);

            m_device.SetRenderState(D3D.RenderState.ZEnable, true);
            m_device.SetRenderState(D3D.RenderState.StencilEnable, true);
            WriteTerrain();
        }

        float zphase = 0;
        float xphase = 0;

        public void WriteTerrain()
        {

            // SHADER VARIANT
            D3D.VertexElement[] elements = new D3D.VertexElement[]
            {
                new D3D.VertexElement(0,0,D3D.DeclarationType.Float2,D3D.DeclarationMethod.Default,D3D.DeclarationUsage.Position,0),
                new D3D.VertexElement(1,0,D3D.DeclarationType.Float1,D3D.DeclarationMethod.Default,D3D.DeclarationUsage.Position,1),
                D3D.VertexElement.VertexDeclarationEnd
            };

            multiStreamDeclaration = new D3D.VertexDeclaration(device, elements);

            Vector2[] grid = new Vector2[]
            {
                new Vector2(0,0),
                new Vector2(1,0),
                new Vector2(0,1),
                new Vector2(0,1),
                new Vector2(1,0),
                new Vector2(1,1)
            };
            float[] height = new float[] { 0, 0, 0, 0, 0, 0 };

            terrainBase = new D3D.VertexBuffer(device, 8 * grid.Length, D3D.Usage.None, D3D.VertexFormat.Position, D3D.Pool.Managed);
            SetData<Vector2>(terrainBase, grid, 0, 8, D3D.LockFlags.None);
            
            terrainHeight = new D3D.VertexBuffer(device, 4* grid.Length, D3D.Usage.None, D3D.VertexFormat.Position, D3D.Pool.Managed);
            SetData<float>(terrainHeight, height, 0, 4, D3D.LockFlags.None);


            // DEFAULT PIPELINE VARIANT
            elements = new D3D.VertexElement[]
            {
                new D3D.VertexElement(0,0,D3D.DeclarationType.Float3,D3D.DeclarationMethod.Default,D3D.DeclarationUsage.Position,0),
                D3D.VertexElement.VertexDeclarationEnd
            };

            oneStreamDeclaration = new D3D.VertexDeclaration(device, elements);
                  
            Vector3[] vertices = new Vector3[grid.Length];
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = new Vector3(grid[i].x, height[i], grid[i].y);
            }
            terrainVerts = new D3D.VertexBuffer(device, 12 * vertices.Length, D3D.Usage.None, D3D.VertexFormat.Position, D3D.Pool.Managed);
            SetData<Vector3>(terrainVerts, vertices, 0, 12, D3D.LockFlags.None);

        }

        public void UpdateWave()
        {
            float[] height = new float[]
            { 
                zphase % 1,
                xphase % 1,
                xphase % 1,
                zphase % 1,
                zphase % 1,
                xphase % 1
            };

            zphase += 0.1f;
            xphase += 0.1f;

            SetData<float>(terrainHeight, height, 0, 4, D3D.LockFlags.None);
        }

        int Frames = 0;
        int LastTickCount = 0;
        float LastFrameRate = 0;
        
        void UpdateFramerate()
        {
            Frames++;
            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                LastFrameRate = (float)Frames * 1000 / Math.Abs(Environment.TickCount - LastTickCount);
                LastTickCount = Environment.TickCount;
                Frames = 0;
            }
            text.DrawText(null, string.Format("Framerate : {0:0.00} fps", LastFrameRate), 10, 430, DX.ColorBGRA.FromBgra(Color.Red.ToArgb()));
        }
        /// <summary>
        /// This method is dedicated completely to rendering our 3D scene and is
        /// is called by the OnPaint() event-handler.
        /// </summary>
        private void Render()
        {
            System.Threading.Thread.Sleep(10);

            Engine.Graphics.Viewport viewport = new Engine.Graphics.Viewport();
            viewport.Width = this.ClientSize.Width;
            viewport.Height = this.ClientSize.Height;
            viewport.X = 0;
            viewport.Y = 0;
            viewport.MaxDepth = 1;
            viewport.MinDepth = 0;

            world = Matrix4.RotationYawPitchRoll(Engine.Maths.MathUtils.DegreeToRadian(spinX), Engine.Maths.MathUtils.DegreeToRadian(spinY), 0.0f);
            view = Matrix4.MakeViewLH(new Vector3(0, 10, 0), new Vector3(0, 0, 0), Vector3.UnitX);
            proj = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), viewport, 0.1f, 1000.0f);

            Clear(Color.Blue);

            device.BeginScene();
            device.SetRenderState(D3D.RenderState.CullMode, D3D.Cull.None);
            device.SetRenderState(D3D.RenderState.FillMode, D3D.FillMode.Solid);

            if (SHADERMODE)
            {
                //UpdateWave();

                device.VertexDeclaration = multiStreamDeclaration;
                device.SetStreamSource(0, terrainBase, 0, 8);
                device.SetStreamSource(1, terrainHeight, 0, 4);

                // remember that WVP_directx == PVW_math becuase (ABC)^T == (C^T)(B^T)(A^T)
                Matrix4 WorldViewProj_math = proj * view * world;
                DX.Matrix WorldViewProj_dx = Convert(world) * Convert(view) * Convert(proj);

                WorldViewProj_dx = Convert(WorldViewProj_math);

                terraineffect.SetValue("WorldViewProj", WorldViewProj_dx);
                int numpasses = terraineffect.Begin(0);
                for (int i = 0; i < numpasses; i++)
                {
                    terraineffect.BeginPass(i);
                    device.DrawPrimitives(D3D.PrimitiveType.TriangleList, 0, 2);
                    terraineffect.EndPass();
                }
                terraineffect.End();
            }
            else
            {

                device.SetTransform(D3D.TransformState.World, Convert(world));
                device.SetTransform(D3D.TransformState.Projection, Convert(proj));
                device.SetTransform(D3D.TransformState.View, Convert(view));
                
                device.VertexDeclaration = oneStreamDeclaration;
                device.SetStreamSource(0, terrainVerts, 0, 12);


                device.DrawPrimitives(D3D.PrimitiveType.TriangleList, 0, 2);
            }

            UpdateFramerate();

            device.EndScene();
            device.Present();
        }

        public void SetData<T>(D3D.VertexBuffer m_vbuffer, T[] vertices, int offset, int bstride, D3D.LockFlags mode) where T : struct
        {
            DX.DataStream dataStream = m_vbuffer.Lock(bstride * offset, bstride * vertices.Length, (D3D.LockFlags)mode);
            dataStream.WriteRange<T>(vertices, 0, vertices.Length);
            m_vbuffer.Unlock();
        }
        /// <summary>
        /// Traspose matrix operation, directx use colon major instad row major 
        /// </summary>
        public static DX.Matrix Convert(Matrix4 matrix)
        {
            DX.Matrix m = new DX.Matrix();
            m.M11 = matrix.m00;
            m.M12 = matrix.m10;
            m.M13 = matrix.m20;
            m.M14 = matrix.m30;

            m.M21 = matrix.m01;
            m.M22 = matrix.m11;
            m.M23 = matrix.m21;
            m.M24 = matrix.m31;

            m.M31 = matrix.m02;
            m.M32 = matrix.m12;
            m.M33 = matrix.m22;
            m.M34 = matrix.m32;

            m.M41 = matrix.m03;
            m.M42 = matrix.m13;
            m.M43 = matrix.m23;
            m.M44 = matrix.m33;

            return m;
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DX9Form
            // 
            this.ClientSize = new System.Drawing.Size(769, 515);
            this.Location = new System.Drawing.Point(100, 100);
            this.Name = "DX9Form";
            this.ResumeLayout(false);

        }
    }
}
