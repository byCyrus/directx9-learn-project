float4x4 WorldViewProj;

texture base_Tex   : BaseTex;
texture bump_Tex   : NormalTex;

sampler2D baseMap = sampler_state
{
   Texture = (base_Tex);
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = ANISOTROPIC;
   MAGFILTER = ANISOTROPIC;
   MIPFILTER = ANISOTROPIC;
};

sampler2D bumpMap = sampler_state
{
   Texture = (bump_Tex);
   ADDRESSU = WRAP;
   ADDRESSV = WRAP;
   MINFILTER = LINEAR;
   MAGFILTER = LINEAR;
   MIPFILTER = LINEAR;
};

struct VS_INPUT
{
    float4  Position  : POSITION;
};

struct VS_OUTPUT
{
    float4  Position  : POSITION;    
    float4  Color     : COLOR0;
};

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = mul(input.Position, WorldViewProj);
	output.Color = float4(0,1,1,1);

	return output;
}


//////////////////////////////////////
// Techniques specs follow
//////////////////////////////////////
technique t0
{
    pass p0
    {
        VertexShader = compile vs_2_0 VShader();
        PixelShader =  NULL;
    }
}