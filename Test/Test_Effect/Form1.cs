﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Renderer;
using Engine.Tools;
using Engine.Maths;
using Font = Engine.Graphics.Font;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        GraphicDevice device;
        Font font;
        Effect effect;
        VertexDeclaration declaration;
        
        Material material;

        VertexBuffer vertexbuffer;
        IndexBuffer indexBuffer;

        TrackBallCamera cameractrl;

        int LastTickCount = 1; // ms
        int Frames = 0;
        float LastFrameRate = 0;
        bool draw = true;

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            
            InitializeComponent();

            cameractrl = new TrackBallCamera(this, new Vector3(2, 1, 2), Vector3.Zero, 0.1f,1000.0f);

            device = new GraphicDevice(this);
            font = new Font(device, Engine.Graphics.Font.FontName.Arial, 8);


            //colormap = new Texture(device, "rock.dds", true, 0);
            //bumpmap = new Texture(device, "rockN.dds", true, 0);

            material = Material.Default;
            material.Specular = Color.White;
            material.SpecularSharpness = 25;

            effect = new Effect(device, Properties.Resources.Effect);
            effect.Parameters.Add("Technique", new EffectAttributeTechnique("Default"));
            effect.Parameters.Add("Transform",new EffectAttributeMatrix("xWorldViewProj"));
            effect.Parameters.Add("Diffuse", new EffectAttributeColor(Color.Green,"xColor"));

            //effect.Technique = "t0";
            //effect.SetParam("base_Tex", colormap);
            //effect.SetParam("bump_Tex", bumpmap);
            
            InitGraphics();
        }
        void InitGraphics()
        {
            declaration = new VertexDeclaration(device, typeof(BVertex), BVertex.elements);

            BVertex[] vertices = new BVertex[]
            {
                new BVertex(0,0,0,  0,1,0,  0,0,0,  0,0,0, 0,0),
                new BVertex(1,0,0,  0,1,0,  0,0,0,  0,0,0, 1,0),
                new BVertex(0,0,1,  0,1,0,  0,0,0,  0,0,0, 0,1),
                new BVertex(1,0,1,  0,1,0,  0,0,0,  0,0,0, 1,1)
            };

            Face16[] tri = new Face16[]
            {
                new Face16(0,2,1),
                new Face16(2,3,1)
            };

            vertexbuffer = new VertexBuffer(device, declaration.Format, vertices.Length, false, true);
            vertexbuffer.StreamID = 0;
            indexBuffer = new IndexBuffer(device, IndexInfo.GetIndexFormat(typeof(Face16)), tri.Length, false, true);

            VertexStream vdata = vertexbuffer.OpenStream();
            vdata.WriteCollection<BVertex>(vertices, 0, vertices.Length, 0);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = vertices.Length;

            IndexStream idata = indexBuffer.OpenStream();
            idata.WriteCollection<Face16>(tri, IndexInfo.GetIndexFormat(typeof(Face16)), 0, tri.Length, 0, 0);
            indexBuffer.CloseStream();
            indexBuffer.Count = tri.Length;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            device.Dispose();
            base.OnClosing(e);
        }

        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            device.Resize(this.ClientSize);
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!draw) return;

            System.Threading.Thread.Sleep(100);

            Vector3 move = new Vector3(0.1f, 0, 0);
            //cameractrl.camera.viewcomponent.eye += move;
            //cameractrl.camera.viewcomponent.targhet += move;
            //cameractrl.camera.viewcomponent.Update();

            Matrix4 world = Matrix4.Identity;
            Matrix4 view = cameractrl.View;
            Matrix4 proj = cameractrl.Projection;

            /*
            Matrix4 view = Matrix4.MakeViewLH(new Vector3(-1, 1, -1), new Vector3(1, 0, 1), Vector3.UnitY);
            Matrix4 proj = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), device.viewport, 0.1f, 1000.0f);
            */

            Frames++;
            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                LastFrameRate = (float)Frames * 1000 / Math.Abs(Environment.TickCount - LastTickCount);
                LastTickCount = Environment.TickCount;
                Frames = 0;
            }
            bool state = device.BeginDraw();
            Debug.Assert(state, "Render Fail");
            if (state)
            {
                device.SetRenderTarghet();
                device.Clear(Color.CornflowerBlue);

                declaration.SetToDevice();
                vertexbuffer.SetToDevice();
                indexBuffer.SetToDevice();

                Console.WriteLine("Start");

                effect.Parameters["Transform"] = proj * view * world;

                int count = effect.Begin();
                for (int pass = 0; pass < count; pass++)
                {
                    effect.Parameters["Diffuse", true] = pass == 0 ? Color.Blue : Color.Green;
                    effect.BeginPass(pass);
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, indexBuffer.Count);
                    effect.EndPass();
                }
                effect.End();


                font.Draw("FPS : " + LastFrameRate.ToString(), 10, 10, Color.Black);
                font.Draw(string.Format("Verts : {0} Triangles : {1}", vertexbuffer.Count, indexBuffer.Count), 10, 24, Color.Black);

                device.EndDraw();
                device.PresentDraw();
            }

            this.Invalidate();
        }
    }
}
