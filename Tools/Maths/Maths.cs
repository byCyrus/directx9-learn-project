﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Maths
{
    public static class MathUtils
    {
        public static int[] powersOfTwo = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };

        /// <summary>
        /// </summary>
        /// <param name="Degree"></param>
        /// <returns>Radians</returns>
        public static float DegreeToRadian(float Degree)
        {
            return (float)(Math.PI * Degree/ 180.0);
        }
        /// <summary>
        /// TODO : convert for directxLH
        /// </summary>
        /// <param name="r">radial distance </param>
        /// <param name="t">inclination </param>
        /// <param name="g">azimuth </param>
        public static void CartesianToSpherical(float x, float y, float z, out float r, out float t, out float g)
        {
            r = (float)Math.Sqrt(x * x + y * y + z * z);
            t = (float)Math.Acos(-z / r);
            g = (float)Math.Atan(y / r);
        }
        /// <summary>
        /// TODO : convert for directxLH
        /// </summary>
        /// <param name="r">radius </param>
        /// <param name="t">inclination </param>
        /// <param name="o">azimuth </param>
        public static void SphericalToCartesian(float r, float t, float g, out float x, out float y, out float z)
        {
            x = (float)(r * Math.Sin(t) * Math.Cos(g));
            y = (float)(r * Math.Sin(t) * Math.Sin(g));
            z = (float)(r * Math.Cos(t));
        }
        /// <summary>
        /// </summary>
        /// <param name="r">radial distance</param>
        /// <param name="g">azimuth </param>
        /// <param name="h">equal to y</param>
        public static void CartesianToCylindrical(float x, float y, float z, out float r, out float g, out float h)
        {
            r = (float)Math.Sqrt(x * x + z * z);
            g = (float)Math.Asin(z / r);
            h = y;
        }
        /// <summary>
        /// </summary>
        /// <param name="Radian"></param>
        /// <returns>Degree</returns>
        public static float RadianToDegree(float Radian)
        {
            return (float)(180.0 * Radian / Math.PI );
        }
        /// <summary>
        /// Linearly interpolates between two values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="amount">amount = 0 return value1 , amount = 1 return value2</param>
        /// <returns></returns>
        public static float Lerp(float value1, float value2, float amount)
        {
            return value1 + (value2 - value1) * amount;
        }
        /// <summary>
        /// Is power or 2 ?
        /// </summary>
        public static bool IsPowOf2(int value)
        {
            if (value < 1) return false;
            return (value & (value - 1)) == 0;
        }
        /// <summary>
        /// n!
        /// </summary>
        public static float fattoriale(float n)
        {
            float result = 1;
            for (int i = 1; i <= n; i++)
                result *= i;
            return result;
        }
    }
}
