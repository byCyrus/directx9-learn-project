﻿#region LGPL License

/*
Axiom Graphics Engine Library
Copyright © 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#endregion

using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

using Engine.Tools;
using Engine.Graphics;

namespace Engine.Maths
{
    /// <summary>
    /// Vector3 struct, math copy for Axiom Graphics Engine Library
    /// </summary>
    [StructLayout(LayoutKind.Sequential , Pack = 4 , Size = sizeof(float)*3)]
    [DebuggerDisplay("{x},{y},{z}")]
    public struct Vector3
    {
        public const int sizeinbyte = sizeof(float) * 3;

        public float x, y, z;

        static float abs(float f) { return f > 0 ? f : -f; }

        public Vector3(double x, double y, double z) :
            this((float)x, (float)y, (float)z) { }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public bool isNaN
        {
            get
            {
                return float.IsInfinity(x) || float.IsNaN(x) ||
                       float.IsInfinity(y) || float.IsNaN(y) ||
                       float.IsInfinity(z) || float.IsNaN(z);
            }
        }

        public float this[int i]
        {
#if UNSAFE
            get
            {
                unsafe
                {
                    fixed (float* pX = &this.x)
                    {
                        return *(pX + i);
                    }
                }
            }
             
            set
            {
                unsafe
                {
                    fixed (float* pX = &this.x)
                    {
                        *(pX + i) = value;
                    }
                }
            }
#else
            get
            {
                switch (i)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
            set
            {
                switch (i)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    default: throw new ArgumentException("i must be 0,1,2");
                }
            }
#endif
        }

        public override string ToString()
        {
            if (this.isNaN) return "NULL_VECTOR";
            return (string.Format("{0,4};{1,4};{2,4}", x, y, z));
        }

        /// <summary>
        /// A "null" vector, not zero, used example when you want considerate the value not processed or divided by 0
        /// </summary>
        public static readonly Vector3 NaN = new Vector3(float.NaN, float.NaN, float.NaN);
        public static readonly Vector3 Zero = new Vector3(0, 0, 0);
        public static readonly Vector3 UnitX = new Vector3(1, 0, 0);
        public static readonly Vector3 UnitY = new Vector3(0, 1, 0);
        public static readonly Vector3 UnitZ = new Vector3(0, 0, 1);

        #region operator overload
        /// <summary>
        /// Mull left.x*right.x , left.y*right.y , left.z*right.z 
        /// </summary>
        public static Vector3 operator *(Vector3 left, Vector3 right)
        {
            Vector3 retVal;
            retVal.x = left.x * right.x;
            retVal.y = left.y * right.y;
            retVal.z = left.z * right.z;
            return retVal;
        }
        /// <summary>
        /// Divide left.x/right.x , left.y/right.y , left.z/right.z 
        /// </summary>
        public static Vector3 operator /(Vector3 left, Vector3 right)
        {
            Vector3 vector;
            vector.x = left.x / right.x;
            vector.y = left.y / right.y;
            vector.z = left.z / right.z;
            return vector;
        }
        public static Vector3 operator /(Vector3 left, float scalar)
        {
            if (scalar <= float.Epsilon) throw new ArgumentException("Cannot divide a Vector3 by zero");
            Vector3 vector;
            var inverse = 1.0f / scalar;
            vector.x = left.x * inverse;
            vector.y = left.y * inverse;
            vector.z = left.z * inverse;
            return vector;
        }
        public static Vector3 operator +(Vector3 left, Vector3 right)
        {
            return new Vector3(left.x + right.x, left.y + right.y, left.z + right.z);
        }
        public static Vector3 operator +(Vector3 v, float scalar)
        {
            return new Vector3(v.x + scalar, v.y + scalar, v.z + scalar);
        }
        public static Vector3 operator *(Vector3 left, float scalar)
        {
            Vector3 retVal;
            retVal.x = left.x * scalar;
            retVal.y = left.y * scalar;
            retVal.z = left.z * scalar;
            return retVal;
        }
        public static Vector3 operator *(float scalar, Vector3 right)
        {
            return right * scalar;
        }
        public static Vector3 operator -(Vector3 left, Vector3 right)
        {
            return new Vector3(left.x - right.x, left.y - right.y, left.z - right.z);
        }
        public static Vector3 operator -(Vector3 left)
        {
            return new Vector3(-left.x, -left.y, -left.z);
        }
        public static bool operator ==(Vector3 left, Vector3 right)
        {
            if (abs(left.x - right.x) > float.Epsilon) return false;
            if (abs(left.y - right.y) > float.Epsilon) return false;
            if (abs(left.z - right.z) > float.Epsilon) return false;
            return true;
        }
        public static bool operator !=(Vector3 left, Vector3 right)
        {
            return !(left == right);
        }


        public override bool Equals(object obj)
        {
            return (this == (Vector3)obj);
        }
        #endregion

        #region Math

        /// <summary>
        /// Get the squared legth
        /// </summary>
        public float LengthSq
        {
            get { return x * x + y * y + z * z; }
        }
        /// <summary>
        /// Get the length
        /// </summary>
        public float Length
        {
            get { return (float)Math.Sqrt(LengthSq); }
        }
        /// <summary>
        /// sqrt(x*x + y*y + z*z)
        /// </summary>
        public static float GetLength(Vector3 vector)
        {
            return vector.Length;
        }
        /// <summary>
        /// x*x + y*y + z*z
        /// </summary>
        public static float GetLengthSquared(Vector3 vector)
        {
            return vector.LengthSq;
        }
        /// <summary>
        /// |x| + |y| + |z|
        /// </summary>
        public static float GetManhattanLength(Vector3 vector)
        {
            return abs(vector.x) + abs(vector.y) + abs(vector.z);
        }
        /// <summary>
        /// Return the distance from a point to line
        /// </summary>
        /// <param name="line0">a point of line</param>
        /// <param name="line1">a point of line</param>
        /// <param name="point">the point from line</param>
        /// <returns></returns>
        public static float GetPointLineDistance(Vector3 line0, Vector3 line1, Vector3 point)
        {
            Vector3 line01 = line1 - line0;
            Vector3 dist = point - line0;

            return (float)Math.Sqrt(Vector3.GetLengthSquared(Vector3.Cross(line01, dist)) / Vector3.GetLengthSquared(line01));
        }
        /// <summary>
        /// Get the normal of a triangle, if is degeneralized return a Vector3.Zero
        /// </summary>
        public static Vector3 GetNormalTriangle(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            Vector3 N = Vector3.Cross(v1 - v0, v2 - v0);
            float l = N.Length;
            return (l > float.Epsilon) ? N / l : Vector3.Zero;
        }
        /// <summary>
        /// multiple x,y,z * scalar
        /// </summary>
        public void MulScalar(float scalar)
        {
            x *= scalar;
            y *= scalar;
            z *= scalar;
        }
        /// <summary>
        /// Normalize the vector and get the calculated length. 
        /// </summary>
        public float Normalize()
        {
            float length = Length;

            if (length > float.Epsilon)
            {
                x /= length;
                y /= length;
                z /= length;
            }
            return length;
        }
        /// <summary>
        /// Get a new normalized vector3 copy
        /// </summary>
        public Vector3 Normal
        {
            get
            {
                Vector3 vect = new Vector3(x, y, z);
                vect.Normalize();
                return vect;
            }
        }
        public static Vector3 GetNormal(Vector3 vector)
        {
            return vector.Normal; 
        }
        public static float Distance(Vector3 a, Vector3 b)
        {
            return (a - b).Length;
        }
        public static float Dot(Vector3 a, Vector3 b) 
        { 
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }
        public static Vector3 Cross(Vector3 a, Vector3 b) 
        { 
            return new Vector3((a.y * b.z) - (a.z * b.y), (a.z * b.x) - (a.x * b.z), (a.x * b.y) - (a.y * b.x));
        }
        #endregion


        /// <summary>
        /// Transform the Point vector using matrix, then result is transform * vector
        /// </summary>
        /// <remarks>
        /// Vector3 are calculated as Vector4.w = 1 , 
        /// </remarks>
        public static Vector3 TransformCoordinate(Vector3 vector, Matrix4 transform)
        {
            return transform * vector;
        }

        /// <summary>
        /// Transform the Normal vector using matrix, the result is transform_without_traslation * vector
        /// </summary>
        /// <remarks>
        /// need avoid traslation, it will be used example when transform a normal vector, Vector4.w = 0
        /// </remarks>
        public static Vector3 TransformNormal(Vector3 vector, Matrix4 transform)
        {
            Vector3 result = new Vector3();

            float inverseW = 1.0f / (transform.m30 * vector.x + transform.m31 * vector.y + transform.m32 * vector.z + transform.m33 * 1.0f);

            result.x = (vector.x * transform.m00 + vector.y * transform.m01 + vector.z * transform.m02) * inverseW;
            result.y = (vector.x * transform.m10 + vector.y * transform.m11 + vector.z * transform.m12) * inverseW;
            result.z = (vector.x * transform.m20 + vector.y * transform.m21 + vector.z * transform.m22) * inverseW;

            return result;
        }


        public static Vector3 TransformCoordinate(Vector3 vector, Matrix3 rotation)
        {
            /*
            float X = (((coord.x * transform.m00) + (coord.y * transform.m10)) + (coord.z * transform.m20)) + transform.m30;
            float Y = (((coord.x * transform.m01) + (coord.y * transform.m11)) + (coord.z * transform.m21)) + transform.m31;
            float Z = (((coord.x * transform.m02) + (coord.y * transform.m12)) + (coord.z * transform.m22)) + transform.m32;
            float W = 1 / ((((coord.x * transform.m03) + (coord.y * transform.m13)) + (coord.z * transform.m23)) + transform.m33);
            return new Vector3(X * W, Y * W, Z * W);
            */

            return rotation * vector;
        }
        public static Vector3 TransformCoordinate(Vector3 vector, Quaternion rotation)
        {
            float x = rotation.x + rotation.x;
            float y = rotation.y + rotation.y;
            float z = rotation.z + rotation.z;
            
            float wx = rotation.w * x;
            float wy = rotation.w * y;
            float wz = rotation.w * z;

            float xx = rotation.x * x;
            float xy = rotation.x * y;
            float xz = rotation.x * z;
            
            float yy = rotation.y * y;
            float yz = rotation.y * z;
            float zz = rotation.z * z;

            return new Vector3(
                ((vector.x * ((1.0f - yy) - zz)) + (vector.y * (xy - wz))) + (vector.z * (xz + wy)),
                ((vector.x * (xy + wz)) + (vector.y * ((1.0f - xx) - zz))) + (vector.z * (yz - wx)),
                ((vector.x * (xz - wy)) + (vector.y * (yz + wx))) + (vector.z * ((1.0f - xx) - yy)));
        }

        #region Left Hand Rule

        /// <summary>
        /// Return the origin of ray when you click on screen, the point depend by depthZ , default is 0.0
        /// </summary>
        /// <remarks>
        /// the directx big matrix is Proj * View * World , isn't commutative
        /// </remarks>
        /// <param name="depthZ">-1.0 = in the eye postion , 0.0 = nearZ plane , 1.0 = farZ plane</param>
        public static Vector3 Unproject(int mouseX, int mouseY, float depthZ, Viewport viewport, Matrix4 proj, Matrix4 view, Matrix4 world)
        {
            Vector4 source = new Vector4();
            source.x = ((float)(mouseX - viewport.X) / (float)viewport.Width * 2f) - 1f;
            source.y = (((float)(mouseY - viewport.Y) / (float)viewport.Height) * -2f) + 1f;
            source.z = (float)(depthZ - viewport.MinDepth) / (float)(viewport.MaxDepth - viewport.MinDepth);
            source.w = 1f;

            source = Matrix4.Inverse(proj * view * world) * source;

            // homogenous calculation and test if is a zero-division
            if (source.w * source.w > float.Epsilon)
                source = source * (1f / source.w);

            return (Vector3)source;
        }



        /// <summary>
        /// get the planar projection point.
        /// </summary>
        public static Vector3 Project(Vector3 vector, Matrix4 world)
        {
            Vector4 vect = (Vector4)vector;
            vect = world * vect;
            return (Vector3)vect;
        }
        /// <summary>
        /// get the screen point, the z value are the depthZ
        /// </summary>
        public static Vector3 Project(Vector3 vector, Viewport viewport, Matrix4 proj, Matrix4 view, Matrix4 world)
        {          
            Matrix4 matrix = proj * view * world;
            Vector4 source = matrix * (Vector4)vector;

            // homogenous calculation and test if is a zero-division
            if (source.w * source.w > float.Epsilon)
                source = source * (1f / source.w);

            Vector3 screen = new Vector3();
            screen.x = (source.x + 1f) * 0.5f * viewport.Width + viewport.X;
            screen.y = (-source.y + 1f) * 0.5f * viewport.Height + viewport.Y;
            screen.z = source.z * (viewport.MaxDepth - viewport.MinDepth) + viewport.MinDepth;

            return screen;
        }


        #endregion

        public static implicit operator Vector3(Vector4 vector)
        {
            return new Vector3(vector.x, vector.y, vector.z);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
        }
    }
}