﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;
using Engine.Tools;


namespace Engine.Graphics
{
    /// <summary>
    /// MicrosoftDX vertex declaration
    /// </summary>
    public class DX9VertexDeclaration : IDisposable
    {
        D3D.Device m_device;
        D3D.VertexDeclaration m_declaration;

        public DX9VertexDeclaration(DX9Device device, IList<VertexElement> elements)
        {
            m_device = device.m_device;

            int count = elements.Count;
            D3D.VertexElement[] m_elements = new D3D.VertexElement[count + 1];

            for (int i = 0; i < count; i++) m_elements[i] = Helper.Convert(elements[i]);
            m_elements[count] = Helper.Convert(VertexElement.VertexDeclarationEnd);

            m_declaration = new D3D.VertexDeclaration(m_device, m_elements);
        }

        public void SetToDevice()
        {
            m_device.VertexDeclaration = m_declaration;
        }
        public void Dispose()
        {
            if (m_declaration != null && !m_declaration.Disposed)
                m_declaration.Dispose();
        }
    }

    /// <summary>
    /// MicrosoftDX vertex buffer
    /// </summary>
    public class DX9VertexBuffer : IDisposable
    {
        D3D.Device m_device;
        D3D.VertexBuffer m_vbuffer;

        /// <summary>
        /// MicrosoftDX9 vertex buffer
        /// </summary>
        /// <param name="numverts">num or T elements</param>
        /// <param name="readable">if false is only writable</param>
        /// <param name="managed">if false put in a Pool.default, but need a shadow copy</param>
        public DX9VertexBuffer(DX9Device device, VertexFormat fvf, int byteSize, Usage usage, Pool memory)
        {
            m_device = device.m_device;
            m_vbuffer = new D3D.VertexBuffer(m_device, byteSize, (D3D.Usage)usage, (D3D.VertexFormats)fvf, (D3D.Pool)memory);
        }
        /// <summary>
        /// Set data original implementation
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        /// <param name="boffset">num of vertice to jump</param>
        public void SetData<T>(T[] vertices, int offset, int bstride, LockFlags mode) where T : struct
        {
            m_vbuffer.SetData(vertices, offset*bstride, (D3D.LockFlags)mode);
        }

        /// <summary>
        /// Get data original implementation
        /// </summary>
        /// <param name="count">number of vertices</param>
        /// <param name="boffset">number of vertices to jump</param>
        /// <param name="bstride">size in byte of one vertex</param>
        public T[] GetData<T>(int count, int offset, int bstride) where T : struct
        {
            T[] data = (T[])m_vbuffer.Lock(offset * bstride, typeof(T), D3D.LockFlags.ReadOnly, count);
            m_vbuffer.Unlock();
            return data;
        }

        /// <summary>
        /// Return the managed pointers of gpu buffer
        /// </summary>
        /// <param name="boffset">offset in bytes</param>
        /// <param name="bsize">size to lock in bytes</param>
        public IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            return m_vbuffer.Lock(boffset, bsize, (D3D.LockFlags)mode).InternalData;
        }
        /// <summary>
        /// Release the managed pointers of gpu buffer
        /// </summary>
        public void UnLock()
        {
            m_vbuffer.Unlock();
        }
        /// <summary>
        /// Apply buffer to device
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        public void SetStreamSource(int nstream, int bstride)
        {
            m_device.SetStreamSource(nstream, m_vbuffer, 0, bstride);
        }

        public void Dispose()
        {
            m_vbuffer.Dispose();
        }

        public override string ToString()
        {
            return "MicrosoftDx9VertexBuffer_" + m_vbuffer.Description.Type.ToString();
        }
    }

    /// <summary>
    /// MicrosoftDX index buffer
    /// </summary>
    public class DX9IndexBuffer : IDisposable
    {
        D3D.Device m_device;
        D3D.IndexBuffer m_ibuffer;

        /// <summary>
        /// </summary>
        /// <param name="maxsize">max number of vertices what can be stored</param>
        /// <param name="usage">define if is a static or dynamic buffer</param>
        /// <param name="type">define the fvf of vertices</param>
        public DX9IndexBuffer(DX9Device device, bool is32bit, int byteSize, Usage usage, Pool memory)
        {
            m_device = device.m_device;
            m_ibuffer = new D3D.IndexBuffer(m_device, byteSize, (D3D.Usage)usage, (D3D.Pool)memory, !is32bit);
        }
        /// <summary>
        /// Original set data implementation
        /// </summary>
        /// <param name="bstride">size in byte of one vertex</param>
        /// <param name="boffset">num of vertice to jump</param>
        public void SetData<T>(T[] vertices, int offset, int bstride, LockFlags mode) where T : struct
        {
            m_ibuffer.SetData(vertices, offset * bstride, (D3D.LockFlags)mode);
        }

        /// <summary>
        /// Original get data implementation
        /// </summary>
        /// <param name="count">number of indices</param>
        /// <param name="boffset">number of indices to jump</param>
        /// <param name="bstride">size in byte of one index</param>
        public T[] GetData<T>(int count, int offset, int bstride) where T : struct
        {
            T[] data = (T[])m_ibuffer.Lock(offset * bstride, typeof(T), D3D.LockFlags.ReadOnly, count);
            m_ibuffer.Unlock();
            return data;
        }

        /// <summary>
        /// Return the managed pointers of gpu buffer
        /// </summary>
        /// <param name="boffset">offset in bytes</param>
        /// <param name="bsize">size to lock in bytes</param>
        public IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            return m_ibuffer.Lock(boffset, bsize, (D3D.LockFlags)mode).InternalData;
        }
        /// <summary>
        /// Release the managed pointers of gpu buffer
        /// </summary>
        public void UnLock()
        {
            m_ibuffer.Unlock();
        }

        /// <summary>
        /// Apply buffer to device
        /// </summary>
        public void SetToDevice()
        {
            m_device.Indices = m_ibuffer;
        }

        public void Dispose()
        {
            m_ibuffer.Dispose();
        }
        public override string ToString()
        {
            return "MicrosoftDx9IndexBuffer_" + m_ibuffer.Description.Type.ToString();
        }
    }
}
