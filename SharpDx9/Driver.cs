﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;
using Engine.Maths;

namespace Engine.Graphics
{
    /// <summary>
    /// Graphic Device using SharpDx
    /// </summary>
    public class DX9Device : IDisposable
    {
        public const string graphicVersion = "SharpDx9";

        Control m_control;

        // i'm using "internal" to use these values only inside library
        internal D3D.Device m_device;
        internal D3D.SwapChain m_swapchain;
        internal D3D.Surface m_backbuffer;
        internal D3D.Surface m_zbuffer;
        internal DX.Viewport m_viewport;
        internal D3D.PresentParameters m_param;

        internal bool useZbuffer = false;
        internal bool useStencil = false;
        internal bool full_screen = false;
        internal bool softwareProcessing = false;
        /// <summary>
        /// Directx9 constant
        /// </summary>
        const int MaxLights = 8;

        /// <summary>
        /// </summary>
        /// <param name="control"></param>
        /// <param name="useStencil">for a true 3d world</param>
        /// <param name="tryPureDevice">try the best perfomance, old gpu don't support puredevice</param>
        public DX9Device(Control control, bool useZbuffer, bool useStencil, bool tryPureDevice)
        {
            m_control = control;
            this.useZbuffer = useZbuffer;
            this.useStencil = useZbuffer && useStencil;

            //// initialise directX with presentParameters setting
            Trace.WriteLine("Initialize Device");
            // 8-bit and 24-bit back buffers are not supported in DX9.
            /*
                D3D.Format.R5G6B5,   // 16bit
                D3D.Format.A1R5G5B5, // 16bit with 1 alpha bit (only windowed)
                D3D.Format.X1R5G5B5, // 16bit with 1 ignored bit (only windowed)
                D3D.Format.X8R8G8B8, // 24bit (used 32bit because is more fast)
                D3D.Format.A8R8G8B8, // 32bit
                D3D.Format.A2R10G10B10, // never use (full-screen mode only)
                D3D.Format.Unknown   // (only windowed)

                D3D.Format.D16,    // 16bit z-buffer , 16 for depth
                D3D.Format.D24X8,  // 32bit z-buffer , 24 for depth
                D3D.Format.D24S8,  // 32bit z-buffer , 24 for depth , 8 for stencil
                D3D.Format.D24X4S4 // 32bit z-buffer , 24 for depth , 4 for stencil
            */
            Format backbufferFormat = Format.X8R8G8B8;
            Format depthstencilFormat = useStencil ? Format.D24S8 : Format.D24X8;


            D3D.Direct3D d3d = new D3D.Direct3D();


            Debug.Assert(DX9Enumerations.CheckDeviceType(backbufferFormat, backbufferFormat, true), "Device screen format not compatible");
            if (useZbuffer) Debug.Assert(DX9Enumerations.CheckDepthStencil(backbufferFormat, depthstencilFormat), "Device screen stencil format not compatible");


            //// setting for a correct windows directX application
            m_param = new D3D.PresentParameters();
            m_param.Windowed = !full_screen;
            m_param.DeviceWindowHandle = control.Handle;
            m_param.BackBufferFormat = (D3D.Format)(full_screen ? Format.Unknown : backbufferFormat);
            m_param.EnableAutoDepthStencil = useZbuffer;
            m_param.PresentationInterval = D3D.PresentInterval.Immediate;
            m_param.SwapEffect = D3D.SwapEffect.Discard;
            m_param.BackBufferCount = 1;
            m_param.BackBufferHeight = full_screen ? 0 : control.ClientSize.Height;
            m_param.BackBufferWidth = full_screen ? 0 : control.ClientSize.Width;
            if (useZbuffer) m_param.AutoDepthStencilFormat = (D3D.Format)depthstencilFormat;

            // Check to see if we can use a pure hardware device


            D3D.CreateFlags flags = D3D.CreateFlags.SoftwareVertexProcessing;
            softwareProcessing = true;

            if (tryPureDevice)
            {
                if (DX9Enumerations.SupportsHardwareTransformAndLight)
                {
                    flags = D3D.CreateFlags.HardwareVertexProcessing;
                    softwareProcessing = false;
                }
                if (DX9Enumerations.SupportsPureDevice)
                {
                    flags |= D3D.CreateFlags.PureDevice;
                    softwareProcessing = false;
                }
            }
            Trace.WriteLine("> Device Caps : " + flags.ToString());

            m_device = new D3D.Device(d3d,0, D3D.DeviceType.Hardware, control.Handle, flags, m_param);
            renderstates = new DX9RenderStatesManager(m_device);


            m_device.Disposing += new EventHandler<EventArgs>(OnDeviceReset);
            m_device.Disposed += new EventHandler<EventArgs>(OnDeviceLost);

            SetRendersStates();

            m_viewport = m_device.Viewport;
            m_swapchain = m_device.GetSwapChain(0);
            m_backbuffer = m_device.GetBackBuffer(0, 0);
            m_zbuffer = useZbuffer ? m_device.DepthStencilSurface : null;


            // clean the viewport
            this.Clear(control.BackColor);
        }

        #region Properties
        /// <summary>
        /// Set the render targhet size where scene are rendered
        /// </summary>
        public Viewport viewport
        {
            get { return Helper.Convert(m_viewport); }
            set { m_viewport = Helper.Convert(value); }
        }
        /// <summary> Need reset device to apply </summary>
        public int BackBufferWidth
        {
            get { return m_param.BackBufferWidth; }
            set { m_param.BackBufferWidth = value; }
        }
        /// <summary> Need reset device to apply </summary>
        public int BackBufferHeight
        {
            get { return m_param.BackBufferHeight; }
            set { m_param.BackBufferHeight = value; ; }
        }
        #endregion

        #region events

        void OnDeviceReset(object sender, EventArgs e)
        {
            Console.WriteLine("Device Reseting");
        }
        void OnDeviceLost(object sender, EventArgs e)
        {
            Console.WriteLine("Device Lost");
        }
#endregion

        #region RendersStates

        public DX9RenderStatesManager renderstates { get; private set; }

        /// <summary>
        /// Set default critical render states, used example after device reseting
        /// </summary>
        public void SetRendersStates()
        {
            renderstates.StencilEnable = useStencil;
            renderstates.ZBufferEnable = useZbuffer;
        }

        /// <summary>
        /// Set of Get the testure, can be null
        /// </summary>
        public DX9Texture Texture
        {
            set { m_device.SetTexture(0, value != null ? value.m_texture : null); }
        }
        public Color Ambient
        {
            get { DX.Color color = DX.Color.FromRgba(m_device.GetRenderState(D3D.RenderState.Ambient)); return Color.FromArgb(color.A, color.R, color.G, color.B); }
            set { m_device.SetRenderState(D3D.RenderState.Ambient, (new DX.Color(value.R, value.G, value.B, value.A)).ToRgba()); }
        }
        
        public void SetLight(Light light, int index)
        {
            if (index >= DX9Enumerations.MaxLights)
                throw new ArgumentOutOfRangeException("index of light out of range");
           
            D3D.Light d3dlight = Helper.Convert(light);
            m_device.SetLight(index, ref d3dlight);
            m_device.EnableLight(index, true);
        }
        /// <summary>
        /// Return the light at index, TODO : get a light in Software Device Type generate a exception
        /// </summary>
        public Light GetLight(int index)
        {
            return new Light();
            /*
            if (index >= MaxActiveLights)
                throw new ArgumentOutOfRangeException("index of light out of range");
            return Helper.Convert(m_device.GetLight(index));
            */
        }
        #endregion
        
        #region Methods
        public void DrawIndexedPrimitives(PrimitiveType type, int baseVertex, int minVertexIndex, int numVertices, int startIndex, int numPrimitives)
        {
            if (numPrimitives > 0 && numVertices > 0)
                m_device.DrawIndexedPrimitive((D3D.PrimitiveType)type, baseVertex, minVertexIndex, numVertices, startIndex, numPrimitives);
        }
        public void DrawPrimitives(PrimitiveType type, int startVertex, int numPrimitives)
        {
            if (numPrimitives > 0)
                m_device.DrawPrimitives((D3D.PrimitiveType)type, startVertex, numPrimitives);
        }
        public void DrawUserPrimitives<T>(PrimitiveType type,int numPrimitives,T[] array) where T : struct
        {
            m_device.DrawUserPrimitives<T>((D3D.PrimitiveType)type, numPrimitives, array);
        }
        /// <summary>
        /// reset the device
        /// </summary>
        public void Reset()
        {
            Trace.WriteLine("Device Reset Manualy");

            if (m_backbuffer != null) m_backbuffer.Dispose();
            if (m_zbuffer != null) m_zbuffer.Dispose();
            if (m_swapchain != null) m_swapchain.Dispose();

            m_device.Indices = null;
            m_device.SetTexture(0, null);
            m_device.SetStreamSource(0, null, 0, 0);

            m_device.Reset(m_param);

            m_swapchain = m_device.GetSwapChain(0);
            m_backbuffer = m_device.GetBackBuffer(0, 0);
            m_zbuffer = useZbuffer ? m_device.DepthStencilSurface : null;
            m_device.Viewport = m_viewport;
            
            SetRendersStates();
        }
        /// <summary>
        /// </summary>
        public void Clear(Color color)
        {
            D3D.ClearFlags flags = D3D.ClearFlags.Target;
            if (useZbuffer) flags |= D3D.ClearFlags.ZBuffer;
            if (useStencil) flags |= D3D.ClearFlags.Stencil;

            m_device.Clear(flags, DX.ColorBGRA.FromBgra(color.ToArgb()), 1.0f, 0);
        }
        /// <summary>
        /// begin scene , return a device state, if isn't DeviceState.OK can't render 
        /// </summary>
        public DeviceState Begin()
        {
            // don't understand if sharpdx use these values...
            const int DeviceLost = -2005530520;
            const int DeviceNotReset = -2005530519;

            DX.Result result = m_device.TestCooperativeLevel();

            //Okay to render
            if (result.Success) //result.Code == 0
            {
                try
                {
                    m_device.Viewport = m_viewport;
                    m_device.BeginScene();
                }
                catch
                {
                    return DeviceState.InvalidCall;
                }
                return DeviceState.OK;
            }
            switch (result.Code)
            {
                case 0:
                    return DeviceState.OK;
                case DeviceLost:
                    Debug.WriteLine("DeviceIsLost");
                    return DeviceState.DeviceLost;

                case DeviceNotReset:
                    Debug.WriteLine("DeviceIsNotReset");
                    return DeviceState.DeviceNotReset;

                default:
                    Debug.WriteLine("UnknowDeviceState");
                    return DeviceState.InvalidCall;
            }

        }
        /// <summary>
        /// end scene
        /// </summary>
        public void End()
        {
            m_device.SetStreamSource(0, null, 0, 0);
            m_device.Indices = null;

            m_device.EndScene();
        }
        /// <summary>
        /// present on screen
        /// </summary>  
        public void Present()
        {
            m_device.Present();
        }
        /// <summary>
        /// 
        /// </summary>
        public void SetRenderTarghet()
        {
            m_device.SetRenderTarget(0, m_backbuffer);
            if (m_zbuffer!=null) m_device.DepthStencilSurface = m_zbuffer;

            SetRendersStates();
        }
        /// <summary>
        /// reseting
        /// </summary>
        public void ResizeViewport()
        {
            DX.Viewport viewport = new DX.Viewport();
            viewport.Width = m_control.ClientSize.Width;
            viewport.Height = m_control.ClientSize.Height;
            viewport.X = 0;
            viewport.Y = 0;
            viewport.MaxDepth = 1;
            viewport.MinDepth = 0;
            this.m_viewport = viewport;
        }

        #endregion

        /// <summary>
        /// dispose the device
        /// </summary>
        ~DX9Device()
        {
            this.Dispose();
        }

        /// <summary>
        /// dispose
        /// </summary>
        public void Dispose()
        {
            if (m_swapchain != null) m_swapchain.Dispose();
            if (m_device != null) m_device.Dispose();
            if (m_zbuffer != null) m_zbuffer.Dispose();
            if (m_backbuffer != null) m_backbuffer.Dispose();
        }
        /// <summary>
        /// return video card description
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            //stampa la descrizione della scheda video
            //return video card description
            string info = "\n";

            //foreach (D3D.AdapterInformation details in m_device.Direct3D.Adapters)
           /// {
           //     info += details.Details.ToString() + "\n";
           /// }
            info += "use depthstencil : " + useStencil.ToString()+"\n";
            info += "use hardware     : " + (!softwareProcessing).ToString()+"\n";
            return info;
        }
        public void SetStreamSourceFrequency(int stream, int frequency, StreamSource source)
        {
            // in sharp the implementation is 
            //public void SetStreamSourceFrequency(int stream, int frequency, StreamSource source)
            //{
            //    int value = (source == StreamSource.IndexedData) ? 0x40000000 : unchecked((int)0x80000000);
            //    SetStreamSourceFrequency(stream, frequency | value);
            //}
            if (source == StreamSource.IndexedData || source == StreamSource.InstanceData)
            {
                m_device.SetStreamSourceFrequency(stream, frequency, (D3D.StreamSource)source);
            }
            else
            {
                // also for streamSource.NonIndexedData
                m_device.ResetStreamSourceFrequency(stream);
            }
        }
        public void SetVertexShaderNULL()
        {
            m_device.VertexShader = null;
        }
    }

    /// <summary>
    /// SharpDx
    /// </summary>
    public class DX9RenderStatesManager
    {
        internal D3D.Device m_device;

        internal DX9RenderStatesManager(D3D.Device device)
        {
            m_device = device;
        }

        public Material material
        {
            get { return Helper.Convert(m_device.Material); }
            set { m_device.Material = Helper.Convert(value); }
        }

        #region Transformsstates     
        public Matrix4 world
        {
            get { return Helper.Convert(m_device.GetTransform(D3D.TransformState.World)); }
            set { m_device.SetTransform(D3D.TransformState.World, Helper.Convert(value)); }
        }
        public Matrix4 projection
        {
            get { return Helper.Convert(m_device.GetTransform(D3D.TransformState.Projection)); }
            set { m_device.SetTransform(D3D.TransformState.Projection, Helper.Convert(value)); }
        }
        public Matrix4 view
        {
            get { return Helper.Convert(m_device.GetTransform(D3D.TransformState.View)); }
            set { m_device.SetTransform(D3D.TransformState.View, Helper.Convert(value)); }
        }
        #endregion
        
        #region Rendersstates

        public bool AlphaBlendEnable
        {
            get { return m_device.GetRenderState(D3D.RenderState.AlphaBlendEnable) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.AlphaBlendEnable, (bool)value); }
        }
        public Compare AlphaFunction
        {
            get { return (Compare)m_device.GetRenderState(D3D.RenderState.AlphaFunc); }
            set { m_device.SetRenderState(D3D.RenderState.AlphaFunc, (D3D.Compare)value); }
        }
        public bool AlphaTestEnable
        {
            get { return m_device.GetRenderState(D3D.RenderState.AlphaTestEnable) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.AlphaTestEnable, (bool)value); }
        }
        public Cull cullMode
        {
            get { return (Cull)m_device.GetRenderState(D3D.RenderState.CullMode); }
            set { m_device.SetRenderState(D3D.RenderState.AlphaTestEnable, (D3D.Cull)value); }
        }
        public float depth
        {
            get { return (float)m_device.GetRenderState(D3D.RenderState.DepthBias); }
            set { m_device.SetRenderState(D3D.RenderState.DepthBias, (float)value); }
        }
        public FillMode fillMode
        {
            get { return (FillMode)m_device.GetRenderState(D3D.RenderState.FillMode); }
            set { m_device.SetRenderState(D3D.RenderState.FillMode, (D3D.FillMode)value); }
        }
        public bool lightEnable
        {
            get { return m_device.GetRenderState(D3D.RenderState.Lighting) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.Lighting, value); }
        }
        public bool NormalizeNormals
        {
            get { return m_device.GetRenderState(D3D.RenderState.NormalizeNormals) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.NormalizeNormals, value); }
        }
        public bool StencilEnable
        {
            get { return m_device.GetRenderState(D3D.RenderState.StencilEnable) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.StencilEnable, value); }
        }
        public bool ZBufferEnable
        {
            get { return m_device.GetRenderState(D3D.RenderState.ZEnable) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.ZEnable, value); }
        }
        public Compare ZBufferFunction
        {
            get { return (Compare)m_device.GetRenderState(D3D.RenderState.ZFunc); }
            set { m_device.SetRenderState(D3D.RenderState.ZFunc, (D3D.Compare)value); }
        }
        public bool ZBufferWriteEnable
        {
            get { return m_device.GetRenderState(D3D.RenderState.ZWriteEnable) > 0; }
            set { m_device.SetRenderState(D3D.RenderState.ZWriteEnable, value); }
        }
        #endregion
    }
}