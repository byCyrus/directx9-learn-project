﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Graphics;
using Engine.Tools;

namespace Engine.Cdlod
{
    public class QuadTreeSetting
    {
        int NUMLEVELS;
        int NUMNODES;
        bool ForceLod0;
        bool ShowBox;
        bool ShowPatch;
        float RootRadius;

        public int NumLevels
        {
            get { return NUMLEVELS; }
            set { NUMLEVELS = value; NUMNODES = ((1 << (NUMLEVELS * 2)) - 1) / 3; }
        }
        /// <summary> maximum number of total node what level generate</summary>
        public int NumNodes
        { 
            get { return NUMNODES; }
        }
        /// <summary> force draw untill lod0 details</summary>
        public bool ForceDrawLeaf
        {
            get { return ForceLod0; }
            set { ForceLod0 = value; }
        }
        /// <summary> show help box to check if bounding volume are correct</summary>
        public bool ShowHelpBox
        {
            get { return ShowBox; }
            set { ShowBox = value; }
        }
        /// <summary> show the quad patches</summary>
        public bool ShowQuadPatch
        {
            get { return ShowPatch; }
            set { ShowPatch = value; }
        }
        /// <summary> show the quad patches</summary>
        public float RootLevelRadius
        {
            get { return RootRadius; }
            set { RootRadius = value; }
        }

        public QuadTreeSetting()
        {
            NUMLEVELS = 4;
            NUMNODES = ((1 << (NUMLEVELS * 2)) - 1) / 3;
            ForceLod0 = false;
            ShowBox = true;
            ShowPatch = true;
            RootRadius = 40.0f;
        }
    }


    public class QuadTree
    {
        public QuadTreeSetting setting { get; private set; }

        /// <summary> debug </summary>
        public static int QuadNodeDrawCount = 0;

        public BoundaryAABB worldsize;
        public QuadNode root;
        public IHeightMap heights;

        public Texture HeightMap = null;
        public Texture NormalMap = null;
        public Texture DiffuseMap = null;
        public Texture RockMap = null;
        /// <summary>
        /// Contain the precomputed radius(distance from camera eye) that activate the level's lod
        /// </summary>
        public float[] lodCameraRadius;
        /// <summary>
        /// Contain the min and max range for factor linear interpolation (x:minRadius , y:maxRadius) - 0,1
        /// </summary>
        public Vector2[] morphs;
        /// <summary>
        /// Contain the precomputed min and max heights value for all possible QuadNode to fix node's bounding box height
        /// </summary>
        public float[] minHeights, maxHeights;


        public QuadMesh patch = null;
        public BoxHelper boxSpline = null;
        public CameraHelper cameraSphere = null;
        Vector3 Eye;

        float min(float a, float b) { return a < b ? a : b; }
        float max(float a, float b) { return a > b ? a : b; }

        /// <summary>
        /// </summary>
        /// <param name="heightssample">the height map, can be null</param>
        /// <remarks>
        /// heightssample : is not necessary a finner map, just an approximation to precompute fast the height size for each quad node
        /// </remarks>
        public QuadTree(QuadTreeSetting setting , BoundaryAABB TerrainSize, IHeightMap heightSampler,
            Texture heightMap, Texture normalMap , Texture diffuseMap, Texture rockMap)
        {
            this.setting = setting;
            this.HeightMap = heightMap;
            this.NormalMap = normalMap;
            this.DiffuseMap = diffuseMap;
            this.RockMap = rockMap;

            this.heights = heightSampler;
            this.worldsize = TerrainSize;

            // calculate the ranges of distance that activate a quadnode for each level
            lodCameraRadius = new float[setting.NumLevels];
            for (int level = 0; level < setting.NumLevels; level++)
            {
                lodCameraRadius[level] = level == 0 ? 
                    (worldsize.max.x - worldsize.min.x) * 0.5f :
                    lodCameraRadius[level - 1] * 0.5f;
            }


            morphs = new Vector2[setting.NumLevels];
            for (int level = 0; level < setting.NumLevels; level++)
            {
                float r1 = level == 0 ? lodCameraRadius[level] : lodCameraRadius[level - 1] * 0.85f;
                float r0 = r1 * 0.7f;
                morphs[level] = new Vector2(r1, r0);
            }

            // calculate the heights for all nodes
            minHeights = new float[setting.NumNodes];
            maxHeights = new float[setting.NumNodes];
            if (heights != null)
            {
                // initialize a invalid values
                for (int node = 0; node < setting.NumNodes; node++)
                {
                    minHeights[node] = float.PositiveInfinity;
                    maxHeights[node] = float.NegativeInfinity;

                    //minHeights[node] = worldsize.min.y;
                    //maxHeights[node] = worldsize.max.y;
                }
                RecurseMinMaxHeight(0, 0, 0, 0, heights.SizeX, heights.SizeY);
            }

            // initialize root node
            root = new QuadNode(this, TerrainSize, 0, 0, null);
            root.name = "Root";

            //root.RecursiveSplit();
        }

        float angle = 0;
        float altitude = 10;
        /// <summary>
        /// Recursive draw, set once the vertexbuffer and draw all box... only for debug becuase 
        /// drawcall increase exponential.
        /// TODO : use harware instancing to avoid this performance issue
        /// </summary>
        public void Draw(ICamera camera)
        {
            QuadNodeDrawCount = 0;
            QuadMesh.TrianglesCount = 0;

            if (setting.ShowHelpBox && cameraSphere!=null)
            {
                cameraSphere.Draw(camera, Eye);
            }

            if (setting.ShowHelpBox && boxSpline != null)
            {
                boxSpline.SetToDevice(camera);
                if (boxSpline.material != null)
                {
                    // draw all box in one Begin() End(), commit new properties is more fast
                    int numpass = boxSpline.material.Begin();
                    for (int pass = 0; pass < numpass; pass++)
                    {
                        boxSpline.material.BeginPass(pass);
                        root.DrawHelpBox();
                        boxSpline.material.EndPass();
                    }
                    boxSpline.material.End();
                    QuadNodeDrawCount /= numpass;
                   
                }
                else
                {
                    root.DrawHelpBox();
                }
                if (boxSpline.font != null)
                {
                    root.DrawHelpBoxName(boxSpline.font , camera);
                }
            }
            QuadNodeDrawCount = 0;
            if (setting.ShowQuadPatch && patch != null && patch.shaderProgram != null)
            {
                angle += 0.01f;
                altitude -= 0.001f;

                patch.SetToDevice();
                patch.shaderProgram.CameraEye = Eye;
                patch.shaderProgram.LightDirection = Vector3.GetNormal(new Vector3(Math.Sin(angle), -0.5, Math.Cos(angle)));
                patch.shaderProgram.LightDirection = Vector3.GetNormal(new Vector3(-1, -1, -1));
                patch.shaderProgram.ProjViewWorld = camera.Projection * camera.View * Matrix4.Identity;
                patch.shaderProgram.TerrainMax = worldsize.max;
                patch.shaderProgram.TerrainMin = worldsize.min;
                patch.shaderProgram.HeightMap = HeightMap;
                patch.shaderProgram.NormalMap = NormalMap;
                patch.shaderProgram.DiffuseMap = DiffuseMap;
                patch.shaderProgram.RockMap = RockMap;

                patch.shaderProgram.PatchDimension = new Vector2(patch.col-1, patch.row-1);

                Vector2 dim = new Vector2(1.0f / (patch.col - 1), 1.0f / (patch.row - 1));

                int numpass = patch.shaderProgram.Begin();
                for (int pass = 0; pass < numpass; pass++)
                {
                    patch.shaderProgram.BeginPass(pass);
                    root.Draw();
                    patch.shaderProgram.EndPass();
                }
                patch.shaderProgram.End();
                QuadNodeDrawCount /= numpass;
                QuadMesh.TrianglesCount /= numpass;
            }
        }

        /// <summary>
        /// Calculate the node to render 
        /// </summary>
        public void UpdateTree(Vector3 eye , Frustum frustum)
        {
            if (Vector3.Distance(Eye , eye) > 0.001f)
            {
                Eye = eye;
                root.UpdateQuadTree(eye, frustum);
                //root.RecursiveSelect(eye, frustum);
            }
        }

        /// <summary>
        /// Recursive function, update min-max from leaf node to parent node;
        /// </summary>
        void RecurseMinMaxHeight(int node, int level, int minx, int minz, int maxx, int maxz)
        {
            // if is leaf calculate min-max
            bool isleaf = level >= setting.NumLevels - 1;
            //bool isleaf = (node << 2) >= NUMNODES - 1;
            if (isleaf)
            {
                if (minx > maxx || minz > maxz) throw new ArgumentException();

                float miny, maxy;
                heights.GetMinMax(minx, minz, maxx, maxz, out miny, out maxy);
                
                miny = worldsize.min.y + miny * (worldsize.max.y - worldsize.min.y);
                maxy = worldsize.min.y + maxy * (worldsize.max.y - worldsize.min.y);

                //if you want set to all node the same size of world, simply pre-set a value to min and maxHeights
                minHeights[node] = miny;
                maxHeights[node] = maxy;
                //minHeights[node] = min(minHeights[node], miny);
                //maxHeights[node] = max(maxHeights[node], maxy);

            }
            else
            {
                int down = node << 2;
                int hx = (maxx - minx) / 2;
                int hz = (maxz - minz) / 2;

                // LD LR TL TR
                RecurseMinMaxHeight(down + 1, level + 1, minx, minz, minx + hx, minz + hz);
                RecurseMinMaxHeight(down + 2, level + 1, minx + hx, minz, maxx, minz + hz);
                RecurseMinMaxHeight(down + 3, level + 1, minx, minz + hz, minx + hx, maxz);
                RecurseMinMaxHeight(down + 4, level + 1, minx + hx, minz + hz, maxx, maxz);
            }

            // update parent
            int par = node > 0 ? (node - 1) >> 2 : 0;
            minHeights[par] = min(minHeights[par], minHeights[node]);
            maxHeights[par] = max(maxHeights[par], maxHeights[node]);
        }
    }


    public class QuadNode
    {     
        const int DL = 0;
        const int DR = 1;
        const int TL = 2;
        const int TR = 3;

        BoundaryAABB bound;
        int level = 0;
        int node = 0;
        bool selected = false;

        /// <summary>
        /// conectivity with top node , if root is null
        /// </summary>
        QuadNode parent = null;
        QuadTree terrain = null;
        /// <summary>
        /// conectivity with down nodes, if leaf are all null
        /// </summary>
        QuadNode[] child = null;
        /// <summary>
        /// convert a units box in this bound size
        /// </summary>
        Matrix4 boxtransform;
        Color32 boxcolor;

        public string name = "";

        /// <summary>
        /// THe used quad counter
        /// </summary>
        public static int NodeCount { get; private set; }
        /// <summary>
        /// The quad counter stored in memory, NodeCount &lt;= GarbageCount, depend garbage management
        /// </summary>
        public static int GarbageCount { get; private set; }
        /// <summary>
        /// </summary>
        /// <param name="size">the box volume of real world</param>
        public QuadNode(QuadTree terrain, BoundaryAABB size, int level, int node, QuadNode parent)
        {
            this.terrain = terrain;
            this.level = level;
            this.parent = parent;
            this.node = node;
            this.boxcolor = Color32.Rainbow((float)level / terrain.setting.NumLevels);
            this.bound = size;

            Vector3 scale = (bound.max - bound.min) * 0.5f;
            Vector3 traslate = bound.center;
            this.boxtransform = new Matrix4(
                scale.x, 0, 0, traslate.x,
                0, scale.y, 0, traslate.y,
                0, 0, scale.z, traslate.z,
                0, 0, 0, 1);

            if (NodeCount++ >= terrain.setting.NumNodes)
                throw new StackOverflowException("Warning, too many node created");

            GarbageCount++;
        }

        ~QuadNode()
        {
            merge();
            GarbageCount--;
        }

        int max(int i, int j) { return i > j ? i : j; }

        /// <summary>
        /// Return true if it's the last in the tree, so it will be rendered
        /// </summary>
        bool islast
        {
            get
            {
                if (child == null) return true;
                bool advance = false;
                for (int i = 0; i < 4; i++) advance |= child[i].selected;
                return !advance;
            }
        }

        public bool IsLeaf { get { return child == null; } }
    

        /// <summary>
        /// Recursive drawing
        /// </summary>
        public void Draw()
        {
            if (islast)
            {
                terrain.patch.shaderProgram.Morph = terrain.morphs[level];
                terrain.patch.shaderProgram.PatchColor = boxcolor;
                terrain.patch.shaderProgram.PatchOrigin = bound.min;
                terrain.patch.shaderProgram.PatchScale = bound.max - bound.min;
                terrain.patch.DrawMesh();
                QuadTree.QuadNodeDrawCount++;
            }
            else
            {
                for (int i = 0; i < 4; i++) child[i].Draw();
            }
        }
        public void DrawHelpBox()
        {
            if (islast)
            {
                terrain.boxSpline.Draw(boxtransform, boxcolor);
                QuadTree.QuadNodeDrawCount++;
            }
            else
            {
                for (int i = 0; i < 4; i++) child[i].DrawHelpBox();
            }
        }
        public void DrawHelpBoxName(Font font, ICamera camera)
        {
            if (islast)
            {
                Vector3 screen = Vector3.Project(boxtransform.TranslationComponent, camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw(node + "_" + name, (int)screen.x, (int)screen.y, (System.Drawing.Color)boxcolor);
            }
            else
            {
                for (int i = 0; i < 4; i++) child[i].DrawHelpBoxName(font,camera);
            }
        }

        /// <summary>
        /// Generate all nodes
        /// </summary>
        public void RecursiveSplit()
        {
            // if it's last node in the tree can't advance
            if (level >= terrain.setting.NumLevels - 1) return;
            split();
            for (int i = 0; i < 4; i++)
                child[i].RecursiveSplit();
        }

        public void RecursiveSelect(Vector3 eye, Frustum frustum)
        {
            // if it's last node in the tree can't advance
            if (level >= terrain.setting.NumLevels - 1) return;
            
            float dist;
            if (selected = bound.Intersect(eye.x, eye.y, eye.z, terrain.lodCameraRadius[level], out dist))
            {
                if (child != null)
                    for (int i = 0; i < 4; i++)
                        child[i].RecursiveSelect(eye, frustum);
            }
            else
            {
                RecursiveUnSelect();
            }

        }
        
        public void RecursiveUnSelect()
        {
            if (child != null)
                for (int i = 0; i < 4; i++)
                {
                    child[i].selected = false;
                    child[i].RecursiveUnSelect();
                }
        }

        /// <summary>
        /// TODO : need a continuity lod transiction, each lod(i) must have a lod(i) or lod(i-1) neighbours 
        /// Recursive selection , return true if is splitted in 4 child
        /// </summary>
        public bool UpdateQuadTree(Vector3 eye, Frustum frustum)
        {
            float dist;
            selected = true;

            if (terrain.setting.ForceDrawLeaf)
            {
                if (level >= terrain.setting.NumLevels - 1)
                {
                }
                else
                {
                    if (IsLeaf) split();
                    for (int i = 0; i < 4; i++)
                        child[i].UpdateQuadTree(eye, frustum);
                    return true;
                }
            }

            if (!bound.Intersect(eye.x, eye.y, eye.z, terrain.lodCameraRadius[level], out dist))
            {
                merge();
                return false;
            }
            // if last node in the tree can't advance
            if (level >= terrain.setting.NumLevels - 1)
            {
                return true;
            }
            else
            {
                if (!bound.Intersect(eye.x, eye.y, eye.z, terrain.lodCameraRadius[level + 1], out dist))
                {
                    merge();
                    return true;
                }
                else
                {
                    if (IsLeaf) split();

                    for (int i = 0; i < 4; i++)
                    {
                        if (!child[i].UpdateQuadTree(eye, frustum))
                        {
                        }
                    }
                }
            }
            return true;
        }

        void split()
        {
            int down = node << 2;
            BoundaryAABB sizeDL = new BoundaryAABB(
                    new Vector3(bound.center.x, terrain.maxHeights[down + 1], bound.center.z),
                    new Vector3(bound.min.x, terrain.minHeights[down + 1], bound.min.z));

            BoundaryAABB sizeDR = new BoundaryAABB(
                    new Vector3(bound.max.x, terrain.maxHeights[down + 2], bound.center.z),
                    new Vector3(bound.center.x, terrain.minHeights[down + 2], bound.min.z));

            BoundaryAABB sizeTL = new BoundaryAABB(
                    new Vector3(bound.center.x, terrain.maxHeights[down + 3], bound.max.z),
                    new Vector3(bound.min.x, terrain.minHeights[down + 3], bound.center.z));

            BoundaryAABB sizeTR = new BoundaryAABB(
                    new Vector3(bound.max.x, terrain.maxHeights[down + 4], bound.max.z),
                    new Vector3(bound.center.x, terrain.minHeights[down + 4], bound.center.z));

            split(sizeDL, sizeDR, sizeTL, sizeTR);
        }
        /// <summary>
        /// Split this Quad into 4 children, pass a precomputer boundary
        /// </summary>
        void split(BoundaryAABB sizeDL, BoundaryAABB sizeDR, BoundaryAABB sizeTL, BoundaryAABB sizeTR)
        {
            int down = node << 2;
            
            if (child != null) merge();

            child = new QuadNode[4];
            child[DL] = new QuadNode(terrain, sizeDL, level + 1, down + 1, this);
            child[DR] = new QuadNode(terrain, sizeDR, level + 1, down + 2, this);
            child[TL] = new QuadNode(terrain, sizeTL, level + 1, down + 3, this);
            child[TR] = new QuadNode(terrain, sizeTR, level + 1, down + 4, this);
            child[DL].name = "downLeft";
            child[DR].name = "downRight";
            child[TL].name = "topLeft";
            child[TR].name = "topRight";
        }
        /// <summary>
        /// Remove the 4 children and free memory
        /// </summary>
        void merge()
        {
            if (child != null)
                for (int i = 0; i < 4; i++)
                    if (child[i]!=null)
                    {
                        child[i].merge();
                        child[i] = null;
                        NodeCount--;
                    }
            child = null;
            //GC.Collect();
        }

        public override string ToString()
        {
            return string.Format("LOD{0} {1} : size[{2}]", terrain.setting.NumLevels - level, name, bound);
        }
    }
}
