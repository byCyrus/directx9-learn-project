﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Font = Engine.Graphics.Font;

using Engine.Graphics;
using Engine.Tools;
using Unsafe = Engine.Tools.Unsafe;
using Engine.Maths;


namespace Engine.Test
{
    public partial class Form1 : Form
    {
        GraphicDevice device;
        Font font;
        TextureVolume texture;
        bool draw = true;
        VertexDeclaration tcvertex_decl;
        VertexBuffer vertexbuffer;
        IndexBuffer facebuffer;

        TrackBallCamera cameractrl;
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 form = new Form1();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();

            cameractrl = new TrackBallCamera(this, new Vector3(-5, 5, -5), Vector3.Zero,0.1f,1000.0f);
            cameractrl.Enabled = true;
        }
        void GetTestTerrain2(out T3NVertex[] data, out Face16[] faces)
        {
            MeshListPolygon grid = MeshListPolygon.TessellatedRectangle(-10, -10, 10, 10, 100, 100);

            int numverts = grid.vertices.Count;
            int numx = 100 + 1;
            int numy = 100 + 1;

            Vector3[] vertices = new Vector3[numverts];
            for (int i = 0; i < numverts; i++)
                vertices[i] = new Vector3(grid.vertices[i].x, 0, grid.vertices[i].y);

            // get elevations
            Bitmap heights = Properties.Resources.Elevations;
            int m_width = heights.Width;
            int m_lenght = heights.Height;
            for (int j = 0; j < numy; j++)
                for (int i = 0; i < numx; i++)
                {
                    float ii = (float)i / (float)numx * m_width;
                    float jj = (float)j / (float)numy * m_lenght;
                    vertices[i + j * numx].y = heights.GetPixel((int)ii, (int)jj).GetBrightness() * 2f;
                }

            // get Y blend factor
            float miny = float.MaxValue;
            float maxy = float.MinValue;
            for (int i = 0; i < numverts; i++)
            {
                if (vertices[i].y < miny) miny = vertices[i].y;
                if (vertices[i].y > maxy) maxy = vertices[i].y;
            }
            float delta = maxy - miny;


            HMesh mesh = new HMesh(grid.indices.data, vertices, null);
            mesh.FlipMinimizeEdgeLenght(1);
            MeshListGeometry trimesh = mesh.TriMesh;

            Vector3[] normals = GeometryTools.GetDefaultNormal(trimesh.vertices.data, trimesh.indices.data);
            Vector2[] textures = GeometryTools.GetPlanarProjection(trimesh.vertices.data, new Vector2(0, 0), new Vector2(100, 100), Matrix4.RotationX(MathUtils.DegreeToRadian(90)));

            data = new T3NVertex[numverts];
            for (int i = 0; i < numverts; i++)
            {
                data[i] = new T3NVertex();
                data[i].position = trimesh.vertices[i];
                data[i].normal = normals[i];
                Vector3 tex = new Vector3(textures[i].x, textures[i].y, 0.0f);
                tex.z = vertices[i].y / delta;

                data[i].texture = tex;
            }
            faces = trimesh.indices.data;
        }
        
        void GetTestTerrain(out T3NVertex[] data, out Face16[] faces)
        {
            MeshListPolygon grid = MeshListPolygon.TessellatedRectangle(-10, -10, 10, 10, 100, 100);

            int numverts = grid.vertices.Count;
            int numx = 100 + 1;
            int numy = 100 + 1;

            Vector3[] vertices = new Vector3[numverts];
            for (int i = 0; i < numverts; i++)
                vertices[i] = new Vector3(grid.vertices[i].x, 0, grid.vertices[i].y);

            // get elevations
            Bitmap heights = Properties.Resources.Elevations;
            int m_width = heights.Width;
            int m_lenght = heights.Height;
            for (int j = 0; j < numy; j++)
                for (int i = 0; i < numx; i++)
                {
                    float ii = (float)i / (float)numx * m_width;
                    float jj = (float)j / (float)numy * m_lenght;
                    vertices[i + j * numx].y = heights.GetPixel((int)ii, (int)jj).GetBrightness() * 2f;
                }

            // get Y blend factor
            float miny = float.MaxValue;
            float maxy = float.MinValue;
            for (int i = 0; i < numverts; i++)
            {
                if (vertices[i].y < miny) miny = vertices[i].y;
                if (vertices[i].y > maxy) maxy = vertices[i].y;
            }
            float delta = maxy - miny;


            Vector3[] normals = GeometryTools.GetDefaultNormal(vertices, grid.indices.data);
            Vector2[] textures = GeometryTools.GetPlanarProjection(vertices, new Vector2(0, 0), new Vector2(100, 100), Matrix4.RotationX(MathUtils.DegreeToRadian(90)));

            data = new T3NVertex[numverts];
            for (int i = 0; i < numverts; i++)
            {
                data[i] = new T3NVertex();
                data[i].position = vertices[i];
                data[i].normal = normals[i];
                Vector3 tex = new Vector3(textures[i].x, textures[i].y, 0.0f);
                tex.z = vertices[i].y / delta;

                data[i].texture = tex;
            }
            faces = grid.indices.data;
        }

        void GetTestTerrain3()
        {
            int numx = 101;
            int numy = 101;

            MeshListPolygon grid = MeshListPolygon.TessellatedRectangle(-10, -10, 10, 10, numx - 1, numy - 1);

            int numverts = grid.vertices.Count;


            VertexAttribute<Vector3> vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, numverts);
            IndexAttribute<Face16> faces = grid.indices;

            for (int i = 0; i < numverts; i++)
                vertices[i] = new Vector3(grid.vertices[i].x, 0, grid.vertices[i].y);

            // get elevations
            Bitmap heights = Properties.Resources.Elevations;
            int m_width = heights.Width;
            int m_lenght = heights.Height;
            for (int j = 0; j < numy; j++)
                for (int i = 0; i < numx; i++)
                {
                    float ii = (float)i / (float)numx * m_width;
                    float jj = (float)j / (float)numy * m_lenght;
                    vertices.data[i + j * numx].y = heights.GetPixel((int)ii, (int)jj).GetBrightness() * 2f;
                }

            // get Y blend factor
            float miny = float.MaxValue;
            float maxy = float.MinValue;
            for (int i = 0; i < numverts; i++)
            {
                if (vertices[i].y < miny) miny = vertices[i].y;
                if (vertices[i].y > maxy) maxy = vertices[i].y;
            }
            float delta = maxy - miny;


            VertexAttribute<Vector3> normals = new VertexAttribute<Vector3>(DeclarationUsage.Normal,
                GeometryTools.GetDefaultNormal(vertices.data, faces.data));

            Vector2[] tex = GeometryTools.GetPlanarProjection(vertices.data, new Vector2(0, 0), new Vector2(100, 100), Matrix4.RotationX(MathUtils.DegreeToRadian(90)));

            VertexAttribute<Vector3> textures = new VertexAttribute<Vector3>(DeclarationUsage.TexCoord, tex.Length);
                

            for (int i = 0; i < numverts; i++)
                textures[i] = new Vector3(tex[i].x, tex[i].y, vertices[i].y / delta);


            T3NVertex[] array = new T3NVertex[numverts];
            for (int i = 0; i < numverts; i++)
            {
                array[i] = new T3NVertex();
                array[i].position = vertices[i];
                array[i].normal = normals[i];
                array[i].texture = textures[i];
            }


            tcvertex_decl = new VertexDeclaration(device, typeof(T3NVertex), T3NVertex.elements);

            vertexbuffer = new VertexBuffer(device, tcvertex_decl.Format, numverts, false, true);

            VertexStream stream = vertexbuffer.OpenStream();
            stream.WriteCollection<Vector3>(vertices, 0, numverts,0);
            stream.WriteCollection<Vector3>(textures, 0,numverts, 0);
            stream.WriteCollection<Vector3>(normals,0, numverts, 0);
            vertexbuffer.CloseStream();
            vertexbuffer.Count = numverts;

            facebuffer = new IndexBuffer(device, typeof(Face16), faces.Count, false, true);
            IndexStream istream = facebuffer.OpenStream();
            istream.WriteCollection<Face16>(faces, 0, 0);
            facebuffer.CloseStream();
            facebuffer.Count = faces.Count;
        }
        
        void InitializeGraphics()
        {
            device = new GraphicDevice(this);
            font = new Font(device, Engine.Graphics.Font.FontName.Arial, 8);

            Bitmap[] m_maps = new Bitmap[]
            {
                Properties.Resources.bmp1,
                Properties.Resources.bmp2,
                Properties.Resources.bmp3,
                Properties.Resources.bmp4,
                Properties.Resources.bmp5,
                Properties.Resources.bmp6,
                Properties.Resources.bmp7,
                Properties.Resources.bmp8
            };

            int w = m_maps[0].Width;
            int h = m_maps[0].Height;

            texture = new TextureVolume(device, w, h, m_maps.Length, true);
            texture.WriteTextures(m_maps);

            Light sun = Light.Sun;
            sun.Position = new Vector3(1000, 1000, 1000);
            sun.Direction = new Vector3(-1, -1, 0);
            device.lights.Add(sun);

           
            GetTestTerrain3();

            /*
            T3NVertex[] data;
            Face16[] faces;

            GetTestTerrain2(out data, out faces);
            
            tcvertex_decl = new VertexDeclaration(device, typeof(T3NVertex), T3NVertex.elements);
            vertexbuffer = new VertexBuffer(device, tcvertex_decl, data.Length, false, true);
            facebuffer = new IndexBuffer(device, typeof(Face16), faces.Length, false, true);
            
            vertexbuffer.Open();
            vertexbuffer.Write(data);
            vertexbuffer.Close();
            vertexbuffer.Count = data.Length;

            
            facebuffer.Open();
            facebuffer.Write(faces);
            facebuffer.Close();
            facebuffer.Count = faces.Length;
            */

        }
        
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
            cameractrl.Enabled = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            cameractrl.Enabled = true;
            device.Resize(this.ClientSize);
            this.Invalidate();
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            if (!draw) return;

            device.renderstates.world = Matrix4.Identity;
            device.renderstates.projection = cameractrl.Projection;
            device.renderstates.view = cameractrl.View;
            device.renderstates.fillMode = FillMode.Solid;

            bool state = device.BeginDraw();
            Debug.Assert(state, "Render Fail");
            if (state)
            {
                device.SetRenderTarghet();
                device.Clear(Color.CornflowerBlue);
                
                //device.RemoveTexture();

                ////////////////////// cylinder //////////////////////
                if (vertexbuffer.Count > 0 && facebuffer.Count > 0)
                {
                    tcvertex_decl.SetToDevice();
                    vertexbuffer.SetToDevice();
                    facebuffer.SetToDevice();
                    texture.SetToDevice();

                    device.renderstates.lightEnable = true;
                    device.renderstates.material = Material.Default;
                    device.renderstates.fillMode = FillMode.Solid;
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, facebuffer.Count);

                    device.RemoveTexture(); //all edges of wireframe with same color
                    device.renderstates.fillMode = FillMode.WireFrame;
                    device.renderstates.material = new Material(); // material diffuse black
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexbuffer.Count, 0, facebuffer.Count);
                }

                device.EndDraw();
                device.PresentDraw();
            }
        }



        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle)
            {
                System.Threading.Thread.Sleep(10);
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct T3NVertex
    {
        public Vector3 position;
        public Vector3 normal;
        public Vector3 texture;

        public static readonly VertexElement[] elements = new VertexElement[]
        {
            new VertexElement(0, 0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Normal,0),
            new VertexElement(0,24,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.TexCoord,0),
            VertexElement.VertexDeclarationEnd
        };
    }
    public struct CameraValues : ICamera
    {
        public float nearZ;
        public float farZ;
        public Matrix4 World { get; set; }
        public Matrix4 View { get; set; }
        public Matrix4 inview { get; set; }
        public Matrix4 Projection { get; set; }
        /// <summary>
        /// BackBufferSize
        /// </summary>
        public Viewport viewport { get; set; }

        public Vector3 Targhet
        {
            get { throw new NotImplementedException(); }
        }

        public Vector3 Eye
        {
            get { throw new NotImplementedException(); }
        }

        public Viewport Viewport
        {
            get { throw new NotImplementedException(); }
        }
    }
}
