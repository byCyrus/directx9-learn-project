﻿using System;
using System.Runtime.InteropServices;
using System.Text;

using Engine.Maths;
using Engine.Graphics;

namespace Engine.Test
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct BVertex
    {
        public Vector3 position;
        public Vector3 normal;
        public Vector3 tangent;
        public Vector3 binormal;
        public Vector2 texture;

        public BVertex(
            float x, float y, float z,
            float nx, float ny, float nz,
            float tx, float ty, float tz,
            float bx, float by, float bz,
            float tu, float tv)
        {
            position = new Vector3(x, y, z);
            normal = new Vector3(nx, ny, nz);
            tangent = new Vector3(tx, ty, tz);
            binormal = new Vector3(bx, by, bz);
            texture = new Vector2(tu, tv);
        }

        public static readonly VertexElement[] elements = new VertexElement[]
        {
            new VertexElement(0,  0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0), 
            new VertexElement(0, 12, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Normal,   0), 
            new VertexElement(0, 24, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Tangent,  0), 
            new VertexElement(0, 36, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Binormal, 0), 
            new VertexElement(0, 48, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0), 
            VertexElement.VertexDeclarationEnd
        };
    }

}
