﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Maths;

namespace Engine.Graphics
{

    /// <summary>
    /// All vertices type must contain the xyz position coordinate
    /// </summary>
    public interface IVertexPosition
    {
        Vector3 position { get; set; }
    }
    public interface IVertexNormal
    {
        Vector3 normal { get; set; }
    }
    public interface IVertexTexCoord
    {
        Vector2 texture { get; set; }
    }
    public interface IVertexTangent
    {
        Vector3 tangent { get; set; }
    }
    public interface IVertexColor
    {
        Color32 color { get; set; }
    }
    /// <summary>
    /// Not Used
    /// </summary>
    /// <remarks>
    /// Pack = 4 mean the struct aligned values are packed by multiple of 4 bytes, Marshal.SizeOf(VERTEX) == 4*3
    /// </remarks>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 3)]
    public struct VERTEX : IVertexPosition
    {
        public Vector3 m_pos;

        public VERTEX(float x, float y, float z)
        {
            m_pos = new Vector3(x, y, z);
        }
        public VERTEX(Vector3 position)
        {
            m_pos = position;
        }
        public static VERTEX Empty = new VERTEX(0, 0, 0);

        public static readonly VertexElement[] m_elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0)
        };

        public Vector3 position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }

        public override string ToString()
        {
            return string.Format("{0,4} {1,4} {2,4}", m_pos.x, m_pos.y, m_pos.z);
        }
    }

    /// <summary>
    /// Used as default for Splines
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 3 + sizeof(int))]
    public struct CVERTEX : IVertexPosition, IVertexColor
    {
        public Vector3 m_pos;
        public Color32 m_col;

        public CVERTEX(Vector3 position, Color32 color)
        {
            m_pos = position;
            m_col = color;
        }

        public CVERTEX(Vector3 position, Color color) :
            this(position, (Color32)color) { }

        public CVERTEX(float x, float y, float z, Color color) :
            this(new Vector3(x, y, z), color) { }

        public CVERTEX(float x, float y, float z, Color32 color) :
            this(new Vector3(x, y, z), color) { }


        public static readonly VertexElement[] m_elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Color,DeclarationMethod.Default,DeclarationUsage.Color,0)
        };
        public VertexElement[] elements
        {
            get { return m_elements; }
        }

        public Vector3 position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }
        public Color32 color
        {
            get { return m_col; }
            set { m_col = value; }
        }
        public override string ToString()
        {
            return string.Format("{0} ; {1}", m_pos.ToString(), m_col.ToString());
        }
    }

    /// <summary>
    /// Used as default for not-textured mesh
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 6 + sizeof(int))]
    public struct NCVERTEX : IVertexPosition, IVertexNormal, IVertexColor
    {
        public Vector3 m_pos;
        public Vector3 m_norm;
        public Color32 m_col;

        public NCVERTEX(Vector3 position, Vector3 normal, Color32 color)
        {
            m_pos = position;
            m_norm = normal;
            m_col = color;
        }
        public NCVERTEX(Vector3 position, Vector3 normal, Color color)
            : this(position, normal, (Color32)color)
        { }
        public NCVERTEX(float x, float y, float z, float nx, float ny, float nz, Color32 color) :
            this(new Vector3(x, y, z), new Vector3(nx, ny, nz), color)
        { }

        public static readonly VertexElement[] m_elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Normal,0),
            new VertexElement(0,24,DeclarationType.Color,DeclarationMethod.Default,DeclarationUsage.Color,0)
        };

        public Vector3 position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }
        public Vector3 normal
        {
            get { return m_norm; }
            set { m_norm = value; }
        }
        public Color32 color
        {
            get { return m_col; }
            set { m_col = value; }
        }

        public override string ToString()
        {
            return (String.Format("{0}  {1} - {2,4}", m_pos.ToString(), m_norm.ToString(), m_col.ToString()));
        }
    }

    /// <summary>
    /// NOT USED
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 5 + sizeof(int))]
    public struct TCVERTEX : IVertexPosition, IVertexColor, IVertexTexCoord
    {
        public Vector3 m_pos;
        public Vector2 m_uv;
        public Color32 m_col;

        public TCVERTEX(Vector3 position, Vector2 texture, Color32 color)
        {
            m_pos = position;
            m_uv = texture;
            m_col = color;
        }

        public TCVERTEX(Vector3 position, Vector2 texture, Color color) :
            this(position, texture, (Color32)color) { }

        public TCVERTEX(float x, float y, float z, float u, float v, Color32 color)
            : this(new Vector3(x, y, z), new Vector2(u, v), color) { }

        public static readonly VertexElement[] m_elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Float2,DeclarationMethod.Default,DeclarationUsage.TexCoord,0),
            new VertexElement(0,20,DeclarationType.Color,DeclarationMethod.Default,DeclarationUsage.Color,0)
        };

        public Vector3 position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }
        public Vector2 texture
        {
            get { return m_uv; }
            set { m_uv = value; }
        }
        public Color32 color
        {
            get { return m_col; }
            set { m_col = value; }
        }

        public override string ToString()
        {
            return (String.Format("{0}  {1} - {2,4}", m_pos.ToString(), m_uv.ToString(), m_col.ToString()));
        }
    }


    /// <summary>
    /// Used as default for not-textured mesh, test the half float precision , using pack = 4 because 4xHalf is like a 2xInt
    /// Version of NCVERTEX with Normal using float16
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 3 + sizeof(short) * 4 + sizeof(int))]
    public struct NCVERTEX16 : IVertexPosition, IVertexColor, IVertexNormal
    {
        public Vector3 m_pos;
        public Color32 m_col;
        public Vector4x16 m_norm;

        public NCVERTEX16(Vector3 position, Vector3 normal, Color32 color)
        {
            m_pos = position;
            m_norm = (Vector4x16)normal;
            m_col = color;
        }
        public NCVERTEX16(Vector3 position, Vector3 normal, Color color)
            : this(position, normal, (Color32)color)
        { }

        public NCVERTEX16(float x, float y, float z, float nx, float ny, float nz, Color32 color)
            : this(new Vector3(x, y, z), new Vector3(nx, ny, nz), color)
        { }

        public static readonly VertexElement[] m_elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Color,DeclarationMethod.Default,DeclarationUsage.Color,0),
            new VertexElement(0,16,DeclarationType.HalfFour,DeclarationMethod.Default,DeclarationUsage.Normal,0)         
        };

        public Vector3 position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }
        public Vector3 normal
        {
            get { return (Vector3)m_norm; }
            set { m_norm = (Vector4x16)value; }
        }
        public Color32 color
        {
            get { return m_col; }
            set { m_col = value; }
        }


        public override string ToString()
        {
            return (String.Format("{0}  <{1} {2} {3}> - {4,4}", m_pos.ToString(), m_norm.ToString(), m_col.ToString()));
        }
    }

    /// <summary>
    /// Used for texture mesh, the normal are useful for correct lighting.
    /// Version of NTVERTEX with Normal and Texture using float16
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4 , Size = sizeof(float) * 3 + sizeof(short) * 6)]
    public struct NTVERTEX16 : IVertexPosition, IVertexNormal, IVertexTexCoord
    {
        public Vector3 m_pos;
        public Vector4x16 m_norm;
        public Vector2x16 m_uv;

        public NTVERTEX16(Vector3 position, Vector3 normal, Vector2 texture)
        {
            m_pos = position;
            m_norm = (Vector4x16)normal;
            m_uv = (Vector2x16)texture;
        }
        /// <summary>
        /// </summary>
        /// <remarks>
        /// remember that normal vector is a vector4 with w=0, position vector is a vector4 with w=1
        /// </remarks>
        public NTVERTEX16(float x, float y, float z, float nx, float ny, float nz, float u, float v)
            : this(new Vector3(x, y, z), new Vector4x16(nx, ny, nz, 0), new Vector2x16(u, v)) { }


        public static readonly VertexElement[] m_elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.HalfFour,DeclarationMethod.Default,DeclarationUsage.Normal,0),
            new VertexElement(0,20,DeclarationType.HalfTwo,DeclarationMethod.Default,DeclarationUsage.TexCoord,0)
        };

        public Vector3 position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }
        public Vector3 normal
        {
            get { return (Vector3)m_norm; }
            set { m_norm = (Vector4x16)value; }
        }
        public Vector2 texture
        {
            get { return (Vector2)m_uv; }
            set { m_uv = (Vector2x16)value; }
        }

        public override string ToString()
        {
            return (String.Format("{0}  {1}  {2}", m_pos.ToString(), m_norm.ToString(), m_uv.ToString()));
        }
    }
    /// <summary>
    /// Used for texture mesh, the normal are useful for correct lighting
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 4, Size = sizeof(float) * 8)]
    public struct NTVERTEX : IVertexPosition, IVertexNormal, IVertexTexCoord
    {
        public Vector3 m_pos;
        public Vector3 m_norm;
        public Vector2 m_uv;

        public NTVERTEX(Vector3 position, Vector3 normal, Vector2 uv)
        {
            m_pos = position;
            m_norm = normal;
            m_uv = uv;
        }
        public NTVERTEX(float x, float y, float z, float nx, float ny, float nz, float u, float v)
            : this(new Vector3(x, y, z), new Vector3(nx, ny, nz), new Vector2(u, v)) { }


        public static readonly VertexElement[] m_elements = new VertexElement[]
        {
            new VertexElement(0,0,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Position,0),
            new VertexElement(0,12,DeclarationType.Float3,DeclarationMethod.Default,DeclarationUsage.Normal,0),
            new VertexElement(0,24,DeclarationType.Float2,DeclarationMethod.Default,DeclarationUsage.TexCoord,0)
        };

        public Vector3 position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }
        public Vector3 normal
        {
            get { return m_norm; }
            set { m_norm = value; }
        }
        public Vector2 texture
        {
            get { return m_uv; }
            set { m_uv = value; }
        }
        public override string ToString()
        {
            return (String.Format("{0}  {1}   {2}", m_pos.ToString(), m_norm.ToString(), m_uv.ToString()));
        }
    }
}
