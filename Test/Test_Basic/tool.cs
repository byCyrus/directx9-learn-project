﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Text;


using Engine.Graphics;
using Engine.Geometry;
using Engine.Tools;
using Engine.Maths;
using Engine.Renderer;


namespace Engine.Test
{
    public class BillboardObj : IDrawable
    {
        public BillBoardVertex[] geometry;
        public BillBoardInstance[] instance;

        MaterialFX_BILLBOARD.Techniques method = MaterialFX_BILLBOARD.Techniques.ParallelBillboardWithTexture;
        Random rnd = new Random();
        Texture diffusemap;
        bool INSTANCING = true;
        bool INDEXED = true;

        /// <summary>
        /// the billboard vertex definition
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BillBoardVertex
        {
            public Vector2 texcoord;
            public Vector2 scalar;

            public BillBoardVertex(float scalarx, float scalary, float texx, float texy)
            {
                scalar = new Vector2(scalarx, scalary);
                texcoord = new Vector2(texx, texy);         
            }
            public override string ToString()
            {
                return texcoord.ToString() + " ; " + scalar.ToString();
            }
        }
        /// <summary>
        /// the billboard vertex definition
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct BillBoardInstance
        {
            public Vector3 traslation;
            public Color32 color;
            public Vector2 windf;

            public BillBoardInstance(float x, float y, float z, Color32 color, float windx, float windy)
            {
                this.traslation = new Vector3(x, y, z);
                this.color = color;
                this.windf = new Vector2(windx, windy);
            }
            public override string ToString()
            {
                return traslation.ToString() + " ; " + color.ToString();
            }
        }


        MaterialFX_BILLBOARD material;

        int numinstances = 0;
        VertexInfo d_geometry, d_instance;
        VertexDeclaration instance_decl;
        GraphicDevice device;
        VertexBuffer gVB, iVB;
        IndexBuffer iB;

        Vector2 Wind = new Vector2(1,1);
        /// <summary>
        /// </summary>
        /// <param name="iIndex">instance index</param>
        /// <param name="gIndex">geometry's vertex index</param>
        /// <returns></returns>
        public Vector3 encode(int iIndex, int gIndex)
        {
            switch (method)
            {
                case MaterialFX_BILLBOARD.Techniques.CylinderBillboard:
                case MaterialFX_BILLBOARD.Techniques.CylinderBillboardWithTexture:
                    return Vector3.Zero;
                case MaterialFX_BILLBOARD.Techniques.ParallelBillboard:
                case MaterialFX_BILLBOARD.Techniques.ParallelBillboardWithTexture:
                case MaterialFX_BILLBOARD.Techniques.ParallelBillboardWithWireframeTexture:
                    return Vector3.Zero;
                case MaterialFX_BILLBOARD.Techniques.SphericBillboard:
                case MaterialFX_BILLBOARD.Techniques.SphericBillboardWithTexture:
                    return Vector3.Zero;
            }
            return Vector3.Zero;
        }



        public BillboardObj(GraphicDevice device)
        {
            //     0-------1
            //     | \     |         vup
            //     |  \    |         |
            //     |   \   |         |___ vright
            //     |    \  |
            //     |     \ |
            //     3---T---2
            //
            //  p0 = T + s0.x * vup + s0.y * vright

            this.device = device;

            if (INDEXED)
            {
                geometry = new BillBoardVertex[]{
                    new BillBoardVertex( 1,-0.5f, 0,0),
                    new BillBoardVertex( 1, 0.5f, 1,0),
                    new BillBoardVertex( 0, 0.5f, 1,1),
                    new BillBoardVertex( 0,-0.5f, 0,1)};
            }
            else
            {
                geometry = new BillBoardVertex[]{
                    new BillBoardVertex( 0.5f,-0.5f, 0,0),
                    new BillBoardVertex( 0.5f, 0.5f, 1,0),
                    new BillBoardVertex(-0.5f, 0.5f, 1,1),

                    new BillBoardVertex( 0.5f,-0.5f, 0,0),
                    new BillBoardVertex(-0.5f, 0.5f, 1,1),
                    new BillBoardVertex(-0.5f,-0.5f, 0,1)};
            }


            if (INSTANCING)
            {
                List<BillBoardInstance> list = new List<BillBoardInstance>();

                int n = 1;
                for (int x = -n; x <= n; x += 1)
                    for (int y = -n; y <= n; y += 1)
                        for (int z = -n; z <= n; z += 1)
                            list.Add(new BillBoardInstance(x * rnd.Next(50, 100) / 100.0f, y, z * rnd.Next(50, 100) / 100.0f, Color.White, (float)rnd.NextDouble() * 2 - 1, (float)rnd.NextDouble() * 2 - 1));

                Console.WriteLine("Num Billboard : " + list.Count);

                /*
                list.Clear();
                for (int i = 0; i < 1000; i++)
                    list.Add(new BillBoardInstance(
                        ((float)rnd.NextDouble() - 0.5f) * 8,
                        (float)rnd.NextDouble(),
                        ((float)rnd.NextDouble() - 0.5f) * 8,
                        Color.White));
                */


                instance = list.ToArray();

                numinstances = instance.Length;
                for (int i = 0; i < numinstances; i++)
                {
                    float f = (float)i / numinstances;
                    instance[i].color = new Color32(f, 1 - f, f);
                }
            }
            else
            {
                instance = new BillBoardInstance[6];
                instance[0].color = Color.Red;
                instance[1].color = Color.Green;
                instance[2].color = Color.Blue;

                instance[3].color = Color.Red;
                instance[4].color = Color.Blue;
                instance[5].color = Color.White;
            }

            List<VertexElement> e_geometry = new List<VertexElement>();
            List<VertexElement> e_traslation = new List<VertexElement>();

            int c = 4;
            int f2 = 8;
            int f3 = 12;
            e_geometry.Add(new VertexElement(0, 0, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0));
            e_geometry.Add(new VertexElement(0,f2, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TexCoord, 1));

            e_traslation.Add(new VertexElement(1,   0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            e_traslation.Add(new VertexElement(1,  f3, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));
            e_traslation.Add(new VertexElement(1,f3+c, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TexCoord, 2));


            d_geometry = new VertexInfo(e_geometry);
            d_instance = new VertexInfo(e_traslation);


            List<VertexElement> e_global = new List<VertexElement>(e_geometry);
            e_global.AddRange(e_traslation);
            instance_decl = new VertexDeclaration(device, e_global);

            gVB = new VertexBuffer(device, d_geometry, geometry.Length, false, true);
            iVB = new VertexBuffer(device, d_instance, instance.Length, false, true);
            //gVB = new VertexBuffer(typeof(BillBoardVertex), d_geometry, geometry.Length, false);
            //iVB = new VertexBuffer(typeof(VERTEX), d_instance, positions.Length, false);

            gVB.StreamID = 0;
            iVB.StreamID = 1;

            VertexStream gData = gVB.OpenStream();
            gData.WriteCollection<BillBoardVertex>(geometry, 0, geometry.Length, 0);
            gVB.CloseStream();
            gVB.Count = geometry.Length;

            VertexStream pData = iVB.OpenStream();
            pData.WriteCollection<BillBoardInstance>(instance, 0, instance.Length, 0);
            iVB.CloseStream();
            iVB.Count = instance.Length;

            if (INDEXED)
            {
                iB = new IndexBuffer(device, IndexInfo.IndexOne16, 6, false, true);
                IndexStream iData = iB.OpenStream();
                iData.WriteCollection<ushort>(new ushort[] { 0, 1, 2, 0, 2, 3 }, 0, 6, 0);
                iB.CloseStream();
                iB.Count = 6;
            }
            else
            {
                iB = new IndexBuffer(device, IndexInfo.IndexOne16, 6, false, true);
                IndexStream iData = iB.OpenStream();
                iData.WriteCollection<ushort>(new ushort[] { 0, 1, 2, 3, 4, 5 }, 0, 6, 0);
                iB.CloseStream();
                iB.Count = 6;
            }

            if (System.IO.File.Exists(@"..\..\treealpha.png"))
            {
                Bitmap bmp = new Bitmap(@"..\..\treealpha.png");
                diffusemap = new Texture(device, bmp, true, 0);
            }
            else
            {
                diffusemap = Texture.Debug(device, true, 0);
            }

            this.material = new MaterialFX_BILLBOARD(device);
            this.material.Method = method;
            this.material.AllowedDir = Vector3.UnitY;
            this.material.DiffuseMap = diffusemap;
        }


        float angle = 0;

        public void Draw()
        {
            material.WindXZ = new Vector2(
                (Math.Sin(angle) * 0.01 + Math.Cos(angle) * 0.005),
                (Math.Cos(angle) * 0.01 + Math.Sin(angle) * 0.005));
            angle += 0.01f;

            int numpass = material.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                material.BeginPass(pass);


                if (INDEXED)
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 6, 0, 2);
                else
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
                    //device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);

                material.EndPass();
            }
            material.End();

            if (INSTANCING)
            {
                gVB.SetFrequency(1, StreamSource.Reset);
                iVB.SetFrequency(1, StreamSource.Reset);
            }
            device.renderstates.ZBufferEnable = true;
            device.renderstates.ZBufferWriteEnable = true;
            
            device.renderstates.AlphaBlendEnable = false;
            device.renderstates.AlphaTestEnable = false; 
        }

        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }

        public void SetToDevice()
        {
            instance_decl.SetToDevice();

            if (INSTANCING)
            {
                if (INDEXED)
                {
                    gVB.SetFrequency(numinstances, StreamSource.IndexedData);
                    iVB.SetFrequency(1, StreamSource.InstanceData);
                }
                else
                {
                    gVB.SetFrequency(numinstances, StreamSource.IndexedData);
                    iVB.SetFrequency(1, StreamSource.InstanceData);
                    //gVB.SetFrequency(0, StreamSource.None);
                    //iVB.SetFrequency(6, StreamSource.None);
                }
            }
            gVB.SetToDevice();
            iVB.SetToDevice();
            
           // if (INDEXED)
                iB.SetToDevice();
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View;
            material.Eye = camera.Eye;
            material.Targhet = camera.Targhet;
        }

        public Matrix4 transform
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }

    public class MeshObj : IDrawable
    {
        MaterialFX_POSITION material;
        VertexInfo d_geometry;
        VertexDeclaration instance_decl;
        GraphicDevice device;
        VertexBuffer gVB;
        IndexBuffer iB;

        public MeshObj(GraphicDevice device)
        {
            this.material = new MaterialFX_POSITION(device);
            this.material.SolidWireframe = true;
            this.material.WireframeColor = Color.Black;
            this.material.Diffuse = Color.Blue;

            this.device = device;

            MeshListGeometry obj = BaseTriGeometry.CubeOpen();

            d_geometry = new VertexInfo(NTVERTEX.m_elements);
            instance_decl = new VertexDeclaration(device, NTVERTEX.m_elements);
            
            int nverts = obj.vertices.Count;
            NTVERTEX[] vertices = new NTVERTEX[nverts];
            
            for (int i = 0; i < nverts; i++)
            {
                vertices[i] = new NTVERTEX();
                vertices[i].m_pos = obj.vertices[i] + new Vector3(0, 0, 0);
                if (obj.texcoords.Count == nverts) vertices[i].m_uv = obj.texcoords[i];
                if (obj.normals.Count == nverts) vertices[i].m_norm = obj.normals[i];
            }

            gVB = new VertexBuffer(device, d_geometry, nverts, false, true);
            gVB.StreamID = 0;

            VertexStream gData = gVB.OpenStream();
            gData.WriteCollection<NTVERTEX>(vertices, 0, nverts, 0);
            gVB.CloseStream();
            gVB.Count = nverts;

            Face16[] faces = obj.indices.data;
            iB = new IndexBuffer(device, IndexInfo.IndexOne16, faces.Length * 3, false, true);
            IndexStream iData = iB.OpenStream();
            iData.WriteCollection<Face16>(faces, 0, faces.Length, 0);
            iB.CloseStream();
            iB.Count = faces.Length * 3;
        }

        public void Draw()
        {
            int numpass = material.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                material.BeginPass(pass);
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, gVB.Count, 0, iB.Count / 3);
                material.EndPass();
            }
            material.End();
        }

        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }

        public void SetToDevice()
        {
            instance_decl.SetToDevice();
            gVB.SetToDevice();
            iB.SetToDevice();
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View;
            material.Diffuse = Color.Blue;
            material.ApplyParams();
        }
        
        public Matrix4 transform
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

    }

    public class InstanceObj : IDrawable
    {
        bool INSTANCE = true;
        MaterialFX_INSTANCING material;

        int numinstances = 0;
        MeshListGeometry billboard = BaseTriGeometry.Billboard(eAxis.X);
        VertexInfo d_geometry, d_instance;
        VertexDeclaration instance_decl;
        GraphicDevice device;

        VertexBuffer gVB, iVB;
        IndexBuffer iB;

        public int NumTotalVertices
        {
            get { return billboard.numVertices * numinstances; }
        }


        public InstanceObj(GraphicDevice device, int numinstances)
        {
            this.material = new MaterialFX_INSTANCING(device);
            this.device = device;
            this.numinstances = numinstances;

            Vector3[] positions = null;

            if (INSTANCE)
            {
                positions = new Vector3[numinstances];
                int i = 0;
                for (float x = 0; x < 100 && i < numinstances; x += 2)
                    for (float y = 0; y < 100 && i < numinstances; y += 2)
                        for (float z = 0; z < 100 && i < numinstances; z += 2)
                            positions[i++] = new Vector3(x * 0.25f, y * 0.25f, z * 0.25f);
            }
            else
            {
                positions = new Vector3[billboard.numVertices];
                for (int i = 0; i < billboard.numVertices; i++)
                    positions[i] = new Vector3(0, 0, 0);
            }

            List<VertexElement> e_geometry = new List<VertexElement>();
            List<VertexElement> e_traslation = new List<VertexElement>();
            e_geometry.Add(new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            e_geometry.Add(new VertexElement(0, 12, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0));
            e_traslation.Add(new VertexElement(1, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.TexCoord, 0));

            d_geometry = new VertexInfo(e_geometry);
            d_instance = new VertexInfo(e_traslation);


            List<VertexElement> e_global = new List<VertexElement>(e_geometry);
            e_global.AddRange(e_traslation);
            instance_decl = new VertexDeclaration(device, e_global);

            gVB = new VertexBuffer(device, d_geometry, billboard.numVertices, false, true);
            iVB = new VertexBuffer(device, d_instance, positions.Length, false, true);
            //gVB = new VertexBuffer(typeof(CVERTEX), d_geometry, billboard.numVertices, false);
            //iVB = new VertexBuffer(typeof(VERTEX), d_instance, positions.Length, false);
            
            gVB.StreamID = 0;
            iVB.StreamID = 1;

            VertexElement element;

            VertexStream gData = gVB.OpenStream();
            if (!instance_decl.GetElement(DeclarationUsage.Position, 0, out element))
                throw new ArgumentException("Notfound");
            gData.WriteCollection<Vector3>(billboard.vertices.data, element, 0, billboard.numVertices, 0);
            if (!instance_decl.GetElement(DeclarationUsage.Color, 0, out element))
                throw new ArgumentException("Notfound");
            gData.WriteCollection<Color32>(billboard.colors.data, element, 0, billboard.numVertices, 0);
            gVB.CloseStream();
            gVB.Count = billboard.numVertices;

            VertexStream pData = iVB.OpenStream();
            pData.WriteCollection<Vector3>(positions, 0, positions.Length, 0);
            iVB.CloseStream();
            iVB.Count = positions.Length;


            iB = new IndexBuffer(device, IndexInfo.IndexOne16, billboard.numIndices, false, true);
            //iB = new IndexBuffer(IndexInfo.IndexOne16, billboard.numIndices, false);
            IndexStream iData = iB.OpenStream();
            iData.WriteCollection<Face16>(billboard.indices.data, 0, billboard.numIndices / 3, 0);
            iB.CloseStream();
            iB.Count = billboard.numIndices;
        }


        public void Draw()
        {
            if (INSTANCE)
            {
                gVB.SetFrequency(numinstances, StreamSource.IndexedData);
                iVB.SetFrequency(1, StreamSource.InstanceData);
            }
            int numpass = material.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                material.BeginPass(pass);
                device.DrawIndexedPrimitives(billboard.primitive, 0, 0, billboard.numVertices, 0, billboard.numPrimitives);
                material.EndPass();
            }
            material.End();

            if (INSTANCE)
            {
                gVB.SetFrequency(1, StreamSource.Reset);
                iVB.SetFrequency(1, StreamSource.Reset);
            }
        }

        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }

        public void SetToDevice()
        {
            instance_decl.SetToDevice();
            gVB.SetToDevice();
            iVB.SetToDevice();
            iB.SetToDevice();
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View; 
        }
        
        public Matrix4 transform
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
    
    public class AxisObj : IDrawable
    {
        MaterialFX_VCOLOR material;
        GraphicDevice device;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public AxisObj(GraphicDevice device)
        {
            this.material = new MaterialFX_VCOLOR(device);
            this.device = device;
            this.cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(10,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,10,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,10, Color.Blue)
            };
            axis = new VertexBuffer(device, cvertex_decl.Format, verts.Length, false, true);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public Matrix4 transform
        {
            get { return Matrix4.Identity; }
            set { }
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View;
        }

        public void Draw()
        {
            int count = material.Begin();
            for (int pass = 0; pass < count; pass++)
            {
                material.BeginPass(pass);
                device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                material.EndPass();
            }
            material.End();
        }

        public void Draw(MaterialBase material)
        {
            throw new NotImplementedException();
        }
        public void SetToDevice()
        {
            cvertex_decl.SetToDevice();
            axis.SetToDevice();
        }
        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }
    }
}
