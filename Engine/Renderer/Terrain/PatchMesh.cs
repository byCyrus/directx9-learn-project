﻿// by johnwhile
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Renderer.Terrain
{
    internal sealed class PatchMesh : IDisposable
    {
        public int GridSize { get; private set; }
        public int NumVertices { get; private set; }
        public int PrimitiveCount { get; private set; }

        public DxVertexBuffer VertexBuffer { get; private set; }
        public DxIndexBuffer IndexBuffer { get; private set; }
        
        public int TopLeftEndIndex { get; private set; }
        public int TopRightEndIndex { get; private set; }
        public int BottomLeftEndIndex { get; private set; }
        public int BottomRightEndIndex { get; private set; }

        public PatchMesh(GraphicDevice graphicsDevice, int gridSize)
        {
            // GridSize must be leafNodeSize * n, and n must be power of 2.
            GridSize = gridSize;

            int vertexSize = gridSize + 1;

            NumVertices = vertexSize * vertexSize;
            VertexBuffer = new DxVertexBuffer(graphicsDevice.m_device, VERTEX.Declaration, NumVertices, BufferUsage.StaticWriteOnly, true);

            VERTEX[] vertices = new VERTEX[NumVertices];

            for (int z = 0; z < vertexSize; z++)
                for (int x = 0; x < vertexSize; x++)
                    vertices[vertexSize * z + x] = new VERTEX(x / (float)gridSize, 0f, z / (float)gridSize);

            VertexBuffer.SetData<VERTEX>(vertices, 0);

            PrimitiveCount = gridSize * gridSize * 2;
            int indexCount = PrimitiveCount * 3;
            IndexBuffer = new DxIndexBuffer(graphicsDevice.m_device, false, indexCount, BufferUsage.StaticWriteOnly, true);

            ushort[] indices = new ushort[indexCount];
            int index = 0;
            int halfSize = gridSize / 2;

            // Top left.
            for (int z = 0; z < halfSize; z++)
            {
                for (int x = 0; x < halfSize; x++)
                {
                    indices[index++] = (ushort) ((x + 0) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);

                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 1) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);
                }
            }
            TopLeftEndIndex = index;

            // Top right.
            for (int z = 0; z < halfSize; z++)
            {
                for (int x = halfSize; x < gridSize; x++)
                {
                    indices[index++] = (ushort) ((x + 0) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);

                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 1) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);
                }
            }
            TopRightEndIndex = index;

            // Bottom left.
            for (int z = halfSize; z < gridSize; z++)
            {
                for (int x = 0; x < halfSize; x++)
                {
                    indices[index++] = (ushort) ((x + 0) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);

                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 1) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);
                }
            }
            BottomLeftEndIndex = index;

            // Bottom right.
            for (int z = halfSize; z < gridSize; z++)
            {
                for (int x = halfSize; x < gridSize; x++)
                {
                    indices[index++] = (ushort) ((x + 0) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);

                    indices[index++] = (ushort) ((x + 1) + (z + 0) * vertexSize);
                    indices[index++] = (ushort) ((x + 1) + (z + 1) * vertexSize);
                    indices[index++] = (ushort) ((x + 0) + (z + 1) * vertexSize);
                }
            }
            BottomRightEndIndex = index;

            IndexBuffer.SetData<ushort>(indices, 0);
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        bool disposed;

        ~PatchMesh()
        {
            Dispose(false);
        }

        void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                VertexBuffer.Dispose();
                IndexBuffer.Dispose();
            }

            disposed = true;
        }

        #endregion
    }
}
