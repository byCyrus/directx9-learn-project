﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;

namespace Engine.Graphics
{
    public abstract class EffectAttribute
    {
        protected EffectAttribute(string shadername)
        {
            this.handle = new DX9EffectHandle(shadername);
            this.shadername = shadername;
            this.isSet = false;
        }
        protected DX9EffectHandle handle { get; set; }
        /// <summary>
        /// Get of set the value, but not passed to effect
        /// </summary>
        internal abstract object m_value { get; set; }
        /// <summary>
        /// name in shader code of this value
        /// </summary>
        internal string shadername { get; set; }
        /// <summary>
        /// Get or set if value are passed to effect
        /// </summary>
        public bool isSet { get; protected set; }
        /// <summary>
        /// Pass value ot effect
        /// </summary>
        internal virtual void SetParam(Effect effect)
        {
            //Console.WriteLine("Set " + shadername);
            isSet = true;
        }
    }

    public abstract class EffectAttribute<T> : EffectAttribute
    {
        internal override object m_value
        {
            get { return Value; }
            set { Value = (T)value; isSet = false; }
        }
        public abstract T Value { get; set; }
        
        /// <summary>
        /// </summary>
        /// <param name="value">data to pass to shader code</param>
        /// <param name="name">shader value name</param>
        public EffectAttribute(T value, string shadername)
            : base(shadername) { }

        public EffectAttribute(string shadername)
            : this(default(T), shadername) { }


        public override string ToString()
        {
            return string.Format("type: {0} , shader's name: {1} , value: {2}",typeof(T), shadername, Value);
        }
    }
    public class EffectAttributeFloat : EffectAttribute<float>
    {
        public override float Value { get; set; }

        public EffectAttributeFloat(float value, string shadername)
            : base(value, shadername) { }

        internal override void SetParam(Effect effect)
        {
            base.SetParam(effect);
            effect.m_effect.SetValue(base.handle, Value);
        }
    }
    public class EffectAttributeVector2 : EffectAttribute<Vector2>
    {
        public override Vector2 Value { get; set; }

        public EffectAttributeVector2(Vector2 value, string shadername)
            : base(value, shadername) { }
        public EffectAttributeVector2(string shadername)
            : base(shadername) { }

        internal override void SetParam(Effect effect)
        {
            base.SetParam(effect);
            effect.m_effect.SetValue(base.handle, Value);
        }
    }
    public class EffectAttributeVector3 : EffectAttribute<Vector3>
    {
        public override Vector3 Value { get; set; }

        public EffectAttributeVector3(Vector3 value, string shadername)
            : base(value, shadername) { }
        public EffectAttributeVector3(string shadername)
            : base(shadername) { }

        internal override void SetParam(Effect effect)
        {
            base.SetParam(effect);
            effect.m_effect.SetValue(base.handle, Value);
        }
    }
    public class EffectAttributeVector4 : EffectAttribute<Vector4>
    {
        public override Vector4 Value { get; set; }

        public EffectAttributeVector4(Vector4 value, string shadername)
            : base(value, shadername) { }

        public EffectAttributeVector4(string shadername)
            : base(shadername) { }

        internal override void SetParam(Effect effect)
        {
            base.SetParam(effect);
            effect.m_effect.SetValue(base.handle, Value);
        }
    }   
    /// <summary>
    /// For simplicity, can use both System.Drawing.Color and Color32
    /// </summary>
    public class EffectAttributeColor : EffectAttribute<Color32>
    {
        internal override object m_value
        {
            get { return Value; }
            set { Value = (value is Color) ? (Color32)(Color)value : (Color32)value; }
        }
        public override Color32 Value { get; set; }

        public EffectAttributeColor(Color32 value, string shadername)
            : base(value, shadername) { }
        
        internal override void SetParam(Effect effect)
        {
            base.SetParam(effect);
            effect.m_effect.SetValue(base.handle, Value);
        }
    }
    public class EffectAttributeTexture : EffectAttribute<Texture>
    {
        public override Texture Value { get; set; }

        public EffectAttributeTexture(Texture value, string shadername)
            : base(value, shadername) { }

        internal override void SetParam(Effect effect)
        {
            base.SetParam(effect);
            effect.m_effect.SetValue(base.handle, Value.m_texture);
        }
    }
    /// <summary>
    /// </summary>
    /// <remarks>
    /// The WorldViewProj in math notation
    /// using "_dx" for directx notation and "_m" for math notation, this is the conversion :
    /// World_dx * View_dx * Proj_dx  = World_m^T * View_m^T * Proj_m^T = (Proj_m * View_m * World_m)^T
    /// when you pass PVW_m as matrix, the Helper.Conver(Matrix4 m) do traspose so PVW_m^T == WVP_dx
    /// </remarks>
    public class EffectAttributeMatrix : EffectAttribute<Matrix4>
    {
        public override Matrix4 Value { get; set; }

        public EffectAttributeMatrix(Matrix4 value, string shadername)
            : base(value, shadername) { }

        public EffectAttributeMatrix(string shadername)
            : base(shadername) { }
        

        internal override void SetParam(Effect effect)
        {
            base.SetParam(effect);
            effect.m_effect.SetValue(base.handle, Value);
        }
    }
    
    /// <summary>
    /// Each effect must contain only ONE techique
    /// </summary>
    public class EffectAttributeTechnique : EffectAttribute<String>
    {
        internal override object m_value
        {
            get { return Value; }
            set { Value = (string)value;}
        }
        public override string Value 
        {
            get { return shadername; }
            set
            {       
                this.handle = new DX9EffectHandle(value);
                this.shadername = value; 
                this.isSet = false;
            }
        }

        public EffectAttributeTechnique(string shadername)
            : base(shadername) { }

        internal override void SetParam(Effect effect)
        {
            if (effect.started) throw new Exception("Can't change technique after effect Begin()... need to call End()");
            base.SetParam(effect);
            effect.m_effect.SetTechnique(base.handle);
        }
    }

    /// <summary>
    /// The render shader programm
    /// </summary>
    public class Effect : DisposableResource, IResettable
    {
        GraphicDevice m_device;
        internal DX9Effect m_effect;
        string m_code;
        internal bool started = false;
        bool stardedPass = false;

        /// <summary>
        /// Add a new parameter, the key is your custom name
        /// </summary>
        public EffectParameterCollector Parameters;

        /// <summary>
        /// </summary>
        /// <param name="device"></param>
        /// <param name="shadercode"></param>
        public Effect(GraphicDevice device, string shadercode)
        {
            Parameters = new EffectParameterCollector(this);
            m_device = device;
            m_code = shadercode;
            started = stardedPass = false;
            Restore();

            m_device.resourcesToDispose.Add(this);
            m_device.resourcesToRestore.Add(this);
        }

        ~Effect()
        {
            m_device.resourcesToDispose.Remove(this);
            m_device.resourcesToRestore.Remove(this);
        }

        public void Commit()
        {
            m_effect.CommitChanges();
        }
        public int Begin()
        {
            if (Parameters.needUpdate) Parameters.UpdateParams(false);
            started = true;
            return m_effect.Begin(false);
        }
        public void End()
        {
            started = stardedPass = false;
            m_effect.End();
        }
        public void BeginPass(int i)
        {
            stardedPass = true;
            m_effect.BeginPass(i);
        }
        public void EndPass()
        {
            stardedPass = false;
            m_effect.EndPass();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed) m_effect.Dispose();
            disposed = true;
        }

        public void Restore()
        {
            m_effect = new DX9Effect(m_device.m_device, m_code);
            Parameters.UpdateParams(true);
            disposed = false;
        }

        /// <summary>
        /// Manage with a dictionary all paramters what will be used by current effect. Directx9 HLSL don't provide a
        /// function to check if shader's name exist, so is a good idea initialize all possible EffectAttribute collection
        /// when you compile your custum Shader code
        /// </summary>
        public class EffectParameterCollector : IEnumerable<EffectAttribute>
        {
            Dictionary<string, string> shadernames;
            Dictionary<string, EffectAttribute> dictionary;
            Effect effect;
            public bool needUpdate;

            internal EffectParameterCollector(Effect owner)
                : base()
            {
                effect = owner;
                shadernames = new Dictionary<string, string>();
                dictionary = new Dictionary<string, EffectAttribute>();
                needUpdate = false;
            }

            /// <summary>
            /// Add a new Parameter to list
            /// </summary>
            public void Add(string ParamName, EffectAttribute Param)
            {
                if (dictionary.ContainsKey(ParamName))
                    throw new ArgumentException("Parameter name already used");
                if (shadernames.ContainsKey(Param.shadername))
                    throw new ArgumentException("Parameter's shader name already used");

                dictionary.Add(ParamName, Param);
                shadernames.Add(Param.shadername, ParamName);
                needUpdate = true;
            }
            
            /// <summary>
            /// Get or Set a temporanery value. Need to call UpdateParams to commit changes in the effect
            /// ATTENTION : in c# need to pass exactly the same generic value, exept System.Drawing.Color-Engine.Math.Color32
            /// all other implicit cast conversion generate an InvalidCastExeption
            /// </summary>
            public object this[string ParamName]
            {
                get { return this[ParamName, true]; }
                set { this[ParamName, true] = value; }
            }
            
            /// <summary>
            /// </summary>
            /// <param name="commit">if "true": call Commit()</param>
            /// <remarks>
            /// <code>
            /// int count = effect.Begin();
            /// for (int pass = 0; pass &lt; count; pass++)
            /// {
            ///     effect.BeginPass(pass);
            ///     effect.Parameters["ParamName1", false] = value1;
            ///     effect.Parameters["ParamName2", false] = value2;
            ///     effect.Parameters["ParamName3", true] = value3;  
            ///     device.Draw();
            ///     effect.EndPass();
            /// }
            /// effect.End();
            /// </code>
            /// </remarks>
            public object this[string ParamName, bool commit]
            {
                get
                {
                    if (!dictionary.ContainsKey(ParamName))
                        throw new ArgumentNullException("Parameter " + ParamName + " not in list");
                    return dictionary[ParamName].m_value;
                }
                set
                {
                    EffectAttribute attribute= dictionary[ParamName];
                    // assign to temporaney value
                    attribute.m_value = value;
               
                    if (effect.started)
                    { 
                        // if effect is started, set param now and commit() ...
                        attribute.SetParam(effect);
                        if (commit) effect.Commit();
                    }
                    else
                    {
                        // ... else set when effect begin()
                        needUpdate = true;
                    }
                }
            }
            /// <summary>
            /// Update not set parameters, force to set all
            /// </summary>
            public void UpdateParams(bool force)
            {
                needUpdate = false;
                foreach (EffectAttribute value in this)
                {
                    if (force || !value.isSet)
                    {
                        value.SetParam(effect);
                        needUpdate = true;
                    }
                }
                if (needUpdate && effect.started) effect.Commit();
                needUpdate = false;
            }

            public IEnumerator<EffectAttribute> GetEnumerator()
            {
                return dictionary.Values.GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }
        }

    }
}
