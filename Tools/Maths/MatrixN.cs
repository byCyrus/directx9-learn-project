﻿using System;
using System.Diagnostics;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Tools;
using Engine.Graphics;

namespace Engine.Maths
{
    /// <summary>
    /// Generic matrix, slow for directx performance, used only to test math
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    [DebuggerDisplay("M{r}x{c} {this.ToString()}")]
    public struct MatrixN
    {
        /// <summary>
        /// number of rows
        /// i : 0 -> r
        /// </summary>
        int r;
        /// <summary>
        /// number of colums
        /// j : 0 -> c
        /// </summary> 
        int c;
        /// <summary>
        /// field
        /// </summary>
        float[,] m;

        static float abs(float f) { return f > 0 ? f : -f; }

        public MatrixN(Matrix4 matrix)
        {
            r = 4;
            c = 4;
            m = new float[,] {
               { matrix.m00, matrix.m01, matrix.m02, matrix.m03 },
               { matrix.m10, matrix.m11, matrix.m12, matrix.m13 },
               { matrix.m20, matrix.m21, matrix.m22, matrix.m23 },
               { matrix.m30, matrix.m31, matrix.m32, matrix.m33 }};
        }

        public MatrixN(int i, int j)
        {
            if (i == 0 || j == 0) throw new ArgumentException("dimensione della mantrice non può essere zero");
            r = i;
            c = j;
            m = new float[r, c];
        }
        
        public int Rows { get { return r; } }
        public int Columns { get { return c; } }
        public float[] getRow(int i)
        {
            float[] vect = new float[c];
            for (int j = 0; j < c; j++) vect[j] = m[i, j];
            return vect;
        }
        public float[] getCol(int j)
        {
            float[] vect = new float[r];
            for (int i = 0; i < r; i++) vect[i] = m[i, j];
            return vect;
        }
        public void setRow(int i,float[] vect)
        {
            for (int j = 0; j < c; j++)  m[i, j] = vect[j];
        }
        public void setCol(int j, float[] vect)
        {
            for (int i = 0; i < r; i++)  m[i, j] = vect[i];
        }

        public float this[int i, int j]
        {
            get { return m[i, j]; }
            set { m[i, j] = value; }
        }

        /// <summary>
        /// I
        /// </summary>
        public static MatrixN Identity(int r, int c)
        {
            MatrixN I = new MatrixN(r, c);
            for (int i = 0; i < Math.Min(r, c); i++) I.m[i, i] = 1;
            return I;
        }
        /// <summary>
        /// Tij = Mji
        /// </summary>
        public static MatrixN Traspose(MatrixN M)
        {
            MatrixN T = new MatrixN(M.c, M.r);
            for (int i = 0; i < T.r; i++)
                for (int j = 0; j < T.c; j++)
                    T.m[i, j] = M.m[j, i];
            return T;
        }
        /// <summary>
        /// A^(-1)
        /// </summary>
        public static MatrixN Inverse(MatrixN A)
        {
            if (A.r != A.c) throw new ArgumentException("Matrix not invertible");

            int n = A.r;
            //e will represent each column in the identity matrix
            float[] e;
            //x will hold the inverse matrix to be returned
            MatrixN x = new MatrixN(n, n);
        
            // solve will contain the vector solution for the LUP decomposition as we solve
            // for each vector of x.  We will combine the solutions into the double[][] array x.    
            float[] solve;

            //Get the LU matrix and P matrix (as an array)
            MatrixN LU; int[] P; int s;
            LUPDecomposition(A, out LU, out P,out s);

            // Solve AX = e for each column ei of the identity matrix using LUP decomposition
            for (int i = 0; i < n; i++)
            {
                e = new float[n];
                e[i] = 1;
                solve = LUPSolve(LU, P, e);
                for (int j = 0; j < solve.Length; j++)
                {
                    x[j,i] = solve[j];
                }
            }
            return x;
        }

        /// <summary>
        /// Determinant
        /// </summary>
        public static float Det(MatrixN A)
        {
            // det L = 1, 
            // det P = +1 or det P = -1 (-1)^swaps
            // det U = product of its diagonal elements. 
            MatrixN LU; int[] P; int s;
            MatrixN.LUPDecomposition(A, out LU, out P,out s);

            float det = -s;
            for (int i = 0; i < LU.r; i++) det *= LU.m[i, i];
            return det;
        }

        /// <summary>
        /// http://www.rkinteractive.com/blogs/SoftwareDevelopment/post/2013/05/07/Algorithms-In-C-LUP-Decomposition.aspx
        /// In LUP decomposition we want to find three n x n matrices L, U, and P such that PA = LU 
        /// where
        /// L is a unit lower-triangular matrix
        /// U is an upper-trangular matrix
        /// P is a permutation matrix
        /// </summary>
        /// <remarks>
        /// L * U = A
        /// </remarks>
        static void LUPDecomposition(MatrixN A, out MatrixN LU, out int[] P, out int swaps)
        {
            int n = A.r;
            // pi represents the permutation matrix.  We implement it as an array
            // whose value indicates which column the 1 would appear.  We use it to avoid 
            // dividing by zero or small numbers.
            int[] pi = new int[n];
            swaps = 1;
            int kp = 0;
            int pik = 0;
            int pikp = 0;
            float aki = 0;
            float akpi = 0;

            //Initialize the permutation matrix, will be the identity matrix
            for (int i = 0; i < n; i++) pi[i] = i;
    
            for (int k = 0; k < n; k++)
            {
                // In finding the permutation matrix p that avoids dividing by zero we take a slightly different approach.
                // For numerical stability we find the element with the largest absolute value of those in the current first column (column k).
                // If all elements in the current first column are zero then the matrix is singluar and throw an error.
                float maxf = 0;
                for (int i = k; i < n; i++)
                {
                    float abs = Math.Abs(A[i,k]);
                    if (abs > maxf) { maxf = abs; kp = i; }
                }
                if (maxf == 0) throw new Exception("singular matrix");

                if (k != kp) swaps *= -1;

                // These lines update the pivot array (which represents the pivot matrix)
                // by exchanging pi[k] and pi[kp].
                pik = pi[k];
                pikp = pi[kp];
                pi[k] = pikp;
                pi[kp] = pik;

                // Exchange rows k and kpi as determined by the pivot
                for (int i = 0; i < n; i++)
                {
                    aki = A[k,i];
                    akpi = A[kp,i];
                    A[k,i] = akpi;
                    A[kp,i] = aki;
                }

                // Compute the Schur complement
                for (int i = k + 1; i < n; i++)
                {
                    A[i,k] = A[i,k] / A[k,k];
                    for (int j = k + 1; j < n; j++)
                        A[i,j] = A[i,j] - (A[i,k] * A[k,j]);
                }
            }
            P = pi;
            LU = A;
        }

        /// <summary>
        /// Solve Ax = b where LUP = A
        /// </summary>
        static float[] LUPSolve(MatrixN LU, int[] pi, float[] b)
        {
            /* Given L,U,P and b solve for x.
            * Input the L and U matrices as a single matrix LU.
            * Return the solution as a double[].
            * LU will be a n+1xm+1 matrix where the first row and columns are zero.
            * This is for ease of computation and consistency with Cormen et al.
            * pseudocode.
            * The pi array represents the permutation matrix.
            */
            int n = LU.r;

            float[] x = new float[n];
            float[] y = new float[n];
            float suml = 0;
            float sumu = 0;
            float lij = 0;

            /*
            * Solve for y using formward substitution
            * */
            for (int i = 0; i < n; i++)
            {
                suml = 0;
                for (int j = 0; j <= i - 1; j++)
                {
                    /*
                    * Since we've taken L and U as a singular matrix as an input
                    * the value for L at index i and j will be 1 when i equals j, not LU[i][j], since
                    * the diagonal values are all 1 for L.
                    * */
                    if (i == j)
                    {
                        lij = 1;
                    }
                    else
                    {
                        lij = LU[i,j];
                    }
                    suml = suml + (lij * y[j]);
                }
                y[i] = b[pi[i]] - suml;
            }
            //Solve for x by using back substitution
            for (int i = n-1; i >= 0; i--)
            {
                sumu = 0;
                for (int j = i + 1; j < n; j++)
                    sumu = sumu + (LU[i,j] * x[j]);

                x[i] = (y[i] - sumu) / LU[i,i];
            }
            return x;
        }


        #region operator overload
        /// <summary>
        /// Mij = Sum[k:0->p] ( Aik * Bkj )
        /// where Aip, Bpj
        /// </summary>
        public static MatrixN operator *(MatrixN A, MatrixN B)
        {
            int p = A.c;
            if (p != B.r) throw new ArgumentException(string.Format("incompatible A[{0},{1}] x B[{1},{2}]", A.r, A.c, B.r, B.c));

            MatrixN M = new MatrixN(A.r, B.c);

            for (int i = 0; i < M.r; i++)
                for (int j = 0; j < M.c; j++)
                {
                    float sum = 0;
                    for (int k = 0; k < p; k++) sum += A.m[i, k] * B.m[k, j];
                    M.m[i, j] = sum;
                }

            return M;
        }
        public static MatrixN operator *(MatrixN A, float scalar)
        {
            MatrixN M = new MatrixN(A.r, A.c);
            for (int i = 0; i < M.r; i++)
                for (int j = 0; j < M.c; j++)
                    M.m[i, j] = A.m[i, j] * scalar;
            return M;
        }
        public static MatrixN operator +(MatrixN A, MatrixN B)
        {
            if (A.r != B.r || A.c != B.c) throw new ArgumentException(string.Format("incompatible A[{0},{1}] x B[{1},{2}]", A.r, A.c, B.r, B.c));

            MatrixN M = new MatrixN(A.r, A.c);
            for (int i = 0; i < M.r; i++)
                for (int j = 0; j < M.c; j++)
                    M.m[i, j] = A.m[i, j] + B.m[i, j];
            return M;
        }
        public static MatrixN operator +(MatrixN A, float scalar)
        {
            MatrixN M = new MatrixN(A.r, A.c);
            for (int i = 0; i < M.r; i++)
                for (int j = 0; j < M.c; j++)
                    M.m[i, j] = A.m[i, j] + scalar;
            return M;
        }
        public static MatrixN operator -(MatrixN A, MatrixN B)
        {
            if (A.r != B.r || A.c != B.c) throw new ArgumentException(string.Format("incompatible A[{0},{1}] x B[{1},{2}]", A.r, A.c, B.r, B.c));
            MatrixN M = new MatrixN(A.r, A.c);
            for (int i = 0; i < M.r; i++)
                for (int j = 0; j < M.c; j++)
                    M.m[i, j] = A.m[i, j] - B.m[i, j];
            return M;
        }
        public static MatrixN operator -(MatrixN A, float scalar)
        {
            return A + (-scalar);
        }
        public static MatrixN operator -(MatrixN A)
        {
            return A * -1;
        }
        public static bool operator ==(MatrixN A, MatrixN B)
        {
            if (A.r != B.r || A.c != B.c) return false;
            for (int i = 0; i < A.r; i++)
                for (int j = 0; j < A.c; j++)
                    if (abs(A.m[i, j] - B.m[i, j]) > float.Epsilon) return false;
            return true;
        }
        public static bool operator !=(MatrixN A, MatrixN B)
        {
            return !(A == B);
        }
        
        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
        public override bool Equals(object obj)
        {
            return (this == (MatrixN)obj);
        }
        #endregion

        public static void Debug()
        {
            MatrixN A = new MatrixN(4, 4);
            A.setRow(0, new float[] { 2, 0, 2, 0.6f });
            A.setRow(1, new float[] { 3, 3, 4, -2 });
            A.setRow(2, new float[] { 5, 5, 4, 2 });
            A.setRow(3, new float[] { -1, -2, 3.4f, -1 });

            MatrixN B = MatrixN.Inverse(A);

            Matrix4 AA = new Matrix4(2, 0, 2, 0.6f, 3, 3, 4, -2, 5, 5, 4, 2, -1, -2, 3.4f, -1);
            Matrix4 BB = Matrix4.Inverse(AA);

            float detAA = AA.Determinant;
            float detA = MatrixN.Det(A);
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < r; i++)
                for (int j = 0; j < c; j++)
                    str.Append(string.Format("{0,2} ", m[i, j]));
            return str.ToString();
        }

    }
}
