﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Engine.Maths;
using Engine.Tools;
using Engine.Graphics;

using Vector3 = Engine.Maths.Vector3;
using Viewport = Engine.Graphics.Viewport;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

namespace Test___Maths
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector3 max = new Vector3(10,10,10);
            
            BoundaryAABB box = new BoundaryAABB(max, -max);

            float d;
            bool intersect = box.Intersect(0, 20, 0, -9, out d);


        }
    }
}
