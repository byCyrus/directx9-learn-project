﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Engine.Maths;
using Engine.Graphics;

namespace Engine.Roam
{
    public struct NodeData
    {
        public float variance, height;
        public override string ToString()
        {
            return string.Format("var: {0,2} hgt: {1,2}", variance, height);
        }
    }
    public class Patch
    {
        enum SIDE : byte
        {
            OUTSIDE = 0,
            INTERSECT = 1,
            INSIDE = 2,
        }

        public string name = "Patch.0.0";
       
        /// <summary>
        /// force split all triangles, variance limit = 0
        /// </summary>
        public bool splitall = false;
        /// <summary>
        /// force all triangles be visibles, frustum occlusion will be skip
        /// </summary>
        public bool showall = false;
        /// <summary>
        /// the precompute sqrt(2)/2;
        /// </summary>
        static readonly float sqrt2 = (float)Math.Sqrt(2);
        /// <summary>
        /// The box that contain the patch, the height are calculate in the ComputeVariance()
        /// </summary>
        public BoundaryAABB m_box;
        /// <summary>
        /// offset in world units of minx minz point of patch
        /// </summary>
        float m_xoffset, m_zoffset;
        /// <summary>
        /// Side in world units of this patch
        /// </summary>
        float m_xsize, m_zsize;
        /// <summary>
        /// terrain class
        /// </summary>
        Terrain m_terrain;
        /// <summary>
        /// the two base triangle
        /// </summary>
        public TriNode m_LBase, m_RBase;
        /// <summary>
        /// Conectivity
        /// </summary>
        public Patch m_Pup, m_Pdown, m_Pleft, m_Pright;

        /// <summary>
        /// diagonal lengh of triangle node, projected to xz plane (y=0), used to get the radius of cylinder.
        /// </summary>
        /// <remarks>
        /// left and right bynary tree is equal and if use squared patch the child hypo is moltiplied for sqrt(2)/2 factor
        /// hypo size = m_depth only if m_xsize = m_zsize
        /// </remarks>
        float[] hypo;
        /// <summary>
        /// Variance binary tree
        /// </summary>
        NodeData[] m_RVariance, m_LVariance;
        /// <summary>
        /// Current variance (pointer of m_RVariance or m_LVariance used)
        /// </summary>
        NodeData[] m_CVariance;
        /// <summary>
        /// Max depth of variance (is the level of binary tree)
        /// </summary>
        public int m_depth = 10;
        /// <summary>
        /// Limit variance in the LOD test
        /// </summary>
        public float lVariance = 0.2f;
        /// <summary>
        /// </summary>
        bool modified = false;
        /// <summary>
        /// number of triangle from tessellation
        /// </summary>
        public int tri_count { get; private set; }
        /// <summary>
        /// Vertex array
        /// </summary>
        public List<NCVERTEX> pvertices;

        Color patchColor;
        /// <summary>
        /// Indices array
        /// </summary>
        public List<Face> pindices;
        /// <summary>
        /// index of corners
        /// </summary>
        int itl, itr, idl, idr;
        /// <summary>
        /// Top left
        /// </summary>
        Vector3 m_tl { get { return new Vector3(m_xoffset, m_terrain.GetHeight(m_xoffset, m_zoffset + m_zsize), m_zoffset + m_zsize); } }
        /// <summary>
        /// Down left
        /// </summary>
        Vector3 m_dl { get { return new Vector3(m_xoffset, m_terrain.GetHeight(m_xoffset, m_zoffset), m_zoffset); } }
        /// <summary>
        /// Down right
        /// </summary>
        Vector3 m_dr { get { return new Vector3(m_xoffset + m_xsize, m_terrain.GetHeight(m_xoffset + m_xsize, m_zoffset), m_zoffset); } }
        /// <summary>
        /// Top right
        /// </summary>
        Vector3 m_tr { get { return new Vector3(m_xoffset + m_xsize, m_terrain.GetHeight(m_xoffset + m_zsize, m_zoffset + m_zsize), m_zoffset + m_zsize); } }

        float maxf(float f1, float f2)
        {
            if (f1 > f2) return f1;
            else return f2;
        }
        float maxf(float f1, float f2, float f3)
        {
            if (f1 > f2) return maxf(f1, f3);
            else return maxf(f2, f3);
        }
        
        /// <summary>
        /// A patch is a basic quad in relation of size of texture
        /// </summary>
        public Patch(Terrain terrain, ref List<NCVERTEX> verts, ref List<Face> indis, float xoffset, float zoffset, float xside, float zside)
        {
            patchColor = Color.FromArgb(Terrain.rnd.Next(int.MinValue, int.MaxValue));
            tri_count = 0;
            pvertices = verts;
            pindices = indis;

            m_terrain = terrain;
            m_xoffset = xoffset;
            m_zoffset = zoffset;
            m_xsize = xside;
            m_zsize = zside;

            // the y compenent will be compute in the variance recursion
            m_box = BoundaryAABB.GetBoundaryAABB(new Vector3[] { m_tl, m_dl, m_dr, m_tr });

            m_LBase = new TriNode("LBase");
            m_RBase = new TriNode("RBase");
            m_LBase.node = m_RBase.node = 1;

            m_Pup = m_Pdown = m_Pleft = m_Pright = null;

            // the number of node in the binary tree is 2^n, see the document
            m_RVariance = new NodeData[1 << m_depth];
            m_LVariance = new NodeData[1 << m_depth];
            
            // precompute lenght of radius for all level, work only if is squared (xsize=zsize)
            hypo = new float[m_depth];
            hypo[0] = (float)Math.Sqrt(m_xsize * m_xsize + m_zsize * m_zsize) * 0.5f;
            for (int i = 1; i < m_depth; i++)
                hypo[i] = hypo[i - 1] / sqrt2;

            m_RVariance.Initialize();
            m_LVariance.Initialize();
        }
        /// <summary>
        /// reset creates connectivity information for the 
        /// base children of the patch
        /// connect the patch children to the children of the neighbor patches
        /// specified by the m_Pup,m_Pdown,m_Pleft,m_Pright
        /// </summary>
        public void Reset()
        {
            //clear starting relations
            m_LBase.RNeighbor = m_LBase.LNeighbor = m_RBase.RNeighbor = m_RBase.LNeighbor = null;
            m_LBase.RChild = m_LBase.LChild = m_RBase.RChild = m_RBase.LChild = null;

            m_RBase.Parent = m_LBase.Parent = null;

            //attach both base children together
            m_LBase.BNeighbor = m_RBase;
            m_RBase.BNeighbor = m_LBase;

            //connect to neighbor patches..
            if (m_Pup != null)
                m_RBase.LNeighbor = m_Pup.m_LBase;

            if (m_Pdown != null)
                m_LBase.LNeighbor = m_Pdown.m_RBase;

            if (m_Pleft != null)
                m_LBase.RNeighbor = m_Pleft.m_RBase;

            if (m_Pright != null)
                m_RBase.RNeighbor = m_Pright.m_LBase;

            ClearFov();
            ClearIndices();
            ComputeVariance();
        }

        /// <summary>
        /// the variance of triangle is the sum o variance of all children
        /// </summary>
        /// <remarks>
        /// Left child recursion always before right
        /// if T [r,a,l] is triangle defined by vertices with "c" calculated center, the formula for children are
        /// T.Lchild [l,c,a] ; T.Rchild [a,c,r] 
        /// ("lca"-"acr")
        /// </remarks>
        void ComputeVariance()
        {
            // is a good idea calculate the boundary box of this patch using variance calculation
            // because the height is done by the depth and not by global map

            // run the left recursion
            m_CVariance = m_LVariance;
            m_CVariance[0].variance = RecurseComputeVariance(m_tl, m_dl, m_dr,1);

            // run the right recursion
            m_CVariance = m_RVariance;
            m_CVariance[0].variance = RecurseComputeVariance(m_dr, m_tr, m_tl, 1);

            m_LVariance[0].height = m_RVariance[0].height = maxf(m_LVariance[1].height, m_RVariance[1].height);
        }
        /// <summary>
        /// Recurse Conpute Variance.
        /// ensure to use always the [right->apex->left] order (conterclockwire)
        /// </summary>
        float RecurseComputeVariance(Vector3 right, Vector3 apex, Vector3 left, int node)
        {
            //                    left <- node1-> right
            //                 /                         \
            //       node3 (because node2+1)              node2 (because 0001b << 1 = 0010b = 2)
            //     /        \                                 /          \
            //  node7      node6 (0011b<<1=0110=6)       node5             node4 (because 0010b << 1 = 0100b = 4)
            //
            //   left and right child triangle respect the orientation, see left-apex-right orientation

            float var = 0;
            float ymax = 0;

            // get the real center point
            Vector3 center = new Vector3();
            center.x = (left.x + right.x) * 0.5f;
            center.z = (left.z + right.z) * 0.5f;
            center.y = m_terrain.GetHeight(center.x, center.z);
            

            // correct way to update bounding box
            if (m_box.max.y < center.y) m_box.max.y = center.y;
            if (m_box.min.y > center.y) m_box.min.y = center.y;

            // can't advance because we have reach the variance limit : node^2 < tree level
            bool isleaf = (node << 1) >= m_CVariance.Length;

            if (isleaf)
            {
                //not-childred variance
                var = 0;
                ymax = maxf(apex.y, left.y, right.y);
            }
            else
            {
                // binary tree function
                int nodeL = node << 1;
                int nodeR = nodeL + 1;

                // the variance is the sum of the recurse variance and current; "lca"-"acr"
                var = maxf(var, RecurseComputeVariance(left, center, apex, nodeL));
                var = maxf(var, RecurseComputeVariance(apex, center, right, nodeR));
                
                // the height of cylinder isn't the height of current triangle because can be split
                // and the occlusion test can return a smaller height. so is the maximum height of each children
                // this algorithm work only if cylinder use y=0 as bottom base center
                ymax = maxf(m_CVariance[nodeL].height, m_CVariance[nodeR].height);
            }

            // is the difference between linear interpolated height and the real map height
            var += Math.Abs(center.y - (left.y + right.y) * 0.5f);
            m_CVariance[node].variance = var;
            m_CVariance[node].height = ymax; // because for UpCylinder algorithm ymin=0 

            return var;
        }

        /// <summary>
        /// clear the FOV information in all triangles
        /// </summary>
        public void ClearFov()
        {
            m_LBase.ClearFov();
            m_RBase.ClearFov();
        }
        /// <summary>
        /// clear the indices of all triangles
        /// </summary>
        public void ClearIndices()
        {
            itl = itr = idl = idr = TriNode.NULLIDX;
            m_LBase.ClearIndices();
            m_RBase.ClearIndices();
        }
        /// <summary>
        /// Tessellates/updates the patch
        /// runs recurse tesselate function
        /// </summary>
        public void Tesselate()
        {
            //is patch visible ?
            //byte binf = (byte)FovInfo.Default;
            //Vector3 center = (m_tl + m_dr) * 0.5f;
            //SIDE test = MyFrustumCylindrerY(center, hypo[0], m_CVariance[0].height, ref binf);
            //visible = test != SIDE.OUTSIDE;

            //if (!visible) return;

            //run the left recursion
            m_CVariance = m_LVariance;
            RecurseTesselate(m_LBase, m_tl, m_dl, m_dr, 1, eFov.Default, 0);
            //run the right recursion
            m_CVariance = m_RVariance;
            RecurseTesselate(m_RBase, m_dr, m_tr, m_tl, 1, eFov.Default, 0);
        }

        /// <summary>
        /// Recurse Tesselate , tessellates and updates our landscape
        /// ensure to use always the [right->apex->left] order (conterclockwire)
        /// Y component of right apex left are not used
        /// </summary>
        /// <param name="level">help information, not used</param>
        void RecurseTesselate(TriNode tri, Vector3 right, Vector3 apex, Vector3 left, int node, eFov info, int level)
        {
#if DEBUG
            if (tri == null) throw new ArgumentNullException("Pass a null triangle, error in recursion or some child missing");
#endif
            //calculate the center point (eventual split one) (l+r)/2
            Vector3 center = new Vector3();
            center.x = (left.x + right.x) * 0.5f;
            center.z = (left.z + right.z) * 0.5f;

            //Console.WriteLine(tri.ToString()+":");

            // if there is some different FOV info in the fov member use it
            // example when you share the fov information with baseneigbor (you assign a !=FovInfoDefault), visibility was already calculated
            if (tri.fov != eFov.Default)
            {
                info = tri.fov;
            }
            else
            {
                //else compute the visibility
                if (tri.IsVisible || showall)
                {

                    //Console.WriteLine("visible");
                    byte binf = (byte)info;
                    SIDE test = showall ? SIDE.INSIDE : FrustumCylindrerY(center.x, center.z, hypo[level], m_CVariance[node].height, ref binf);
                    //SIDE test = FrustumTriangle(left,right,center, ref binf);
                    info = (eFov)binf;
    
                    switch (test)
                    {
                        //if completely outside then MergeDown and return
                        case SIDE.OUTSIDE:
                            // set visible OFF
                            tri.fov = eFov.None;
                            //tri.IsVisible = false;
                            if (tri.IsSplitted)
                                MergeDown(tri);
                            return;

                        //if intersect use the binf flag to remove from check the planes what contain completly the triangle node
                        case SIDE.INTERSECT:
                            //Console.WriteLine(test + " " + info.ToString());
                            break;

                        //if completly inside, do not perform this check again for the children
                        case SIDE.INSIDE:
                            //info = FovInfo.None;
                            info = eFov.Visible; // all planes flags turn to OFF to skip the frustum computation
                            break;
                    }

                    // if there is a base neighbor share the fov information with him
                    // this because the base neighbor have the same cylinder
                    if (tri.BNeighbor != null && tri.BNeighbor.BNeighbor == tri)
                        tri.BNeighbor.fov = info;
                }
                
            }
            tri.fov = info;
            
            // check if are the leaf triangle in the tree
            if ((node << 1) >= m_CVariance.Length) return;


            float var;
            tri.defferal = -1;

            //check metric computation defferal
            if (tri.defferal > 0)
            {
                //this computation is beeng defered, reuse the old variance
                var = tri.lastvar;
                tri.defferal = -1;
            }
            else
            {
                center.y = m_terrain.GetHeight(center.x, center.z);
                //compute the real view dependent variance 
                var = GetVariance(center, node);

                //store the varinace in the node and calculate the frame deferal
                tri.lastvar = var;
                float d = (float)Math.Abs(var - lVariance) * 10.0f;
                tri.defferal = d;
            }

            //if our variance is above the limit then split
            if (var > lVariance || splitall)
            {
                //split operation - for update it will just exit
                //Console.WriteLine("Split()");
                Split(tri);

                //recurse the left and right leafs
                if (tri.IsSplitted)
                {
                    //"lca"-"acr"
                    RecurseTesselate(tri.LChild, left, center, apex, (node << 1), info, level + 1);
                    RecurseTesselate(tri.RChild, apex, center, right, (node << 1) + 1, info, level + 1);
                }
            }
            else
            {
                //if current variance is lower and this node is split...
                if (tri.IsSplitted)
                    MergeDown(tri);
            }

        }

        /// <summary>
        /// compute the real view dependent variance 
        /// </summary>
        /// <param name="point"></param>
        float GetVariance(Vector3 point,int node)
        {
            float dist = Vector3.Distance(m_terrain.frustum.m_eye, point);
            // f(x) = 1/(x+1)
            return m_CVariance[node].variance / (dist+1);
        }
        /// <summary>
        /// merge down goes down in the tree and when leafs are found they are 
        /// deleted so the parent node remains
        /// </summary>
        void MergeDown(TriNode tri)
        {
            if (!tri.IsSplitted) return; //leaf!!??

            //clear the information for the variance 
            //merge down occurs mainly due to fov info...
            tri.lastvar = -1;
            tri.defferal = -1;

            //good for merge
            if (GoodForMerge(tri))
            {
                //we are about to merge - check the neighbor if we have a diamont
                if (tri.BNeighbor == null)
                {
                    //no diamond
                    //at the border - trivial  merge
                    Merge(tri);
                }
                else
                {
                    // diamond!!!
                    // check the base for good children...
                    if (GoodForMerge(tri.BNeighbor))
                    {
                        Merge(tri.BNeighbor);
                        Merge(tri);
                        return;
                    }
                    else ; // base diamond neighbor is not ready for merge...
                    return;
                }
                return;
            }

            //merge down until we find some leafs 
            MergeDown(tri.LChild);
            MergeDown(tri.RChild);
        }

        /// <summary>
        /// GoodForMerge determines if this node's children are leaves and whether
        /// they are ready to be merged (i.e. the variance is high enough)
        /// </summary>
        bool GoodForMerge(TriNode tri)
        {
            //already merged?
            if (!tri.IsSplitted)
                return false;

            //lvar is more than the desired variance...this is not good
            //in case of fov.. lvar is 0
            //if (tri.lvar > lVariance)
            //    return false;

            //there are no grandchildren
            if ((!tri.LChild.IsSplitted) && (!tri.RChild.IsSplitted)) return true;
            
            return false;
        }
        /// <summary>
        /// merges the children of the spcified triangle node and "frees" the memory
        /// </summary>
        void Merge(TriNode tri)
        {
            //set patch modified flag to true
            //pb_mp[tri.pid] = 1;
            modified = true;

            //keep the connectivity information prior to deleteing nodes

            //get the base neighbor of the left child and connect to parent...
            if (tri.LChild.BNeighbor != null)
            {
                if (tri.LChild.BNeighbor.LNeighbor == tri.LChild)
                    tri.LChild.BNeighbor.LNeighbor = tri;
                if (tri.LChild.BNeighbor.RNeighbor == tri.LChild) 
                    tri.LChild.BNeighbor.RNeighbor = tri;
                if (tri.LChild.BNeighbor.BNeighbor == tri.LChild)
                {
                    tri.LChild.BNeighbor.BNeighbor = tri;
                    if (tri.LNeighbor == tri.LChild.BNeighbor.Parent) tri.LNeighbor = tri.LChild.BNeighbor;
                }

                //parent of the base neighbor of the left child should be 
                //checked
                TriNode par = tri.LChild.BNeighbor.Parent;

                //if there is such parent 
                if (par != null)
                {
                    if (par.LNeighbor == tri.LChild) par.LNeighbor = tri;
                    if (par.RNeighbor == tri.LChild) par.RNeighbor = tri;
                    if (par.BNeighbor == tri.LChild) par.BNeighbor = tri;
                }
            }

            //same for the rchild
            if (tri.RChild.BNeighbor != null)
            {
                if (tri.RChild.BNeighbor.LNeighbor == tri.RChild)tri.RChild.BNeighbor.LNeighbor = tri;
                if (tri.RChild.BNeighbor.RNeighbor == tri.RChild)tri.RChild.BNeighbor.RNeighbor = tri;
                if (tri.RChild.BNeighbor.BNeighbor == tri.RChild)
                {
                    tri.RChild.BNeighbor.BNeighbor = tri;
                    if (tri.RNeighbor == tri.RChild.BNeighbor.Parent)tri.RNeighbor = tri.RChild.BNeighbor;
                }

                TriNode par = tri.RChild.BNeighbor.Parent;
                if (par != null)
                {
                    if (par.LNeighbor == tri.RChild)par.LNeighbor = tri;
                    if (par.RNeighbor == tri.RChild)par.RNeighbor = tri;
                    if (par.BNeighbor == tri.RChild)par.BNeighbor = tri;
                }
            }

            //"delete" the nodes		
            //delete(tri.LChild);
            //delete(tri.RChild);

            //make the current node a "leaf"
            tri.LChild = tri.RChild = null;
            
        }     
              
        /// <summary>
        /// Split merge stuff - does the work with the childnodes
        /// attaches them each to other as well as connects them with the neighbors...
        /// pretty same as in Bryan's article
        /// </summary>
        void Split(TriNode tri)
        {
            //if already split... 
            if (tri.IsSplitted) return;

            //mark the patch as modified - > we introduce a split operation
            modified = true;


            //if isn't a diamond make it so by spliting base
            if (!tri.IsInDiamond)
            {
                Split(tri.BNeighbor);
            }

            //crate nodes...
            tri.LChild = new TriNode(tri.name + ".L");
            tri.RChild = new TriNode(tri.name + ".R");

            tri.LChild.node = (tri.node << 1);
            tri.RChild.node = (tri.node << 1) + 1;


            //currently exception
            //if (!(tri.LChild))  //!!?? Handle this situation !!!!!
            //	return;			 //TODO::: failed allocation

            //spawn the patch owner info to the children
            //tri.RChild.pid = tri.LChild.pid = tri.pid;

            //parental info
            tri.LChild.Parent = tri;
            tri.RChild.Parent = tri;

            //attach neighbors
            tri.RChild.BNeighbor = tri.RNeighbor;
            tri.RChild.RNeighbor = tri.LChild;

            tri.LChild.BNeighbor = tri.LNeighbor;
            tri.LChild.LNeighbor = tri.RChild;


            //some special cases.... link neigbors to our children not us
            //left
            if (tri.LNeighbor != null)
            {

                if (tri.LNeighbor.BNeighbor == tri)
                    tri.LNeighbor.BNeighbor = tri.LChild;//if the left nghb has us as base     
                else if (tri.LNeighbor.LNeighbor == tri)
                    tri.LNeighbor.LNeighbor = tri.LChild;//if the left ngb has us a left 
                else if (tri.LNeighbor.RNeighbor == tri) //if the left ngb has us a right 
                    tri.LNeighbor.RNeighbor = tri.LChild;
                else
                    throw new Exception("unable to link left neighbor");// what the hell went wrong    
            }

            //  Right 
            if (tri.RNeighbor != null)
            {
                if (tri.RNeighbor.BNeighbor == tri)
                    tri.RNeighbor.BNeighbor = tri.RChild;
                else if (tri.RNeighbor.RNeighbor == tri)
                    tri.RNeighbor.RNeighbor = tri.RChild;
                else if (tri.RNeighbor.LNeighbor == tri)
                    tri.RNeighbor.LNeighbor = tri.RChild;
                else
                    throw new Exception("unable to link right neighbor");// Illegal Right Neighbor!
            }

            // down -base - a bit diferrent scheme
            if (tri.BNeighbor != null)
            {
                if (tri.BNeighbor.LChild != null && (tri.BNeighbor.BNeighbor == tri)) //if has children
                {
                    //link our children to his
                    tri.BNeighbor.LChild.RNeighbor = tri.RChild;
                    tri.BNeighbor.RChild.LNeighbor = tri.LChild;
                    //link his to ours...
                    tri.LChild.RNeighbor = tri.BNeighbor.RChild;
                    tri.RChild.LNeighbor = tri.BNeighbor.LChild;
                }
                else
                    Split(tri.BNeighbor);  // Base Neighbor (in a diamond with us) was not split yet, so do that now.
            }
            else
            {
                // An edge triangle, trivial case.
                tri.LChild.RNeighbor = null;
                tri.RChild.LNeighbor = null;
            }
        }

        /// <summary>
        /// Build the vertices and indices array
        /// </summary>
        public void Render()
        {
            tri_count = 0;

            // run left
            m_CVariance = m_LVariance;
            RecurseRender(m_LBase, m_tl, m_dl, m_dr, new Vector2(0, 1), new Vector2(0, 0), new Vector2(1, 0), ref itl, ref idl, ref idr);

            //run right
            m_CVariance = m_RVariance;
            RecurseRender(m_RBase, m_dr, m_tr, m_tl, new Vector2(1, 0), new Vector2(1, 1), new Vector2(0, 1), ref idr, ref itr, ref itl);

            // not generated triangle so the four corner vertices aren't updated
            if (tri_count == 0) return;

            //update indices of the neighbor patches, in the corners the patches share their vertices
            if (m_Pup != null)
            {
                if (m_Pup.idl == TriNode.NULLIDX) m_Pup.idl = itl;
                if (m_Pup.idr == TriNode.NULLIDX) m_Pup.idr = itr;
                if (m_Pup.m_Pleft != null && m_Pup.m_Pleft.idr == TriNode.NULLIDX) m_Pup.m_Pleft.idr = itl;
                if (m_Pup.m_Pright != null && m_Pup.m_Pright.idl == TriNode.NULLIDX) m_Pup.m_Pright.idl = itr;
            }
            if (m_Pdown != null)
            {
                if (m_Pdown.itr == TriNode.NULLIDX) m_Pdown.itr = idr;
                if (m_Pdown.itl == TriNode.NULLIDX) m_Pdown.itl = idl;
                if (m_Pdown.m_Pleft != null && m_Pdown.m_Pleft.itr == TriNode.NULLIDX) m_Pdown.m_Pleft.itr = idl;
                if (m_Pdown.m_Pright != null && m_Pdown.m_Pright.itl == TriNode.NULLIDX) m_Pdown.m_Pright.itl = idr;
            }
            if (m_Pleft != null)
            {
                if (m_Pleft.itr == TriNode.NULLIDX) m_Pleft.itr = itl;
                if (m_Pleft.idr == TriNode.NULLIDX) m_Pleft.idr = idl;
                if (m_Pleft.m_Pup != null && m_Pleft.m_Pup.idr == TriNode.NULLIDX) m_Pleft.m_Pup.idr = itl;
                if (m_Pleft.m_Pdown != null && m_Pleft.m_Pdown.itr == TriNode.NULLIDX) m_Pleft.m_Pdown.itr = idl;
            }
            if (m_Pright != null)
            {
                if (m_Pright.itl == TriNode.NULLIDX) m_Pright.itl = itr;
                if (m_Pright.idl == TriNode.NULLIDX) m_Pright.idl = idr;
                if (m_Pright.m_Pup != null && m_Pright.m_Pup.idl == TriNode.NULLIDX) m_Pright.m_Pup.idl = itr;
                if (m_Pright.m_Pdown != null && m_Pright.m_Pdown.itl == TriNode.NULLIDX) m_Pright.m_Pdown.itl = idr;
            }
        }
        /// <summary>
        /// recurses the tree rendering... node,coordinates of the three points,
        /// indeces for three points (0xffff) means the index is empty.
        /// recurse render follows the logical structure
        /// </summary>
        void RecurseRender(TriNode tri, Vector3 right, Vector3 apex, Vector3 left, Vector2 tr, Vector2 ta, Vector2 tl, ref int ir, ref int ia, ref int il)
        {
            // triangle isn't visible in the current frustum view, skip adding vertices
            if (!tri.IsVisible) return;
            
            Vector3 center = new Vector3();
            center.x = (left.x + right.x) * 0.5f;
            center.z = (left.z + right.z) * 0.5f;
            center.y = m_terrain.GetHeight(center.x, center.z);

            Vector2 tc = new Vector2();
            tc.u = (tl.u + tr.u) * 0.5f;
            tc.v = (tl.v + tr.v) * 0.5f;

                  
            // if triangle node is splitted, we don't render it
            if (tri.IsSplitted)
            {
                //"lca"-"acr"
                RecurseRender(tri.LChild, left, center, apex, tl, tc, ta, ref il, ref tri.ic, ref ia);
                RecurseRender(tri.RChild, apex, center, right, ta, tc, tr, ref ia, ref tri.ic, ref ir);

                //share the center index with base neighbor , if is our diamond
                if (tri.BNeighbor != null && tri.BNeighbor.BNeighbor == tri)
                    tri.BNeighbor.ic = tri.ic;
            }
            // is a leaf triangle node
            else
            {
                if (il == TriNode.NULLIDX) il = PutVertex(left, tl);
                if (ir == TriNode.NULLIDX) ir = PutVertex(right, tr);
                if (ia == TriNode.NULLIDX) ia = PutVertex(apex, ta);
#if DEBUG
                if (ia > ushort.MaxValue || ir > ushort.MaxValue || il > ushort.MaxValue)
                    throw new ArgumentOutOfRangeException("Index overflow");
#endif
                // the indices are passed in conterclockwire order
                PutTriIndeces((ushort)ir, (ushort)ia, (ushort)il);
            }
        }
        /// <summary>
        /// PutTriIndeces insert the tree indeces of the triangle
        /// into the index buffer...
        /// </summary>
        void PutTriIndeces(ushort i, ushort j, ushort k)
        {
#if DEBUG
            if (i == j || j == k || i == k)
                throw new Exception("Incoerent face");
#endif
            pindices.Add(new Face(j, i, k));
            tri_count++;
        }

        /// <summary>
        /// adds a new vertex in the vertex array and returns the index
        /// </summary>
        int PutVertex(Vector3 p,Vector2 t)
        {
            pvertices.Add(new NCVERTEX(p, Vector3.Zero,patchColor));
            /*
            //check for mirroring and mirror the texture cordinates
            if (mirrorx > 0)
                vertices[vcount].uv.u = 1.0f - tu;
            else
                vertices[vcount].uv.u = tu;

            if (mirrory > 0)
                vertices[vcount].uv.v = 1.0f - tv;
            else
                vertices[vcount].uv.v = tv;
            */
            return pvertices.Count - 1;
        }

        /// <summary>
        /// Sphere - Frustum visibility test, info flag define the planes to test. Custom implementation of Frustum.GetSphereSide
        /// </summary>
        SIDE FrustumSphere(Vector3 center, float radius, ref byte info)
        {
            Frustum frustum = m_terrain.frustum;
            bool intersect = false;
            byte mask = 4;//000001-00 (activate plane1)
            for (byte i = 0; i < 6; i++, mask <<= 1)
            {
                if ((info & mask) != 0)
                {
                    float dist = frustum.m_plane[i].GetDistance(center.x, center.y, center.z);

                    // outside a plane                  
                    if (dist + radius < 0)
                    {
                        // planes flags is unusefull, set to zero for safety
                        info &= 3;
                        return SIDE.OUTSIDE;
                    }
                    // completly in the positive side of planes, remove it from flags
                    if (dist - radius > 0)
                    {
                        info &= (byte)~mask;
                    }
                    // intersect the plane, flag plane isn't modified
                    else
                    {
                        intersect = true;
                    }
                }
            }
            return intersect ? SIDE.INTERSECT : SIDE.INSIDE;
        }

        /// <summary>
        /// CylindrerUp - Frustum visibility test, info flag define the planes to test.
        /// </summary>
        SIDE FrustumCylindrerY(float cx,float cz, float radius, float heigth, ref byte info)
        {
            Frustum frustum = m_terrain.frustum;
            byte mask = 4;//000001-00 (activate plane1)
            SIDE result = SIDE.INSIDE;

            for (byte i = 0; i < 6; i++, mask <<= 1)
            {
                if ((info & mask) != 0)
                {
                    Plane p = frustum.m_plane[i];
                    float r = radius * m_terrain.m_planesin[i];
                    // distance of bottom center
                    float d1 = p.A * cx + p.C * cz + p.D; //cy=0 
                    // distance of top center
                    float d2 = d1 + p.B * heigth; //cy = height
                    // top cap circle intersection
                    bool in1 = Math.Abs(d1) < r;
                    // bottom cap circle intersection
                    bool in2 = Math.Abs(d2) < r;

                    //intersect 1
                    if (in1)
                    {
                        result = SIDE.INTERSECT;
                    }
                    else
                    {
                        //intersect 2
                        if (in2)
                        {
                            result = SIDE.INTERSECT;
                        }
                        else
                        {
                            if (d1 < 0)
                            {
                                //outside, exit
                                if (d2 < 0) return SIDE.OUTSIDE;
                                else result = SIDE.INTERSECT;
                            }
                            else
                            {
                                //intersects
                                if (d2 < 0) result = SIDE.INTERSECT;
                                // in positive side of plane, remove it for future computations
                                else info &= (byte)~mask;
                            }
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Triangle - Frustum visibility test
        /// </summary>
        SIDE FrustumTriangle(Vector3 p0,Vector3 p1,Vector3 p2,ref byte info)
        {
            Frustum frustum = m_terrain.frustum;
            byte mask = 4;//000001-00 (activate plane1)
            SIDE result = SIDE.INSIDE;
            
            for (byte i = 0; i < 6; i++, mask <<= 1)
            {
                if ((info & mask) != 0)
                {
                    Plane p = frustum.m_plane[i];

                    float d0 = p.GetDistance(p0);
                    float d1 = p.GetDistance(p1);
                    float d2 = p.GetDistance(p2);

                    if (d0 < 0 &&  d1< 0 &&  d2< 0)
                    {
                        return SIDE.OUTSIDE;
                    }
                    if (d0 > 0 && d1 > 0 && d2 > 0)
                    {
                        // completly inside
                        info &= (byte)~mask;
                    }
                    else
                    {
                        result = SIDE.INTERSECT;
                    }
                }
            }
            return result;
        }


#if DEBUG

        public List<TriNode> triangles = new List<TriNode>();

        public void ComputeGeometry()
        {
            m_CVariance = m_LVariance;
            CGeometric(m_LBase, m_tl, m_dl, m_dr, 1);
            m_CVariance = m_RVariance;
            CGeometric(m_RBase, m_dr, m_tr, m_tl, 1);
        }

        void CGeometric(TriNode tri, Vector3 right, Vector3 apex, Vector3 left, int node)
        {
            if (tri == null) return;

            Vector3 center = (left + right) * 0.5f;
            center.y = m_terrain.GetHeight(center.x, center.z);

            if (tri.LChild != null)
            {
                //"lca"-"acr"
                CGeometric(tri.LChild, left, center, apex, node << 1);
                CGeometric(tri.RChild, apex, center, right, (node << 1) + 1);
            }
            else
            {
                Color color = Color.FromArgb(Terrain.rnd.Next(int.MinValue, int.MaxValue));
                pvertices.Add(new NCVERTEX(right, Vector3.UnitY,color));
                pvertices.Add(new NCVERTEX(apex, Vector3.UnitY, color));
                pvertices.Add(new NCVERTEX(left, Vector3.UnitY, color));

                tri.center = (left + apex + right) / 3f;
                tri.name = string.Format("TNode{0}_v{1,1}", node, m_CVariance[node]);
                triangles.Add(tri);

                if (tri.node != node)
                    throw new Exception("error in implicit tree");
            }
        }


        public TreeNode GetTreeView(string patchname)
        {
            TreeNode root = new TreeNode(patchname);
            root.Nodes.Add(RecurseTreeView(m_LBase));
            root.Nodes.Add(RecurseTreeView(m_RBase));
            return root;
        }
        TreeNode RecurseTreeView(TriNode tri)
        {
            TreeNode node = new TreeNode(tri.name + " " + tri.IsVisible);         
            if (tri.IsSplitted)
            {
                node.Nodes.Add(RecurseTreeView(tri.LChild));
                node.Nodes.Add(RecurseTreeView(tri.RChild));
            }
            return node;
        }

#endif

    }
}
