﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

namespace Engine.Graphics
{
    /// <summary>
    /// SharpDx : Enumerate all graphic card capacities, only for First default adaptes
    /// </summary>
    public static class DX9Enumerations
    {
        const int adapter = 0;
        static D3D.Direct3D d3d = new D3D.Direct3D();
        static D3D.Capabilities caps = d3d.GetDeviceCaps(adapter, D3D.DeviceType.Hardware);

        /// <summary>
        /// Check the screen format, old card can support only 16bit pixels
        /// </summary>
        public static bool CheckDeviceType(Format DisplayFormat,Format BackBufferFormat,bool windowed)
        {
            DX.Result result;
            bool test = d3d.CheckDeviceType(adapter, D3D.DeviceType.Hardware, (D3D.Format)DisplayFormat, (D3D.Format)BackBufferFormat, windowed, out result);
            if (result.Failure) test = false;
            return test;
        }

        public static bool CheckDepthStencil(Format DisplayFormat, Format DepthStencilFormat)
        {
            return d3d.CheckDepthStencilMatch(adapter, D3D.DeviceType.Hardware, (D3D.Format)DisplayFormat, (D3D.Format)DisplayFormat, (D3D.Format)DepthStencilFormat);
        }

        public static Version PixelShaderVersion
        {
            get { return caps.PixelShaderVersion; }
        }
        public static Version VertexShaderVersion
        {
            get { return caps.VertexShaderVersion; }
        }
        public static int MaxVertexShaderConst
        {
            get { return caps.MaxVertexShaderConst; }
        }
        /// <summary>
        /// check if a resolution is supported
        /// </summary>
        public static bool CheckDisplayMode(int width, int height, Format format)
        {
            D3D.AdapterCollection collection = d3d.Adapters;
            foreach (D3D.AdapterInformation info in collection)
            {
                D3D.DisplayMode d = info.CurrentDisplayMode;
                if (d.Width == width & d.Height == height & d.Format.Equals(format))
                    return true;
            }
            return false;
        }
        /// <summary>
        /// </summary>
        public static List<DisplayMode> GetViewsMode(Format format)
        {
            int count = d3d.GetAdapterModeCount(adapter, (D3D.Format)format);
            List<DisplayMode> list = new List<DisplayMode>(count);
            for (int i = 0; i < count; i++)
            {
                D3D.DisplayMode d = d3d.EnumAdapterModes(0, (D3D.Format)format, i);
                list.Add(new DisplayMode { width = d.Width, height = d.Height, refreshRate = d.RefreshRate, format = (Format)d.Format });
            }
            return list;
        }
        public static DisplayMode CurrentDisplayMode
        {
            get
            {
                D3D.DisplayMode d = d3d.GetAdapterDisplayMode(adapter);
                return new DisplayMode { width = d.Width, height = d.Height, refreshRate = d.RefreshRate, format = (Format)d.Format };
            }
        }
        /// <summary>
        /// Some check of max size, squareonly and pow2 capacities
        /// </summary>
        public static bool SupportTextureSize(Size size)
        {
            if (caps.MaxTextureWidth < size.Width || caps.MaxTextureHeight < size.Height) return false;

            if ((caps.TextureCaps & D3D.TextureCaps.SquareOnly)!=0 && size.Height != size.Width) return false;

            if ((caps.TextureCaps & D3D.TextureCaps.Pow2) != 0)
            {
                List<int> pow2 = new List<int> { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };
                if (!pow2.Contains(size.Width) || !pow2.Contains(size.Height)) return false;
            }
            return true;
        }
        /// <summary>
        /// Max simultaneous textures, Directx9 can support max 8 textures
        /// </summary>
        public static int MaxActiveTexture
        {
            get { return caps.MaxSimultaneousTextures; }
        }
        /// <summary>
        /// Used to know if i can use uint instead ushort indices format, remember that 0xFFFF for ushort is used
        /// for reset primitives counter example in Triangle.Fan
        /// </summary>
        public static int MaxVertexIndex
        {
            get { return caps.MaxVertexIndex; }
        }

        public static int MaxPrimitiveCount
        {
            get { return caps.MaxPrimitiveCount; }
        }
        /// <summary>
        /// Directx9 constant = 8
        /// </summary>
        public static int MaxLights
        {
            get { return caps.MaxActiveLights; }
        }
        public static bool SupportsHardwareTransformAndLight
        {
            get { return (caps.DeviceCaps & D3D.DeviceCaps.HWTransformAndLight) !=0 ; }
        }
        public static bool SupportsPureDevice
        {
            get { return (caps.DeviceCaps & D3D.DeviceCaps.PureDevice) != 0; }
        }

        public static void Check(Format format)
        {
            int count = d3d.GetAdapterModeCount(adapter, (D3D.Format)format);


            for (int i=0;i<count;i++)
            {
                D3D.DisplayMode d = d3d.EnumAdapterModes(0, (D3D.Format)format, i);
                Console.WriteLine(d);
            }

        }
    }
}
