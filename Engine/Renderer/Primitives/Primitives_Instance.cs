﻿// by johnwhile
using System;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// Is a simple reference of another geometry, only transform are used
    /// After a little test, Bathing is much faster than Instancing made with
    /// "device.transform.world" setting
    /// </summary>
    public class PrimitiveInst : Node
    {
        /// <summary>
        /// The node there geometry exist
        /// </summary>
        public Node instance = null;
        
        public PrimitiveInst(Node instanceof)
        {
            m_primitive = PrimitiveType.Instance;
            instance = instanceof;
        }
        public override Array GetIndicesStream(int index_offset, bool use32bit)
        {
            return instance.GetIndicesStream(index_offset, use32bit);
        }
        public override Array GetVerticesStream(bool globalCoorSys, VertexDeclaration declaration)
        {
            return instance.GetVerticesStream(globalCoorSys, declaration);
        }
        public override int numIndices
        {
            get { return instance.numIndices; }
        }
        public override int numVertices
        {
            get { return instance.numVertices; }
        }
        public override int PrimitiveIntersection(Ray ray, out float t, out Vector3 intersection)
        {
            return base.PrimitiveIntersection(ray, out t, out intersection);
        }
        /// <summary>
        /// Carefull, affect the instanced node, not this
        /// </summary>
        public override void changeTransform(Matrix4 newtransform)
        {
            instance.changeTransform(newtransform);
        }

        public override int numPrimitives
        {
            get { return instance.numPrimitives; }
        }
    }
}
