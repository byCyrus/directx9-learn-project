﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Maths;

namespace Debugger
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct PositionXYZ
    {
        public Vector3 pos;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct PositionXZ
    {
        public Vector2 pos;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct PositionH
    {
        public float pos;
    }
}
