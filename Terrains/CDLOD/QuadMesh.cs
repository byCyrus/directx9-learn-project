﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Cdlod
{
    /// <summary>
    /// The static mesh geoemtry, a fixed grid in 2D. The size must be a 2^n
    /// All vertices in [0.0 - 1.0] range
    /// </summary>
    public class QuadMesh : DisposableResource, IResettable
    {
        public static int DrawCount = 0;
        public static int TrianglesCount = 0;
        GraphicDevice m_device;
        VertexDeclaration m_decl;
        VertexBuffer m_vb;
        IndexBuffer m_ib;
        PrimitiveType m_type;

        public CdlodShader shaderProgram = null;
        public int row { get; private set; }
        public int col { get; private set; }

        /// <summary>
        /// </summary>
        /// <param name="xsuddivision">remember that num of columns = xsudd + 1</param>
        /// <param name="ysuddivision">remember that num of rows = ysudd + 1</param>
        /// <param name="useStrip">can triangleStrip primitive improve performances ?</param>
        public QuadMesh(GraphicDevice device, int xsuddivision, int ysuddivision, bool useStrip)
        {
            m_device = device;
            row = ysuddivision+1;
            col = xsuddivision+1;

            if (row < 1 || col < 1 || (!useStrip && row * col > ushort.MaxValue - 1))
                throw new ArgumentOutOfRangeException(string.Format("Grid size {0}x{1} too big", row, col));

            m_type = useStrip ? PrimitiveType.TriangleStrip : PrimitiveType.TriangleList;

            Restore();
        }

        public void SetToDevice()
        {
            m_decl.SetToDevice();
            m_vb.SetToDevice();
            m_ib.SetToDevice();
        }

        public void DrawMesh()
        {
            m_device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_vb.Count, 0, m_ib.Count);
            DrawCount++;
            TrianglesCount += m_ib.Count;
        }

        int vertex(int i, int j) { return i + j * col; }

        /// <summary>
        /// <para>xsudd = ysudd = 2</para>
        /// <para>6---7---8</para>
        /// <para>|   |   |</para>
        /// <para>3---4---5</para>
        /// <para>|   |   |</para>
        /// <para>0---1---2</para>
        /// </summary>
        void getIndexedGrid(int xsudd, int ysudd, out Vector2[] vertices, out Face16[] indices)
        {
            int numverts = (xsudd + 1) * (ysudd + 1);
            int numtris = xsudd * ysudd * 2;
            if (numverts > ushort.MaxValue - 1) throw new OverflowException("too many vertices for 16bit indices");

            vertices = new Vector2[numverts];

            for (int n = 0, j = 0; j <= ysudd; j++)
                for (int i = 0; i <= xsudd; i++)
                    vertices[n++] = new Vector2((float)i / xsudd, (float)j / ysudd);

            indices = new Face16[numtris];
            for (int n = 0,j = 0; j < ysudd; j++)
                for (int i = 0; i < xsudd; i++)
                {
                    indices[n++] = new Face16(vertex(i, j), vertex(i, j + 1), vertex(i + 1, j + 1));
                    indices[n++] = new Face16(vertex(i, j), vertex(i + 1, j + 1), vertex(i + 1, j));
                }
        }

        public void Restore()
        {
            Vector2[] vertices;

            if (m_type == PrimitiveType.TriangleList)
            {
                Face16[] indices;
                getIndexedGrid(col-1, row-1, out vertices, out indices);

                List<VertexElement> m_elements = new List<VertexElement>();
                m_elements.Add(new VertexElement(0, 0, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.Position, 0));
                m_decl = new VertexDeclaration(m_device, m_elements);


                m_ib = new IndexBuffer(m_device, IndexInfo.IndexFace16, indices.Length, false, true);
                IndexStream idata = m_ib.OpenStream();
                idata.WriteCollection<Face16>(indices, IndexInfo.IndexFace16, 0, indices.Length, 0, 0);
                m_ib.CloseStream();
                m_ib.Count = indices.Length;
            }
            else
            {
                throw new NotImplementedException();
            }
            m_vb = new VertexBuffer(m_device, m_decl.Format, vertices.Length, false, true);
            VertexStream vdata = m_vb.OpenStream();
            vdata.WriteCollection<Vector2>(vertices, 0, vertices.Length, 0);
            m_vb.CloseStream();
            m_vb.Count = vertices.Length;
        }

        protected override void Dispose(bool disposing)
        {
            m_vb.Dispose();
            m_ib.Dispose();
        }

        public override string ToString()
        {
            return string.Format("{0}x{1}", row, col);
        }

    }

}
