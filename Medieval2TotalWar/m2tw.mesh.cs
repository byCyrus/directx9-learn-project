using System;
using System.Collections;
using System.IO;
using System.Text;

namespace Engine.M2TW
{
    /// <summary>
    ///  MeshFormat class is a collection of method to read and manage the *.mesh file. 
    /// </summary>
    public class MeshFormat
    {
        /// <summary>
        ///  these string store the error message, in some case the code don't get error but a "memorandum"
        ///  was written when debugging
        /// </summary>
        public string debug = "all ok";

        /// <summary>
        ///  each class contain a flag system, was true for first calling
        /// </summary>
        internal static bool flag = true;

        public static uint totalverts = 0;
        public int variant = 0;
        public Structure[] structure;
        public TextureSection[] texture;
        public sBoundingSphere bsphere;
        public uint numtext = 0;
        public uint numstr = 0;
        public uint nummesh = 0;


        /// <summary>
        ///  this function initialize a new set of flag, call it if you create a new instance of MeshFormat.
        /// </summary>
        public static void clearAllflags(bool status)
        {
            // this flag are used to work with the same WorldFormat's class but some little short0 appear
            Structure.flagmesh = true;

            Engine.M2TW.Globals.counterA = 0;
            Engine.M2TW.Globals.counterB = 0;
            flag = status;
            Point2Data.flag = status;
            Point3Data.flag = status;
            Point4Data.flag = status;
            FacesGroup.flag = status;
            FacesData.flag = status;
            VerticesData.flag = status;
            StructureBones.flag = status;
            Structure.flag = status;
            TextureUnknow.flag = status;
            TextureMaterial.flag = status;
            TextureSection.flag = status;
        }

        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public Int64 OpenFile(string filepath)
        {
            Int64 filepos = -1;
            using (BinaryReader file = new BinaryReader(File.OpenRead(filepath)))
            {
                try
                {
                    ReadBin(file);
                    filepos = -1;
                }
                catch (Exception ex)
                {
                    filepos = file.BaseStream.Position;
                    debug = (string)ex.Message;
                    Console.WriteLine("!!!! ERROR !!!!\n" + debug);
                }
            }
            return filepos;
        }
        
        /// <summary>
        ///  these functions return -1 if file was correctly processed, otherwise return the last file position for debugging.
        /// </summary>
        public Int64 SaveFile(string filepath)
        {
            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(filepath)))
                if (!WriteBin(file)) return file.BaseStream.Position;
            return -1;
        }

        /// <summary>
        ///  thise function read a binary data, at the beginning use the methods "clearflags" and place file seek at
        ///  start.
        /// </summary>
        public bool ReadBin(BinaryReader file)
        {
            clearAllflags(true);
            int i = 0;
            nummesh = 0;

            file.BaseStream.Seek(0, SeekOrigin.Begin);

            // header of world file
            string headerStr = Utils.ReadGameString(file);// = "serialization::archive";
            if (headerStr == null) { return false; }
            byte[] headerBytes = new byte[9];// = { 3, 4, 4, 8, 1, 0, 0, 0};
            file.Read(headerBytes, 0, 9);

            variant = file.ReadInt32();

            switch (variant)
            {
                case 0: break; // is a siege engine
                case 1: file.ReadInt32(); break; // is a units , integer0
                default: throw new ArgumentOutOfRangeException("variant", variant, "Unknow mesh variant, exit for safety");
            }
            // get from worldformat class

            //------------------------
            // Structure section
            //------------------------ 
            numstr = file.ReadUInt32();
            if (numstr > 256) { throw new ArgumentOutOfRangeException("numstr",numstr, "ERROR : Structure number out of range, exit for safety"); }
            structure = new Structure[numstr];
            for (i = 0; i < numstr; i++)
            {
                structure[i] = new Structure();
                structure[i].ReadBin(file);
                nummesh += structure[i].facesdata.nummesh;
            }
            
            //------------------------
            // Texture section
            //------------------------
            if (flag) file.ReadUInt16(); // short0
            numtext = file.ReadUInt32();
            if (numtext != numstr) { throw new ArgumentOutOfRangeException("numtext", numtext, "ERROR : Texture number not equal to Structure number, exit for safety"); }
            texture = new TextureSection[numtext];
            for (i = 0; i < numtext; i++)
            {
                texture[i] = new TextureSection();
                texture[i].ReadBin(file);
            }

            //------------------------
            // End section
            //------------------------
            bsphere = sBoundingSphere.FromFile(file);
            
            flag = false;
            return true;
        }

        /// <summary>
        ///  same of ReadBin
        /// </summary>
        public bool WriteBin(BinaryWriter file)
        {
            clearAllflags(true);
            return false;
        }

        /// <summary>
        /// this function rebuild a DECOMPRESSED pkg file for rebuild animinstance file, so need a mesh's 
        /// building variant (but generally works with any type) , the COMPRESSION process was made by
        /// "AniminstanceTransition" class.
        /// </summary>
        public AniminstanceTransition GetPkgTemporary()
        {
            // attention, don't use faces class, only vertex section, so may be the presence of many meshes
            if (structure.Length == 0) return null;
            
            // we use a clean version
            AniminstanceTransition.flag = false;
            AniminstanceVertexEntry.flag = false;

            return null;
        }
        public override string ToString()
        {
            string str = "MESH debug:\n";
            return str;
        }

    }
}
