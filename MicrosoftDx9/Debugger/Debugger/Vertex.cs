﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

using Engine.Maths;

namespace Debugger
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    [DebuggerDisplay("{x},{y},{z}")]
    public struct PositionColor
    {
        public Vector3 pos;
        public Vector2 uv;
        public Vector3 norm;
        public int color;

        public static readonly D3D.VertexElement[] elements = new D3D.VertexElement[]
        {
            new D3D.VertexElement( 0,   0, D3D.DeclarationType.Float3, D3D.DeclarationMethod.Default, D3D.DeclarationUsage.Position,          0),
            new D3D.VertexElement( 0,  12, D3D.DeclarationType.Float2, D3D.DeclarationMethod.Default, D3D.DeclarationUsage.TextureCoordinate, 0),
            new D3D.VertexElement( 0,  20, D3D.DeclarationType.Float3, D3D.DeclarationMethod.Default, D3D.DeclarationUsage.Normal,            0),
            new D3D.VertexElement( 0,  32, D3D.DeclarationType.Color,  D3D.DeclarationMethod.Default, D3D.DeclarationUsage.Color,             0),
            D3D.VertexElement.VertexDeclarationEnd 
        };
        public static readonly D3D.VertexFormats format = D3D.VertexFormats.Position | D3D.VertexFormats.Diffuse | D3D.VertexFormats.Texture0 | D3D.VertexFormats.Normal;
    }


    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct PositionXZ
    {
        public Vector2 pos;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct PositionH
    {
        public float pos;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct VertexT3
    {
        public Vector3 position;
        public Vector3 normal;
        public Vector3 texture0;

        //vertex format
        public static D3D.VertexFormats Format
        {
            get 
            { 
                return D3D.VertexFormats.Position | 
                       D3D.VertexFormats.Normal |
                       D3D.VertexFormats.Texture0 |
                       D3D.VertexTextureCoordinate.Size3(0);
            }
        }

        public VertexT3(float x, float y, float z, float nx, float ny, float nz, float tu, float tv, float tw)
        {
            position = new Vector3(x, y, z);
            normal = new Vector3(nx, ny, nz);
            texture0 = new Vector3(tu, tv, tw);
        }

        /// <summary>
        /// generate vertex elements
        /// </summary>
        /// <returns>elements</returns>
        public static readonly D3D.VertexElement[] element = new D3D.VertexElement[]
        {
			new D3D.VertexElement(0,  0, D3D.DeclarationType.Float3, D3D.DeclarationMethod.Default, D3D.DeclarationUsage.Position, 0), 
			new D3D.VertexElement(0, 12, D3D.DeclarationType.Float3, D3D.DeclarationMethod.Default, D3D.DeclarationUsage.Normal, 0), 
			new D3D.VertexElement(0, 24, D3D.DeclarationType.Float3, D3D.DeclarationMethod.Default, D3D.DeclarationUsage.TextureCoordinate, 0), 
			D3D.VertexElement.VertexDeclarationEnd
        };
    }

    public static class OpenCube
    {
        public static PositionColor[] vertexstream
        {
            get
            {
                PositionColor[] array = new PositionColor[vertices.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i].pos = vertices[i];
                    array[i].color = (int)colors[i].argb;
                    array[i].uv = textures[i];
                    array[i].norm = vertices[i].Normal;
                }
                return array;
            }
        }


        public static Vector3[] vertices = new Vector3[]
        {
	        new Vector3(-1.0f, 1.0f,-1.0f ),
	        new Vector3( 1.0f, 1.0f,-1.0f ),
	        new Vector3(-1.0f,-1.0f,-1.0f ),
	        new Vector3( 1.0f,-1.0f,-1.0f ),

	        new Vector3(-1.0f, 1.0f, 1.0f ),
	        new Vector3(-1.0f,-1.0f, 1.0f ),
	        new Vector3( 1.0f, 1.0f, 1.0f ),
	        new Vector3( 1.0f,-1.0f, 1.0f ),

	        new Vector3(-1.0f, 1.0f, 1.0f ),
	        new Vector3( 1.0f, 1.0f, 1.0f ),
	        new Vector3(-1.0f, 1.0f,-1.0f ),
	        new Vector3( 1.0f, 1.0f,-1.0f ),

	        new Vector3(-1.0f,-1.0f, 1.0f ),
	        new Vector3(-1.0f,-1.0f,-1.0f ),
	        new Vector3( 1.0f,-1.0f, 1.0f ),
	        new Vector3( 1.0f,-1.0f,-1.0f ),

	        new Vector3( 1.0f, 1.0f,-1.0f ),
	        new Vector3( 1.0f, 1.0f, 1.0f ),
	        new Vector3( 1.0f,-1.0f,-1.0f ),
	        new Vector3( 1.0f,-1.0f, 1.0f ),

	        new Vector3(-1.0f, 1.0f,-1.0f ),
	        new Vector3(-1.0f,-1.0f,-1.0f ),
	        new Vector3(-1.0f, 1.0f, 1.0f ),
	        new Vector3(-1.0f,-1.0f, 1.0f )
        };
        public static Color32[] colors = new Color32[]
        {
            Color.Red,
            Color.Red,
            Color.Red,
            Color.Red,
            Color.Green,
            Color.Green,
            Color.Green,
            Color.Green,
            Color.Blue,
            Color.Blue,
            Color.Blue,
            Color.Blue,
            Color.Yellow,
            Color.Yellow,
            Color.Yellow,
            Color.Yellow,
            Color.Magenta,
            Color.Magenta,
            Color.Magenta,
            Color.Magenta,
            Color.Cyan,
            Color.Cyan,
            Color.Cyan,
            Color.Cyan
        };
        public static Vector2[] textures = new Vector2[]
        {
	        new Vector2( 0.0f, 0.0f ),
	        new Vector2( 1.0f, 0.0f ),
	        new Vector2( 0.0f, 1.0f ),
	        new Vector2( 1.0f, 1.0f ),

	        new Vector2( 1.0f, 0.0f ),
	        new Vector2( 1.0f, 1.0f ),
	        new Vector2( 0.0f, 0.0f ),
	        new Vector2( 0.0f, 1.0f ),

	        new Vector2( 0.0f, 0.0f ),
	        new Vector2( 1.0f, 0.0f ),
	        new Vector2( 0.0f, 1.0f ),
	        new Vector2( 1.0f, 1.0f ),

            new Vector2( 0.0f, 0.0f ),
            new Vector2( 1.0f, 0.0f ),
            new Vector2( 0.0f, 1.0f ),
            new Vector2( 1.0f, 1.0f ),

	        new Vector2( 0.0f, 0.0f ),
	        new Vector2( 1.0f, 0.0f ),
	        new Vector2( 0.0f, 1.0f ),
	        new Vector2( 1.0f, 1.0f ),

	        new Vector2( 1.0f, 0.0f ),
	        new Vector2( 1.0f, 1.0f ),
	        new Vector2( 0.0f, 0.0f ),
	        new Vector2( 0.0f, 1.0f )
        };
    }

    public static class Tool
    {
        public static string GetBinaryString(int n)
        {
            char[] b = new char[32];
            int pos = 31;
            int i = 0;
            while (i < 32)
            {
                b[pos] = ((n & (1 << i)) != 0) ? '1' : '0';
                pos--;
                i++;
            }
            return new string(b);
        }
        public static string GetBinaryString(short n)
        {
            char[] b = new char[16];
            int pos = 15;
            int i = 0;
            while (i < 16)
            {
                b[pos] = ((n & (1 << i)) != 0) ? '1' : '0';
                pos--;
                i++;
            }
            return new string(b);
        }
        public static string GetBinaryString(byte n)
        {
            char[] b = new char[8];
            int pos = 7;
            int i = 0;
            while (i < 8)
            {
                b[pos] = ((n & (1 << i)) != 0) ? '1' : '0';
                pos--;
                i++;
            }
            return new string(b);
        }
    }


    [Flags]
    public enum VertexFormatss
    {
        Texture0 = 0,
        None = 0,
        Position = 2,             //          00000010
        Transformed = 4,          //          00000100
        PositionBlend1 = 6,       //          00000110
        PositionBlend2 = 8,       //          00001000
        TextureCountShift = 8,    //          00001000
        PositionBlend3 = 10,      //          00001010
        PositionBlend4 = 12,      //          00001100
        PositionBlend5 = 14,      //          00001110
        Normal = 16,              //          00010000
        PositionNormal = 18,      //          00010010
        PointSize = 32,           //          00100000
        Diffuse = 64,             //          01000000
        Specular = 128,           //          10000000
        Texture1 = 256,           // 00000001 00000000
        Texture2 = 512,           // 00000010 00000000
        Texture3 = 768,           // 00000011 00000000
        Texture4 = 1024,          // 00000100 00000000
        Texture5 = 1280,          // 00000101 00000000
        Texture6 = 1536,          // 00000110 00000000
        Texture7 = 1792,          // 00000111 00000000
        Texture8 = 2048,          // 00001000 00000000
        TextureCountMask = 3840,  // 00001111 00000000
        LastBetaUByte4 = 4096,    // 00010000 00000000
        PositionW = 16386,        // 01000000 00000010
        PositionMask = 16398,     // 01000000 00001110
        LastBetaD3DColor = 32768, // 10000000 00000000

        size2_0 = 0,               // 
        size3_0 = 65536,           // 00000001 00000000 00000000     
        size4_0 = 131072,          // 00000010 00000000 00000000
        size1_0 = 196608,          // 00000011 00000000 00000000
        size3_1 = 262144,          // 00000100 00000000 00000000
        size4_1 = 524288,          // 00001000 00000000 00000000
        size1_1 = 786432           // 00001100 00000000 00000000
    }
}
