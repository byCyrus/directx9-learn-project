﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Graphics;

namespace Engine.Cdlod
{
    public class Terrain
    {
        Random rnd = new Random();
        GraphicDevice device;

        public BoundaryAABB worldsize;
        public QuadTree quadTree;

        public Terrain(GraphicDevice device, BoundaryAABB TerrainSize )
        {
            this.device = device;
            this.worldsize = TerrainSize;




            Bitmap bmp = new Bitmap(@"..\..\..\canyon.jpg");
            IHeightMap elevations = HeightMap.FromBitmap(bmp);

            Texture heightMap = new Texture(device, bmp, true, 0);
            Texture normalMap = new Texture(device, @"..\..\..\canyon.dds", true, 0);


            //Bitmap bmp = new Bitmap(@"..\..\..\debug.jpg");
            //IHeightMap elevations = HeightMap.FromBitmap(bmp);

            //Texture heightMap = new Texture(device, bmp, true, 0);
            //Texture normalMap = new Texture(device, @"..\..\..\debug.dds", true, 0);

            Texture diffuseMap = new Texture(device, @"..\..\..\Grass.jpg", true, 0);
            Texture rockMap = new Texture(device, @"..\..\..\Rock.jpg", true, 0);
            Engine.Graphics.Font font = new Engine.Graphics.Font(device, Engine.Graphics.Font.FontName.Arial, 8);


            QuadTreeSetting setting = new QuadTreeSetting();
            setting.NumLevels = 4;
            setting.ShowHelpBox = true;
            setting.ForceDrawLeaf = false;

            quadTree = new QuadTree(setting, worldsize, elevations, heightMap, normalMap, diffuseMap, rockMap);
            
            // initialize the static geometries
            quadTree.patch = new QuadMesh(device, 32, 32,false);
            quadTree.patch.shaderProgram = new CdlodShader(device);
            quadTree.patch.shaderProgram.WireframeMode = false;

            quadTree.boxSpline = new BoxHelper(device);
            quadTree.boxSpline.material = TerrainForm.materialP;
            //quadTree.boxSpline.font = font;

            quadTree.cameraSphere = new CameraHelper(device, quadTree.lodCameraRadius);
            quadTree.cameraSphere.shader = TerrainForm.materialP;
        }

        public void Update(Vector3 eye,Frustum frustum)
        {
            quadTree.UpdateTree(eye, frustum);
        }

        public void Draw(ICamera camera)
        {
            quadTree.Draw(camera);
        }
    }

    public class Water
    {
        GraphicDevice device;
        BoundaryAABB TerrainSize;
        WaterShader shader;

        VertexBuffer vertexbuffer;
        VertexDeclaration declaration;

        public Water(GraphicDevice device, BoundaryAABB size)
        {
            this.device = device;
            this.TerrainSize = size;
            this.shader = new WaterShader(device);
            this.shader.WireframeMode = false;

            declaration = new VertexDeclaration(device, VERTEX.m_elements);

            float h = size.min.y + (size.max.y - size.min.y) * 0.1f;

            VERTEX[] vertices = new VERTEX[]
            {
                new VERTEX(size.min.x,h,size.max.z),
                new VERTEX(size.max.x,h,size.max.z),
                new VERTEX(size.min.x,h,size.min.z),

                new VERTEX(size.min.x,h,size.min.z),
                new VERTEX(size.max.x,h,size.max.z),
                new VERTEX(size.max.x,h,size.min.z),
            };


            vertexbuffer = new VertexBuffer(device, declaration.Format, 6, false, true);
            VertexStream data = vertexbuffer.OpenStream();
            data.WriteCollection<VERTEX>(vertices, 0, 6, 0);
            vertexbuffer.CloseStream();
        }

        public void Update(Vector3 eye, Frustum frustum)
        {

        }
        public void Draw(ICamera camera)
        {
            declaration.SetToDevice();
            vertexbuffer.SetToDevice();


            device.renderstates.AlphaBlendEnable = true;
            device.renderstates.ZBufferWriteEnable = false;

            int numpass = shader.Begin();
            for (int pass = 0; pass < numpass; pass++)
            {
                shader.BeginPass(pass);
                shader.ProjViewWorld = camera.Projection * camera.View;
                shader.CameraEye = camera.Eye;
                shader.LightDirection = Vector3.GetNormal(new Vector3(-1, -1, -1));
                device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                shader.EndPass();
            }
            shader.End();

            device.renderstates.AlphaBlendEnable = false;
            device.renderstates.ZBufferWriteEnable = true;

        }
    }
}
