﻿// by johnwhile
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;

namespace Engine.Tools
{
    /// <summary>
    /// Manage the Primitives Points geometries NOT-INDEXED
    /// </summary>
    public class PointGeometry : BaseGeometry
    {
        public VertexAttribute<Vector3> vertices;
        public VertexAttribute<Color32> colors;

        public PointGeometry() : base()
        {
            vertices = null;
            colors = null;
        }
        /// <summary>
        /// Copy from source instance
        /// </summary>
        public PointGeometry(PointGeometry src)
            : base(src)
        {
            vertices = src.vertices;
            colors = src.colors;
        }
        public override PrimitiveType primitive
        {
            get { return PrimitiveType.PointList; }
        }
        public override bool IsIndexed
        {
            get { return false; }
        }
        public override int numVertices
        {
            get { return (vertices != null) ? vertices.Count : 0; }
        }

        public override int numPrimitives
        {
            get { return numVertices; }
        }
        public override int numIndices
        {
            get { return 0; }
        }
        /// <summary>
        /// change transform but vertices will be in the same position
        /// </summary>
        /// <param name="newtransform"></param>
        public override void changeTransform(Matrix4 newtransform)
        {
            Matrix4 matrix = base.transform * Matrix4.Inverse(newtransform);
            if (vertices != null)
                for (int i = 0; i < numVertices; i++)
                    vertices[i] = matrix * vertices[i];
            transform = newtransform;
        }
        /// <summary>
        /// Write all vertex channels to unmanaged buffer (like the graphic buffer) using DeclarationUsage flags to match them
        /// </summary>
        /// <param name="buffer">the destination buffer</param>
        /// <param name="bufferSize">size in bytes of destination buffer</param>
        /// <param name="bufferOffset">offset in bytes of destination buffer where write beginning</param>
        /// <param name="bufferInfo">descriptor of buffer element to understand how write a fragmaented structure</param>
        /// <returns>return false if found some error</returns>
        public bool WriteAttibutesToBuffers(IntPtr buffer, VertexInfo bufferInfo, int bufferSize, int bufferOffset)
        {
            if (vertices != null) vertices.WriteToBuffers(buffer, bufferInfo, bufferSize, bufferOffset);
            if (colors != null) colors.WriteToBuffers(buffer, bufferInfo, bufferSize, bufferOffset);
            return true;
        }
    }
}
