﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

namespace Engine.Cdlod
{
    public class AxisObj
    {
        public MaterialFX_VCOLOR material = null;
        GraphicDevice device;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public Vector3[] axe = new Vector3[3];

        public AxisObj(GraphicDevice device)
        {
            this.device = device;
            this.cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(50,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,20,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,50, Color.Blue)
            };

            axe[0] = verts[1].position;
            axe[1] = verts[3].position;
            axe[2] = verts[5].position;

            axis = new VertexBuffer(device, cvertex_decl.Format, verts.Length, false, true);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public void Draw(ICamera camera)
        {
            cvertex_decl.SetToDevice();
            axis.SetToDevice();

            if (material != null)
            {
                material.ProjViewWorld = camera.Projection * camera.View * Matrix4.Identity;
                int count = material.Begin();
                for (int pass = 0; pass < count; pass++)
                {
                    material.BeginPass(pass);
                    device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                    material.EndPass();
                }
                material.End();
            }
            device.renderstates.projection = camera.Projection;
            device.renderstates.view = camera.View;
            device.renderstates.world = Matrix4.Identity;
            device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
        }
    }

    public class FrustumObj
    {
        GraphicDevice m_device;
        VertexDeclaration cvertex_decl;
        VertexBuffer frustumV;
        IndexBuffer frustumE;
        Frustum m_frustum;


        public Frustum frustum
        {
            get { return m_frustum; }
            set { m_frustum = value; RenderFrustum(); }
        }

        public FrustumObj(GraphicDevice device)
        {
            m_device = device;
            cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);
            frustumV = new VertexBuffer(device, cvertex_decl.Format, 10, false, true);
            frustumE = new IndexBuffer(device, IndexInfo.IndexEdge16, 13, false, true);
        }


        void RenderFrustum()
        {
            Edge16[] edges;
            CVERTEX[] verts;
            m_frustum.Render(out verts, out edges);

            VertexStream vdata = frustumV.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            frustumV.CloseStream();
            frustumV.Count = verts.Length;

            IndexStream idata = frustumE.OpenStream();
            idata.WriteCollection<Edge16>(edges, 0, edges.Length, 0);
            frustumE.CloseStream();
            frustumE.Count = edges.Length;
        }

        public void Draw()
        {
            cvertex_decl.SetToDevice();
            m_device.renderstates.lightEnable = false;
            if (frustumV.Count > 0)
            {
                cvertex_decl.SetToDevice();
                frustumV.SetToDevice();
                frustumE.SetToDevice();
                m_device.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, frustumV.Count, 0, frustumE.Count);
            }
        }
    }


    public class PlayerObj
    {
        GraphicDevice device;
        public MaterialFX_VCOLOR material = null;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;
        Matrix4 transform;


        public PlayerObj(GraphicDevice device)
        {
            this.device = device;
            this.cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(1,-2,1, Color.Green),
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(1,-2,-1, Color.Green),
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(-1,-2,-1, Color.Green),
                new CVERTEX(0,0,0, Color.Black),
                new CVERTEX(-1,-2,1, Color.Green),
            };
            axis = new VertexBuffer(device, cvertex_decl.Format, verts.Length, false, true);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;

            transform = Matrix4.Identity;
        }

        public Vector3 PlayerPos
        {
            get { return transform.TranslationComponent; }
            set { transform.TranslationComponent = value; }
        }

        public void Draw(ICamera camera)
        {
            cvertex_decl.SetToDevice();
            axis.SetToDevice();

            if (material != null)
            {
                material.ProjViewWorld = camera.Projection * camera.View * transform;
                int count = material.Begin();
                for (int pass = 0; pass < count; pass++)
                {
                    material.BeginPass(pass);
                    device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                    material.EndPass();
                }
                material.End();
            }
            else
            {
                device.renderstates.projection = camera.Projection;
                device.renderstates.view = camera.View;
                device.renderstates.world = transform;
                device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
            }
        }

    }


    /// <summary>
    /// Drawable box in units size
    /// </summary>
    public class BoxHelper
    {
        GraphicDevice device;
        VertexBuffer vb;
        VertexDeclaration decl;
        LineListGeometry box = (LineListGeometry)BaseLineGeometry.BoxGizmo(1, 1, 1);
        Matrix4 Proj;
        Matrix4 View;

        public MaterialFX_POSITION material = null;
        public Engine.Graphics.Font font = null;

        public BoxHelper(GraphicDevice device)
        {
            this.device = device;
            decl = new VertexDeclaration(device, VERTEX.m_elements);

            vb = new VertexBuffer(device, decl.Format, box.numVertices, false, true);
            VertexStream data = vb.OpenStream();
            data.WriteCollection<Vector3>(box.vertices, 0, box.numVertices, 0);
            vb.CloseStream();
        }

        public void SetToDevice(ICamera camera)
        {
            if (material != null)
            {
                // precompute a part of WVP matrix
                Proj = camera.Projection;
                View = camera.View;
            }
            else
            {
                device.renderstates.projection = camera.Projection;
                device.renderstates.view = camera.View;
            }
            decl.SetToDevice();
            vb.SetToDevice();
        }
        public void Draw(Matrix4 world, Color32 vcolor)
        {
            if (material != null)
            {
                material.ProjViewWorld = Proj * View * world;
                material.Diffuse = vcolor;
            }
            else
            {
                device.renderstates.world = world;
            }
            device.DrawPrimitives(box.primitive, 0, box.numPrimitives);
        }
    }


    public class MaterialFX_trasparent : MaterialFX
    {
        public MaterialFX_trasparent(GraphicDevice device)
            : base(device, Engine.Cdlod.Properties.Resources.QuadMap)
        {
            effect.Parameters.Add("Technique", new EffectAttributeTechnique("WireframeTechique2"));
            effect.Parameters.Add("Transform", new EffectAttributeMatrix(Matrix4.Identity, "WorldViewProj"));
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }

        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
    }

    public class CameraHelper
    {
        public MaterialFX_POSITION shader;
        GraphicDevice device;
        VertexBuffer vb;
        VertexDeclaration decl;
        IndexBuffer ib;


        float[] range;
        Color32[] colors;

        public CameraHelper(GraphicDevice device, float[] range)
        {
            this.device = device;
            MeshListGeometry usphere = BaseTriGeometry.Sphere(10, 10, 1);
            this.range = range;

            decl = new VertexDeclaration(device, VERTEX.m_elements);

            vb = new VertexBuffer(device, decl.Format, usphere.numVertices, false, true);
            VertexStream data = vb.OpenStream();
            data.WriteCollection<Vector3>(usphere.vertices, 0, usphere.numVertices, 0);
            vb.CloseStream();
            vb.Count = usphere.numVertices;

            ib = new IndexBuffer(device, IndexInfo.IndexFace16, usphere.numPrimitives, false, true);
            IndexStream idata = ib.OpenStream();
            idata.WriteCollection<Face16>(usphere.indices, 0, 0);
            ib.CloseStream();
            ib.Count = usphere.numPrimitives;

            this.colors = new Color32[range.Length];
            for (int level = 0; level < range.Length; level++)
                colors[level] = Color32.Rainbow((float)level / range.Length);
        }

        public void Draw(ICamera camera, Vector3 position)
        {
            decl.SetToDevice();
            vb.SetToDevice();
            ib.SetToDevice();

            device.renderstates.cullMode = Cull.Clockwise;

            int numpass = shader.Begin();

            for (int pass = 0; pass < numpass; pass++)
            {
                shader.BeginPass(pass);


                for (int level = 0 ; level<range.Length;level++)
                {
                    //if (level != range.Length-2) continue;

                    float r = range[level];

                    Matrix4 transform = new Matrix4(
                    r, 0, 0, position.x,
                    0, r, 0, position.y,
                    0, 0, r, position.z,
                    0, 0, 0, 1);
                    shader.ProjViewWorld = camera.Projection * camera.View * transform;
                    shader.Diffuse = colors[level];
                    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vb.Count, 0, ib.Count);
                }
                shader.EndPass();
            }
            shader.End();
            device.renderstates.cullMode = Cull.CounterClockwise;
        }
    }
}
