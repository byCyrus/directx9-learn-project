﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Engine.Maths;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

namespace Engine.Graphics
{
    /// <summary>
    /// MicrosoftDx
    /// </summary>
    public class DX9EffectHandle
    {
        internal D3D.EffectHandle handle;
        string name;
        public DX9EffectHandle(string name)
        {
            handle = D3D.EffectHandle.FromString(name);
            this.name = name;
        }
        public override string ToString()
        {
            return name;
        }
    }
    /// <summary>
    /// MicrosoftDx
    /// </summary>
    public class DX9Effect : IDisposable
    {
#if DEBUG
        D3D.ShaderFlags debug = D3D.ShaderFlags.Debug;
#else
        D3D.ShaderFlags debug = D3D.ShaderFlags.None;
#endif
        D3D.Effect m_effect;
        D3D.Device m_device;

        public DX9Effect(DX9Device device, string shadercode)
        {
            m_device = device.m_device;
            string errors ;
            m_effect = D3D.Effect.FromString(m_device, shadercode, null, debug, null, out errors);

            if (!String.IsNullOrEmpty(errors))
                throw new Exception(errors.ToString());
        }
        public void SetValue(DX9EffectHandle handle, DX9Texture texture)
        {
            m_effect.SetValue(handle.handle, texture.m_texture);
        }
        public void SetValue(DX9EffectHandle handle, Matrix4 matrix)
        {
            m_effect.SetValue(handle.handle, Helper.Convert(matrix));
        }
        public void SetValue(DX9EffectHandle handle, Vector4 vector)
        {
            m_effect.SetValue(handle.handle, Helper.Convert(vector));
        }
        public void SetValue(DX9EffectHandle handle, Vector3 vector)
        {
            m_effect.SetValue(handle.handle, new float[] { vector.x, vector.y, vector.z });
            //setStruct<Vector3>(handle.handle, vector);
        }
        public void SetValue(DX9EffectHandle handle, float value)
        {
            m_effect.SetValue(handle.handle, value);
        }
        public void SetValue(DX9EffectHandle handle, int value)
        {
            m_effect.SetValue(handle.handle, value);
        }
        
        
        public void CommitChanges()
        {
            m_effect.CommitChanges();
        }

        public string Technique
        {
            get { return m_effect.Technique.ToString(); }
            set { m_effect.Technique = value; }
        }
        public int Begin(bool savestates)
        {
            return m_effect.Begin(savestates ? D3D.FX.None : D3D.FX.DoNotSaveState);
        }
        public void End()
        {
            m_effect.End();
        }
        public void BeginPass(int i)
        {
            m_effect.BeginPass(i);
        }
        public void EndPass()
        {
            m_effect.EndPass();
        }

        void setStruct<T>(D3D.EffectHandle handle, T value) where T :struct
        {
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(T)));
            Marshal.StructureToPtr(value, ptr, true);
            int size = Marshal.SizeOf(value);
            unsafe
            {
                m_effect.SetValue(handle, ptr.ToPointer(), size);
            }
        }
        public void Dispose()
        {
            m_effect.Dispose();
        }
    }
}
