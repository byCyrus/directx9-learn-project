﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Renderer;
using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Scene
{
    /// <summary>
    /// Sort node in the scene using Octree methods, can improve picking and collision when node are
    /// well partitioned into octree... but work bad when node are too big and aren't completly inside
    /// octree. The ray start to intersect the octrees with traversal raytracing algorithm, then use a
    /// brutal force interection for all nodes inside it.
    /// </summary>
    public class SceneManagerOctree : SceneManagerBasic
    {
        Octree root = null;
        /// <summary>
        /// Need the range of world and level of suddivisions
        /// </summary>
        public SceneManagerOctree(Vector3 Max, Vector3 Min, int MaxLevel) : base()
        {
            Octree.MAXLEVEL = MaxLevel;
            root = new Octree(Min, Max, 0);
        }

        public override void AddSceneNode(ISceneNode node)
        {
            base.AddSceneNode(node);
            root.InsertObj(node);
        }


        #region Picking
        // Get minimum scene node list what ray CAN hit
        List<ISceneNode> minimizedNodesList = new List<ISceneNode>();
        /// <summary>
        /// Generate a minimizedNodesList where there are all possible collisions
        /// </summary>
        void WalkToOctree(Ray ray , Octree bucket, bool firstHit)
        {
            // append parent nodes list
            if (bucket.nodesList.Count > 0)
            {
                minimizedNodesList.AddRange(bucket.nodesList);
            }

            List<Octree> children = bucket.GetChildren();
            if (children == null) return;

            foreach (Octree child in children)
            {
                WalkToOctree(ray, child, firstHit);
            }
        }


        public override ISceneNode PickSceneNode(Ray ray, bool firstHit)
        {
            root.UnSelectAll();

            bool insideWorld = root.ProcessRay(ray);
            if (!insideWorld) return null;

            minimizedNodesList.Clear();
            WalkToOctree(ray, root, firstHit);
            // found only empty octree
            if (minimizedNodesList.Count == 0) return null;

            if (firstHit)
            {
                return minimizedNodesList[0];
            }
            else
            {
                //TODO: Make iteration select when firstHit is false
            }
            return null;
        }
        
        #endregion


        /// <summary>
        /// Draw to panel the scene using your custom options 
        /// </summary>
        public override void Render(GraphicPanel panel, ViewportOption options)
        {
            if (renderer == null) return;

            renderer.Draw();

            foreach (ISceneNode node in Nodes)
            {
                Point pos = panel.WorldToPoint(node.transform.TranslationComponent);
                renderer.fontDx.Draw(node.name.ToString(), pos.X, pos.Y, Color.Black);
            }
            //LineBoxInstance.SetBuffer(m_device);
            //root.DrawBox(m_device);
        }
        public override void ToConsole()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            StringBuilder str = new StringBuilder("Octree partitioning\n");
            str.Append(root.ToString());
            Console.WriteLine(str.ToString());
            Console.ResetColor();
        }
        public override void Dispose()
        {
            base.Dispose();
            root = null;
        }
    }

    /// <summary>
    /// Define the Index of children quads for fast access to child[] array
    /// </summary>
    internal enum eOctIndex : sbyte
    {
        /*        3--------7
         *       /:       /|
         *      2--------6 |
         *      | :      | |      Y
         *      | 1......|.5      | Z
         *      |;       |/       |/
         *      0--------4        *----- X
         */
        /// <summary>"-1 undefined"</summary>
        None = -1,
        /// <summary>"0 left down front"</summary>
        LDF = 0,
        /// <summary>"1 left down back"</summary>
        LDB = 1,
        /// <summary>"2 left top front"</summary>
        LTF = 2,
        /// <summary>"3 left top back"</summary>
        LTB = 3,
        /// <summary>"4 rigth down front"</summary>
        RDF = 4,
        /// <summary>"5 rigth top front"</summary>
        RDB = 5,
        /// <summary>"6 right down back"</summary>   
        RTF = 6,
        /// <summary>"7 right top back"</summary>
        RTB = 7
    }

    /// <summary>
    /// Is the Box
    /// </summary>
    internal class Octree
    {
        /// <summary>
        /// Safety limit of partitioning
        /// </summary>
        public static int MAXLEVEL = 2;
        /// <summary>
        /// 0x100 = X , 0x010 = Y  , 0x001 = Z -> 1:Z 2:Y 3:YZ 4:X 5:XZ 6:XY 7:XYZ 
        /// </summary>
        static byte mainRayFlag = 0x000;
        /// <summary>
        /// store the ray into global value for quick access
        /// </summary>
        static Ray mainRay;

        public double tx0, ty0, tz0, tx1, ty1, tz1;



        static bool firstHit = true; // if you want select back node do a loop of all nodes hit by ray
        

        bool terminal = true; //last box ?
        byte level = 0; // usually < 10
        sbyte index = (sbyte)eOctIndex.None; //get the type of octant 
        Vector3 min = Vector3.Zero;
        Vector3 max = Vector3.Zero;

        /// <summary>
        /// store the pointer of nodes, in C# the class are pass like reference
        /// </summary>
        public List<ISceneNode> nodesList = new List<ISceneNode>(0); 
        
        Octree[] children = new Octree[8];

        bool select = false;

        /// <summary>
        /// If Min and Max values isn't very vital,if scenenode exit from octree it will be put outsize
        /// </summary>
        /// <param name="Min"></param>
        /// <param name="Max"></param>
        public Octree(Vector3 Min, Vector3 Max, byte mlevel, eOctIndex eindex)
        {
            min = Min;
            max = Max;
            level = mlevel;

            /*
            if (mlevel < MAXLEVEL)
                for (sbyte i = 0; i < 8; i++)
                    this.children[i] = InitializeChild(i);
            */
        }
        public Octree(Vector3 Min, Vector3 Max, byte mlevel) : this(Min, Max, mlevel, eOctIndex.None) { }


        /// <summary>
        /// Initialize a new instance of octree only if are required, else return null
        /// </summary>
        Octree InitializeChild(sbyte i)
        {
            terminal = false;
            Vector3 mid = (max + min) * 0.5f;
            Octree child;
            switch (i)
            {
                case 0: child = new Octree(new Vector3(min.x, min.y, min.z), new Vector3(mid.x, mid.y, mid.z), (byte)(level + 1)); break;
                case 1: child = new Octree(new Vector3(min.x, min.y, mid.z), new Vector3(mid.x, mid.y, max.z), (byte)(level + 1)); break;
                case 2: child = new Octree(new Vector3(min.x, mid.y, min.z), new Vector3(mid.x, max.y, mid.z), (byte)(level + 1)); break;
                case 3: child = new Octree(new Vector3(min.x, mid.y, mid.z), new Vector3(mid.x, max.y, max.z), (byte)(level + 1)); break;
                case 4: child = new Octree(new Vector3(mid.x, min.y, min.z), new Vector3(max.x, mid.y, mid.z), (byte)(level + 1)); break;
                case 5: child = new Octree(new Vector3(mid.x, min.y, mid.z), new Vector3(max.x, mid.y, max.z), (byte)(level + 1)); break;
                case 6: child = new Octree(new Vector3(mid.x, mid.y, min.z), new Vector3(max.x, max.y, mid.z), (byte)(level + 1)); break;
                case 7: child = new Octree(new Vector3(mid.x, mid.y, mid.z), new Vector3(max.x, max.y, max.z), (byte)(level + 1)); break;
                default: return null;
            }
            child.index = i;
            return child;
        }



        public void SetRayParameters(double Tx0, double Ty0, double Tz0, double Tx1, double Ty1, double Tz1)
        {
            tx0 = Tx0;
            ty0 = Ty0;
            tz0 = Tz0;
            tx1 = Tx1;
            ty1 = Ty1;
            tz1 = Tz1;
        }
        /// <summary>
        /// Before all operation with all octree set the generalized ray calculation to improve speed
        /// </summary>
        /// <param name="ray">your original ray</param>
        /// <returns>return false if ray is ouside octree and all other process arent' necessary</returns>
        public bool ProcessRay(Ray ray)
        {
            if (ray.isEmpty) return false;

            mainRay = ray;
            Ray generalizedRay = ray; // struct are copied by value not by reference

            mainRayFlag = 0;
            if (ray.dir.x < 0)
            {
                generalizedRay.orig.x = this.max.x + this.min.x - ray.orig.x;
                generalizedRay.dir.x = -ray.dir.x;
                mainRayFlag |= 4; //bitwise OR (latest bits are XYZ)
            }
            if (ray.dir.y < 0)
            {
                generalizedRay.orig.y = this.max.y + this.min.y - ray.orig.y;
                generalizedRay.dir.y = -ray.dir.y;
                mainRayFlag |= 2;
            }
            if (ray.dir.z < 0)
            {
                generalizedRay.orig.z= this.max.z + this.min.z - ray.orig.z;
                generalizedRay.dir.z = -ray.dir.z;
                mainRayFlag |= 1;
            }

            double divx = 1 / generalizedRay.dir.x; // IEEE stability fix
            double divy = 1 / generalizedRay.dir.y;
            double divz = 1 / generalizedRay.dir.z;

            tx0 = (this.min.x - generalizedRay.orig.x) * divx;
            ty0 = (this.min.y - generalizedRay.orig.y) * divy;
            tz0 = (this.min.z - generalizedRay.orig.z) * divz;
            tx1 = (this.max.x - generalizedRay.orig.x) * divx;
            ty1 = (this.max.y - generalizedRay.orig.y) * divy;
            tz1 = (this.max.z - generalizedRay.orig.z) * divz;

            double tmin = Math.Max(Math.Max(tx0, ty0), tz0);
            double tmax = Math.Min(Math.Min(tx1, ty1), tz1);

            // if tmin > tmax where aren't collision with current octree;
            return (tmin < tmax);
        }


        /// <summary>
        /// Get all children hit by ray, else return null
        /// </summary>
        public List<Octree> GetChildren()
        {
            // a ray can hit maximum 4 child
            List<Octree> raychild = new List<Octree>(4);

            if (mainRay.isEmpty)
            {
                throw new Exception("generalized ray paramaters not initialized, call ProcessRay() before all operations");
            }
            if (terminal)
            {
                return null;
            }
            double txm = 0.5 * (tx0 + tx1);
            double tym = 0.5 * (ty0 + ty1);
            double tzm = 0.5 * (tz0 + tz1);

            int nextOctantId = GetFirstChild(tx0, ty0, tz0, txm, tym, tzm);
            do
            {
                // This process add all Children hit by ray
                Octree currOctant = null;
                double a = 0;
                double b = 0;
                double c = 0;
                double d = 0;
                double e = 0;
                double f = 0;

                /* need test this fucntion : */
                //currOctant = this.children[nextOctantId ^ mirrorFlag];
                switch (nextOctantId)
                {
                    case 0:
                        currOctant = this.children[mirrorFlag];
                        a = tx0; b = ty0; c = tz0; d = txm; e = tym; f = tzm;
                        nextOctantId = GetNeighboring(txm, tym, tzm, 4, 2, 1);
                        break;
                    case 1:
                        currOctant = this.children[1 ^ mainRayFlag];
                        a = tx0; b = ty0; c = tzm; d = txm; e = tym; f = tz1;
                        nextOctantId = GetNeighboring(txm, tym, tz1, 5, 3, 8);
                        break;
                    case 2:
                        currOctant = this.children[2 ^ mainRayFlag];
                        a = tx0; b = tym; c = tz0; d = txm; e = ty1; f = tzm;
                        nextOctantId = GetNeighboring(txm, ty1, tzm, 6, 8, 3);
                        break;
                    case 3:
                        currOctant = this.children[3 ^ mainRayFlag];
                        a = tx0; b = tym; c = tzm; d = txm; e = ty1; f = tz1;
                        nextOctantId = GetNeighboring(txm, ty1, tz1, 7, 8, 8);
                        break;
                    case 4:
                        currOctant = this.children[4 ^ mainRayFlag];
                        a = txm; b = ty0; c = tz0; d = tx1; e = tym; f = tzm;
                        nextOctantId = GetNeighboring(tx1, tym, tzm, 8, 6, 5);
                        break;
                    case 5:
                        currOctant = this.children[5 ^ mainRayFlag];
                        a = txm; b = ty0; c = tzm; d = tx1; e = tym; f = tz1;
                        nextOctantId = GetNeighboring(tx1, tym, tz1, 8, 7, 8);
                        break;
                    case 6:
                        currOctant = this.children[6 ^ mainRayFlag];
                        a = txm; b = tym; c = tz0; d = tx1; e = ty1; f = tzm;
                        nextOctantId = GetNeighboring(tx1, ty1, tzm, 8, 8, 7);
                        break;
                    case 7:
                        currOctant = this.children[7 ^ mainRayFlag];
                        a = txm; b = tym; c = tzm; d = tx1; e = ty1; f = tz1;
                        nextOctantId = 8;
                        break;
                }
                if (nextOctantId < 8)
                {
                    if (currOctant != null)
                    {
                        currOctant.select = true;
                        currOctant.SetRayParameters(a, b, c, d, e, f);
                        raychild.Add(currOctant);
                    }
                }
            } while (nextOctantId < 8);

            return raychild;
        }


        /// Are all static properties because are indipendent functions
        #region Octree Traversal RayTracing algorithm

        /// <summary>
        /// 0x100 = x , 0x010 = Y  , 0x001 = Z -> 1:Z 2:Y 3:YZ 4:x 5:xZ 6:xY 7:xYZ 
        /// </summary>
        static byte mirrorFlag = 0x000;

        /// <summary>
        /// Get the first child box hit by ray (with all direction componens positives)
        /// </summary>
        static int GetFirstChild(double tx0, double ty0, double tz0, double txm, double tym, double tzm)
        {
            byte answer = 0;
            // select the entry plane and set bits
            if (tx0 > ty0)
            {
                if (tx0 > tz0)
                {   // PLANE YZ
                    if (tym < tx0) answer |= 2;    // set bit at position 1
                    if (tzm < tx0) answer |= 1;    // set bit at position 0
                    return (int)answer;
                }
            }
            else
            {
                if (ty0 > tz0)
                {   // PLANE XZ
                    if (txm < ty0) answer |= 4;    // set bit at position 2
                    if (tzm < ty0) answer |= 1;    // set bit at position 0
                    return (int)answer;
                }
            }
            // PLANE XY
            if (txm < tz0) answer |= 4;    // set bit at position 2
            if (tym < tz0) answer |= 2;    // set bit at position 1
            return (int)answer;
        }

        /// <summary>
        /// Get the next neighboring node comparing the Min(x1,y1,z1),
        /// Min = X -> exit plane YZ
        /// Min = Y -> exit plane XY
        /// Min = Z -> exit plane XY
        /// </summary>
        /// <param name="yz">neighboring octree when ray exit to YZ plane</param>
        /// <param name="xz">neighboring octree when ray exit to XZ plane</param>
        /// <param name="xy">neighboring octree when ray exit to XY plane</param>
        static int GetNeighboring(double txm, double tym, double tzm, int yz, int xz, int xy)
        {
            if (txm < tym)
            {
                if (txm < tzm) return yz;  // YZ plane
            }
            else
            {
                if (tym < tzm) return xz; // XZ plane
            }
            return xy; // XY plane;
        }

        static ISceneNode ProcessSubtree(double tx0, double ty0, double tz0, double tx1, double ty1, double tz1, Octree octree)
        {
            if (octree == null) return null;

            double txm, tym, tzm;
            int currNode;
            ISceneNode obj = null;

            if (tx1 < 0 || ty1 < 0 || tz1 < 0) return null;


            //Console.WriteLine("process otree " + octree.level + " " + octree.id.ToString());
            octree.select = true;

            if (obj != null)
            {
                //Console.WriteLine("HIT : " + obj.ToString());
                return obj;
            }

            if (octree.terminal)
            {
                //Reached leaf node
                //Console.WriteLine("octree " + octree.level + " is terminal");
                return null;
            }
            else
            {
                // Reached node
            }
            txm = 0.5 * (tx0 + tx1);
            tym = 0.5 * (ty0 + ty1);
            tzm = 0.5 * (tz0 + tz1);
            currNode = GetFirstChild(tx0, ty0, tz0, txm, tym, tzm);
            do
            {
                // currNode 8 == EXIT
                switch (currNode)
                {
                    case 0:
                        obj = ProcessSubtree(tx0, ty0, tz0, txm, tym, tzm, octree.children[mirrorFlag]);
                        currNode = GetNeighboring(txm, tym, tzm, 4, 2, 1);
                        break;
                    case 1:
                        obj = ProcessSubtree(tx0, ty0, tzm, txm, tym, tz1, octree.children[1 ^ mirrorFlag]);
                        currNode = GetNeighboring(txm, tym, tz1, 5, 3, 8);
                        break;
                    case 2:
                        obj = ProcessSubtree(tx0, tym, tz0, txm, ty1, tzm, octree.children[2 ^ mirrorFlag]);
                        currNode = GetNeighboring(txm, ty1, tzm, 6, 8, 3);
                        break;
                    case 3:
                        obj = ProcessSubtree(tx0, tym, tzm, txm, ty1, tz1, octree.children[3 ^ mirrorFlag]);
                        currNode = GetNeighboring(txm, ty1, tz1, 7, 8, 8);
                        break;
                    case 4:
                        obj = ProcessSubtree(txm, ty0, tz0, tx1, tym, tzm, octree.children[4 ^ mirrorFlag]);
                        currNode = GetNeighboring(tx1, tym, tzm, 8, 6, 5);
                        break;
                    case 5:
                        obj = ProcessSubtree(txm, ty0, tzm, tx1, tym, tz1, octree.children[5 ^ mirrorFlag]);
                        currNode = GetNeighboring(tx1, tym, tz1, 8, 7, 8);
                        break;
                    case 6:
                        obj = ProcessSubtree(txm, tym, tz0, tx1, ty1, tzm, octree.children[6 ^ mirrorFlag]);
                        currNode = GetNeighboring(tx1, ty1, tzm, 8, 8, 7);
                        break;
                    case 7:
                        obj = ProcessSubtree(txm, tym, tzm, tx1, ty1, tz1, octree.children[7 ^ mirrorFlag]);
                        currNode = 8;
                        break;
                }

                if (obj != null) return obj;

            } while (currNode < 8);

            return null;
        }

        #endregion

#if DEBUG
        public void UnSelectAll()
        {
            select = false;
            foreach (Octree octree in children)
                if (octree != null) octree.UnSelectAll();
        }
#endif
        /// <summary>
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="loop"></param>
        public ISceneNode GetNextObj(Ray ray, bool FirstHit)
        {
#if DEBUG
            UnSelectAll();
#endif
            mainRay = ray;
            firstHit = FirstHit;
            Ray generalizedRay = ray; // struct are copied by value not by reference

            mirrorFlag = 0;
            if (ray.dir.x < 0)
            {
                generalizedRay.orig.x = this.max.x + this.min.x - ray.orig.x;
                generalizedRay.dir.x = -ray.dir.x;
                mirrorFlag |= 4; //bitwise OR (latest bits are XYZ)
            }
            if (ray.dir.y < 0)
            {
                generalizedRay.orig.y = this.max.y + this.min.y - ray.orig.y;
                generalizedRay.dir.y = -ray.dir.y;
                mirrorFlag |= 2;
            }
            if (ray.dir.z < 0)
            {
                generalizedRay.orig.z = this.max.z + this.min.z - ray.orig.z;
                generalizedRay.dir.z = -ray.dir.z;
                mirrorFlag |= 1;
            }

            double divx = 1 / generalizedRay.dir.x; // IEEE stability fix
            double divy = 1 / generalizedRay.dir.y;
            double divz = 1 / generalizedRay.dir.z;

            double tx0 = (this.min.x - generalizedRay.orig.x) * divx;
            double ty0 = (this.min.y - generalizedRay.orig.y) * divy;
            double tz0 = (this.min.z - generalizedRay.orig.z) * divz;
            double tx1 = (this.max.x - generalizedRay.orig.x) * divx;
            double ty1 = (this.max.y - generalizedRay.orig.y) * divy;
            double tz1 = (this.max.z - generalizedRay.orig.z) * divz;

            double tmin = Math.Max(Math.Max(tx0, ty0), tz0);
            double tmax = Math.Min(Math.Min(tx1, ty1), tz1);

            //Console.WriteLine("START RAY PROCESS " + generalizedRay.ToString());

            if (tmin < tmax)
            {
                return ProcessSubtree(tx0, ty0, tz0, tx1, ty1, tz1, this);
            }
            return null;
        }

        /// <summary>
        /// The goal of algorithm: insert the polygon (or its semplified volume) into discrete
        /// </summary>
        public void InsertObj(ISceneNode obj)
        {
            eOctIndex qi = eOctIndex.None;

            if (this.level < Octree.MAXLEVEL)
            {
                Vector3 center = (max + min) * 0.5f;
                Vector3 omax = obj.boundAABB.max;
                Vector3 omin = obj.boundAABB.min;

                bool left = omax.x <= center.x;
                bool right = omin.x >= center.x;

                bool down = omax.y <= center.y;
                bool top = omin.y >= center.y;

                bool front = omax.z <= center.z;
                bool back = omin.z >= center.z;

                if (left)
                {
                    if (top)
                    {
                        if (front) qi = eOctIndex.LTF;
                        else if (back) qi = eOctIndex.LTB;
                    }
                    else if (down)
                    {
                        if (front) qi = eOctIndex.LDF;
                        else if (back) qi = eOctIndex.LDB;
                    }
                }
                else if (right)
                {
                    if (top)
                    {
                        if (front) qi = eOctIndex.RTF;
                        else if (back) qi = eOctIndex.RTB;
                    }
                    else if (down)
                    {
                        if (front) qi = eOctIndex.RDF;
                        else if (back) qi = eOctIndex.RDB;
                    }
                }
            }

            //Console.WriteLine(obj.name.ToString() + " --> " + qi.ToString());
            // the bounding box of polygon is too big and can't be stored into children octree
            // set into nodes list
            if (qi == eOctIndex.None)
            {
                nodesList.Add(obj);
            }
            // else insert into calculated quad's child
            else
            {
                sbyte i = (sbyte)qi;
                Octree child = children[i];
                if (child == null) child = InitializeChild(i); // initialize when is necessary
                child.InsertObj(obj);
                children[i] = child;
            }
        }


        public override string ToString()
        {
            String str = "Octree id:" + index + " level:" + level + "\n";
            //str += "min " + min.x + " " + min.y + " " + min.z + "\n";
            //str += "max " + max.x + " " + max.y + " " + max.z + "\n";

            foreach (ISceneNode node in nodesList)
                str += " " + node.ToString() + "\n";

            str += "\n";
            foreach (Octree octree in children)
                if (octree != null) str += octree.ToString();

            return str;
        }

        public void DrawBox(GraphicDevice device)
        {
            Matrix4 transform = Matrix4.Scaling(max - min) * Matrix4.Translating(min);
            //LineBoxInstance.DrawIstance(device, transform, select);

            foreach (Octree octree in children)
                if (octree != null) octree.DrawBox(device);
        }
    }
}
