﻿using System.Windows.Forms;
namespace Engine.Test
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkbtn = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // checkbtn
            // 
            this.checkbtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkbtn.AutoSize = true;
            this.checkbtn.Location = new System.Drawing.Point(12, 521);
            this.checkbtn.Name = "checkbtn";
            this.checkbtn.Size = new System.Drawing.Size(101, 23);
            this.checkbtn.TabIndex = 0;
            this.checkbtn.Text = "TrackBall Camera";
            this.checkbtn.UseVisualStyleBackColor = true;
            this.checkbtn.CheckedChanged += new System.EventHandler(this.checkbtn_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 556);
            this.Controls.Add(this.checkbtn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public CheckBox checkbtn;
    }
}

