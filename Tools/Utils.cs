﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

namespace Engine.Tools
{
    [Flags]
    public enum eAxis : byte
    {
        None = 0x00,
        X = 0x01,
        Y = 0x02,
        Z = 0x04,
        XY = X | Y,
        XZ = X | Z,
        YZ = Y | Z
    }



    public static class Tool
    {

        /// <summary>
        /// return n^2;
        /// </summary>
        public static int Pow2(int n)
        {
            return n << 1;
        }

        public static bool GetFlag(Byte value, Byte flag) { return (value & flag) != 0; }

        public static void SetFlag(ref Byte value, Byte flag, bool flagval)
        {
            if (flagval) value |= flag;
            else value &= (byte)~flag;
        }

        /// <summary>
        /// Swap to values, val1 and val2 can be from array
        /// </summary>
        public static void Swap<T>(ref T val1, ref T val2)
        {
            T tmp = val1;
            val1 = val2;
            val2 = tmp;
        }
        /// <summary>
        /// Swap to values for a list class
        /// </summary>
        public static void Swap<T>(List<T> list, int i, int j)
        {
            T temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }

        public static string GetBinaryString(int n)
        {
            char[] b = new char[32];
            int pos = 31;
            int i = 0;
            while (i < 32)
            {
                b[pos] = ((n & (1 << i)) != 0) ? '1' : '0';
                pos--;
                i++;
            }
            return new string(b);
        }
        public static string GetBinaryString(short n)
        {
            char[] b = new char[16];
            int pos = 15;
            int i = 0;
            while (i < 16)
            {
                b[pos] = ((n & (1 << i)) != 0) ? '1' : '0';
                pos--;
                i++;
            }
            return new string(b);
        }
        public static string GetBinaryString(byte n)
        {
            char[] b = new char[8];
            int pos = 7;
            int i = 0;
            while (i < 8)
            {
                b[pos] = ((n & (1 << i)) != 0) ? '1' : '0';
                pos--;
                i++;
            }
            return new string(b);
        }

        public static object RawDeserialize(byte[] rawData, int position, Type anyType)
        {
            int rawsize = Marshal.SizeOf(anyType);
            if (rawsize > rawData.Length)
                return null;
            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.Copy(rawData, position, buffer, rawsize);
            object retobj = Marshal.PtrToStructure(buffer, anyType);
            Marshal.FreeHGlobal(buffer);
            return retobj;
        }

        public static byte[] RawSerialize(object anything)
        {
            int rawSize = Marshal.SizeOf(anything);
            IntPtr buffer = Marshal.AllocHGlobal(rawSize);
            Marshal.StructureToPtr(anything, buffer, false);
            byte[] rawDatas = new byte[rawSize];
            Marshal.Copy(buffer, rawDatas, 0, rawSize);
            Marshal.FreeHGlobal(buffer);
            return rawDatas;
        }
    }
}
