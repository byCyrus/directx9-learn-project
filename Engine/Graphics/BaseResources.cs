﻿using System;
using System.Collections.Generic;
using System.Text;
using Engine.Tools;

namespace Engine.Graphics
{
    public delegate void DeviceEventHandler(GraphicDevice device);

    public abstract class DeviceResource : DisposableResource
    {
        public event DeviceEventHandler OnDeviceLost;
        public event DeviceEventHandler OnDeviceReset;
    }
}
