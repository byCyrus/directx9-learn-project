﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using Engine.Maths;
using System.Diagnostics;

namespace Engine.Graphics
{
    /// <summary>
    /// Basic decriptor of a singular element used in the buffer. Can't store a type
    /// because vertices can be made using a custom format, without using a specified struct type
    /// </summary>
    public abstract class BufferInfo
    {
        /// <summary>
        /// The type of struct used to build buffer. In VertexBuffer can be null if buffer are made
        /// using only VertexElements
        /// </summary>
        public Type type { get; protected set; }
        /// <summary>
        /// Size in bytes of buffer element struct
        /// </summary>
        public int bytesize { get; protected set; }
    }

    /// <summary>
    /// Descriptor of vertex structure used in a vertex buffer, are composed by a list of <seealso cref="VertexElement"/>
    /// </summary>
    public class VertexInfo : BufferInfo
    {
        int m_streamCount = 0;
        /// <summary>
        /// vertex elements list are in correct order, VertexDeclarationEnd are omited
        /// </summary>
        public List<VertexElement> Elements { get; private set; }

        // Because DeclarationUsage can be used only once, are unique for all attribute.
        // TODO : check if these is true also when implement geometric instance of shader program
        Dictionary<string, int> dictionary;

        public VertexInfo(IList<VertexElement> elements)
            : this(elements, null) { }

        public VertexInfo(IList<VertexElement> elements , Type type)
        {
            this.Elements = new List<VertexElement>();
            this.dictionary = new Dictionary<string, int>();
            this.type = type;

            foreach (VertexElement elm in elements)
                if (elm.type != DeclarationType.Unused)
                    Elements.Add(elm);

            Elements.Sort(ElementComparer);

            for (int i = 0; i < Elements.Count; i++)
            {
                VertexElement elm = Elements[i];
                dictionary.Add(elm.SemanticName, i);
                bytesize += elm.bytesize;
                if (elm.stream > m_streamCount - 1) m_streamCount++;
            }

            if (type != null && Marshal.SizeOf(type) != bytesize)
                throw new ArgumentException("type " + type.Name + " don't have a correct size");
        }
        /// <summary>
        /// A good idea is sort by stream and by offset
        /// </summary>
        static int ElementComparer(VertexElement x, VertexElement y)
        {
            if (x.stream > y.stream) return 1;
            else if (x.stream < y.stream) return -1;
            else if (x.offset > y.offset) return 1;
            else if (x.offset < y.offset) return -1;
            else return 0;
        }

        /// <summary>
        /// Return the elements data using the unique Pair "Usage-UsageIndex" values, if not found return false
        /// </summary>
        public bool GetElement(DeclarationUsage usage, int usageidx, out VertexElement element)
        {
            string semantic = VertexElement.GetSemanticName(usage, usageidx);
            if (dictionary.ContainsKey(semantic))
            {
                element = Elements[dictionary[semantic]];
                return true;
            }
            else
            {
                element = VertexElement.VertexDeclarationEnd;
                return false;
            }
        }

        /// <summary>
        /// Get the number of stream used by 
        /// </summary>
        public int StreamCount { get { return m_streamCount; } }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append(string.Format("Size: {0}b, Format: ", bytesize));
            foreach (VertexElement elm in Elements)
                str.Append(elm.usage.ToString() + ",");
            return str.ToString();
        }
    }

    /// <summary>
    /// Type of indices, contain information about format
    /// </summary>
    public class IndexInfo : BufferInfo
    {
        /// <summary>
        /// Directx9 use only 16 or 32 bit indices, byte indices only in OpenGl
        /// </summary>
        public bool is32Bit { get; private set; }
        /// <summary>
        /// counter of indices in the Index Struct, to use Face and Edge extension
        /// </summary>
        public int numOfIndis { get; private set; }
        /// <summary>
        /// Index will be get only from manager
        /// </summary>
        private IndexInfo() { }

        /// <summary>
        /// Return Null if not found in the database
        /// </summary>
        public static IndexInfo GetIndexFormat(Type type)
        {
            return manager.GetIndexFormat(type);
        }
        public static IndexInfo IndexOne16 { get { return manager.GetIndexFormat(typeof(ushort)); } }
        public static IndexInfo IndexOne32 { get { return manager.GetIndexFormat(typeof(uint)); } }
        public static IndexInfo IndexEdge16 { get { return manager.GetIndexFormat(typeof(Edge16)); } }
        public static IndexInfo IndexEdge32 { get { return manager.GetIndexFormat(typeof(Edge32)); } }
        public static IndexInfo IndexFace16 { get { return manager.GetIndexFormat(typeof(Face16)); } }
        public static IndexInfo IndexFace32 { get { return manager.GetIndexFormat(typeof(Face32)); } }

        public override string ToString()
        {
            return string.Format("Size: {0}b, Type: {1} , {2} , num {3}", bytesize, type.Name.ToString(), is32Bit ? "x32" : "x16", numOfIndis);
        }

        #region Singleton Implementation
        static IndexInfoManager singleton = null;
        static IndexInfoManager manager
        {
            get
            {
                if (singleton == null) singleton = new IndexInfoManager();
                return singleton;
            }
        }
        class IndexInfoManager
        {
            // the database
            Dictionary<Type, IndexInfo> dictionary;
            public IndexInfoManager()
            {
                IndexInfo int16 = new IndexInfo { type = typeof(Int16), is32Bit = false, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(Int16)) };
                IndexInfo int32 = new IndexInfo { type = typeof(Int32), is32Bit = true, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(Int32)) };
                IndexInfo uint16 = new IndexInfo { type = typeof(UInt16), is32Bit = false, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(UInt16)) };
                IndexInfo uint32 = new IndexInfo { type = typeof(UInt32), is32Bit = true, numOfIndis = 1, bytesize = Marshal.SizeOf(typeof(UInt32)) };
                IndexInfo edge16 = new IndexInfo { type = typeof(Edge16), is32Bit = false, numOfIndis = 2, bytesize = Marshal.SizeOf(typeof(Edge16)) };
                IndexInfo edge32 = new IndexInfo { type = typeof(Edge32), is32Bit = true, numOfIndis = 2, bytesize = Marshal.SizeOf(typeof(Edge32)) };
                IndexInfo face16 = new IndexInfo { type = typeof(Face16), is32Bit = false, numOfIndis = 3, bytesize = Marshal.SizeOf(typeof(Face16)) };
                IndexInfo face32 = new IndexInfo { type = typeof(Face32), is32Bit = true, numOfIndis = 3, bytesize = Marshal.SizeOf(typeof(Face32)) };

                dictionary = new Dictionary<Type, IndexInfo>();
                dictionary.Add(typeof(Int16), int16);
                dictionary.Add(typeof(Int32), int32);
                dictionary.Add(typeof(UInt16), uint16);
                dictionary.Add(typeof(UInt32), uint32);
                dictionary.Add(typeof(Face16), face16);
                dictionary.Add(typeof(Face32), face32);
                dictionary.Add(typeof(Edge16), edge16);
                dictionary.Add(typeof(Edge32), edge32);
            }

            public IndexInfo GetIndexFormat(Type type)
            {
                if (!dictionary.ContainsKey(type)) throw new ArgumentException("This type not implemented");
                return dictionary[type];
            }

            public override string ToString()
            {
                StringBuilder str = new StringBuilder();
                str.AppendLine("IndexType list: ");
                foreach (IndexInfo type in dictionary.Values)
                    str.AppendLine(type.ToString());
                return str.ToString();
            }
        }
        #endregion

    }

    /// <summary>
    /// Return the wrapper of directx buffer
    /// </summary>
    public abstract class DataStream
    {
        public IntPtr buffer { get; protected set; }
        public int bufferSize { get; protected set; }
        public bool isReadable { get; protected set; }
        public bool isLocked { get; protected set; }
        /// <summary>
        /// Position in bytes in the buffer, used only for write with increment
        /// </summary>
        public int PositionInBytes { get; set; }

        public int flushBytes { get; protected set; } 

        public DataStream(IntPtr bufferPtr, int buffersize, bool isReadable)
        {
            if (bufferPtr == IntPtr.Zero) throw new ArgumentNullException("buffer pointer null");
            this.buffer = bufferPtr;
            this.bufferSize = buffersize;
            this.isReadable = isReadable;
            this.PositionInBytes = 0;
            this.flushBytes = 0;
            this.isLocked = true;
        }

        public void Close()
        {
            isLocked = false;
            buffer = IntPtr.Zero;
            bufferSize = 0;
            isReadable = false;
        }
        public override string ToString()
        {
            return string.Format("Size: {0}b , Readable: {1}", bufferSize, isReadable);
        }
    }

    /// <summary>
    /// the vertices stream return when open a vertex buffer, implement the writes functions
    /// </summary>
    public class VertexStream : DataStream
    {
        const int sizefloat = sizeof(float);
        const int sizeint16 = sizeof(short);
        const int sizeint32 = sizeof(int);
        
        /// <summary>
        /// Descriptor of buffer's element format
        /// </summary>
        public VertexInfo bufferFormat { get; protected set; }

        /// <summary>
        /// Get the graphic data tool from a graphic buffer
        /// </summary>
        /// <param name="bufferPtr">gpu graphic buffer</param>
        /// <param name="bufferFormat">buffer element descriptor</param>
        /// <param name="Count">size int elements of buffer</param>
        /// <param name="canRead">check if is a readable buffer</param>
        public VertexStream(IntPtr bufferPtr, VertexInfo bufferFormat, int Count, bool canRead)
            : base(bufferPtr, Count * bufferFormat.bytesize, canRead)
        {
            this.bufferFormat = bufferFormat;
        }

        #region Write fragmented collections of values
        /// <summary>
        /// Write a pinnable array, more fast than copy bytes by bytes
        /// </summary>
        /// <param name="Offset">attribute to don't write</param>
        /// <param name="Count">num of attributes to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        public void WriteCollection<T>(VertexAttribute<T> vertexattribute, int Offset, int Count, int BufferOffset) where T : struct
        {
            VertexElement element;

            if (bufferFormat.GetElement(vertexattribute.Usage, vertexattribute.UsageIdx, out element))
            {
                WriteCollection<T>(vertexattribute.data, element, Offset, Count, BufferOffset);
            }
        }

        /// <summary>
        /// Write a generic collection of struct, using a VertexElement to use a specified position in buffer. Take a look to this code
        /// <code>
        /// WriteCollection&lt;float&gt;(float1, texchannel, 0, 4, 0);
        /// WriteCollection&lt;Vector2&gt;(float2, texchannel, 0, 2, 0);
        /// WriteCollection&lt;Vector4&gt;(float4, texchannel, 0, 1, 0);
        /// </code>
        /// the function is designed to implement also different array's version of same format
        /// </summary>
        /// <param name="element">the information about position of T in buffer</param>
        /// <param name="Offset">number of array's elements to jump before start writting</param>
        /// <param name="Count">num of array's elements to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <remarks>
        /// <para>........................+..............................................................+..........</para>
        /// <para>      BufferOffset      | Array[Offset] , Array[Offset+1], , , Array[Offset + Count]   |          </para>
        /// <para>........................+..............................................................+..........</para>
        /// </remarks>
        public void WriteCollection<T>(IList<T> array, VertexElement element, int Offset, int Count, int BufferOffset) where T : struct
        {
            if (element.Equals(VertexElement.VertexDeclarationEnd)) throw new ArgumentException("Invalid element structure");

            if (Count <= 0) return;

            int sourceLenght = array.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int source_item = Marshal.SizeOf(typeof(T));
            int buffer_offset = bufferFormat.bytesize * BufferOffset;
            int source_size = (Offset + Count) * source_item;
            int source_offset = Offset * source_item;
            int source_start = element.offset;
            int source_end = bufferFormat.bytesize - element.offset - element.bytesize;
            int source_filling = source_size / element.bytesize * bufferFormat.bytesize;

            if (bufferSize - buffer_offset < source_filling)
                throw new ArgumentException("not enought buffer space");

            if (array.GetType() == typeof(T[]))
            {
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject();
                MemoryTool.WriteFragmentStruct(
                    buffer, bufferSize, buffer_offset,
                    source, source_size, source_offset,
                    source_start, element.bytesize, source_end);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteFragmentStructByStruct<T>(
                    buffer, bufferSize, buffer_offset,
                    array, (Offset + Count), Offset,
                    source_start, element.bytesize, source_end);
            }
        }
        #endregion

        #region Write default compact array
        /// <summary>
        /// Write a generic collection of struct, without VertexElement info the array will be write continuously
        /// </summary>
        /// <param name="Offset">attribute to don't write</param>
        /// <param name="Count">num of attributes to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <remarks>
        /// <para>........................+.................................................................+..........</para>
        /// <para>      BufferOffset      | Array[Offset] , Array[Offset+1],      , Array[Offset + Count]   |          </para>
        /// <para>........................+.................................................................+..........</para>
        /// </remarks>
        public void WriteCollection<T>(IList<T> array, int Offset, int Count, int BufferOffset) where T : struct
        {
            if (Count <= 0) return;
            int sourceLenght = array.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int typesize = Marshal.SizeOf(typeof(T));
            int sourceSize = typesize * (Count + Offset);
            int sourceOffset = typesize * Offset;
            int bufferoffset = bufferFormat.bytesize * BufferOffset;

            if (bufferSize - bufferoffset < sourceSize - sourceOffset)
                throw new ArgumentException("not enought buffer space");

            if (array.GetType() == typeof(T[]))
            {
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject();
                MemoryTool.WriteStruct(buffer, bufferSize, bufferoffset, source, sourceSize, sourceOffset);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteStructByStruct<T>(buffer, bufferSize, bufferoffset, array, (Offset + Count), Offset);
            }

        }
        #endregion

        #region Write with increment
        public unsafe void WriteAndIncrement(Vector3 vector)
        {
            // pointer arithmetic in bytes
            Vector3* ptr = (Vector3*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = vector;
            PositionInBytes += Vector3.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Color32 color)
        {
            Color32* ptr = (Color32*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = color;
            PositionInBytes += Color32.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Vector2 vector)
        {
            Vector2* ptr = (Vector2*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = vector;
            PositionInBytes += Vector2.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(float value)
        {
            float* ptr = (float*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizefloat;
        }
        #endregion
    }

    /// <summary>
    /// the indices stream return when open a indices buffer
    /// </summary>
    public class IndexStream : DataStream
    {
        const int sizeint16 = sizeof(short);
        const int sizeint32 = sizeof(int);

        /// <summary>
        /// Descriptor of buffer's element format
        /// </summary>
        IndexInfo bufferFormat;

        /// <summary>
        /// Get the graphic data tool from a graphic buffer
        /// </summary>
        /// <param name="bufferPtr">gpu graphic buffer</param>
        /// <param name="bufferinfo">buffer element descriptor</param>
        /// <param name="Count">size int elements of buffer</param>
        /// <param name="canRead">check if is a readable buffer</param>
        public IndexStream(IntPtr bufferPtr, IndexInfo bufferFormat, int Count, bool canRead)
            : base(bufferPtr, Count * bufferFormat.bytesize, canRead)
        {
            this.bufferFormat = bufferFormat;
        }

        #region Write default compact array with casting 16bit to 32bit
        
        /// <summary>
        /// </summary>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <param name="IndexOffset">an optional value to sum to each indices when writting, usefull for batching method, can be negative</param>
        public void WriteCollection<T>(IndexAttribute<T> indexattribute, int BufferOffset, int IndexOffset) where T : struct
        {
            IndexInfo format = indexattribute.format;
            int source_size = format.bytesize * indexattribute.Count;
            int buffer_offset = bufferFormat.bytesize * BufferOffset;

            int source_nindis = indexattribute.Count / format.bytesize * format.numOfIndis;
            int source_filling = source_nindis * (bufferFormat.is32Bit ? sizeof(uint) : sizeof(ushort));
            if (bufferSize - buffer_offset < source_filling)
                throw new ArgumentException("not enought buffer space");

            IntPtr source = indexattribute.GetPointer();
            MemoryTool.WriteIndex(buffer, bufferFormat, bufferSize, buffer_offset, source, format, source_size, 0, IndexOffset);
            indexattribute.FreePointer();
        }
        /// <summary>
        /// a particual implementation for attribute stored in a generic list, the indices must be write index by index and can't use the faster
        /// vertsion with a pinnable array
        /// </summary>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        /// <param name="IndexOffset">an optional value to sum to each indices when writting, usefull for batching method, can be negative</param>
        public void WriteCollection<T>(IList<T> indices, IndexInfo format, int Offset, int Count, int BufferOffset, int IndexOffset) where T : struct
        {
            if (Count <= 0) return;
            
            int sourceLenght = indices.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int sizeofT = Marshal.SizeOf(typeof(T));
            int source_size = format.bytesize * (Offset + Count);
            int source_offset = format.bytesize * Offset;
            int buffer_offset = bufferFormat.bytesize * BufferOffset;

            int source_filling = Count * format.numOfIndis * (bufferFormat.is32Bit ? sizeof(uint) : sizeof(ushort));
            if (bufferSize - buffer_offset < source_filling)
                throw new ArgumentException("not enought buffer space");

            if (sizeofT != format.bytesize)
                throw new Exception("source array not match with indexInfo definition");

            if (indices.GetType() == typeof(T[]))
            {
                GCHandle handle = GCHandle.Alloc(indices, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject(); 
                // can use packed version because i don't need take care about casting indices
                if (IndexOffset == 0 && (format.is32Bit == bufferFormat.is32Bit))
                    MemoryTool.WriteStruct(buffer, bufferSize, buffer_offset, source, source_size, source_offset);
                else
                    MemoryTool.WriteIndex(buffer, bufferFormat, bufferSize, buffer_offset, source, format, source_size, source_offset, IndexOffset);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteIndexByIndex<T>(buffer, bufferFormat, bufferSize, buffer_offset, indices, format, Count + Offset, Offset, IndexOffset);
            }
        }

        /// <summary>
        /// Write a generic collection of struct, without IndexInfo the array will not be casting in uint or ushort
        /// </summary>
        /// <param name="Offset">attribute to don't write</param>
        /// <param name="Count">num of attributes to write</param>
        /// <param name="BufferOffset">number of buffer's vertex to jump before start writting</param>
        public void WriteCollection<T>(IList<T> array, int Offset, int Count, int BufferOffset) where T : struct
        {
            if (Count <= 0) return;
            int sourceLenght = array.Count;
            if (Offset < 0 || Count + Offset > sourceLenght)
                throw new ArgumentException("array's Count and Offset wrong");

            int typesize = Marshal.SizeOf(typeof(T));
            int sourceSize = typesize * (Count + Offset);
            int sourceOffset = typesize * Offset;
            int bufferoffset = bufferFormat.bytesize * BufferOffset;

            if (bufferSize - bufferoffset < sourceSize - sourceOffset)
                throw new ArgumentException("not enought buffer space");

            if (array.GetType() == typeof(T[]))
            {
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                IntPtr source = handle.AddrOfPinnedObject();
                MemoryTool.WriteStruct(buffer, bufferSize, bufferoffset, source, sourceSize, sourceOffset);
                handle.Free();
            }
            else
            {
                MemoryTool.WriteStructByStruct<T>(buffer, bufferSize, bufferoffset, array, (Offset + Count), Offset);
            }
        }
        
        #endregion

        #region Write with increment
        public unsafe void WriteAndIncrement(Face32 face)
        {
            // pointer arithmetic in bytes
            Face32* ptr = (Face32*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = face;
            PositionInBytes += Face32.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Edge32 edge)
        {
            Edge32* ptr = (Edge32*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = edge;
            PositionInBytes += Edge32.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Face16 face)
        {
            // pointer arithmetic in bytes
            Face16* ptr = (Face16*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = face;
            PositionInBytes += Face16.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(Edge16 edge)
        {
            Edge16* ptr = (Edge16*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = edge;
            PositionInBytes += Edge16.sizeinbyte;
        }
        public unsafe void WriteAndIncrement(uint value)
        {
            uint* ptr = (uint*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint32;
        }
        public unsafe void WriteAndIncrement(int value)
        {
            int* ptr = (int*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint32;
        }
        public unsafe void WriteAndIncrement(ushort value)
        {
            ushort* ptr = (ushort*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint16;
        }
        public unsafe void WriteAndIncrement(short value)
        {
            short* ptr = (short*)((byte*)buffer.ToPointer() + PositionInBytes);
            *ptr = value;
            PositionInBytes += sizeint16;
        }
        #endregion
    }
}
