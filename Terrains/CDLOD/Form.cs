﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

using Font = Engine.Graphics.Font;


namespace Engine.Cdlod
{
    public partial class TerrainForm : Form
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            TerrainForm form = new TerrainForm();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        GraphicDevice device;
        public static MaterialFX_VCOLOR materialVC;
        public static MaterialFX_POSITION materialP;

        TrackBallCamera2 camera;
        Terrain terrain;
        Water water;
        AxisObj axis;
        Font font;
        PlayerObj player;
        Stopwatch stopWatch = new Stopwatch();

        int frames = 0;
        int LastTickCount = 0;
        int lastFramescount = 0;
        int StartTime = 0;
        

        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        public TerrainForm()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            device = new GraphicDevice(this);
            font = new Font(device, Engine.Graphics.Font.FontName.Arial,8); 
            materialVC = new MaterialFX_VCOLOR(device);
            materialP = new MaterialFX_POSITION(device);

            axis = new AxisObj(device);
            axis.material = materialVC;

            player = new PlayerObj(device);
            player.material = materialVC;

            player.PlayerPos = new Vector3(-50, 0, -50);
            camera = new TrackBallCamera2(this, new Vector3(-70, 40, -70), Vector3.Zero, Vector3.UnitY, 0.1f, 1000.0f);
            
            device.lights.Add(Light.Sun);

            BoundaryAABB TerrainSize = new BoundaryAABB(new Vector3(50, 0, 50), new Vector3(-50, 5, -50));
            terrain = new Terrain(device, TerrainSize);
            water = new Water(device, TerrainSize);

            StartTime = Environment.TickCount; 
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            frames++;

            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                lastFramescount = frames;
                LastTickCount = Environment.TickCount;
                frames = 0;
            }
            
            if (device.BeginDraw())
            {
                Frustum frustum = new Frustum();


                device.SetRenderTarghet();
                device.Clear(Color.CornflowerBlue);

                axis.Draw(camera);
                player.Draw(camera);




                //terrain.Update(camera.Eye, new Frustum());
                stopWatch.Start();
                terrain.Update(player.PlayerPos, frustum);
                TimeSpan ts0 = stopWatch.Elapsed;
                terrain.Draw(camera);
                TimeSpan ts1 = stopWatch.Elapsed;
                stopWatch.Stop();
                stopWatch.Reset();

                water.Update(player.PlayerPos, frustum);
                water.Draw(camera);

                Vector3 pos;
                pos = Vector3.Project(axis.axe[0], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("X", (int)pos.x, (int)pos.y, Color.Red);
                pos = Vector3.Project(axis.axe[1], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("Y", (int)pos.x, (int)pos.y, Color.Green);
                pos = Vector3.Project(axis.axe[2], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("Z", (int)pos.x, (int)pos.y, Color.Blue);


                font.Draw("FPS   : " + lastFramescount.ToString(), 10, 10, Color.Black);
                font.Draw(string.Format("Nodes : {0}/{1}", QuadNode.NodeCount, terrain.quadTree.setting.NumNodes), 10, 20, Color.Black);
                font.Draw("Triangles : " + QuadMesh.TrianglesCount, 10, 30, Color.Black);
                font.Draw("Updating in : " + ts0.Milliseconds, 10, 40, Color.Black);
                font.Draw("Drawing in  : " + (ts1 - ts0).Milliseconds, 10, 50, Color.Black);


                device.EndDraw();
                device.PresentDraw();
            }

        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W || e.KeyCode == Keys.Up) player.PlayerPos += Vector3.UnitX;
            if (e.KeyCode == Keys.S || e.KeyCode == Keys.Down) player.PlayerPos -= Vector3.UnitX;

            if (e.KeyCode == Keys.A || e.KeyCode == Keys.Left) player.PlayerPos += Vector3.UnitZ;
            if (e.KeyCode == Keys.D || e.KeyCode == Keys.Right) player.PlayerPos -= Vector3.UnitZ;

            if (e.KeyCode == Keys.Q) player.PlayerPos += Vector3.UnitY*0.5f;
            if (e.KeyCode == Keys.Z) player.PlayerPos -= Vector3.UnitY*0.5f;

            if (e.KeyCode == Keys.Space) terrain.quadTree.patch.shaderProgram.WireframeMode = !terrain.quadTree.patch.shaderProgram.WireframeMode;
        }
   
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (device == null) return;
            device.Resize(this.ClientSize);
        }

        #region GAME LOOP
        bool draw = true;
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {
                System.Threading.Thread.Sleep(10); //reduce CPU usage 
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
