﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Engine.Maths;
using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

namespace Engine.Graphics
{
    /// <summary>
    /// SharpDx
    /// </summary>
    public class DX9EffectHandle
    {
        internal D3D.EffectHandle handle;
        string name;
        public DX9EffectHandle(string name)
        {
            this.name = name;
            handle = new D3D.EffectHandle(name);
        }
        public override string ToString()
        {
            return name;
        }
    }

    /// <summary>
    /// SharpDx
    /// </summary>
    public class DX9Effect :IDisposable
    {
#if DEBUG
        D3D.ShaderFlags debug = D3D.ShaderFlags.Debug;
#else
        D3D.ShaderFlags debug = D3D.ShaderFlags.None;
#endif
        D3D.Effect m_effect;
        D3D.Device m_device;

        public DX9Effect(DX9Device device, string shadercode)
        {
            m_device = device.m_device;
            try
            {
                m_effect = D3D.Effect.FromString(m_device, shadercode, debug);
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }
    
        public void SetValue(DX9EffectHandle handle, DX9Texture texture)
        {
            m_effect.SetTexture(handle.handle, texture.m_texture);
        }
        public void SetValue(DX9EffectHandle handle, Matrix4 matrix)
        {
            m_effect.SetValue(handle.handle, Helper.Convert(matrix));
        }
        public void SetValue(DX9EffectHandle handle, Vector4 vector)
        {
            m_effect.SetValue(handle.handle, Helper.Convert(vector));
        }
        public void SetValue(DX9EffectHandle handle, Vector3 vector)
        {
            setStruct<Vector3>(handle, vector);
        }
        public void SetValue(DX9EffectHandle handle, Vector2 vector)
        {
            //m_effect.SetValue(handle.handle, new float[] { vector.x, vector.y });
            setStruct<Vector2>(handle, vector);
        }
        public void SetValue(DX9EffectHandle handle, int value)
        {
            m_effect.SetValue(handle.handle, value);
        }
        public void SetValue(DX9EffectHandle handle, float value)
        {
            m_effect.SetValue(handle.handle, value);
        }
        public void SetTechnique(DX9EffectHandle handle)
        {
            m_effect.Technique = handle.handle;
        }
        public void CommitChanges()
        {
            m_effect.CommitChanges();
        }
        
        /// <summary>
        /// i want assign renderstate for each shader, is more usefull
        /// </summary>
        public int Begin(bool savestates)
        {
            return m_effect.Begin(savestates ? D3D.FX.None : D3D.FX.DoNotSaveState);
        }
        public void End()
        {
            m_effect.End();
        }
        public void BeginPass(int i)
        {
            m_effect.BeginPass(i);
        }
        public void EndPass()
        {
            m_effect.EndPass();
        }

        void setStruct<T>(DX9EffectHandle handle, T value) where T : struct
        {
            m_effect.SetValue<T>(handle.handle, value);
        }

        public void Dispose()
        {
            m_effect.Dispose();
        }
    }
}
