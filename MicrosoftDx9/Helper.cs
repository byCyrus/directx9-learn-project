﻿using System;
using System.Drawing;
using System.Text;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Graphics
{
    public class Helper
    {
        public static VertexFormat GetVertexTextureCoordinate(int size, int coordidx)
        {
            switch (size)
            {
                case 1: return (VertexFormat)D3D.VertexTextureCoordinate.Size1(coordidx);
                case 2: return (VertexFormat)D3D.VertexTextureCoordinate.Size2();
                case 3: return (VertexFormat)D3D.VertexTextureCoordinate.Size3(coordidx);
                case 4: return (VertexFormat)D3D.VertexTextureCoordinate.Size4(coordidx);
                default: throw new ArgumentException("size must be in range[1-4]");
            }
        }

        public static Vector3 UnprojectOrig(Vector3 screen,Viewport viewport,Matrix4 proj,Matrix4 view,Matrix4 world)
        {
            return Convert(DX.Vector3.Unproject(Convert(screen), Convert(viewport), Convert(proj), Convert(view), Convert(world)));
        }
        public static DX.Vector4 Convert(Vector4 vector)
        {
            return new DX.Vector4(vector.x, vector.y, vector.z,vector.w);
        }
        public static Vector4 Convert(DX.Vector4 vector)
        {
            return new Vector4(vector.X, vector.Y, vector.Z, vector.W);
        }
        public static DX.Vector3 Convert(Vector3 vector)
        {
            return new DX.Vector3(vector.x, vector.y, vector.z);
        }
        public static Vector3 Convert(DX.Vector3 vector)
        {
            return new Vector3(vector.X, vector.Y, vector.Z);
        }

        public static DX.Quaternion Convert(Quaternion vector)
        {
            return new DX.Quaternion(vector.x, vector.y, vector.z, vector.w);
        }
        public static Quaternion Convert(DX.Quaternion vector)
        {
            return new Quaternion(vector.X, vector.Y, vector.Z, vector.W);
        }
        /// <summary>
        /// Directx matrix is trasposed
        /// </summary>
        /// <param name="matrix"></param>
        public static DX.Matrix Convert(Matrix4 matrix)
        {
            DX.Matrix m = new DX.Matrix();
            m.M11 = matrix.m00;
            m.M12 = matrix.m10;
            m.M13 = matrix.m20;
            m.M14 = matrix.m30;

            m.M21 = matrix.m01;
            m.M22 = matrix.m11;
            m.M23 = matrix.m21;
            m.M24 = matrix.m31;

            m.M31 = matrix.m02;
            m.M32 = matrix.m12;
            m.M33 = matrix.m22;
            m.M34 = matrix.m32;

            m.M41 = matrix.m03;
            m.M42 = matrix.m13;
            m.M43 = matrix.m23;
            m.M44 = matrix.m33;

            return m;
        }
        /// <summary>
        /// Directx matrix is trasposed
        /// </summary>
        /// <param name="matrix"></param>
        public static Matrix4 Convert(DX.Matrix matrix)
        {
            Matrix4 m = new Matrix4();
            m.m00 = matrix.M11;
            m.m01 = matrix.M21;
            m.m02 = matrix.M31;
            m.m03 = matrix.M41;

            m.m10 = matrix.M12;
            m.m11 = matrix.M22;
            m.m12 = matrix.M32;
            m.m13 = matrix.M42;

            m.m20 = matrix.M13;
            m.m21 = matrix.M23;
            m.m22 = matrix.M33;
            m.m23 = matrix.M43;

            m.m30 = matrix.M14;
            m.m31 = matrix.M24;
            m.m32 = matrix.M34;
            m.m33 = matrix.M44;

            return m;
        }

        public static D3D.VertexElement Convert(VertexElement element)
        {
            return new D3D.VertexElement(element.stream,
                                         element.offset,
                                         (D3D.DeclarationType)element.type,
                                         (D3D.DeclarationMethod)element.method,
                                         (D3D.DeclarationUsage)element.usage,
                                         element.usageIndex);
        }

        public static D3D.Material Convert(Material material)
        {
            return new D3D.Material
            {
                Ambient = material.Ambient,
                Diffuse = material.Diffuse,
                Emissive = material.Emissive,
                Specular = material.Specular,
                SpecularSharpness = material.SpecularSharpness
            };
        }
        public static Material Convert(D3D.Material material)
        {
            return new Material
            {
                Ambient = material.Ambient,
                Diffuse = material.Diffuse,
                Emissive = material.Emissive,
                Specular = material.Specular,
                SpecularSharpness = material.SpecularSharpness
            };
        }
        public static D3D.Light Convert(Light light)
        {
            return new D3D.Light
            {
                Ambient = light.Ambient,
                Diffuse = light.Diffuse,
                Direction = Convert(light.Direction),
                Position = Convert(light.Position),
                Specular = light.Specular,
                Type = (D3D.LightType)light.Type,
                Attenuation0 = light.Attenuation0,
                Attenuation1 = light.Attenuation1,
                Attenuation2 = light.Attenuation2,
                Falloff = light.Falloff,
                OuterConeAngle = light.OuterConeAngle,
                Range = light.Range,
                InnerConeAngle = light.InnerConeAngle
            };
        }
        public static Light Convert(D3D.Light light)
        {
            return new Light
            {
                Ambient = light.Ambient,
                Diffuse = light.Diffuse,
                Direction = Convert(light.Direction),
                Position = Convert(light.Position),
                Specular = light.Specular,
                Attenuation0 = light.Attenuation0,
                Attenuation1 = light.Attenuation1,
                Attenuation2 = light.Attenuation2,
                Falloff = light.Falloff,
                OuterConeAngle = light.OuterConeAngle,
                Range = light.Range,
                InnerConeAngle = light.InnerConeAngle,
                Type = (LightType)light.Type
            };
        }
        public static Viewport Convert(D3D.Viewport view)
        {
            return new Viewport
            {
                Height = view.Height,
                Width = view.Width,
                MaxDepth = view.MaxZ,
                MinDepth = view.MinZ,
                X = view.X,
                Y = view.Y
            };
        }
        public static D3D.Viewport Convert(Viewport view)
        {
            return new D3D.Viewport
            {
                Height = view.Height,
                Width = view.Width,
                MaxZ = view.MaxDepth,
                MinZ = view.MinDepth,
                X = view.X,
                Y = view.Y
            };
        }
        #region TO DEBUG...
        public static Matrix4 LookAtLH(Vector3 eye, Vector3 target, Vector3 up)
        {
            return Convert(DX.Matrix.LookAtLH(Convert(eye), Convert(target), Convert(up)));
        }
        public static Matrix4 PerspectiveFovLH(float fovy, float aspectRatio, float near, float far)
        {
            return Convert(DX.Matrix.PerspectiveFovLH(fovy, aspectRatio, near, far));
        }
        #endregion
    }
}