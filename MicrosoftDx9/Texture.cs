﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

using Engine.Tools;
using System.IO;

namespace Engine.Graphics
{
    public class DX9TextureState
    {
        // default set
        public TextureFilter MinFilter = TextureFilter.None;
        public TextureFilter MagFilter = TextureFilter.None;

        internal void SetStates(D3D.Device m_device, int stage)
        {
            m_device.SamplerState[stage].MinFilter = (D3D.TextureFilter)MinFilter;
            m_device.SamplerState[stage].MagFilter = (D3D.TextureFilter)MagFilter;
            m_device.TextureState[stage].ColorOperation = (D3D.TextureOperation)ColorOperation;
            m_device.TextureState[stage].ColorArgument1 = (D3D.TextureArgument)ColorArgument1;
            m_device.TextureState[stage].ColorArgument2 = (D3D.TextureArgument)ColorArgument1;
            m_device.TextureState[stage].ColorOperation = (D3D.TextureOperation)AlphaOperation;

            m_device.SetTextureStageState(stage,D3D.TextureStageStates.TextureCoordinateIndex,TexCoordIndex);
            m_device.SetTextureStageState(stage,D3D.TextureStageStates.BumpEnvironmentMaterial00,BumpEnvironmentMat[0,0]);
            m_device.SetTextureStageState(stage,D3D.TextureStageStates.BumpEnvironmentMaterial01,BumpEnvironmentMat[0, 1]);
            m_device.SetTextureStageState(stage,D3D.TextureStageStates.BumpEnvironmentMaterial10,BumpEnvironmentMat[1, 0]);
            m_device.SetTextureStageState(stage,D3D.TextureStageStates.BumpEnvironmentMaterial11,BumpEnvironmentMat[1, 1]);
            m_device.SetTextureStageState(stage,D3D.TextureStageStates.BumpEnvironmentLuminanceOffset,BumpEnvironmentLOffset);
            m_device.SetTextureStageState(stage,D3D.TextureStageStates.BumpEnvironmentLuminanceScale,BumpEnvironmentLScale);
        }

#pragma warning disable
        #region default
        //Per-stage constant color. Use packed integer colors to set this state.
        int Constant = 0;
        //Setting to select the destination register for the result of this stage.
        TextureArgument ResultArg = TextureArgument.Current;
        //Settings for the third alpha operand for triadic operations. 
        TextureArgument AlphaArg0 = TextureArgument.Current;
        //Settings for the third color operand for triadic operations.
        TextureArgument ColorArg0 = TextureArgument.Current;
        //Specifies transformation options for texture coordinates.
        TextureTransform TextureTransformFlags = TextureTransform.Disable;
        //Floating-point offset value for bump-map luminance
        public float BumpEnvironmentLOffset = 0.0f;
        //Floating-point scale value for bump-map luminance
        public float BumpEnvironmentLScale = 0.0f;
        //Index of the texture coordinate set to use with this texture stage.
        //You can specify up to eight sets of texture coordinates per vertex.
        //If a vertex does not include a set of texture coordinates at the specified index, the system defaults to the u and v coordinates (0,0).
        public int TexCoordIndex = 0;
        //coefficient in a bump-mapping matrix
        public float[,] BumpEnvironmentMat = new float[,] { { 0, 0 }, { 0, 0 } };

        //Texture-stage state is the second alpha argument for the stage
        TextureArgument AlphaArg2 = TextureArgument.Current;
        //Texture-stage state is the first alpha argument for the stage
        public TextureArgument AlphaArgument1 = TextureArgument.Texture;
        //Texture-stage state is a texture alpha blending operation. D3D.TextureOperation.Disable for stage > 0
        public TextureOperation AlphaOperation = TextureOperation.SelectArg1;
        //Texture-stage state is the second color argument for the stage
        public TextureArgument ColorArgument2 = TextureArgument.Current;
        //Texture-stage state is the first color argument for the stage
        public TextureArgument ColorArgument1 = TextureArgument.Texture;
        //Texture-stage state is a texture color blending operation D3D.TextureOperation.Disable for stage > 0
        public TextureOperation ColorOperation = TextureOperation.Modulate;
        #endregion
#pragma warning restore
        /// <summary>
        /// A stage > 0 use different setting
        /// </summary>
        public static DX9TextureState GetDefaultSetting(int stage)
        {
            DX9TextureState states = new DX9TextureState();
            if (stage > 0)
            {
                states.TexCoordIndex = 0;
                states.AlphaOperation = TextureOperation.Disable;
                states.ColorOperation = TextureOperation.Disable;
            }
            return states;
        }
    }

    /// <summary>
    /// MicrosoftDX texture2D. A simple matrix WxH of pixels
    /// </summary>
    public class DX9Texture : IDisposable
    {
        D3D.Device m_device;
        public D3D.Texture m_texture;
        public DX9TextureState m_state;

        private DX9Texture(DX9Device device, int stage)
        {
            m_device = device.m_device;
            m_state = DX9TextureState.GetDefaultSetting(0);
        } 
        
        public static DX9Texture FromBitmap(DX9Device device, Bitmap bitmap, Usage usage, Pool memory)
        {
            return DX9Texture.FromBitmap(device, bitmap, usage, memory, 0);
        }
        public static DX9Texture FromBitmap(DX9Device device, Bitmap bitmap, Usage usage, Pool memory, int stage)
        {
            if (usage != Usage.None && usage != Usage.RenderTarget)
                throw new ArgumentException("usage not allow");
            DX9Texture text = new DX9Texture(device, stage);
            text.m_texture = D3D.Texture.FromBitmap(device.m_device, bitmap, (D3D.Usage)usage, (D3D.Pool)memory);
            return text;
        }
        public static DX9Texture FromFilename(DX9Device device, string filename, Usage usage, Pool memory, int stage)
        {
            if (usage != Usage.None && usage != Usage.RenderTarget)
                throw new ArgumentException("usage not allow");
            DX9Texture text = new DX9Texture(device, stage);
            text.m_texture = D3D.TextureLoader.FromFile(device.m_device, filename);
            return text;
        }

        public IntPtr Lock()
        {
            DX.GraphicsStream ret = m_texture.LockRectangle(0, D3D.LockFlags.None);
            return ret.InternalData;
        }
        public void UnLock()
        {
            m_texture.UnlockRectangle(0);
        }
        public void SetToDevice(int stage)
        {
            m_state.SetStates(m_device, stage);
            m_device.SetTexture(stage, (D3D.BaseTexture)m_texture);
        }
        public void Dispose()
        {
            m_texture.Dispose();
        }
    }
    /// <summary>
    /// MicrosoftDX VolumeTexture, a matrix WxHxD of pixels where D&lt;=8
    /// </summary>
    public class DX9VolumeTexture : IDisposable
    {
        D3D.Device m_device;
        int m_width, m_height, m_depth;

        public DX9TextureState m_state;
        public D3D.VolumeTexture m_texture;

        public DX9VolumeTexture(DX9Device device, int width, int height, int depth, Usage usage, Pool memory)
        {
            m_device = device.m_device;
            m_state = DX9TextureState.GetDefaultSetting(0);

            if (usage != Usage.None && usage != Usage.RenderTarget)
                throw new ArgumentException("usage not allow");

            m_width = width;
            m_height = height;
            m_depth = depth;

            if (depth > 8) throw new ArgumentException("can use more than 8 textures");

            m_texture = new D3D.VolumeTexture(m_device, width, height, depth, 1, (D3D.Usage)usage, D3D.Format.X8R8G8B8, (D3D.Pool)memory);
        }

        /// <summary>
        /// Lock entire buffer of all texture TODO : lock only some
        /// </summary>
        public IntPtr Lock(LockFlags mode)
        {
            DX.GraphicsStream data = m_texture.LockBox(0, (D3D.LockFlags)mode);
            return data.InternalData;
        }
        public void UnLock()
        {
            m_texture.UnlockBox(0);
        }
        public void SetToDevice()
        {
            m_state.SetStates(m_device, 0);
            m_device.SetTexture(0, m_texture);
        }

        public void Dispose()
        {
            m_texture.Dispose();
        }
    }
}