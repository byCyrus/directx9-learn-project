﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using DX3D = Microsoft.DirectX.Direct3D;

using Vector3 = Engine.Utils.Vector3;
using Engine.Utils;
using Engine.Graphics;

namespace Engine.Renderer
{
    /// <summary>
    /// TriMesh custom objects , <see cref="IAutoBuffer"/>
    /// </summary>
    public class DxAutoEditableMesh : DxAutoNode
    {
        public bool showNormals { get; set; }
        public bool showVertexIndex { get; set; }
        public DxTriMesh mesh;

        VertexBuffer vertexBuffer;
        IndexBuffer indexBuffer;
        VertexFormats format;
        Type type;

        VertexBuffer normalBuffer;

        public DxAutoEditableMesh()
        {
            showNormals = false;
            showVertexIndex = false;
        }

        public override int PrimitiveIntersection(Ray ray, out float t,out Vector3 intersection)
        {
            throw new NotImplementedException();
        }
        public override void OnRenderBegin(Device device)
        {
            if (vertexBuffer == null)
                this.OnDeviceReset(device);
        }
        public override void OnDeviceReset(Device device)
        {
            if (mesh == null) return;

            if (mesh.faces != null)
            {
                int numIndices = mesh.faces.Length * 3;
                ushort[] indicesTmp = new ushort[numIndices];
                for (int i = 0; i < mesh.faces.Length; i++)
                {
                    indicesTmp.SetValue(mesh.faces[i].I, i * 3 + 0);
                    indicesTmp.SetValue(mesh.faces[i].J, i * 3 + 1);
                    indicesTmp.SetValue(mesh.faces[i].K, i * 3 + 2);
                }
                indexBuffer = new IndexBuffer(typeof(ushort), numIndices, device, Usage.None, Pool.Managed);
                indexBuffer.SetData(indicesTmp, 0, LockFlags.Discard);
            }
            else
            {
                indexBuffer = null;
            }

            Array verticesTmp;
            VertexFormats vertextype = VertexFormats.Position;

            if (mesh.normals != null) vertextype |= VertexFormats.Normal;
            if (mesh.textures != null) vertextype |= VertexFormats.Texture1;
            if (mesh.colors != null) vertextype |= VertexFormats.Diffuse;

            switch (vertextype)
            {
                case (VertexFormats.Position | VertexFormats.Diffuse):
                    format = CVERTEX.Format;
                    type = typeof(CVERTEX);
                    verticesTmp = new CVERTEX[mesh.numVertices];
                    for (int i = 0; i < mesh.numVertices; i++)
                        verticesTmp.SetValue(new CVERTEX(mesh.vertices[i], mesh.colors[i]), i);
                    break;

                case (VertexFormats.Position | VertexFormats.Normal | VertexFormats.Diffuse):
                    format = NCVERTEX.Format;
                    type = typeof(NCVERTEX);
                    verticesTmp = new NCVERTEX[mesh.numVertices];
                    for (int i = 0; i < mesh.numVertices; i++)
                        verticesTmp.SetValue(new NCVERTEX(mesh.vertices[i], mesh.normals[i], mesh.colors[i]), i);
                    break;

                case (VertexFormats.Position | VertexFormats.Normal | VertexFormats.Texture1):
                    format = NTVERTEX.Format;
                    type = typeof(NTVERTEX);
                    verticesTmp = new NTVERTEX[mesh.numVertices];
                    for (int i = 0; i < mesh.numVertices; i++)
                        verticesTmp.SetValue(new NTVERTEX(mesh.vertices[i], mesh.normals[i], mesh.textures[i]), i);
                    break;

                default:
                    verticesTmp = new VERTEX[mesh.numVertices];
                    format = VERTEX.Format;
                    type = typeof(VERTEX);
                    for (int i = 0; i < mesh.numVertices; i++)
                        verticesTmp.SetValue(new VERTEX(mesh.vertices[i]), i);
                    break;
            }
            if (mesh.numVertices > 0)
            {
                vertexBuffer = new VertexBuffer(type, mesh.numVertices, device, Usage.None, format, Pool.Default);
                vertexBuffer.SetData(verticesTmp, 0, LockFlags.Discard);
            }

            if (showNormals)
            {
                verticesTmp = new CVERTEX[mesh.numVertices * 2];
                for (int i = 0; i < mesh.numVertices; i++)
                {
                    verticesTmp.SetValue(new CVERTEX(mesh.vertices[i], Color.Yellow), i * 2);
                    verticesTmp.SetValue(new CVERTEX(mesh.vertices[i] + mesh.normals[i], Color.Yellow), i * 2 + 1);
                }
                normalBuffer = new VertexBuffer(typeof(CVERTEX), mesh.numVertices * 2, device, Usage.None, CVERTEX.Format, Pool.Default);
                normalBuffer.SetData(verticesTmp, 0, LockFlags.Discard);
            }

        }
        public override void OnDeviceLost()
        {
            if (vertexBuffer != null) vertexBuffer.Dispose();
            vertexBuffer = null;
            if (indexBuffer != null) indexBuffer.Dispose();
            indexBuffer = null;
        }
        public override void OnDrawing(Device device)
        {
            if (vertexBuffer == null)
            {
                OnDeviceReset(device);
            }
            if (vertexBuffer != null)
            {
                device.RenderState.Lighting = true;
                device.Lights[0].Enabled = true;
                device.VertexFormat = format;
                //device.Transform.World = mesh.transform.matrix * Camera.world;
                device.Indices = indexBuffer;
                device.SetStreamSource(0, vertexBuffer, 0);
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, mesh.numVertices, 0, mesh.numTriangles);
                //device.DrawPrimitives(PrimitiveType.TriangleList, 0, numIndices / 3);
            }
            if (showVertexIndex)
            {
                for (int i = 0; i < mesh.numVertices; i++)
                {
                    Vector3 p = mesh.vertices[i];
                    //p.Project(device.Viewport, Camera.projection, Camera.view, Camera.world);
                    //EngineCore.text.DrawText(null, String.Format("{0,1}", i), new Point((int)p.X, (int)p.Y), Color.Yellow);
                }
            }
            if (showNormals)
            {
                device.RenderState.Lighting = false;
                device.VertexFormat = CVERTEX.Format;
                device.SetStreamSource(0, normalBuffer, 0);
                device.DrawPrimitives(PrimitiveType.LineList, 0, mesh.numVertices);
            }
        }
    }

    /// <summary>
    /// Grid custom objects , <see cref="IAutoBuffer"/>
    /// </summary>
    public class DxAutoGrid : DxAutoNode
    {
        public static uint classHandleID { get { return 1; } }
        //             
        //             
        //         1___3___5___7___9          Y
        //        /___/___/___/___/           |__Z
        //       /___/___/___/___/           /
        //      0   2   4   6   8           X
        //
        VertexBuffer vertexBuffer;
        CVERTEX[] vertices;

        public DxAutoGrid()
        {
            base.handle = 1;

            vertices = new CVERTEX[38];

            int i = 0;
            for (int z = -4; z <= 4; z++)
            {
                vertices[i++] = new CVERTEX(4, 0, z, z == 0 ? Color.Black.ToArgb() : Color.Gray.ToArgb());
                vertices[i++] = new CVERTEX(-4, 0, z, z == 0 ? Color.Black.ToArgb() : Color.Gray.ToArgb());
            }
            for (int x = -4; x <= 4; x++)
            {
                vertices[i++] = new CVERTEX(x, 0, 4, x == 0 ? Color.Black.ToArgb() : Color.Gray.ToArgb());
                vertices[i++] = new CVERTEX(x, 0, -4, x == 0 ? Color.Black.ToArgb() : Color.Gray.ToArgb());
            }
            vertices[i++] = new CVERTEX(0, 5, 0, Color.Black.ToArgb());
            vertices[i++] = new CVERTEX(0, 0, 0, Color.Black.ToArgb());
        }


        public override int PrimitiveIntersection(Ray ray, out float t, out Vector3 intersection)
        {
            throw new NotImplementedException();
        }

        public override void OnRenderBegin(Device device)
        {
            if (vertexBuffer == null)
                this.OnDeviceReset(device);
        }

        public override void OnDeviceReset(Device device)
        {
            if (vertices != null & vertices.Length > 0)
            {
                vertexBuffer = new VertexBuffer(typeof(CVERTEX), vertices.Length, device, Usage.WriteOnly, CVERTEX.Format, Pool.Managed);
                vertexBuffer.SetData(vertices, 0, LockFlags.Discard);
            }
        }
        public override void OnDeviceLost()
        {
            //vertices are already stored
            if (vertexBuffer != null) vertexBuffer.Dispose();
            vertexBuffer = null;
        }
        public override void OnDrawing(Device device)
        {
            if (vertexBuffer != null)
            {
                device.RenderState.Lighting = false;
                device.VertexFormat = CVERTEX.Format;
                //device.Transform.World = Camera.world;
                device.SetStreamSource(0, vertexBuffer, 0);
                device.DrawPrimitives(PrimitiveType.LineList, 0, vertices.Length / 2);
            }
        }
    }

    /// <summary>
    /// XYZ axis custom objects <see cref="IAutoBuffer"/>
    /// </summary>    
    public class DxAutoAxis : DxAutoNode
    {
        public static uint classHandleID { get { return 2; } }
        //             
        //       Y     
        //       |
        //       |____Z
        //      /
        //     X
        //
        VertexBuffer vertexBuffer;
        CVERTEX[] vertices;

        public DxAutoAxis()
        {
            base.handle = 2;

            vertices = new CVERTEX[]{
                new CVERTEX(1,0,0,Color.Red.ToArgb()),
                new CVERTEX(0,0,0,Color.Red.ToArgb()),
                new CVERTEX(0,1,0,Color.Green.ToArgb()),
                new CVERTEX(0,0,0,Color.Green.ToArgb()),
                new CVERTEX(0,0,1,Color.Blue.ToArgb()),
                new CVERTEX(0,0,0,Color.Blue.ToArgb())};
        }

        public override int PrimitiveIntersection(Ray ray, out float t, out Vector3 intersection)
        {
            throw new NotImplementedException();
        }
        public override void OnRenderBegin(Device device)
        {
            /*
            Vector3 screenPos = new Vector3(50, device.Viewport.Height - 50, 0);
            Vector3 near = Vector3.Unproject(screenPos, device.Viewport, Camera.projection, Camera.view, Camera.world);
            Vector3 far = Vector3.Unproject(screenPos - new Vector3(0, 0, 1), device.Viewport, Camera.projection, Camera.view, Camera.world);
            
            base.transform.Position = near + Vector3.Normalize(near - far) * 20;

            if (vertexBuffer == null)
                this.OnDeviceReset(device);
             * */
        }
        public override void OnDeviceReset(Device device)
        {
            if (vertices != null & vertices.Length > 0)
            {
                vertexBuffer = new VertexBuffer(typeof(CVERTEX), vertices.Length, device, Usage.WriteOnly, CVERTEX.Format, Pool.Managed);
                vertexBuffer.SetData(vertices, 0, LockFlags.Discard);
            }
        }
        public override void OnDeviceLost()
        {
            //vertices are already stored
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = null;
        }
        public override void OnDrawing(Device device)
        {
            if (vertexBuffer != null)
            {

                //Vector3 look = new Vector3(CameraValues.view.M13, CameraValues.view.M23, CameraValues.view.M33); 
                //EngineB.text.DrawText(null, "[xyz]", new Point((int)screenPos.X, (int)screenPos.Y), Color.Yellow);

                //base.Position += look;
                device.RenderState.Lighting = false;
                device.VertexFormat = CVERTEX.Format;
                //device.Transform.World = base.transform.matrix * Camera.world;
                device.SetStreamSource(0, vertexBuffer, 0);
                device.DrawPrimitives(PrimitiveType.LineList, 0, vertices.Length / 2);
            }
        }
    }

    public class DxAutoSelection : DxAutoNode
    {
        public static uint classHandleID { get { return 3; } }
        //  8 corner vertices          01           11
        //           5______4         /___03    13__/
        //           /     /|         |             |
        //         1/_____/0|         02           12
        //          | 7   | /6       01 = V0 -X      
        //          |_____|/         02 = V0 -Y
        //         3      2          03 = V0 -Z
        //                    
        IndexBuffer indexBuffer;
        VertexBuffer vertexBuffer;
        CVERTEX[] vertices;
        ushort[] indices;

        void updateLenght(float zoom)
        {
            if (zoom > 1) zoom = 1;
            if (zoom < 0) zoom = 0;
            for (int i = 0; i < 8; i++)
            {
                //corner i
                Vector3 intern = vertices[i].p * (1 - zoom);
                
                //edge1
                vertices[8 + i * 3 + 0].p = vertices[i].p;
                vertices[8 + i * 3 + 0].p.x = intern.x;
                //edge2
                vertices[8 + i * 3 + 1].p = vertices[i].p;
                vertices[8 + i * 3 + 1].p.y = intern.y;
                //edge3
                vertices[8 + i * 3 + 2].p = vertices[i].p;
                vertices[8 + i * 3 + 2].p.z = intern.z;
            }
        }

        public DxAutoSelection()
        {
            base.handle = 3;

            Vector3[] corner = new Vector3[]{
                new Vector3( 1, 1, 1),
                new Vector3( 1, 1,-1),
                new Vector3( 1,-1, 1),
                new Vector3( 1,-1,-1),
                new Vector3(-1, 1, 1),
                new Vector3(-1, 1,-1),
                new Vector3(-1,-1, 1),
                new Vector3(-1,-1,-1)};

            vertices = new CVERTEX[32];
            // fixed corner values
            vertices[0] = new CVERTEX(1, 1, 1, Color.White.ToArgb());
            vertices[1] = new CVERTEX(1, 1, -1, Color.White.ToArgb());
            vertices[2] = new CVERTEX(1, -1, 1, Color.White.ToArgb());
            vertices[3] = new CVERTEX(1, -1, -1, Color.White.ToArgb());
            vertices[4] = new CVERTEX(-1, 1, 1, Color.White.ToArgb());
            vertices[5] = new CVERTEX(-1, 1, -1, Color.White.ToArgb());
            vertices[6] = new CVERTEX(-1, -1, 1, Color.White.ToArgb());
            vertices[7] = new CVERTEX(-1, -1, -1, Color.White.ToArgb());
            // variable lenght
            for (int i = 8; i < 32; i++)
                vertices[i] = new CVERTEX(0, 0, 0, Color.White.ToArgb());//Color.CornflowerBlue);

            indices = new ushort[48];
            int j = 0;
            for (int i = 0; i < 8; i++)
            {
                indices[j++] = (ushort)i;
                indices[j++] = (ushort)(8 + i * 3 + 0);
                indices[j++] = (ushort)i;
                indices[j++] = (ushort)(8 + i * 3 + 1);
                indices[j++] = (ushort)i;
                indices[j++] = (ushort)(8 + i * 3 + 2);
            }
            updateLenght(1);
        }

        public override int PrimitiveIntersection(Ray ray, out float t , out Vector3 intersection)
        {
            throw new NotImplementedException();
        }
        public override void OnRenderBegin(Device device)
        {
            //updateLenght(Camera.zoom / 70f);
            
            if (vertexBuffer != null) vertexBuffer.Dispose();
            vertexBuffer = null;
            vertexBuffer = new VertexBuffer(typeof(CVERTEX), vertices.Length, device, Usage.WriteOnly, CVERTEX.Format, Pool.Managed);
            vertexBuffer.SetData(vertices, 0, LockFlags.Discard);

            if (indexBuffer != null) indexBuffer.Dispose();
            indexBuffer = null;
            indexBuffer = new IndexBuffer(typeof(ushort), indices.Length, device, Usage.WriteOnly, Pool.Managed);
            indexBuffer.SetData(indices, 0, LockFlags.Discard);
        }
        public override void OnDeviceReset(Device device)
        {
            // the managed buffer don't need Reset
        }
        public override void OnDeviceLost()
        {
            //vertices are already stored
            if (vertexBuffer != null) vertexBuffer.Dispose();
            vertexBuffer = null;
            if (indexBuffer != null) indexBuffer.Dispose();
            indexBuffer = null;
        }
        public override void OnDrawing(Device device)
        {
            if (vertexBuffer != null || indexBuffer != null)
            {
                device.RenderState.Lighting = false;
                device.VertexFormat = CVERTEX.Format;
                
                device.SetStreamSource(0, vertexBuffer, 0);
                device.Indices = indexBuffer;

                //device.Transform.World = base.transform.matrix * Camera.world;
                device.DrawIndexedPrimitives( PrimitiveType.LineList, 0, 0, vertices.Length, 0, indices.Length / 2);
            }
        }
    }
}