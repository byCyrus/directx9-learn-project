﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

using Engine.Tools;
using Engine.Maths;

namespace Engine.Graphics
{
    /// <summary>
    /// Graphic Device using Microsoft managed sdk
    /// </summary>
    /// <remarks>
    /// Disable LoaderLock exception because is a problem of directx managed library
    /// </remarks>
    public class DX9Device : IDisposable
    {
        public const string graphicVersion = "Microsoft Directx 9";

        private Control m_control;

        /// <summary>
        /// Set the render targhet size where scene are rendered
        /// </summary>
        public Viewport viewport
        {
            get { return Helper.Convert(m_viewport); }
            set { m_viewport = Helper.Convert(value); }
        }
        public Viewport[] SupportedDisplayMode;

        // i'm using "internal" to use these values only inside library
        internal D3D.Device m_device;
        internal D3D.SwapChain m_swapchain; // main shaopchain
        internal D3D.Surface m_backbuffer;
        internal D3D.Surface m_zbuffer;
        internal D3D.Viewport m_viewport;
        internal D3D.PresentParameters m_param;

        internal bool useStencil = false;
        internal bool useZbuffer = false;
        internal bool full_screen = false;
        internal bool softwareProcessing = false;
        
        /// <summary>
        /// Directx9 constant
        /// </summary>
        const int MaxLights = 8;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="useStencil">for a true 3d world</param>
        /// <param name="tryPureDevice">try the best perfomance, old gpu don't support puredevice</param>
        public DX9Device(Control control, bool useZbuffer, bool useStencil, bool tryPureDevice)
        {
            m_control = control; 
            this.useZbuffer = useZbuffer;
            this.useStencil = useZbuffer && useStencil;
            
            //// initialise directX with presentParameters setting
            // 8-bit and 24-bit back buffers are not supported in DX9.
            /*
                D3D.Format.R5G6B5,   // 16bit
                D3D.Format.A1R5G5B5, // 16bit with 1 alpha bit (only windowed)
                D3D.Format.X1R5G5B5, // 16bit with 1 ignored bit (only windowed)
                D3D.Format.X8R8G8B8, // 24bit (used 32bit because is more fast)
                D3D.Format.A8R8G8B8, // 32bit
                D3D.Format.A2R10G10B10, // never use (full-screen mode only)
                D3D.Format.Unknown   // (only windowed)

                D3D.DepthFormat.D16,    // 16bit z-buffer , 16 for depth
                D3D.DepthFormat.D24X8,  // 32bit z-buffer , 24 for depth
                D3D.DepthFormat.D24S8,  // 32bit z-buffer , 24 for depth , 8 for stencil
                D3D.DepthFormat.D24X4S4 // 32bit z-buffer , 24 for depth , 4 for stencil
            */
            Format backbufferFormat = Format.X8R8G8B8;
            Format depthstencilFormat = useStencil ? Format.D24S8 : Format.D24X8;


            Debug.Assert(DX9Enumerations.CheckDeviceType(backbufferFormat, backbufferFormat, true), "Device screen format not compatible");
            if (useZbuffer) Debug.Assert(DX9Enumerations.CheckDepthStencil(backbufferFormat, depthstencilFormat), "Device screen stencil format not compatible");

            //setting for a correct windows directX application
            m_param = new D3D.PresentParameters();
            m_param.Windowed = true;
            m_param.DeviceWindowHandle = control.Handle;
            m_param.BackBufferFormat = (D3D.Format)(full_screen ? Format.Unknown : backbufferFormat);
            m_param.EnableAutoDepthStencil = useZbuffer;
            m_param.PresentationInterval = D3D.PresentInterval.Immediate;
            m_param.SwapEffect = D3D.SwapEffect.Discard;
            m_param.BackBufferCount = 1;
            m_param.BackBufferHeight = full_screen ? 0 : control.ClientSize.Height;
            m_param.BackBufferWidth = full_screen ? 0 : control.ClientSize.Width;

            if (useZbuffer) m_param.AutoDepthStencilFormat = (D3D.DepthFormat)depthstencilFormat;


            D3D.CreateFlags flags = D3D.CreateFlags.SoftwareVertexProcessing;
            softwareProcessing = true;
            if (tryPureDevice)
            {
                if (DX9Enumerations.SupportsHardwareTransformAndLight)
                {
                    flags = D3D.CreateFlags.HardwareVertexProcessing;
                    softwareProcessing = false;
                }
                if (DX9Enumerations.SupportsPureDevice)
                {
                    flags |= D3D.CreateFlags.PureDevice;
                    softwareProcessing = false;
                }
            }
            Debug.WriteLine("Device Caps : " + flags.ToString());

            m_device = new D3D.Device(0, D3D.DeviceType.Hardware, control.Handle, flags, m_param);

            renderstates = new DX9RenderStatesManager(m_device);

            m_device.DeviceReset += new EventHandler(OnDeviceReset);
            m_device.DeviceLost += new EventHandler(OnDeviceLost);

            SetRendersStates();

            m_viewport = m_device.Viewport;
            m_swapchain = m_device.GetSwapChain(0);
            m_backbuffer = m_device.GetBackBuffer(0, 0, D3D.BackBufferType.Mono);
            m_zbuffer = useZbuffer ? m_device.DepthStencilSurface : null;

            // clean the viewport
            this.Clear(control.BackColor);
        }

        #region RenderStates

        public DX9RenderStatesManager renderstates { get; private set; }
        /// <summary>
        /// Set default render states, used example after device reseting
        /// </summary>
        public void SetRendersStates()
        {
            renderstates.StencilEnable = useStencil;
            renderstates.ZBufferEnable = useZbuffer;
        }
        
        public Color32 Ambient
        {
            get { return m_device.RenderState.Ambient; }
            set { m_device.RenderState.Ambient = value; }
        }
        public void SetLight(Light light, int index)
        {
            if (index >= DX9Enumerations.MaxLights)
                throw new ArgumentOutOfRangeException("index of light out of range");

            m_device.Lights[index].Ambient = light.Ambient;
            m_device.Lights[index].Diffuse = light.Diffuse;
            m_device.Lights[index].Direction = Helper.Convert(light.Direction);
            m_device.Lights[index].Position = Helper.Convert(light.Position);
            m_device.Lights[index].Specular = light.Specular;
            m_device.Lights[index].Type = (D3D.LightType)light.Type;
            m_device.Lights[index].Enabled = true;
        }
        public Light GetLight(int index)
        {
            if (index >= DX9Enumerations.MaxLights)
                throw new ArgumentOutOfRangeException("index of light out of range");
            return Helper.Convert(m_device.Lights[index]);
        }
        /// <summary>
        /// Set of Get the testure, can be null
        /// </summary>
        public DX9Texture Texture
        {
            set { m_device.SetTexture(0, value != null ? value.m_texture : null); }
        }
        public Material Material
        {
            get { return Helper.Convert(m_device.Material); }
            set { m_device.Material = Helper.Convert(value); }
        }      
        #endregion

        #region Properties

        public int BackBufferWidth
        {
            get { return m_param.BackBufferWidth; }
            set { m_param.BackBufferWidth = value; }
        }
        public int BackBufferHeight
        {
            get { return m_param.BackBufferHeight; }
            set { m_param.BackBufferHeight = value; }
        }
        #endregion

        #region Events
        void OnDeviceLost(object sender, EventArgs e)
        {
            D3D.Device dev = (D3D.Device)sender;
            Debug.WriteLine("Device Lost Event");

            if (m_device.Disposed)
            {
                Debug.WriteLine("Device already disposed in lost event, dispose before closing app");
                return;
            }

            m_device.SetStreamSource(0, null, 0, 0);
            m_device.SetTexture(0, null);
            m_device.Indices = null;

            if (m_swapchain != null) m_swapchain.Dispose();
            if (m_backbuffer != null) m_backbuffer.Dispose();
            if (m_zbuffer != null) m_zbuffer.Dispose();

            int result = 0;
            bool state = m_device.CheckCooperativeLevel(out result);

        }
        void OnDeviceReset(object sender, EventArgs e)
        {
            D3D.Device dev = (D3D.Device)sender;
            Debug.WriteLine("Device Reset Event");

            m_swapchain = m_device.GetSwapChain(0);
            m_backbuffer = m_device.GetBackBuffer(0, 0, D3D.BackBufferType.Mono);
            m_zbuffer = useZbuffer ? m_device.DepthStencilSurface : null;

            m_viewport = new D3D.Viewport();
            m_viewport.MaxZ = 1;
            m_viewport.MinZ = 0;
            m_viewport.Height = m_control.ClientSize.Height;
            m_viewport.Width = m_control.ClientSize.Width;
            m_viewport.X = 0;
            m_viewport.Y = 0;

            m_device.Viewport = m_viewport;

            SetRendersStates();
        }
        #endregion

        #region Methods
        
        /// <summary>
        /// reset the device
        /// </summary>
        public void Reset()
        {
            Debug.WriteLine("Device Reset Manualy");

            int result;
            bool state = m_device.CheckCooperativeLevel(out result);

            try
            {
                m_device.Reset(m_param);
            }
            catch(Exception e)
            {
                state = m_device.CheckCooperativeLevel(out result);
                Console.WriteLine(e.ToString() + "\n" + ((D3D.ResultCode)result).ToString());
            }

            bool wait = true;
            int seconds = 0;
            while (wait)
            {
                try
                {
                    m_device.TestCooperativeLevel();
                    wait = false;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Test Coop : " + e.ToString());
                    wait = true;
                    System.Threading.Thread.Sleep(100);
                }
                if (seconds > 10)
                    throw new Exception("Device wan't be reset");
                seconds++;
            }
        }

        /// <summary>
        /// Clear backbuffer using a color
        /// </summary>
        /// <param name="color"></param>
        public void Clear(Color color)
        {
            D3D.ClearFlags flags = D3D.ClearFlags.Target;
            if (useZbuffer) flags |= D3D.ClearFlags.ZBuffer;
            if (useStencil) flags |= D3D.ClearFlags.Stencil;
            m_device.Clear(flags, color, 1.0f, 0);
        }
        /// <summary>
        /// begin scene
        /// </summary>
        public DeviceState Begin()
        {
            int result = 0;

            if (m_device.CheckCooperativeLevel(out result))
            {
                //Okay to render
                try
                {
                    //m_device.SetRenderTarget(0, m_backbuffer);
                    m_device.Viewport = m_viewport;
                    m_device.BeginScene();

                }
                catch (D3D.DeviceLostException)
                {
                    Debug.WriteLine("DeviceLostException");
                    m_device.CheckCooperativeLevel(out result);
                    return DeviceState.DeviceLost;
                }
                catch (D3D.DeviceNotResetException)
                {
                    Debug.WriteLine("DeviceNotResetException");
                    m_device.CheckCooperativeLevel(out result);
                    return DeviceState.DeviceNotReset;
                }
                return DeviceState.OK;
            }
            if (result == (int)D3D.ResultCode.DeviceLost)
            {
                //Can't Reset yet, wait for a bit
                //System.Threading.Thread.Sleep(500);
                Debug.WriteLine("DeviceIsLost");
                return DeviceState.DeviceLost;
            }
            else if (result == (int)D3D.ResultCode.DeviceNotReset)
            {
                //Reset();
                Debug.WriteLine("DeviceIsNotReset");
                return DeviceState.DeviceNotReset;
            }

            return DeviceState.OK;
        }
        /// <summary>
        /// Set current backbuffer and depthstencilbuffer
        /// </summary>
        public void SetRenderTarghet()
        {
            m_device.SetRenderTarget(0, m_backbuffer);
            if (m_zbuffer != null) m_device.DepthStencilSurface = m_zbuffer;

            SetRendersStates();

        }
        /// <summary>
        /// end scene
        /// </summary>
        public void End()
        {
            // for my old pc need pass null buffer after each render to avoid "lines issue", i don't know why
            m_device.SetStreamSource(0, null, 0);
            m_device.Indices = null;

            m_device.EndScene();
        }
        /// <summary>
        /// present on screen
        /// </summary>
        public void Present()
        {
            //m_swapchain.Present(m_control);
            //m_backbuffer.ReleaseGraphics();
            m_device.Present();
        }
        /// <summary>
        /// reseting
        /// </summary>
        public void ResizeViewport()
        {
            D3D.Viewport viewport = new D3D.Viewport();
            viewport.Width = m_control.ClientSize.Width;
            viewport.Height = m_control.ClientSize.Height;
            viewport.X = 0;
            viewport.Y = 0;
            viewport.MaxZ = 1;
            viewport.MinZ = 0;
            this.m_viewport = viewport;
        }


        public void DrawIndexedPrimitives(PrimitiveType type, int baseVertex, int minVertexIndex, int numVertices, int startIndex, int numPrimitives)
        {
            if (numPrimitives > 0 && numVertices > 0)
                m_device.DrawIndexedPrimitives((D3D.PrimitiveType)type, baseVertex, minVertexIndex, numVertices, startIndex, numPrimitives);
        }
        public void DrawPrimitives(PrimitiveType type, int startVertex, int numPrimitives)
        {
            if (numPrimitives > 0)
                m_device.DrawPrimitives((D3D.PrimitiveType)type, startVertex, numPrimitives);
        }
        public void DrawUserPrimitives<T>(PrimitiveType type, int numPrimitives, T[] array) where T : struct
        {
            m_device.DrawUserPrimitives((D3D.PrimitiveType)type, numPrimitives, array);
        }
        #endregion

        /// <summary>
        /// dispose the device
        /// </summary>
        ~DX9Device()
        {
            this.Dispose();
        }
        /// <summary>
        /// dispose
        /// </summary>
        public void Dispose()
        {
            if (m_device != null) m_device.Dispose();
            if (m_backbuffer != null) m_backbuffer.Dispose();
            if (m_zbuffer != null) m_zbuffer.Dispose();
            if (m_swapchain != null) m_swapchain.Dispose();
        }
        /// <summary>
        /// return video card description
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            //stampa la descrizione della scheda video
            //return video card description
            string info = D3D.Manager.Adapters[0].Information.Description + "\n";
            info += "use depthstencil : " + useStencil.ToString()+"\n";
            info += "use hardware     : " + (!softwareProcessing).ToString()+"\n";
            return info;
        }

        public void SetStreamSourceFrequency(int stream, int frequency, StreamSource source)
        {
            Console.WriteLine(string.Format("{0}: f{1}|s{2}", stream, frequency, source));
            switch(source)
            {
                case StreamSource.IndexedData:
                case StreamSource.InstanceData:
                    m_device.SetStreamSourceFrequency(stream, (int)source | frequency); 
                    break;
                case StreamSource.NonIndexedData:
                    m_device.SetStreamSourceFrequency(stream, 1 | frequency); 
                    break;
                case StreamSource.None:
                    m_device.SetStreamSourceFrequency(stream, frequency);
                    break;
                case StreamSource.Reset:
                    m_device.SetStreamSourceFrequency(stream, 1); 
                    break;
                default: throw new NotImplementedException();
            }

        }
        public void SetVertexShaderNULL()
        {
            m_device.VertexShader = null;
        }
    }


    public class DX9RenderStatesManager
    {
        internal D3D.Device m_device;

        internal DX9RenderStatesManager(D3D.Device device)
        {
            m_device = device;
        }

        public Material material
        {
            get { return Helper.Convert(m_device.Material); }
            set { m_device.Material = Helper.Convert(value); }
        }


        #region Transformsstates

        public Matrix4 World
        {
            get { return Helper.Convert(m_device.Transform.World); }
            set { m_device.Transform.World = Helper.Convert(value); }
        }
        public Matrix4 Projection
        {
            get { return Helper.Convert(m_device.Transform.Projection); }
            set { m_device.Transform.Projection = Helper.Convert(value); }
        }
        public Matrix4 View
        {
            get { return Helper.Convert(m_device.Transform.View); }
            set { m_device.Transform.View = Helper.Convert(value); }
        }
        #endregion

        #region RendersStates
        public bool AlphaBlendEnable
        {
            get { return m_device.RenderState.AlphaBlendEnable; }
            set { m_device.RenderState.AlphaBlendEnable = value; }
        }
        public Compare AlphaFunction
        {
            get { return (Compare)m_device.RenderState.AlphaFunction; }
            set { m_device.RenderState.AlphaFunction = (D3D.Compare)value; }
        }
        public bool AlphaTestEnable
        {
            get { return m_device.RenderState.AlphaTestEnable; }
            set { m_device.RenderState.AlphaTestEnable = value; }
        }
        public Cull CullMode
        {
            get { return (Cull)m_device.RenderState.CullMode; }
            set { m_device.RenderState.CullMode = (D3D.Cull)value; }
        }
        public float DepthBias
        {
            get { return m_device.RenderState.DepthBias; }
            set { m_device.RenderState.DepthBias = value; }
        }
        public FillMode FillMode
        {
            get { return (FillMode)m_device.RenderState.FillMode; }
            set { m_device.RenderState.FillMode = (D3D.FillMode)value; }
        }
        public bool Lighting
        {
            get { return m_device.RenderState.Lighting; }
            set { m_device.RenderState.Lighting = value; }
        }
        public bool NormalizeNormals
        {
            get { return m_device.RenderState.NormalizeNormals; }
            set { m_device.RenderState.NormalizeNormals = value; }
        }
        public bool StencilEnable
        {
            get { return m_device.RenderState.StencilEnable; }
            set { m_device.RenderState.StencilEnable = value; }
        }
        public bool ZBufferEnable
        {
            get { return m_device.RenderState.ZBufferEnable; }
            set { m_device.RenderState.ZBufferEnable = value; }
        }
        public Compare ZBufferFunction
        {
            get { return (Compare)m_device.RenderState.ZBufferFunction; }
            set { m_device.RenderState.ZBufferFunction = (D3D.Compare)value; }
        }
        public bool ZBufferWriteEnable
        {
            get { return m_device.RenderState.ZBufferWriteEnable; }
            set { m_device.RenderState.ZBufferWriteEnable = value; }
        }
        #endregion
    }
}