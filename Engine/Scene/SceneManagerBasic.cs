﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Engine.Renderer;
using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;

namespace Engine.Scene
{
    /// <summary>
    /// A scene manager contain the 3d world and the renderer engine. It must be indipendent from renderer
    /// because need to work also without graphics for debug. The node are sorted with a uint key (unique id)
    /// </summary>
    public class SceneManagerBasic : IDisposable
    {
        /// <summary>
        /// used to measure the dt for timeline
        /// </summary>
        private int LastTickCount = 1;

        struct SceneNodeRayParam : IComparable<SceneNodeRayParam>
        {
            public float t;
            public int idx;
            public uint h;
            public SceneNodeRayParam(float rayParam, int index, uint handle)
            {
                t = rayParam;
                h = handle;
                idx = index;
            }
            public int CompareTo(SceneNodeRayParam other)
            {
                return (t > other.t) ? 1 : (t < other.t ? -1 : 0);
            }
            public override string ToString()
            {
                return (t + " " + idx + " " + h);
            }
        }

        protected EngineCore renderer = null;
        
        /// <summary>
        /// The collections of scene nodes, can't use a simple List or Dictionary
        /// </summary>
        public UnsortedCollector<ISceneNode> Nodes = new UnsortedCollector<ISceneNode>();

        /// <summary>
        /// Collection of indipendent scene nodes like selection gizmo...
        /// </summary>
        public UnsortedCollector<ISceneNode> HelpNodes = new UnsortedCollector<ISceneNode>();

        /// <summary>
        /// Scene Manager can run also without renderer when you are debuging, but
        /// if you assign a new Device the engine must be reinizialized
        /// </summary>
        public GraphicDevice RendererDevice
        {
            set
            {
                if (!InitializeEngine(value))
                    throw new ExecutionEngineException("Engine initialization error");
            }
        }
        /// <summary>
        /// Necessary for engine, no-device no-party , if Device changed need to reinizialize the engine
        /// because all managed buffers are lost
        /// </summary>
        bool InitializeEngine(GraphicDevice device)
        {
            if (device == null)
                throw new ArgumentNullException();

            if (renderer != null) renderer.Dispose();
            renderer = null;
            
            try
            {
                renderer = new EngineCore(device);

                foreach (ISceneNode snode in Nodes)
                    foreach (Node node in snode.geometries)
                        this.renderer.AddNode(node);

                foreach (ISceneNode snode in HelpNodes)
                    foreach (Node node in snode.geometries)
                        this.renderer.AddNode(node);
            }
            catch
            {
                return false;
            }
            return true;
        }
        
        /// <summary>
        /// Draw to panel the scene using your custom options 
        /// </summary>
        public virtual void Render(GraphicPanel panel, ViewportOption options)
        {
            // When the difference of current and last tickcount > 10 ms do the animations operation
            // So the whole time line are subdivide in 10ms (1Second is too big), dt = 10ms
            // A movie usually use 29FPS = 1000/29 = 34ms for each frames 
            float dt = Environment.TickCount - LastTickCount;
            if (dt > 34)
            {
                LastTickCount = Environment.TickCount;
                UpdateAnimation(dt);
            }

            foreach (ISceneNode snode in HelpNodes)
            {
                snode.Update(panel.camera);
                foreach(Node node in snode.geometries)
                    renderer.UpdateNode(node, UpdateModeEnum.OnlyVertices);
            }

            if (renderer == null) return;

            renderer.Draw();


            // This is like frustum culling to dont' draw objets outside view 
            Vector3 cameraEye = panel.camera.eye;
            float near = panel.camera.nearZ;
            float far = panel.camera.farZ;

            int height = panel.ClientSize.Height;
            int width = panel.ClientSize.Width;
            
            foreach (ISceneNode node in Nodes)
            {
                float dist = Vector3.GetLength(node.boundSphere.center - cameraEye);
                if (dist < far && dist > near)
                {
                    Point screenPos = panel.WorldToPoint(node.boundSphere.center);
                    if (screenPos.X > 0 && screenPos.X < width && screenPos.Y > 0 && screenPos.Y < height)
                        renderer.fontDx.Draw(node.name.ToString(), screenPos.X,screenPos.Y, node.Selected ? Color.White : Color.Black);
                }
            }
        }

        public virtual void AddSceneNode(ISceneNode node)
        {
            Nodes.Add(node);
            if (renderer != null)
            {
                foreach (Node obj in node.geometries)
                    renderer.AddNode(obj);
            }
        }
        public void CancSceneNode(uint handle)
        {
            Nodes.Canc(handle);
        }
        public void CancSceneNode(ISceneNode node)
        {
            Nodes.Canc(node);
        }


        public virtual void AddHelpSceneNode(ISceneNode node)
        {
            HelpNodes.Add(node);
            if (renderer != null)
            {
                foreach (Node obj in node.geometries)
                    renderer.AddNode(obj);
            }
        }
        public void CancHelpSceneNode(uint handle)
        {
            HelpNodes.Canc(handle);
        }
        public void CancHelpSceneNode(ISceneNode node)
        {
            HelpNodes.Canc(node);
        }


        public virtual void UpdateSceneNode(ISceneNode node)
        {
            // only vertices updating for this moment
            uint handle = node.handle;
            if (handle > 0)
                for (int i = 0; i < node.geometries.Length; i++)
                    renderer.UpdateNode(node.geometries[i], UpdateModeEnum.OnlyVertices);
        }


        #region Picking
        ISceneNode lastSelect = null;
        
        /// <summary>
        /// Brutal force
        /// </summary>
        protected ISceneNode PickSceneNodeDebug(Ray ray, List<ISceneNode> nodesList)
        {
            float t0, t1;
            foreach (ISceneNode node in Nodes)
            {
                if (node.boundSphere.IntersectRay(ray, out t0,out t1))
                    return node;
            }
            return null;
        }
        /// <summary>
        /// Get the nearest scene node from a unsorted node list
        /// </summary>
        /// <param name="nodelist">list of node inside a bucket</param>
        /// <param name="method">0 is more accurate</param>
        /// <returns>return null if not found</returns>
        protected ISceneNode PickNearestSceneNode(Ray ray, List<ISceneNode> nodesList , int method)
        {
            List<SceneNodeRayParam> NodesRayHitParam = new List<SceneNodeRayParam>(nodesList.Count);

            // first get all node hit by ray (using BoundingSphere)
            // hit or no-hit is always O(n)
            int i = 0;
            float t = 0, t0 = 0, t1 = 0;

            foreach (ISceneNode node in nodesList)
            {
                if (node.boundSphere.IntersectRay(ray, out t0, out t1))
                    NodesRayHitParam.Add(new SceneNodeRayParam(t0, i, node.handle));
                i++;
            }
            NodesRayHitParam.TrimExcess();
            int numNodeHit = NodesRayHitParam.Count;
            
            //only one sphere found
            if (numNodeHit == 1)
            {             
                if (nodesList[NodesRayHitParam[0].idx].IntersectGeometries(ray, out t))
                    return nodesList[NodesRayHitParam[0].idx];
                else
                    return null;
            }
            if (numNodeHit > 1)
            {
                // all node hit by ray are sorted using their bounding sphere collision test
                NodesRayHitParam.Sort();
                switch (method)
                {
                    /* IS ACCURATE BECAUSE TEST ALL GEOMETRIES*/
                    case 0:
                        // first get all node hit by ray (using IntersectGeometries)
                        for (i = 0; i < numNodeHit; i++)
                        {
                            SceneNodeRayParam rayparam = NodesRayHitParam[i];
                            // if not intersect apply a infinite cost
                            bool test = nodesList[NodesRayHitParam[i].idx].IntersectGeometries(ray, out t);
                            //Console.WriteLine(nodesList[NodesRayHitParam[i].idx].name.ToString() + " " + test.ToString());
                            rayparam.t = test ? t : float.PositiveInfinity;
                            NodesRayHitParam[i] = rayparam;
                        }

                        NodesRayHitParam.Sort();

                        // if first ray parameter is INF mean that don't exit geometric intersection
                        if (float.IsPositiveInfinity(NodesRayHitParam[0].t))
                            return null;
                        else
                            return nodesList[NodesRayHitParam[0].idx];

                    /* NOT ACCURATE BECAUSE IT SUPPOSE THAT GEOMETRIES ARE SORTED IN RAY DIRECTION BY IT'S BOUNDING SPHERE */
                    case 1:
                        for (i = 0; i < numNodeHit; i++)
                            // exit at first intersection
                            if (nodesList[NodesRayHitParam[i].idx].IntersectGeometries(ray, out t))
                                return nodesList[NodesRayHitParam[i].idx];
                        return null;
                }
            }
            return null;
        }
        /// <summary>
        /// Get the nearest scene node from a unsorted node list, but return the next back
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="nodesList"></param>
        /// <param name="currentSelect">if null do <see cref="PickNearestSceneNode"/></param>
        protected ISceneNode PickNextBackSceneNode(Ray ray, List<ISceneNode> nodesList, ISceneNode currentSelect)
        {
            if (currentSelect == null) return this.PickNearestSceneNode(ray, nodesList, 0);

            List<SceneNodeRayParam> NodesRayHitParam = new List<SceneNodeRayParam>();
            // first get all node hit by ray (using BoundingSphere)
            // hit or no-hit is always O(n)
            int i = 0;
            float t = 0, t0 = 0, t1 = 0;
            bool exist = false;
            foreach (ISceneNode node in nodesList)
            {
                if (node.boundSphere.IntersectRay(ray, out t0, out t1))
                {
                    NodesRayHitParam.Add(new SceneNodeRayParam(t0, i, node.handle));
                    if (node.handle==currentSelect.handle)
                        exist = true;
                }
                i++;
            }
            NodesRayHitParam.TrimExcess();
            int numNodeHit = NodesRayHitParam.Count;
            //only one sphere found
            if (numNodeHit == 1)
            {
                if (nodesList[NodesRayHitParam[0].idx].IntersectGeometries(ray, out t))
                    return nodesList[NodesRayHitParam[0].idx];
                else
                    return null;
            }
            if (numNodeHit > 1)
            {
                // all node hit by ray are sorted using their bounding sphere collision test
                NodesRayHitParam.Sort();

                // if exist==true start from this selected node else start from first
                int imin = 0;
                if (exist)
                    while (imin < numNodeHit && NodesRayHitParam[imin++].h != currentSelect.handle);
                // if selected node is the last node, start from begin
                if (imin >= numNodeHit) imin = 0;

                //Console.WriteLine("jump to " + imin + " (" + numNodeHit + ")");

                // first get all node hit by ray (using IntersectGeometries)
                for (i = imin; i < numNodeHit; i++)
                {
                    SceneNodeRayParam rayparam = NodesRayHitParam[i];
                    // if not intersect apply a infinite cost
                    bool test = nodesList[NodesRayHitParam[i].idx].IntersectGeometries(ray, out t);
                    //Console.WriteLine(nodesList[NodesRayHitParam[i].idx].name.ToString() + " " + test.ToString());
                    rayparam.t = test ? t : float.PositiveInfinity;
                    NodesRayHitParam[i] = rayparam;
                }

                NodesRayHitParam.Sort(imin, numNodeHit - imin, null);

                // if first ray parameter is INF mean that don't exit geometric intersection
                if (float.IsPositiveInfinity(NodesRayHitParam[imin].t))
                    return null;
                else
                    return nodesList[NodesRayHitParam[imin].idx];
            }
            return null;
        }
        
        /// <summary>
        /// Brutal Interection of each Bounding Sphere + Its intersection implementetion.
        /// if firstHit = true, try to find the first hit node
        /// If Ray hit nothing the function is O(n)
        /// </summary>
        public virtual ISceneNode PickSceneNode(Ray ray, bool firstHit)
        {
            //return PickSceneNodeDebug(ray, Nodes.nodesList);
            if (firstHit)
            {
                // nodesList == Nodes.nodesList because isn't optimized into buckets
                return PickNearestSceneNode(ray, Nodes.valueList, 0);
            }
            else
            {
                lastSelect = PickNextBackSceneNode(ray, Nodes.valueList, lastSelect);
                return lastSelect;
            }
        }

        /// <summary>
        /// Brutal Interection of each Bounding Sphere + Its intersection implementetion.
        /// </summary>
        public virtual ISceneNode PickSceneHelpNode(Ray ray)
        {
            return PickNearestSceneNode(ray, HelpNodes.valueList, 0);
        }
        
        
        #endregion


        /// <summary>
        /// Animations test, only to check if renderer update correctly
        /// the buffers,
        /// </summary>
        public void UpdateAnimation(float dt_ms)
        {

            // simple rotation movement for first node
            float velocity = (float)(MathUtils.DegreeToRadian(1.0f)/ 100.0); // 1 degree(in radian) / 100 ms 
            
            ISceneNode node = Nodes[0];
            //Console.WriteLine("Updating node : " + node.handle.ToString("X4") + " time :" + dt_ms + " ms");

            node.transform = node.transform * Matrix4.RotationYawPitchRoll(velocity * dt_ms, velocity * dt_ms * 2.0f, velocity * dt_ms * 3.0f);
            UpdateSceneNode(node);

            ISceneNode gizmo = HelpNodes[0];
            gizmo.position = gizmo.position + new Vector3(0.1f, 0.1f, 0.1f);
            UpdateSceneNode(gizmo);
        }

        /// <summary>
        /// Reset all scene
        /// </summary>
        public virtual void ClearScene()
        {
            renderer.ClearNodes();
            Nodes.Clear();
            HelpNodes.Clear();
        }
        public virtual void Dispose()
        {
            renderer.Dispose();
            renderer = null;
        }
        public virtual void ToConsole()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (ISceneNode node in Nodes)
            {
                Console.WriteLine(node.ToString());
            }
            Console.WriteLine("");
            Console.ResetColor();
        }
    }


}
