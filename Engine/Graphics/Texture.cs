﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;

namespace Engine.Graphics
{
    public abstract class TextureBase : DeviceResource, IResettable
    {
        internal static TextureBase prevtexture = null;
        /// <summary>
        /// Tell if texture was assigned to device
        /// </summary>
        protected bool inUse
        {
            get { return TextureBase.prevtexture == this; } 
        }

        protected GraphicDevice m_device;
        protected bool managed;
        protected Pool m_memory;
        protected Usage m_usage;
        protected int m_width, m_height;


        protected TextureBase(GraphicDevice device, bool managed , int width, int height)
        {
            this.m_device = device;
            this.managed = managed;
            this.m_usage = Usage.None;
            this.m_memory = managed ? Pool.Managed : Pool.Default;
            this.m_width = width;
            this.m_height = height;

            if (!managed)
            {
                device.resourcesToDispose.Add(this);
                device.resourcesToRestore.Add(this);
            }
        }
        
        ~TextureBase()
        {
            if (!managed)
            {
                m_device.resourcesToDispose.Remove(this);
                m_device.resourcesToRestore.Remove(this);
            }
        }
        /// <summary>
        /// An internal method check if texture is already set, this avoid a unecessary reloading
        /// </summary>
        public abstract void SetToDevice();

        public abstract void Restore();

        public int Windth { get { return m_width; } }
        public int Height { get { return m_height; } }
    }


    /// <summary>
    /// A simple texture
    /// </summary>
    public class Texture : TextureBase
    {
        //  0,0 _______--> U (1,0)
        //   |         |
        //   |  BMP    |
        //   |         |
        //   |_________| (1,1)
        //   !
        //   V (0,1)

        internal DX9Texture m_texture;

        ImageDestination m_resource;
        Bitmap m_bitmap;
        string m_filename;
        int m_stage = 0;

        public DX9TextureState TextureStates
        {
            get { return m_texture.m_state; }
            set { m_texture.m_state = value; }
        }

        private Texture(GraphicDevice device, bool managed,int width, int height, int stage)
            : base(device, managed,width,height)
        {
            this.StageID = stage;
        }

        /// <summary>
        /// if bitmap is NULL, use a default image
        /// </summary>
        /// <param name="bitmap">can be NULL</param>
        /// <param name="managed"></param>
        /// <param name="stage">defulat 0, used for simultaneous textures</param>
        public Texture(GraphicDevice device, Bitmap bitmap, bool managed, int stage)
            : this(device, managed, bitmap.Width, bitmap.Height, stage)
        {
            if (bitmap != null && GraphicDevice.capabilities.SupportTextureSize(bitmap.Size))
                m_bitmap = bitmap;
            else
                m_bitmap = Engine.Properties.Resources.empytexture0;

            m_texture = DX9Texture.FromBitmap(m_device.m_device, m_bitmap, m_usage, m_memory, stage);

            m_resource = ImageDestination.FromBitmap;

            disposed = false;
        }
        /// <summary>
        /// filename can not be null or not found
        /// </summary>
        public Texture(GraphicDevice device, string filename, bool managed, int stage)
            : this(device, managed, 0,0,stage)
        {
            filename = System.IO.Path.GetFullPath(filename);

            if (!String.IsNullOrEmpty(filename) && System.IO.File.Exists(filename))
                m_filename = filename;
            else
                throw new ArgumentNullException("filename <" + filename + "> not exist");

            m_texture = DX9Texture.FromFilename(device.m_device, m_filename, m_usage, m_memory, m_stage);

            base.m_height = m_texture.m_height;
            base.m_width = m_texture.m_width;

            m_resource = ImageDestination.FromFilename;
            disposed = false;
        }

        /// <summary>
        /// A texture from emptytexture0 bitmap
        /// </summary>
        public static Texture Debug(GraphicDevice device, bool managed, int stage)
        {
            return new Texture(device, Engine.Properties.Resources.empytexture0, managed, stage);
        }

        public void WriteTextures(Bitmap bitmap)
        {
            if (bitmap.Width != m_width || bitmap.Height != m_height)
                throw new ArgumentException("Bitmap with different size");

            IntPtr ptr = m_texture.Lock();
            unsafe
            {
                int i = 0;
                uint* p_targhet = (uint*)ptr.ToPointer();
                for (int x = 0; x < m_height; x++)
                    for (int y = 0; y < m_width; y++)
                    {
                        Color32 color = (Color32)bitmap.GetPixel(x, y);
                        p_targhet[i++] = color.argb;
                    }
            }
            m_texture.UnLock();

        }


        public int StageID
        {
            get { return m_stage; }
            set
            {
                if (value > GraphicDevice.capabilities.MaxActiveTexture || value > 7)
                    throw new ArgumentOutOfRangeException("Can't use this stage");
                m_stage = value;
            }
        }

        /// <summary>
        /// An internal method check if texture is already set, this avoid a unecessary reloading
        /// </summary>
        public override void SetToDevice()
        {
            if (!inUse)
            {
                m_texture.SetToDevice(m_stage);
                TextureBase.prevtexture = this;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                m_texture.Dispose();
                TextureBase.prevtexture = null;
            }
            disposed = true;
        }

        public override void Restore()
        {
            if (disposed)
            {
                switch (m_resource)
                {
                    case ImageDestination.FromBitmap :
                        m_texture = DX9Texture.FromBitmap(m_device.m_device, m_bitmap, m_usage, m_memory,m_stage);
                        break;
                    case ImageDestination.FromFilename:
                        m_texture = DX9Texture.FromFilename(m_device.m_device, m_filename, m_usage, m_memory, m_stage);
                        break;
                }
                
                disposed = false;
            }
        }

        enum ImageDestination
        {
            FromBitmap,
            FromFilename
        }
    }

    /// <summary>
    /// A fusion of maximum 8 images
    /// </summary>
    public class TextureVolume : TextureBase
    {
        internal DX9VolumeTexture m_texture;
        Bitmap[] m_bitmaps;
        protected int m_deph;

        public TextureVolume(GraphicDevice device, int width, int height, int numTextures, bool managed)
            : base(device, managed,width,height)
        {
            if (numTextures > 8) throw new NotSupportedException();
            this.m_deph = numTextures;

            m_texture = new DX9VolumeTexture(device.m_device, width, height, numTextures, m_usage, m_memory);

            // need change the interpolation blending of two textures
            m_texture.m_state.MagFilter = TextureFilter.Linear;
            m_texture.m_state.MinFilter = TextureFilter.Linear;
        }

        public void WriteTextures(Bitmap[] bitmaps)
        {
            if (bitmaps.Length != m_deph)
                throw new ArgumentException("please use the same number of textures");

            for (int n = 0; n < m_deph; n++)
                if (bitmaps[n].Width != m_width || bitmaps[n].Height != m_height)
                    throw new ArgumentException("Bitmap with different size");

            m_bitmaps = bitmaps;

            IntPtr ptr = m_texture.Lock(LockFlags.Normal);

            long i = 0;
            for (int n = 0; n < m_deph; n++)
            {
                /*
                unsafe
                {
                    byte* p_targhet = (byte*)ptr.ToPointer();
                    for (int x = 0; x < m_height; x++)
                        for (int y = 0; y < m_width; y++)
                        {
                            Color color = m_bitmaps[n].GetPixel(x, y);
                            p_targhet[i++] = color.B;
                            p_targhet[i++] = color.G;
                            p_targhet[i++] = color.R;
                            p_targhet[i++] = color.A;
                        }
                }
                */
                unsafe
                {
                    int* p_targhet = (int*)ptr.ToPointer();
                    for (int x = 0; x < m_height; x++)
                        for (int y = 0; y < m_width; y++)
                        {
                            Color color = m_bitmaps[n].GetPixel(x, y);
                            int argb = color.ToArgb();
                            p_targhet[i++] = argb;
                        }
                }
            }

            m_texture.UnLock();

        }

        public override void SetToDevice()
        {
            if (!inUse)
            {
                m_texture.SetToDevice();
                TextureBase.prevtexture = this;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                m_texture.Dispose();
                if (TextureBase.prevtexture == this) TextureBase.prevtexture = null;
            }
            disposed = true;
        }

        public override void Restore()
        {
            if (disposed)
            {
                //m_texture = DX9Texture.FromBitmap(m_device.m_device, m_bitmaps, m_usage, m_memory);
                disposed = false;
            }
        }
    }


    /// <summary>
    /// NOT WORK
    /// A mix of three texture to do bump effect, not-shader implementation, device must support it 
    /// http://msdn.microsoft.com/en-us/library/windows/desktop/bb206304(v=vs.85).aspx
    /// </summary>
    public class BumpTexture
    {
        Texture baseTexture;
        Texture bumpMap;
        Texture envMap;

        public BumpTexture(GraphicDevice device, Bitmap diffuse, Bitmap bump, Bitmap environment, bool managed)
        {
            if (diffuse == null) diffuse = Engine.Properties.Resources.empytexture1;
            if (bump == null) bump = Engine.Properties.Resources.normal;

            baseTexture = new Texture(device, diffuse, managed, 0);
            bumpMap = new Texture(device, bump, managed, 1);
            envMap = new Texture(device, environment, managed, 2);

            // Stage 0: The base texture
            baseTexture.TextureStates = new DX9TextureState
            {
                ColorOperation = TextureOperation.Modulate,
                ColorArgument1 = TextureArgument.Texture,
                ColorArgument2 = TextureArgument.Diffuse,
                AlphaOperation = TextureOperation.SelectArg1,
                AlphaArgument1 = TextureArgument.Texture,
                TexCoordIndex = 1
            };
            // Stage 1: The bump map - Use luminance for this example.
            bumpMap.TextureStates = new DX9TextureState
            {
                ColorOperation = TextureOperation.BumpEnvironmentMapLuminance,
                ColorArgument1 = TextureArgument.Texture,
                ColorArgument2 = TextureArgument.Current,
                TexCoordIndex = 1
            };
            // Stage 2: A specular environment map
            envMap.TextureStates = new DX9TextureState
            {
                ColorOperation = TextureOperation.Add,
                ColorArgument1 = TextureArgument.Texture,
                ColorArgument2 = TextureArgument.Diffuse,
                TexCoordIndex = 0
            };
            //Setting the matrix to the identity causes the system to use the delta values in the bump map unmodified, but this is not a requirement.
            bumpMap.TextureStates.BumpEnvironmentMat = new float[,] { { 1, 0 }, { 0, 1 } };

            //f you set the bump mapping operation to include luminance (D3DTOP_BUMPENVMAPLUMINANCE),
            //you must set the luminance controls. The luminance controls configure how the system computes 
            //luminance before modulating the color from the texture in the next stage.
            bumpMap.TextureStates.BumpEnvironmentLScale = 0.5f;
            bumpMap.TextureStates.BumpEnvironmentLOffset = 0.0f;
        }

        public void SetToDevice()
        {
            baseTexture.SetToDevice();
            bumpMap.SetToDevice();
            envMap.SetToDevice();
        }
    }
}
