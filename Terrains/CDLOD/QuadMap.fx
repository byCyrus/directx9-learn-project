//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 WorldViewProj : WORLDVIEWPROJ;

float3   xPatchOrigin; // the min(x,y,z) position in global coord system of current patch
float3   xPatchScale;  // the scale factor of patch's vertices in (dx,dy,dz) format
float2   xPatchDim;    // the patch suddivision
float4   xPatchColor;  // debug patch with a custum color if diffuse texture aren't available

float2   xMorph;

float3   xTerrainMin;  // terrain bound min used to scale world
float3   xTerrainMax;  // terrain bound max used to scale world

float3   xLightDir;
float3   xEye;

Texture  xHeightMap;
Texture  xNormalMap;
Texture  xColorMap;
Texture  xRockMap;

sampler heightSampler = sampler_state 
{ 
	texture = <xHeightMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};
sampler normalSampler = sampler_state 
{ 
	texture = <xNormalMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};
sampler colorSampler = sampler_state 
{ 
	texture = <xColorMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
sampler rockSampler = sampler_state 
{ 
	texture = <xRockMap>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU  = WRAP; 
	AddressV  = WRAP;
};
//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float2  Position  : POSITION;
};

struct VS_OUTPUT
{   
	float4 Position : POSITION;
    float4 Color    : COLOR0;
	float2 TexCoord : TEXCOORD0;
	float3 Normal   : TEXCOORD1;
	float3 View     : TEXCOORD2;
	float  Height   : TEXCOORD3;
};


// convert unit vertex in global position
float4 GetGlobalPOS(float2 vertex)
{
	float4 pos = 0;
	pos.x = (vertex.x * xPatchScale.x) + xPatchOrigin.x;
	pos.z = (vertex.y * xPatchScale.z) + xPatchOrigin.z;
	pos.w = 1;
	return pos;
}

// find texcoord in the range [0,1] and swap V to orient correctly to XZ plane
// -----> u         z
// |                |
// |                |____x
// v
float2 GetGlobalUV(float4 vertex)
{
	float2 tex = 0;
	tex.x = (vertex.x - xTerrainMin.x) / (xTerrainMax.x - xTerrainMin.x);
	tex.y = 1.0f - (vertex.z - xTerrainMin.z) / (xTerrainMax.z - xTerrainMin.z);
	return tex;
}

// get height from map in 0.0f - 1.0f range
float GetHeight(float2 texcoord)
{
	float h = tex2Dlod(normalSampler, float4(texcoord.xy, 0, 0)).a;

	return h;
	//return tex2Dlod(heightSampler, float4(texcoord.xy, 0, 0)).r;
}

void GetFourCornerHeight(float4 pos , out float n , out float s , out float e , out float w , out float dz , out float dx)
{
    dz = xPatchScale.x / xPatchDim.y;
    dx = xPatchScale.z / xPatchDim.x;
    n = GetHeight(GetGlobalUV(pos + float4(0,0,dz,0)));
    s = GetHeight(GetGlobalUV(pos - float4(0,0,dz,0)));
    e = GetHeight(GetGlobalUV(pos + float4(dx,0,0,0)));
    w = GetHeight(GetGlobalUV(pos - float4(dx,0,0,0)));
}

// get normal from height with 4 points
float3 GetNormalfromHeight4a(float4 pos,float h)
{
	float n, s, e, w, dz, dx;
	GetFourCornerHeight(pos , n , s , e , w , dz , dx);

	float3 vn = float3(0, n-h , dz);
	float3 ve = float3(dx, e-h , 0);
	float3 vs = float3(0, s-h , -dz);
	float3 vw = float3(-dx, w-h , 0);

    return float3(normalize(cross(vn,ve) + cross(vs,vw)));
}

// get normal from height with 4 points
float3 GetNormalfromHeight4b(float4 pos,float h)
{
	float n, s, e, w, dz, dx;
	GetFourCornerHeight(pos , n , s , e , w , dz , dx);

    float3 ns = float3(0, n-s , dz);
    float3 ew = float3(dx, e-w , 0);

    return float3(normalize(cross(ns, ew)));
}

// get normal from height with 4 points
float3 GetNormalfromHeight4c(float4 pos,float h)
{
	float n, s, e, w, dz, dx;
	GetFourCornerHeight(pos , n , s , e , w , dz , dx);

	float3 vn = float3(0, n-h , dz);
	float3 ve = float3(dx, e-h , 0);
	float3 vs = float3(0, s-h , -dz);
	float3 vw = float3(-dx, w-h , 0);

    return float3(normalize(cross(vn,ve) + cross(ve,vs) + cross(vs,vw) + cross(vw,vn)));
}

// get normal from height with 8 point, h = middle
float3 GetNormalfromHeight8(float4 pos,float h)
{
	float n, s, e, w, dz, dx;
	GetFourCornerHeight(pos , n , s , e , w , dz , dx);

	float ne = GetHeight(GetGlobalUV(pos + float4(dx,0,dz,0)));
	float se = GetHeight(GetGlobalUV(pos + float4(dx,0,-dz,0)));
    float sw = GetHeight(GetGlobalUV(pos + float4(-dx,0,-dz,0)));
	float nw = GetHeight(GetGlobalUV(pos + float4(-dx,0,dz,0)));

	float3 ns = float3(0, n-s , 2*dz);
    float3 ew = float3(4*dx, e-w , 0);

    return float3(normalize(cross(ns, ew)));
}

float3 GetNormalfromMap(float2 texCoord)
{
    return tex2Dlod(normalSampler, float4(texCoord,0,0)).xzy * 2.0f - 1.0f;
}
// position = global position XZ
// vertex   = grid vertex in [0,1] range
float2 MorphVertex(float2 vertex, float2 position, float morphLerpK)
{
	float2 f = frac(vertex * xPatchDim.xy * 0.5f);

	return position - f * (xPatchScale.xz / xPatchDim.xy) * (1-morphLerpK) * 2;
}


VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = GetGlobalPOS(input.Position);


	float dist = distance(output.Position.xyz, xEye);
	float morphLerpK = (dist - xMorph.x) / (xMorph.y - xMorph.x);

	output.Position.xz = MorphVertex(input.Position, output.Position.xz, saturate(morphLerpK));
	
	
	output.TexCoord = GetGlobalUV(output.Position);
	float h = GetHeight(output.TexCoord);	
	
	//output.Normal = GetNormalfromHeight4b(output.Position , h);
	output.Normal = GetNormalfromMap(output.TexCoord);
	
	output.Position.y = xTerrainMin.y + h * (xTerrainMax.y - xTerrainMin.y);

	output.Height = h;
	output.Position = mul(output.Position, WorldViewProj);
	output.View = xEye - output.Position.xyz;

	return output;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader(VS_OUTPUT input) : COLOR0
{
	float3 normal = input.Normal;
	float dt = (0, dot(normal, -xLightDir));
	return xPatchColor * dt;
}
float4 PShader_alphatest(VS_OUTPUT input) : COLOR0
{
	float a = tex2D(normalSampler, input.TexCoord).a;
	return float4(a,a,a,1);
}

float4 PShader_diffuse(VS_OUTPUT input) : COLOR0
{
	float4 grass = tex2D(colorSampler, input.TexCoord * 100.0f);
    float4 rocks = tex2D(rockSampler, input.TexCoord * 100.0f);	

	// a ~= 1 when vertical , transition in 0.95-0.85 but clamp in range [0-1]
	float a = abs(dot(float3(0, 1, 0), input.Normal));	
	a = clamp((a - 0.55f)/(0.75f - 0.55f), 0.0f , 1.0f);
	grass.rgb = (grass.rgb * a) + (rocks.rgb * (1-a));
	


	float3 normal = tex2D(normalSampler, input.TexCoord).xyz * 2.0f - 1.0f;
	
	float dt = max(0.1f , dot(normal, -xLightDir));


	//float dt = max(0, dot(input.Normal, -xLightDir));
	
	if (input.Height<0.1f)
	{
		grass = float4(0,0,0,1);
		dt = 1.0f;
	}

	return grass * dt;
}

float4 PShader_normal(VS_OUTPUT input) : COLOR0
{
	float4 grass = tex2D(colorSampler, input.TexCoord * 1000.0f);
    float4 rocks = tex2D(rockSampler, input.TexCoord * 1000.0f);	

	//finner normal
	float3 normal= tex2D(normalSampler, input.TexCoord).xyz * 2.0f - 1.0f;
	float d = dot(normal, -xLightDir);
	float dt = max(0.2f, d + 0.5f);

	//float inclination = max(0, dot(normal,float3(0,1,0)));
	//float lerpVal = (input.WorldPos.y - 1.5f)/(2.5f - 1.5f);
	//return lerp( grass, rocks, lerpVal) * dt;
	
	//float a = abs(dot(input.Normal, float3(1, 0, 1)));
	float a = dot(float3(0, 1, 0), input.Normal);

	grass.rgb = (grass.rgb * (1-a)) + (rocks.rgb * a);

	return grass * dt;
}

float4 PShader_black(VS_OUTPUT input) : COLOR0
{
	return float4(0,0,0,1);
}


//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique DefaultTechique
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
		ZWriteEnable    = TRUE;
		AlphaBlendEnable = FALSE;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_diffuse();
    }
}
technique WireframeTechique2
{
    pass p0
    {
		CullMode = CCW;
		FillMode = Solid;
		DepthBias = 0;
		ZWriteEnable    = TRUE;
		AlphaBlendEnable = FALSE;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader();
    }
	pass p1
    {
		CullMode = CCW;
		FillMode = Wireframe;
		DepthBias = -0.00001f;
        VertexShader = compile vs_3_0 VShader();
		PixelShader =  compile ps_3_0 PShader_black();
    }
}