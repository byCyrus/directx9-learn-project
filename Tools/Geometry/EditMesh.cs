﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

using Engine.Maths;
using Engine.Graphics;

namespace Engine.Tools
{
    /// <summary>
    /// Vertex of HalfEdge format
    /// </summary>
    public class HVertex : IVertexPosition
    {
        /// <summary>
        /// unique id of vertices, is the index in the list but used only when mesh are converted in 
        /// default format
        /// </summary>
        public int ID;
        public Vector3 position { get; set; }
        public bool marked;

        public HVertex(int id)
        {
            this.marked = false;
            this.ID = id;
            this.position = Vector3.Zero;
        }
        public HVertex(Vector3 pos):this(-1)
        {
            this.position = pos;
        }
        public override string ToString()
        {
            return "v" + ID.ToString();
        }
        
        public static HVertex Avarage(HVertex v0, HVertex v1)
        {
            HVertex vm = new HVertex(-1);
            vm.position = (v0.position + v1.position) * 0.5f;
            return vm;
        }
    }

    public class HVertexFull : HVertex , IVertexColor , IVertexNormal,IVertexTexCoord
    {
        public Vector3 normal { get; set; }
        public Vector2 texture { get; set; }
        public Color32 color { get; set; }

        public HVertexFull(int id) : base(id) 
        {
            this.normal = Vector3.Zero;
            this.texture = Vector2.Zero;
            this.color = Color.Blue;
        }
        public HVertexFull(Vector3 pos,Color color)
            : base(-1)
        {
            this.position = pos;
            this.color = color;
        }
        public HVertexFull(Vector3 pos)
            : base(-1)
        {
            this.position = pos;
            this.color = Color.Blue;
        }
        public HVertexFull(float x, float y, float z)
            : this(new Vector3(x, y, z)) 
        { }

        public static HVertexFull Avarage(HVertexFull v0, HVertexFull v1)
        {
            HVertexFull vm = new HVertexFull(-1);
            vm.position = (v0.position + v1.position) * 0.5f;
            vm.texture = (v0.texture + v1.texture) * 0.5f;
            vm.normal = (v0.normal + v1.normal) * 0.5f;

            vm.color = Color.FromArgb(
                (int)(v0.color.R * 0.5f + v1.color.R * 0.5f),
                (int)(v0.color.G * 0.5f + v1.color.G * 0.5f),
                (int)(v0.color.B * 0.5f + v1.color.B * 0.5f));
            return vm;
        }

    }
    
    /// <summary>
    /// Edge, the halfedge structure require 2 pointers to vertices and maximum 2 faces
    /// </summary>
    public class HEdge
    {
        /// <summary>
        /// unique id of edges, is the index in the list, not used
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Use the address of vertex to allow change
        /// </summary>
        public HVertex[] v = new HVertex[2];
        
        public HFace[] f = new HFace[2];
        public bool marked { get; set; }

        public HEdge() : this(-1) { }      
        public HEdge(int id)
        {
            this.marked = false;
            this.ID = id;
        }
        public HEdge(HVertex v0, HVertex v1) : this(v0, v1, -1) { }
        public HEdge(HVertex v0, HVertex v1, int id)
            : this(id)
        {
            this.v[0] = v0;
            this.v[1] = v1;
        }
        public int GetVertexIndex(HVertex vertex)
        {
            if (v[0] == vertex) return 0;
            else if (v[1] == vertex) return 1;
            else return -1;
        }
        public override string ToString()
        {
            return string.Format("e{0}({1},{2}) f{3} f{4}", ID, (v[0]).ID, (v[1]).ID, (f[0] != null) ? f[0].ID.ToString() : "-", (f[1] != null) ? f[1].ID.ToString() : "-");
        }
    }

    /// <summary>
    /// Face, in theory there aren't limit about vertex indices, depend that struct you use
    /// when convert in default format, 16 or 32 bit
    /// </summary>
    public class HFace
    {
        /// <summary>
        /// index in the list, not used
        /// </summary>
        public int ID;
        /// <summary>
        /// The position in the array match with GetEdgeIdxContainVertsID() algorithm to mantain a sort or "chain" of edges
        /// </summary>
        /// <remarks>
        /// I use this scheme:
        /// e[0] = e(v0,v1)
        /// e[1] = e(v1,v2)
        /// e[2] = e(v0,v2)
        /// </remarks>
        public HEdge[] e = new HEdge[3];
        /// <summary>
        /// Set vertices in counterclockwire
        /// </summary>
        public HVertex[] v = new HVertex[3];
        /// <summary>
        /// a flag for special use
        /// </summary>
        public bool marked { get; set; }
        /// <summary>
        /// Create a face and assign an id
        /// </summary>
        public HFace(int id) 
        {
            this.marked = false;
            this.ID = id;
        } 
        /// <summary>
        /// Create empty face
        /// </summary>
        public HFace() : this(-1) { }
        /// <summary>
        /// </summary>
        public HFace(HVertex v0, HVertex v1, HVertex v2, int id)
            : this(id)
        {
            v[0] = v0; 
            v[1] = v1; 
            v[2] = v2;
        }
        /// <summary>
        /// Create face using 3 vertices
        /// </summary>
        public HFace(HVertex v0, HVertex v1, HVertex v2) : this(v0, v1, v2, -1) { }
        /// <summary>
        /// Return the edge index in this face what contain id1 and id2 vertices.
        /// </summary>
        /// <returns>
        /// -1 not fount
        /// -2 incoerent id, the face contain two identical vertices
        /// </returns>
        public int GetEdgeChainIndex(HVertex vx, HVertex vy)
        {
            // using flag to un-mark the vertices that mach with eij
            // 110 ; 6 return edge[0] because find first and second index
            // 011 ; 3 return edge[1] because find second and third index
            // 101 ; 5 return edge[2] because find first and third index
            // 111 ; 7 face contain two vertice with same index, error
            // 000 001 010 100; 0 1 2 4 face don't contain this edge(id1,id2), return -1
            byte pos = 0;
            byte mark = 4;
            for (int i = 0; i < 3; i++, mark>>=1)
            {
                if (v[i]==vx || v[i] == vy)
                    pos |= mark;
            }
            if (pos == 6) return 0;
            else if (pos == 3) return 1;
            else if (pos == 5) return 2;
            else if (pos == 7) return -2;
            else return -1;
        }
        /// <summary>
        /// return the position of edge in the e[] array
        /// </summary>
        public int GetEdgeIndex(HEdge edge)
        {
            for (int i = 0; i < 3; i++) if (e[i] == edge) return i;
            return -1;
        }
        /// <summary>
        /// return the position of vertex in the v[] array
        /// </summary>
        public int GetVertexIndex(HVertex vertex)
        {
            for (int i = 0; i < 3; i++)
                if (v[i] == vertex) return i;
            return -1;
        }
        /// <summary>
        /// return the position of edge in the e[] array that contain vertex
        /// </summary>
        /// <param name="ie">the first e[ie] contain vertex v</param>
        /// <param name="iv">the vertex v, not check is it's in face.v[]</param>
        /// <returns>the second e[i] contain vertex v</returns>
        public int GetNextEdgeContainVertex(int ie, HVertex vertex)
        {
            ie = (ie + 1) % 3;
            if (e[ie].v[0] == vertex || e[ie].v[0] == vertex) return ie;
            else return (ie + 1) % 3;
        }    
        /// <summary>
        /// return if face don't contain duplicated vertices, edges are in "chain" order
        /// </summary>
        public bool IsCoerent(out string message)
        {
                if (v[0] == v[1] || v[0] == v[2] || v[1] == v[2])
                {
                    message = string.Format("collapsed vertices {0} {1} {2}", v[0] , v[1] ,v[2]);
                    return false;
                }
                int i, j, k;
                GetSortedEdges(out i, out j, out k);
                if (i != 0 || j != 1 || k != 2)
                {
                    message = string.Format("wrong edge order {0} {1} {2}", i, j, k);
                    return false;
                }
                message = "ok";
                return true;
        }
        /// <summary>
        /// is possible that edges are not sorted in default chain
        /// </summary>
        /// <param name="i0">the correct position what e[0] must have , if == 0 edge0 are in correct position</param>
        public void GetSortedEdges(out int i0, out int i1, out int i2)
        {
            /// I use this scheme:
            /// e[0] = e(v0,v1)
            /// e[1] = e(v1,v2)
            /// e[2] = e(v0,v2)
            i0 = GetEdgeChainIndex((e[0].v[0]), (e[0].v[1]));
            i1 = GetEdgeChainIndex((e[1].v[0]), (e[1].v[1]));
            i2 = GetEdgeChainIndex((e[2].v[0]), (e[2].v[1]));
        }
        /// <summary>
        /// take a look to comment, ie0 is the first edge contain v, ie1 in the opposite edge of v,
        /// ie2 is the second edge contain v
        /// </summary>
        /// <param name="containv">next edge contain vertex</param>
        /// <param name="oppositev">edge in opposite than vertex</param>
        /// <param name="ie">edge index contain vertex and so start</param>
        public void GetSortedChainEdges(int ie, HVertex vertex, out int oppositev, out int containv)
        {
            //       /'\
            //  ie  /   \ oppositev
            //     /     \
            //    /       \
            //  v'<--------' 
            //         containv
            oppositev = (ie + 1) % 3;
            containv = (oppositev + 1) % 3;

            if (e[oppositev].v[0] == vertex || e[oppositev].v[1] == vertex)
            {
                // swap
                int tmp = containv; containv = oppositev; oppositev = tmp;
            }
        }
        /// <summary>
        /// Get the index of edge in opposite side from vertex, is the edge that not contain v
        /// Return -1 if not found
        /// </summary>
        public int GetOppositeEdge(HVertex vertex)
        {
            //       /'\
            //      /   \ ie
            //     /     \
            //    /       \
            //  v'<--------' 
            for (int i = 0; i < 3; i++)
                if (e[i].v[0] != vertex && e[i].v[1] != vertex) 
                    return i; 
            return -1;
        }
        /// <summary>
        /// Get the index of vertex in opposite side from edge, is the vertex that not contain e
        /// Return 0,1,or 2, no safety test 
        /// </summary>
        public int GetOppositeVertex(HEdge edge)
        {
            for (int i = 0; i < 2; i++)
                if (v[i] != edge.v[0] && v[i] != edge.v[1]) return i;
            return 2;
        }

        public void SetEdgeInChainOrder()
        {
            HEdge e0 = e[0];
            HEdge e1 = e[1];
            HEdge e2 = e[2];
            int i0, i1, i2;
            GetSortedEdges(out i0, out  i1, out  i2);
            e[i0] = e0;
            e[i1] = e1;
            e[i2] = e2;
        }

        public override string ToString()
        {
            return string.Format("f{0} [{1} {2} {3}] -> {4} {5} {6}", ID, (v[0]).ID, (v[1]).ID, (v[2]).ID, e[0], e[1], e[2]);
        }
    }

    /// <summary>
    /// Half-Edge mesh structure, used to work with geometry but bad to store data
    /// </summary>
    public class HMesh : BaseGeometry
    {
        // vertes edge and faces table
        public List<HVertex> m_verts;
        public List<HEdge> m_edges;
        public List<HFace> m_faces;

        public override PrimitiveType primitive
        {
            get { return PrimitiveType.TriangleList; }
        }
        public override bool IsIndexed
        {
            get { return true; }
        }
        public override int numVertices
        {
            get { return m_verts.Count; }
        }
        public override int numIndices
        {
            get { return (m_faces != null) ? m_faces.Count * 3 : 0; }
        }
        public override int numPrimitives
        {
            get { return m_faces.Count; }
        }

        /// <summary>
        /// Estract or Insert the base geometry format (for rendering) 
        /// </summary>
        public MeshListGeometry TriMesh
        {
            get { return getTriMesh(); }
            set { setTriMesh(value.indices.data, value.vertices.data, value.colors.data); }
        }

        public int NumVerts { get { return m_verts.Count; } }
        public int NumFaces { get { return m_faces.Count; } }


        public HMesh()
        {
            globalcoord = Matrix4.Identity;
            globalcoord_inv = Matrix4.Identity;
            boundSphere = BoundarySphere.NaN;

            m_verts = new List<HVertex>();
            m_edges = new List<HEdge>();
            m_faces = new List<HFace>();
        }
        
        /// <summary>
        /// Initialize a Half-Edge structure using a base static mesh, edge will be calculated
        /// </summary>
        public HMesh(Face16[] faces, Vector3[] vertices, Color32[] colors)
            : this()
        {
            setTriMesh(faces, vertices, colors);
            calculateEdgeTable();
        }
        
        
        /// <summary>
        /// return true if HalfEdge structure is correct
        /// </summary>
        public bool CoerenceTest(out string message)
        {
            bool fail = false;
            string message_f = "Face : OK";
            string message_v = "Vertex : OK";
            string message_e = "Edge : OK";

            for (int i = 0; i < m_faces.Count; i++)
            {
                string txt;
                if (!m_faces[i].IsCoerent(out txt))
                {
                    message_f = "Face" + i + " : " + txt;
                    fail = true;
                    break;
                }
            }

            //find T junction
            if (!fail)
            foreach (HEdge e in m_edges)
            {
                HFace fa = e.f[0];
                HFace fb = e.f[1];
                
                if (fa == null)
                {
                    message_e = "Edge : first connected faces NULL";
                    fail = true;
                    break;
                }

                if (fa.e[0] != e && fa.e[1] != e && fa.e[2] != e)
                {
                    message_f = "Face : face " + fa.ToString() + " not connected with edge " + e.ToString() + "\n";
                    fail = true;
                    break;
                }

                if (fb != null && fb.e[0] != e && fb.e[1] != e && fb.e[2] != e)
                {
                    message_f = "Face : face " + fa.ToString() + " not connected with edge " + e.ToString() + "\n";
                    fail = true;
                    break;
                }
                if (fail) break;
            }
            message = message_v + "\n" + message_f + "\n" + message_e;


            return !fail;
        }

        /// <summary>
        /// Build the edge table using vertices and faces tab, carefull need clear before start
        /// </summary>
        protected void calculateEdgeTable()
        {
            m_edges.Clear();
            int ecount = 0;

            for (int i = 0; i < m_faces.Count; i++)
            {
                HFace face = m_faces[i];
                if (face.marked) continue;

                for (int j = 0; j < 3; j++)
                {
                    //edge(x,y) not created
                    if (face.e[j]==null)
                    {
                        // create the edge(x,y) where x and y are the vertex ID of j-th edge
                        // using a fix order
                        switch (j)
                        {
                            case 0: face.e[j] = new HEdge(face.v[0], face.v[1]); break;
                            case 1: face.e[j] = new HEdge(face.v[1], face.v[2]); break;
                            case 2: face.e[j] = new HEdge(face.v[2], face.v[0]); break;
                        }
                         // if edge is created at first time, assign it in first position
                        face.e[j].f[0] = face;

                        // foreach all other faces, find a faces that contain same edge(x,y) using vertex id x and y
                        for (int ii = i + 1; ii < m_faces.Count; ii++)
                        {
                            // completly assigned, not need test
                            if (m_faces[ii].marked) continue;
                            // if idx == -1, the current face don't contain the x and y vertex
                            int idx = m_faces[ii].GetEdgeChainIndex(face.e[j].v[0], face.e[j].v[1]);
                            if (idx>-1)
                            {
                                // link the current edge
                                m_faces[ii].e[idx] = face.e[j];
                                // link in second position
                                face.e[j].f[1] = m_faces[ii];
                                // a edge contain only 2 neighbour faces, T junction is wrong,
                                // exit from searching
                                break;
                            }
                        }
                        face.e[j].ID = ecount++;
                        m_edges.Add(face.e[j]);
                    }
                }
                // mark this face as completly assigned
                face.marked = true;
            }
            m_edges.TrimExcess();
        }

        /// <summary>
        /// Collapse the edge, the two vertices will be fuse in a middle position, 
        /// incoerent or degenerated faces will be deleted.
        /// </summary>
        public void Collapse(HEdge e)
        {
            //Console.WriteLine("collapse " + e.ToString());

            HFace f0a = null, f0b = null, f1a = null, f1b = null;
            HEdge e0a = null, e0b = null, e1a = null, e1b = null;

            HFace f0 = e.f[0];
            HFace f1 = e.f[1];
            HVertex vA = e.v[0];
            HVertex vB = e.v[1];

            vB.position = (vA.position + vB.position) * 0.5f;
            
            m_edges.Remove(e);

            if (f0 != null)
            {
                m_faces.Remove(f0);

                int ie, iea, ieb;
                // position of e in f
                ie = f0.GetEdgeIndex(e);
                // position of eA and eB in f
                f0.GetSortedChainEdges(ie, vA, out ieb, out iea);
                e0a = f0.e[iea];
                e0b = f0.e[ieb];
                f0a = e0a.f[0] != f0 ? e0a.f[0] : e0a.f[1];
                f0b = e0b.f[0] != f0 ? e0b.f[0] : e0b.f[1];

                m_edges.Remove(e0a);
            }
            if (f1 != null)
            {
                m_faces.Remove(f1);

                int ie, iea, ieb;
                // position of e in f
                ie = f1.GetEdgeIndex(e);
                // position of eA and eB in f
                f1.GetSortedChainEdges(ie, vA, out ieb, out iea);
                e1a = f1.e[iea];
                e1b = f1.e[ieb];
                f1a = e1a.f[0] != f1 ? e1a.f[0] : e1a.f[1];
                f1b = e1b.f[0] != f1 ? e1b.f[0] : e1b.f[1];

                m_edges.Remove(e1a);
            }

            // substitute all vA reference in all faces and edges. The brute force is only
            // way otherwise we need to add List<HFace> faces_renferences in HVertex but require a lot of memory;
            foreach (HFace f in m_faces)
            {
                int iva = f.GetVertexIndex(vA);
                if (iva > -1)
                {
                    f.v[iva] = vB;
                    for (int i = 0; i < 3; i++)
                        if (!f.e[i].marked)
                        {
                            iva = f.e[i].GetVertexIndex(vA);
                            if (iva > -1)
                            {
                                f.e[i].v[iva] = vB;
                            }
                            // mark as visited
                            f.e[i].marked = true;
                        }
                }
            }

            // assign new conettivity that have be lost with removed faces
            if (f0a != null && f0b!=null)
            {
                int iea = f0a.GetEdgeIndex(e0a);
                int ieb = f0b.GetEdgeIndex(e0b);
                f0a.e[iea] = f0b.e[ieb];

                if (f0b.e[ieb].f[0] == f0) f0b.e[ieb].f[0] = f0a; else f0b.e[ieb].f[1] = f0a; 
            }
            if (f1a != null && f1b != null)
            {
                int iea = f1a.GetEdgeIndex(e1a);
                int ieb = f1b.GetEdgeIndex(e1b);
                f1a.e[iea] = f1b.e[ieb];

                if (f1b.e[ieb].f[0] == f1) f1b.e[ieb].f[0] = f1a; else f1b.e[ieb].f[1] = f1a;
            }

            for (int i = m_faces.Count - 1; i >= 0; i--)
            {
                //if (!m_faces[i].IsCoerent)
                //    m_faces.RemoveAt(i);
            }

            foreach (HEdge edg in m_edges)
                edg.marked = false;
                
            m_verts.Remove(vA);
        }
        /// <summary>
        /// Turn the edge in the square formed by its two connected faces
        /// </summary>
        public void Flip(HEdge e)
        {
            HFace f0 = e.f[0];
            HFace f1 = e.f[1];

            // to flip need both faces
            if (f0 == null || f1 == null) return;

            HVertex vA = e.v[0];
            HVertex vB = e.v[1];
            HEdge e0a, e0b, e1a, e1b;
            HVertex v0,v1;

            // get the edges and vertices
            int ie, iea, ieb;
            ie = f0.GetEdgeIndex(e);
            f0.GetSortedChainEdges(ie, vA, out ieb, out iea);
            e0a = f0.e[iea];
            e0b = f0.e[ieb];
            v0 = e0a.v[0] != vA ? e0a.v[0] :e0a.v[1];

            ie = f1.GetEdgeIndex(e);
            f1.GetSortedChainEdges(ie, vA, out ieb, out iea);
            e1a = f1.e[iea];
            e1b = f1.e[ieb];
            v1 = e1a.v[0] != vA ? e1a.v[0] :e1a.v[1];

            e.v[0] = v0;
            e.v[1] = v1;

            // set in chain order
            f0.v[0] = v1;
            f0.v[1] = vB;
            f0.v[2] = v0;
            f0.e[0] = e1b;
            f0.e[1] = e0b;
            f0.e[2] = e;

            f1.v[0] = v0;
            f1.v[1] = vA;
            f1.v[2] = v1;
            f1.e[0] = e0a;
            f1.e[1] = e1a;
            f1.e[2] = e;

            // fix conectivity
            if (e1b.f[0] == f1) e1b.f[0] = f0; else e1b.f[1] = f0;
            if (e0a.f[0] == f0) e0a.f[0] = f1; else e0a.f[1] = f1;
        }

        /// <summary>
        /// turn all edge for minimizing edge lenght
        /// </summary>
        public void FlipMinimizeEdgeLenght(int iteration)
        {
            for (int i = 0; i < iteration; i++)
            {
                foreach (HEdge e in m_edges)
                {
                    HFace f0 = e.f[0];
                    HFace f1 = e.f[1];
                    if (f0 != null && f1 != null)
                    {
                        int iv0 = f0.GetOppositeVertex(e);
                        int iv1 = f1.GetOppositeVertex(e);
                        float lenght = Vector3.GetManhattanLength(e.v[0].position - e.v[1].position);
                        float fliplenght = Vector3.GetManhattanLength(f0.v[iv0].position - f1.v[iv1].position);

                        if (fliplenght < lenght * 0.9f) Flip(e);
                    }
                }
            }
        }

        /// <summary>
        /// Increase the tessellation splitting each faces in 4 sub-faces with Piramid layout.
        /// TODO : remove edgeSorted param
        /// </summary>
        /// <param name="edgeSorted">if edges index in each faces are sorted, skip some internal sorting</param>
        public void TriangleTessellate(bool edgeSorted)
        {
            /* pseudocode:
             * foreach old faces 
             *    split in 4 faces
             *    foreach 3 border of face
             *      if not splitted, split in 2 edge and generate middle vertex
             *         add these 2 new edge to list
             *      else get the 2 splitted edges and middle vertex from e_crack list associated to this border
             *    add this 4 new faces to list
             * delete old faces and edges
             *
             *                 v0
             *                /  \
             *           eLT /    \ eRT
             *              /  T   \
             *           vcL________vcR
             *            / \  C   / \
             *       eLD /  eCL   eCR \ eRD
             *          /  L  \  /  R  \
             *         /_______\/_______\
             *        v1      vcD       v2
             *            eDL      eDR
             */
            // the two crack edge for each edges. Need an array to custom access to all position 
            HEdge[] e_crack = new HEdge[m_edges.Count * 2];
            // the new center edges.
            HEdge[] tmp_edges = new HEdge[m_faces.Count * 3];

            // not need array, this temporary list will substitute the main list
            List<HFace> tmp_faces = new List<HFace>(m_faces.Count * 4);

            int vcount = m_verts.Count;
            int ecount = 0;

            for (int f = 0; f < m_faces.Count; f++)
            {
                HVertex vL, vD, vR;
                HEdge eLT, eLD, eDR, eDL, eRD, eRT;

                HFace froot = m_faces[f];

                HVertex v0 = froot.v[0];
                HVertex v1 = froot.v[1];
                HVertex v2 = froot.v[2];

                //use always this scheme
                //f.e[0] = e(v0,v1)
                //f.e[1] = e(v1,v2)
                //f.e[2] = e(v0,v2)

                int iL = 0;
                int iD = 1;
                int iR = 2;

                if (!edgeSorted)
                {
                    // example:  eL is the e(v0,v1), but is possible that iL!=0, GetSortedEdges() resolve this issue
                    froot.GetSortedEdges(out iL, out iD, out iR);
                }
                HEdge eL = froot.e[iL]; 
                HEdge eD = froot.e[iD];
                HEdge eR = froot.e[iR];
 
                // build the reference of new child faces
                HFace fT = new HFace(tmp_faces.Count);
                tmp_faces.Add(fT);
                HFace fL = new HFace(tmp_faces.Count);
                tmp_faces.Add(fL);
                HFace fC = new HFace(tmp_faces.Count);
                tmp_faces.Add(fC);
                HFace fR = new HFace(tmp_faces.Count);
                tmp_faces.Add(fR);

                // link the neigthbour faces using the id of border edge splitted or not in two crack edges
                linkborder(eL.ID, e_crack, out vL, v0, v1, out eLT, out eLD, fT, fL);
                linkborder(eD.ID, e_crack, out vD, v1, v2, out eDL, out eDR, fL, fR);
                linkborder(eR.ID, e_crack, out vR, v2, v0, out eRD, out eRT, fR, fT);

                // centers faces and edges 
                HEdge eCL = new HEdge(vL, vD);
                HEdge eCR = new HEdge(vD, vR);
                HEdge eCT = new HEdge(vR, vL);

                tmp_edges[ecount++] = eCL;
                tmp_edges[ecount++] = eCR;
                tmp_edges[ecount++] = eCT;


                fT.v[0] = (v0);
                fT.v[1] = (vL);
                fT.v[2] = (vR);

                fL.v[0] = (vL);
                fL.v[1] = (v1);
                fL.v[2] = (vD);

                fC.v[0] = (vD);
                fC.v[1] = (vR);
                fC.v[2] = (vL);

                fR.v[0] = (vR);
                fR.v[1] = (vD);
                fR.v[2] = (v2);

                //use always this scheme to sort edges inside face
                //f.e[0] = e(v0,v1)
                //f.e[1] = e(v1,v2)
                //f.e[2] = e(v0,v2)

                fT.e[0] = eLT;
                fT.e[1] = eCT;
                fT.e[2] = eRT;

                fL.e[0] = eLD;
                fL.e[1] = eDL;
                fL.e[2] = eCL;

                fC.e[0] = eCR;
                fC.e[1] = eCT;
                fC.e[2] = eCL;

                fR.e[0] = eCR;
                fR.e[1] = eDR;
                fR.e[2] = eRD;

                // link central edges, they are always new so f[] is empty
                eCR.f[0] = fC;
                eCT.f[0] = fC;
                eCL.f[0] = fC;
                eCR.f[1] = fR;
                eCT.f[1] = fT;
                eCL.f[1] = fL;
            }

            m_faces = tmp_faces;

            // rewrite main edges list, id are always
            ecount = 0;
            m_edges.Clear();
            
            for (int i = 0; i < e_crack.Length; i++)
                m_edges.Add(e_crack[i]);
      
            for (int i = 0; i < tmp_edges.Length; i++)
                m_edges.Add(tmp_edges[i]);

            m_verts.TrimExcess();
            m_edges.TrimExcess();
            m_faces.TrimExcess();

            // rebuild the order
            clearlist();
        }

        /// <summary>
        /// Increase the tessellation splitting each faces in 4 sub-faces with TriangleFan layout.
        /// TODO : remove edgeSorted param
        /// </summary>
        /// <param name="edgeSorted">if edges index in each faces are sorted, skip some internal sorting</param>
        public void TriangleFanTessellate(bool edgeSorted)
        {
            /* pseudocode:
             * foreach old faces 
             *    split in 4 faces
             *    foreach 3 border of face
             *      if not splitted, split in 2 edge and generate middle vertex
             *         add these 2 new edge to list
             *      else get the 2 splitted edges and middle vertex from e_crack list associated to this border
             *    add this 4 new faces to list
             * delete old faces and edges
             *
             *                 v0
             *                / | \
             *           eLT /  |  \ eRT
             *              /   |   \
             *           vcL   eCT  vcR
             *            / \   |   / \
             *       eLD /  eCL |  eCR \ eRD
             *          /     \ | /     \
             *         /_______\|/_______\
             *        v1      vcD       v2
             *            eDL      eDR
             */
            throw new NotImplementedException();
        }
        /// <summary>
        /// create connettivity between two neighbour faces define by the edge
        /// </summary>
        /// <param name="eID">id of edged to split</param>
        /// <param name="e_crack">list of splitted edges</param>
        /// <param name="vM">middle vertex</param>
        /// <param name="e0m">first crack edge</param>
        /// <param name="em1">second crack edge</param>
        void linkborder(int eID, HEdge[] e_crack, out HVertex vM, HVertex v0, HVertex v1, out HEdge e0m, out HEdge em1, HFace f0, HFace f1)
        {
            HEdge ecrack0 = e_crack[eID * 2];
            HEdge ecrack1 = e_crack[eID * 2 + 1];

            // if edges was splitted previosly
            if (ecrack0 != null)
            {
                // vM is the common vertices of two cracked edges
                vM = ecrack0.v[0].Equals(ecrack1.v[0]) ? ecrack0.v[0] : ecrack0.v[1];

                // get the correct order
                if (ecrack0.v[0].Equals(v0) || ecrack0.v[1].Equals(v0))
                {
                    e0m = ecrack0;
                    em1 = ecrack1;
                }
                else
                {
                    e0m = ecrack1;
                    em1 = ecrack0;
                }
                //link faces to border edges, if was from splitted edge, the first face is assigned previosly
                e0m.f[1] = f0;
                em1.f[1] = f1;
            }
            // not splitted, create the middle vertex and two split edges
            else
            {
                vM = HVertex.Avarage(v0, v1);
                vM.ID = m_verts.Count;
                m_verts.Add(vM);

                e0m = new HEdge(v0, vM);
                em1 = new HEdge(vM, v1);

                ecrack0 = e0m;
                ecrack1 = em1;

                // assign the currect face to first position, if the main edge have a second faces,
                // the new faces will be link in the second position
                e0m.f[0] = f0;
                em1.f[0] = f1;
            }
            e_crack[eID * 2] = ecrack0;
            e_crack[eID * 2 + 1] = ecrack1;
        }
        /// <summary>
        /// Before extract geometry in default structure, need to clear vertices ID
        /// </summary>
        protected void clearlist()
        {
            for (int i = 0; i < m_faces.Count; i++)
            {
                m_faces[i].ID = i;
                m_faces[i].marked = false;
            }
            for (int i = 0; i < m_verts.Count; i++)
            {
                m_verts[i].ID = i;
                m_verts[i].marked = false;
            }
            for (int i = 0; i < m_edges.Count; i++)
            {
                m_edges[i].ID = i;
                m_edges[i].marked = false;
            }
        }
        /// <summary>
        /// Convert the half-edge structure in the base structure
        /// </summary>
        MeshListGeometry getTriMesh()
        {
            clearlist();

            MeshListGeometry mesh = new MeshListGeometry();

            if (m_verts.Count > ushort.MaxValue - 1)
                throw new OverflowException("too many vertices for a 16 bit indices");

            mesh.vertices = new VertexAttribute<Vector3>(DeclarationUsage.Position, m_verts.Count);
            mesh.colors = new VertexAttribute<Color32>(DeclarationUsage.Color, m_verts.Count);
            mesh.normals = new VertexAttribute<Vector3>(DeclarationUsage.Normal, m_verts.Count);
            mesh.texcoords = new VertexAttribute<Vector2>(DeclarationUsage.TexCoord, m_verts.Count);

            if (m_verts.GetType().GetElementType() == typeof(HVertexFull))
            {

                for (int i = 0; i < m_verts.Count; i++)
                {
                    HVertexFull v = (HVertexFull)m_verts[i];
                    mesh.vertices[i] = v.position;
                    mesh.colors[i] = v.color;
                    mesh.normals[i] = v.normal;
                    mesh.texcoords[i] = v.texture;
                }
            }
            else
            {
                for (int i = 0; i < m_verts.Count; i++)
                {
                    mesh.vertices[i] = m_verts[i].position;
                }
            }
            //fix the id
            for (int i = 0; i < m_verts.Count; i++) m_verts[i].ID = i;

            mesh.indices = new IndexAttribute<Face16>(m_faces.Count);
            for (int i = 0; i < m_faces.Count; i++)
            {
                mesh.indices[i] = new Face16(
                    (ushort)m_faces[i].v[0].ID,
                    (ushort)m_faces[i].v[1].ID,
                    (ushort)m_faces[i].v[2].ID);
            }
            return mesh;
        }
        /// <summary>
        /// Convert the base structure to half-edge structure
        /// </summary>
        void setTriMesh(IList<Face16> faces, Vector3[] vertices, Color32[] colors)
        {
            m_verts.Clear();
            m_faces.Clear();

            if (vertices != null)
            {
                if (colors != null)
                {
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        HVertexFull v = new HVertexFull(i);
                        v.color = colors[i];
                        v.position = vertices[i];
                        m_verts.Add(v);
                    }
                }
                else
                {
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        HVertex v = new HVertex(i);
                        v.position = vertices[i];
                        m_verts.Add(v);
                    }
                }
            }
            else
            {
                throw new ArgumentNullException();
            }

            foreach (Face16 f in faces)
                m_faces.Add(new HFace(m_verts[f.I], m_verts[f.J], m_verts[f.K]));

            m_verts.TrimExcess();
            m_faces.TrimExcess();
        }

        /// <summary>
        /// a triangle to tessellate for debugging
        /// </summary>
        /// <returns></returns>
        public static HMesh DebugGeometry()
        {
            List<Vector3> vertex = new List<Vector3>();
            vertex.Add(new Vector3(50, 100, 0));
            vertex.Add(new Vector3(100, 0, 0));
            vertex.Add(new Vector3(0, 0, 0));

            List<Color32> color = new List<Color32>();
            color.Add(Color.Blue);
            color.Add(Color.Red);
            color.Add(Color.Green);       
            
            List<Face16> tri = new List<Face16>();
            tri.Add(new Face16(0, 2, 1));

            HMesh mesh = new HMesh(tri.ToArray(), vertex.ToArray(), color.ToArray());

            mesh.TriangleTessellate(true);
            mesh.TriangleTessellate(true);
            mesh.Collapse(mesh.m_edges[24]);
            mesh.clearlist();
            return mesh;
        }

        /// <summary>
        /// Use incremental algorithm to build a convex mesh using a vertices list
        /// </summary>
        /// <param name="vertices">any collection of points</param>
        public static HMesh GetConvexHull(IList<IVertexPosition> vertices)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Calculate all texture using a cylinder surface define by a matrix in current mesh world transform
        /// space obviosly, if you have a global cylinderWorld matrix, change in cylinderWorld = cylinderWorld * World^-1
        /// </summary>
        public void CylindricalTextureProjection(Matrix4 cylinderWorld)
        {
            float minh = float.MaxValue;
            float maxh = float.MinValue;

            if (m_verts.GetType().GetElementType() != typeof(HVertexFull)) return;

            for (int i = 0; i < m_verts.Count; i++)
            {
                Vector3 v = Vector3.TransformCoordinate(m_verts[i].position, cylinderWorld);
                float r, g, h;
                MathUtils.CartesianToCylindrical(v.x, v.y, v.z, out r, out g, out h);
                
                if (v.x < 0) g = (float)Math.PI - g;
                if (v.y < minh) minh = v.y;
                if (v.y > maxh) maxh = v.y;

                HVertexFull vertex = (HVertexFull)m_verts[i];
                vertex.texture = new Vector2((float)(g / Math.PI / 2.0), h);
            }
            // scale height
            for (int i = 0; i < m_verts.Count; i++)
            {
                Vector2 tex = ((HVertexFull)m_verts[i]).texture;
                tex.y = (tex.y - minh) / (maxh - minh);
                ((HVertexFull)m_verts[i]).texture = tex;
            }
        }
    }

}
