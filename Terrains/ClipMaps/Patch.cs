﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Graphics;
using Engine.Tools;


namespace Engine.ClipMaps
{
    /// <summary>
    /// A static 2d grid, each vertex are in unit's coordinates and are valid for whole clipmaps system.
    /// </summary>
    /// <remarks>
    /// A base vertex are in unit coord system, example : [1,2], in shader code will be converted in global position
    /// using various offsets
    /// </remarks>
    public class StaticPatchMesh : DisposableResource, IResettable
    {
        public static int DrawCount = 0;
        public static int TrianglesCount = 0;
        GraphicDevice m_device;
        VertexDeclaration m_decl;
        VertexBuffer m_vb;
        IndexBuffer m_ib;

        public int row { get; private set; }
        public int col { get; private set; }

        public StaticPatchMesh(GraphicDevice device, int Row, int Col)
        {
            m_device = device;
            row = Row;
            col = Col;
            if (Row < 1 || Col < 1 || Row * Col > ushort.MaxValue - 1)
                throw new ArgumentOutOfRangeException(string.Format("Grid size {0}x{1} too big", row, col));
            Restore();
        }

        public void DrawMesh()
        {
            m_decl.SetToDevice();
            m_vb.SetToDevice();
            m_ib.SetToDevice();
            m_device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_vb.Count, 0, m_ib.Count);
            DrawCount++;
            TrianglesCount += m_ib.Count;
        }

        /// <summary>
        /// Tessellated Grid, the vertices are in units length, to convert in float you can simple multiple by a conversion
        /// factor (ushort2 vertex => new float2(vertex.x * f.x , vertex.y * f.y)
        /// </summary>
        /// <param name="xsuddivision">1 = no suddivision, rectangle have 4 vertices and 2 faces</param>
        static void tessellatedRectangle(int xsuddivision, int ysuddivision, out ushort[] vertices, out Face16[] indices)
        {
            int numverts = (xsuddivision + 1) * (ysuddivision + 1);
            int numtris = xsuddivision * ysuddivision * 2;
            int row = xsuddivision + 1;

            // if you must use more than 65535 vertices for each patch, you need to change also Clipmap.fx shader code
            // and used default float2 (or uint2) instead half2
            if (numverts > ushort.MaxValue - 1)
                throw new OverflowException("too many vertices for 16bit indices");

            vertices = new ushort[numverts * 2];
            indices = new Face16[numtris];

            int n = 0;
            int i = 0;
            for (ushort y = 0; y <= ysuddivision; y++)
            {
                for (ushort x = 0; x <= xsuddivision; x++)
                {
                    vertices[n++] = x;
                    vertices[n++] = y;
                    //vertices[n++] = new Vector2x16(X, Y);
                }
            }

            n = 0;
            i = 0;
            for (int y = 0; y < ysuddivision; ++y)
            {
                for (int x = 0; x < xsuddivision; ++x)
                {
                    indices[i++] = new Face16(n + 1, n, row + (n + 1));
                    indices[i++] = new Face16(row + (n + 1), n, row + n);
                    n += 1;
                }
                n += 1;
            }
        }

        public void Restore()
        {
            ushort[] unitVertices;
            Face16[] indices;

            tessellatedRectangle(col, row, out unitVertices, out indices);

            List<VertexElement> m_elements = new List<VertexElement>();
            m_elements.Add(new VertexElement(0, 0, DeclarationType.Short2, DeclarationMethod.Default, DeclarationUsage.Position, 0));
            m_decl = new VertexDeclaration(m_device, m_elements);

            // use static buffer, not managed so you can automatize restore function each time device reseting
            //m_vb = new VertexBuffer(typeof(Vector2), m_decl.Format, m_obj.numVertices, false);

            m_vb = new VertexBuffer(m_device, m_decl.Format, unitVertices.Length, false, true);
            m_ib = new IndexBuffer(m_device, IndexInfo.IndexFace16, indices.Length, false, true);

            VertexStream vdata = m_vb.OpenStream();
            vdata.WriteCollection<ushort>(unitVertices, 0, unitVertices.Length, 0);
            m_vb.CloseStream();
            m_vb.Count = unitVertices.Length;

            IndexStream idata = m_ib.OpenStream();
            idata.WriteCollection<Face16>(indices, IndexInfo.IndexFace16, 0, indices.Length, 0, 0);
            m_ib.CloseStream();
            m_ib.Count = indices.Length;
        }

        protected override void Dispose(bool disposing)
        {
            m_vb.Dispose();
            m_ib.Dispose();
        }

        public override string ToString()
        {
            return string.Format("{0}x{1}", row, col);
        }

    }

    /// <summary>
    /// A static patch, same location for each level.
    /// Example : all TopLeft0 MxM patch are identical for each level, so originX and originY are the same
    /// </summary>
    /// <remarks>
    /// to shader code will be pass the unit's offset "PatchOrigin", is an offset relative to static mesh
    /// </remarks>
    public class StaticPatch
    {
        public StaticPatchMesh mesh { get; private set; }

        public string name;
        public Color32 color;
        /// <summary> origin offset from level origin </summary>
        public int originX { get; private set; }
        public int originY { get; private set; }

        public StaticPatch(StaticPatchMesh mesh, int originx, int originy)
        {
            this.mesh = mesh;
            this.originX = originx;
            this.originY = originy;
        }

        /// <summary>
        /// Commit to effect the patchcolor and patchorigin values
        /// </summary>
        public void DrawPatch(ClipMapsMaterial material)
        {
            material.PatchColor = color;
            material.PatchOrigin = new Vector2(originX, originY);
            mesh.DrawMesh();
            //Console.WriteLine("Draw : " + this.ToString());
        }

        public override string ToString()
        {
            return name + "_" + mesh.ToString();
        }
    }
}
