using System;
using System.Diagnostics;
using System.Collections;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.IO;

using Engine.Maths;

namespace Engine.M2TW
{
    /// <summary>
    ///  A class that rappresent a tipical game mesh data, was used only to store the calculated data.
    ///  and optimized to be rendered int the engine
    /// </summary>
    public class M2TWMesh
    {
        /// <summary>
        ///  Was used as personal name in mesh files
        /// </summary>
        public string firstname = "";
        /// <summary>
        ///  Was used as group name in mesh files
        /// </summary>
        public string secondname = "";
        /// <summary>
        ///  Was used for appear probability in mesh files
        /// </summary>
        public bool show = true;

        #region Additional vertices data

        public Face[] faces;
        public Vector3[] vertices;
        public Vector3[] normals;
        public Vector3[] tangents;
        public Vector3[] binormals;
        public Vector2[] uvw;
        public Vector2[] weights;
        public Byte4[] boneid;
        public Byte4[] lights;
        public Byte4[] anims;
        public Byte4[] meshanim1;
        public Byte4[] meshanim2;

        public int numTriangles { get { return faces.Length / 3; } }
        public int numVertices { get { return vertices.Length; } }

        #endregion

        public BoundarySphere bsphere;
        public BoundaryOBB enclosedbox;

        public override string ToString()
        {
            StringBuilder str = new StringBuilder(firstname + "," + secondname + "," + show+ "\n");
            str.Append("numfaces : " + numTriangles + "\n");
            str.Append("numverts : " + numVertices + "\n");
            return str.ToString();
        }
        /// <summary>
        /// Unpacker Version 2, extract a singular mesh from regouped meshes, need a decompression algorithm because game set the ripetitive vertices with the same index
        /// </summary>
        protected bool UnpackMesh2(FacesGroup facegroup, VerticesData verticesdata)
        {
            int nfaces = (int)facegroup.numfaces;

            // first steep , copy the faces index and build the sorted array (bitarray for this goal)
            sFace[] facearray = facegroup.face;

            if (facearray.Length != nfaces)
            {
                Console.WriteLine("ERROR, num faces not match");
                return false;
            }

            // vertex's index conversion table
            ushort[] indexconversion = new ushort[nfaces * 3];
            for (int i = 0; i < nfaces; i++)
            {
                indexconversion[i * 3 + 0] = facearray[i].face.I;
                indexconversion[i * 3 + 1] = facearray[i].face.J;
                indexconversion[i * 3 + 2] = facearray[i].face.K;
            }

            ushort ileft = 0;
            ushort iright = (ushort)(nfaces * 3 - 1);
            BitArray sorted = new BitArray(nfaces * 3, false);
            while (ileft < iright)
            {
                // indice globale a sinistra
                ushort idx = indexconversion[ileft];
                for (int i = ileft; i <= iright; i++)
                {
                    // escludo dalla conversion gli indici gi� convertiti e quelli non uguali
                    if (!sorted[i] && indexconversion[i] == idx)
                    {
                        indexconversion[i] = ileft;
                        sorted[i] = true;
                    }
                }
                ileft++;
            }
            sorted = null;
            faces = new Face[nfaces];
            int vertssize = 0;
            for (int i = 0; i < nfaces; i++)
            {
                faces[i].I = indexconversion[i * 3 + 0];
                faces[i].J = indexconversion[i * 3 + 1];
                faces[i].K = indexconversion[i * 3 + 2];

                if (faces[i].I > vertssize) vertssize = faces[i].I;
                if (faces[i].J > vertssize) vertssize = faces[i].J;
                if (faces[i].K > vertssize) vertssize = faces[i].K;
            }
            vertssize++;
            indexconversion = null;

            for (int i = 0; i < verticesdata.numpoint2; i++)
            {
                Point2Data data = verticesdata.point2data[i];
                switch (data.Type)
                {
                    case (M2TWVertexEnum.uvw):
                        this.uvw = new Vector2[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.uvw[faces[j].I] = data.v[facearray[j].face.I].vector;
                            this.uvw[faces[j].J] = data.v[facearray[j].face.J].vector;
                            this.uvw[faces[j].K] = data.v[facearray[j].face.K].vector;
                        }
                        break;

                    case (M2TWVertexEnum.weight):
                        this.weights = new Vector2[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.weights[faces[j].I] = data.v[facearray[j].face.I].vector;
                            this.weights[faces[j].J] = data.v[facearray[j].face.J].vector;
                            this.weights[faces[j].K] = data.v[facearray[j].face.K].vector;
                        }
                        break;

                    default:
                        Debug.WriteLine("data.Type " + data.Type + " assignment in x2float are never tested in this case");
                        break;
                }
            }

            for (int i = 0; i < verticesdata.numpoint3; i++)
            {
                Point3Data data = verticesdata.point3data[i];
                switch (data.Type)
                {
                    case (M2TWVertexEnum.vertex):
                        this.vertices = new Vector3[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.vertices[faces[j].I] = data.v[facearray[j].face.I].vector;
                            this.vertices[faces[j].J] = data.v[facearray[j].face.J].vector;
                            this.vertices[faces[j].K] = data.v[facearray[j].face.K].vector;
                        }
                        break;

                    case (M2TWVertexEnum.normal):
                        this.normals = new Vector3[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.normals[faces[j].I] = data.v[facearray[j].face.I].vector;
                            this.normals[faces[j].J] = data.v[facearray[j].face.J].vector;
                            this.normals[faces[j].K] = data.v[facearray[j].face.K].vector;
                        }
                        break;

                    case (M2TWVertexEnum.uvw):
                        this.uvw = new Vector2[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.uvw[faces[j].I] = ((sPoint2D)data.v[facearray[j].face.I]).vector;
                            this.uvw[faces[j].J] = ((sPoint2D)data.v[facearray[j].face.J]).vector;
                            this.uvw[faces[j].K] = ((sPoint2D)data.v[facearray[j].face.K]).vector;
                        }
                        break;

                    default:
                        Debug.WriteLine("data.Type " + data.Type + " assignment in 3xfloat are never tested in this case");
                        break;
                }
            }
            for (int i = 0; i < verticesdata.numpoint4; i++)
            {
                Point4Data data = verticesdata.point4data[i];
                switch (data.Type)
                {
                    case (M2TWVertexEnum.vertex):
                        this.vertices = new Vector3[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.vertices[faces[j].I] = ((sPoint3D)data.v[facearray[j].face.I]).vector;
                            this.vertices[faces[j].J] = ((sPoint3D)data.v[facearray[j].face.J]).vector;
                            this.vertices[faces[j].K] = ((sPoint3D)data.v[facearray[j].face.K]).vector;
                        }
                        break;

                    case (M2TWVertexEnum.normal):
                        this.normals = new Vector3[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.normals[faces[j].I] = ((sPoint3D)data.v[facearray[j].face.I]).vector;
                            this.normals[faces[j].J] = ((sPoint3D)data.v[facearray[j].face.J]).vector;
                            this.normals[faces[j].K] = ((sPoint3D)data.v[facearray[j].face.K]).vector;
                        }
                        break;

                    case (M2TWVertexEnum.uvw):
                        this.uvw = new Vector2[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.uvw[faces[j].I] = ((sPoint2D)data.v[facearray[j].face.I]).vector;
                            this.uvw[faces[j].J] = ((sPoint2D)data.v[facearray[j].face.J]).vector;
                            this.uvw[faces[j].K] = ((sPoint2D)data.v[facearray[j].face.K]).vector;
                        }
                        break;

                    case (M2TWVertexEnum.boneid):
                        this.boneid = new Byte4[vertssize];
                        for (int j = 0; j < nfaces; j++)
                        {
                            this.boneid[faces[j].I] = data.v[facearray[j].face.I].bytes;
                            this.boneid[faces[j].J] = data.v[facearray[j].face.J].bytes;
                            this.boneid[faces[j].K] = data.v[facearray[j].face.K].bytes;
                        }
                        break;

                    // do nothing because these data aren't utilized 
                    case (M2TWVertexEnum.binorm): break;
                    case (M2TWVertexEnum.tang): break;
                    case (M2TWVertexEnum.rgblight): break;
                    case (M2TWVertexEnum.rgbanim): break;
                    case (M2TWVertexEnum.meshanim1): break;
                    case (M2TWVertexEnum.meshanim2): break;
                    default:
                        Debug.WriteLine("data.Type " + data.Type.ToString() + " assignment in 4xbyte are never tested in this case");
                        break;
                }
            }
            return true;
        }
        /// <summary>
        /// Unpacker Version 1, extract a singular mesh from regouped meshes, need a decompression algorithm because game set the ripetitive vertices with the same index
        /// </summary>
        protected bool UnpackMesh1(FacesGroup facegroup, VerticesData verticesdata)
        {
            int nfaces = (int)facegroup.numfaces;
            int MAXFACES = ushort.MaxValue + 1;
            BitArray sorted = new BitArray(MAXFACES, false);

            // first steep , copy the faces index and build the sorted array (bitarray for this goal)
            sFace[] facearray = facegroup.face;

            if (facearray.Length != nfaces)
            {
                Console.WriteLine("ERROR, num faces not match");
                return false;
            }
            int i = 0;
            ushort range_max = 0;
            for (i = 0; i < nfaces; i++)
            {
                sFace f = facearray[i];

                sorted[f.x] = true;
                sorted[f.y] = true;
                sorted[f.z] = true;

                if (range_max < f.x) range_max = f.x;
                if (range_max < f.y) range_max = f.y;
                if (range_max < f.z) range_max = f.z;

            }


            //sorted.Length = maxsize+1;
            /*
            for (i = 0; i <= maxsize + 1; i++)
               Console.Write(sorted[(int)i]? 1 : 0);
            Console.WriteLine("");
            */
            // second steep, find the new index. min and max is the range of utilized sorted value
            // with minvertsize you can reduce the search, this don't happen for hight compressed data
            ushort range_min = 0;
            while (!sorted[range_min++]) ;
            if (range_min == 0)
            {
                Console.WriteLine("ERROR, possible all zero index of faces");
                return false;
            }

            range_min--;

            faces = new Face[nfaces];

            for (i = 0; i < nfaces; i++)
            {
                ushort j = 0;
                sFace globalface = facearray[i];

                faces[i] = new Face(0, 0, 0);

                for (j = range_min; j < globalface.x; j++)
                    if (sorted[j])
                        this.faces[i].I++;

                for (j = range_min; j < globalface.y; j++)
                    if (sorted[j])
                        this.faces[i].J++;

                for (j = range_min; j < globalface.z; j++)
                    if (sorted[j])
                        this.faces[i].K++;

            }

            // cont the vertices utilized, will be used to inizialize array size
            int vertssize = 0;
            for (i = range_min; i <= range_max; i++)
                if (sorted[i]) vertssize++;
            //Console.WriteLine("vertssize = " + vertssize + " range_min = " + range_min + " range_max = " + range_max);

            // thirs steep, build the local vertices array, and explicit conversion was made
            // to convert sPoint4B --> sPoint2D or sPoint3D
            if (range_max - range_min + 1 < vertssize)
            {
                Console.WriteLine("vertssize too big from real size or sorted bitarray");
                return false;
            }

            for (i = 0; i < verticesdata.numpoint2; i++)
            {
                Point2Data data = verticesdata.point2data[i];
                ushort idx = 0;
                switch (data.Type)
                {
                    case (M2TWVertexEnum.uvw):
                        this.uvw = new Vector2[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j])
                                this.uvw[idx++] = data.v[j].vector;
                        break;

                    case (M2TWVertexEnum.weight):
                        this.weights = new Vector2[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j])
                                this.weights[idx++] = data.v[j].vector;
                        break;

                    default:
                        Debug.WriteLine("data.Type " + data.Type + " assignment in x2float are never tested in this case");
                        break;
                }
            }

            for (i = 0; i < verticesdata.numpoint3; i++)
            {
                Point3Data data = verticesdata.point3data[i];
                ushort idx = 0;
                switch (data.Type)
                {
                    case (M2TWVertexEnum.vertex):
                        this.vertices = new Vector3[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j]) this.vertices[idx++] = data.v[j].vector;
                        break;

                    case (M2TWVertexEnum.normal):
                        this.normals = new Vector3[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j]) this.normals[idx++] = data.v[j].vector;
                        break;

                    case (M2TWVertexEnum.uvw):
                        this.uvw = new Vector2[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j]) this.uvw[idx++] = ((sPoint2D)data.v[j]).vector;
                        break;
                    default:
                        Debug.WriteLine("data.Type " + data.Type + " assignment in 3xfloat are never tested in this case");
                        break;
                }
            }
            for (i = 0; i < verticesdata.numpoint4; i++)
            {
                Point4Data data = verticesdata.point4data[i];
                ushort idx = 0;
                switch (data.Type)
                {
                    case (M2TWVertexEnum.vertex):
                        this.vertices = new Vector3[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j]) this.vertices[idx++] = ((sPoint3D)data.v[j]).vector;
                        break;
                    case (M2TWVertexEnum.normal):
                        this.normals = new Vector3[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j]) this.normals[idx++] = ((sPoint3D)data.v[j]).vector;
                        break;
                    case (M2TWVertexEnum.uvw):
                        this.uvw = new Vector2[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j]) this.uvw[idx++] = ((sPoint2D)data.v[j]).vector;
                        break;
                    case (M2TWVertexEnum.boneid):
                        this.boneid = new Byte4[vertssize];
                        for (ushort j = range_min; j <= range_max; j++)
                            if (sorted[j]) this.boneid[idx++] = data.v[j].bytes;
                        break;

                    // do nothing because these data aren't utilized 
                    case (M2TWVertexEnum.binorm): break;
                    case (M2TWVertexEnum.tang): break;
                    case (M2TWVertexEnum.rgblight): break;
                    case (M2TWVertexEnum.rgbanim): break;
                    case (M2TWVertexEnum.meshanim1): break;
                    case (M2TWVertexEnum.meshanim2): break;
                    default:
                        Debug.WriteLine("data.Type " + data.Type.ToString() + " assignment in 4xbyte are never tested in this case");
                        break;
                }
            }
            return true;
        }
    }

    /// <summary>
    ///  To work with each mesh in the Complex's structure, i need a class that rappresent all data, address ecc...
    ///  The derived class GameTriMesh was used also for other standard m2tw geometry like mesh files. 
    /// </summary>
    public class M2TWWorldNode : M2TWMesh
    {
        public byte c = 0; //range 0-255 by bottom table 3
        public byte s = 0; //range 0-255 by bottom table 3
        public ushort m = 0; //range 0-65535 20000 objects are like two towns , global m (outside structure)
        public ushort mm = 0; // is a local m index (inside structure)
        public short o = -1; //range 0-65535 20000 objects are very espansive , the -1 rappresent the NO-OBJECT's MESH
        public byte g = 0; //range 0-255 maximum 6 damage group
        public int matid = 0; //range 0-255 maximum 80 texture but i need int for other calculation
        //68byte for each mesh * 20000 = 1.36 MB

        /// <summary>
        ///  Like 3dstudio script, the function return a rappresentative name. Setting
        /// </summary>
        public string meshNameFormat
        {
            get { return (secondname + ":" + firstname + ":" + ((show) ? 0 : 1)); }
        }
        /// <summary>
        /// i used a string like : "mesh-0001-cpx-01-str1-obj-001-1_".split = {"mesh","0001","cpx,"01","str1","obj","001","1"}
        /// str index can't be used because depend of texture. ATTENTION get are used when import a world, set are used only when
        /// build a new scene
        /// </summary>
        public string worldNameFormat
        {
            get { return (String.Format("mesh-{0:0000}-cpx{1:00}-str{2:0}-obj-{3:000}-{4:0}_", m + 1, c + 1, s + 1, o + 1, g + 1).ToString()); }
        }

        /// <summary>
        /// Unpacking structure to optain a standard DirectX format for each mesh, the WorldFormat must
        /// be valid, if you use other WorldFormat instance, the address don't match...
        /// </summary>
        public bool ExtractTriMesh(WorldFormat world)
        {
            if (c > world.meshdata.complex.Length - 1) return false;
            if (s > world.meshdata.complex[c].structure.Length - 1) return false;
            if (mm > world.meshdata.complex[c].structure[s].facesdata.mesh.Length - 1) return false;


            try
            {
                // le dimensioni massime che un indice pu� avere � 65536
                Structure structure = world.meshdata.complex[c].structure[s];
                FacesGroup facesgroup = structure.facesdata.mesh[this.mm];
                VerticesData verticesdata = structure.verticesdata;

                this.firstname = facesgroup.firstname;
                this.secondname = facesgroup.secondname;
                this.show = facesgroup.show;

                UnpackMesh2(facesgroup, verticesdata);
            }
            catch (Exception e)
            {
                Console.WriteLine(String.Format("crash for c{0:D} s{1:D} m{2:D}", c, s, mm));
                Console.WriteLine(e.ToString());
                return false;
            }
            return true;
        }
        /// <summary>
        /// Unpacking structure to optain a standard DirectX format for each mesh, the MeshFormat must
        /// be valid, if you use other MeshFormat instance, the address don't match...
        /// </summary>
        public bool ExtractTriMesh(MeshFormat mesh)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return (String.Format("cpx:{0:D3} str:{1:D3} m:{2:D4} mm:{3:D4} obj:{4:D3} group:{5:D} mat:{6:D3}\n", c, s, m, mm, o, g, matid));
        }
    }
}