﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Maths;

namespace Engine.Graphics
{
    /// <summary>
    /// If buffer is not managed, you can assign the rewrite delegate function to automatize
    /// </summary>
    public class IndexBuffer : BaseBuffer
    {
        IndexStream m_datastream = null;
        IndexInfo m_indexType;
        DX9IndexBuffer m_buffer;

        /// <summary>
        /// Emulator mode, generate a Array that simulate the directx buffer
        /// </summary>
        public IndexBuffer(IndexInfo format, int size, bool readable)
            : base(null, true, readable, true, true)
        {
            m_indexType = format;
            m_shadowtype = format.type;
            m_typeSize = format.bytesize;

            if (!m_shadowtype.IsValueType || m_shadowtype.IsEnum)
                throw new ArgumentException("type must be a struct with fized size");

            if (format.bytesize != Marshal.SizeOf(format.type))
                throw new ArgumentException("size of type " + format.type.ToString() + " don't match with declaration");


            InitBuffer(size);
        }

        /// <summary>
        /// If buffer is not managed, you can assign the rewrite delegate function to automatize
        /// </summary>
        /// <param name="size">capacity of vertices</param>
        /// <param name="managed">if not managed need to dispose before device reseting</param>
        /// <param name="indexType">Define the type used to rappresent indices, can be a 16 or 32bit number or Face struct</param>
        public IndexBuffer(GraphicDevice device, Type indexType, int size, bool dynamic, bool managed) :
            this(device, IndexInfo.GetIndexFormat(indexType), size, dynamic, false, managed) { }

        /// <summary>
        /// If buffer is not managed, you can assign the rewrite delegate function to automatize
        /// </summary>
        /// <param name="size">capacity of vertices</param>
        /// <param name="managed">if not managed need to dispose before device reseting</param>
        /// <param name="indexType">Define the type used to rappresent indices, can be a 16 or 32bit number or Face struct</param>
        public IndexBuffer(GraphicDevice device, IndexInfo indexType, int size, bool dynamic, bool managed) :
            this(device, indexType, size, dynamic, false, managed) { }

        /// <summary>
        /// If buffer is not managed, you can assign the rewrite delegate function to automatize
        /// </summary>
        /// <param name="size">capacity of vertices</param>
        /// <param name="managed">if not managed need to dispose before device reseting</param>
        /// <param name="indexType">Define the type used to rappresent indices, can be a 16 or 32bit number or Face struct</param>
        public IndexBuffer(GraphicDevice device, IndexInfo indexType, int size, bool dynamic, bool readable, bool managed)
            : base(device, dynamic,readable, managed)
        {
            this.m_indexType = indexType;
            m_shadowtype = indexType.type;
            m_typeSize = indexType.bytesize;
            InitBuffer(size);
        }
        /// <summary>
        /// Recreate a empty index buffer but with different size, Count will be set to zero
        /// </summary>
        /// <param name="count">capacity of buffer in currect elements type size, not in bytes</param>
        protected override void InitBuffer(int count)
        {
            if (count < 1) throw new Exception("Directx9 can't initialize a buffer with zero bytes");
            if (!disposed) Dispose(true);
            m_size = count * m_typeSize;
            m_count = 0;
            m_incrementoffset = 0;

            if (m_shadowEnabled)
            {
                m_shadowbuffer = Array.CreateInstance(m_shadowtype, count);
            }
            else
            {
                m_buffer = new DX9IndexBuffer(m_device.m_device, m_indexType.is32Bit, m_size, m_usage, m_memory);
            }
            disposed = false;
        }


        protected override void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (m_buffer!=null)
                        m_buffer.Dispose();
                    m_buffer = null;
                    m_count = 0;
                    m_incrementoffset = 0;
                }
                disposed = true;
            }
        }

        public override void SetToDevice()
        {
            if (disposed) throw new Exception("buffer not restored");
            if (m_buffer != null)
                m_buffer.SetToDevice();
        }

        /// <summary>
        /// Open stream
        /// </summary>
        public IndexStream OpenStream(int Offset, int Count, bool ReadOnly)
        {
            if (Offset > 0 && m_shadowEnabled) throw new NotSupportedException("offset not implemented in emulator mode");
            if (m_datastream != null) throw new Exception("vertex buffer already open, please use the same data stream instance");

            IntPtr ptr = base.Open(Offset, Count, ReadOnly);        
            m_datastream = new IndexStream(ptr, m_indexType, Count, base.IsReadable);           
            return m_datastream;
        }
        /// <summary>
        /// Open in writeonly mode
        /// </summary>
        public IndexStream OpenStream(int Offset, int Count)
        {
            return OpenStream(Offset, Count, false);
        }
        /// <summary>
        /// Open whole in writeonly mode
        /// </summary>
        public IndexStream OpenStream()
        {
            return OpenStream(0, m_size / m_typeSize, false);
        }

        /// <summary>
        /// </summary>
        /// <param name="boffset">in bytes</param>
        /// <param name="bsize">in bytes</param>
        protected override IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            if (disposed) throw new Exception("buffer not restored");
            m_incrementoffset = 0;
            m_flushbytes = 0;
            if (m_buffer != null)
            {
                return m_buffer.Lock(boffset, bsize, mode);
            }
            else if (m_shadowbuffer != null)
            {
                // emulator mode
                m_shadowhandle = GCHandle.Alloc(m_shadowbuffer, GCHandleType.Pinned);
                return m_shadowhandle.AddrOfPinnedObject();
            }
            else
            {
                return IntPtr.Zero;
            }
        }

        protected override void UnLock()
        {
            if (disposed) throw new Exception("buffer not restored");
            m_incrementoffset = 0;
            m_datastream.Close();
            m_datastream = null;

            if (m_buffer != null)
            {
                m_buffer.UnLock();
            }
            if (m_shadowbuffer != null && m_shadowhandle.IsAllocated)
            {
                m_shadowhandle.Free();
            }
        }
    }

}
