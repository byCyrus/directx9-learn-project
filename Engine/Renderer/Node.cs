﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    public class NodeCoreProperties
    {
        /// <summary>
        /// count how many time the node are added, can be a safety test to check if code try to do the same thing more time.
        /// </summary>
        public byte stackoverflowcounter_Add = 0;
        /// <summary><seealso cref="stackoverflowcounter_Add"/> </summary>
        public byte stackoverflowcounter_Remove = 0;
        /// <summary><seealso cref="stackoverflowcounter_Add"/> </summary>
        public byte stackoverflowcounter_Update = 0;

        /// <summary>
        /// The group there is the node
        /// </summary>
        public BatchGroup m_group = null;

        public UpdateFlags updateflags = UpdateFlags.None;     
        /// <summary>
        /// Get or Set if need rewrite vertices
        /// </summary>
        public bool needUpdateVertices
        {
            get { return (updateflags & UpdateFlags.Vertices) != 0; }
            set { if (value) updateflags |= UpdateFlags.Vertices; else updateflags &= ~UpdateFlags.Vertices; }
        }
        /// <summary>
        /// Get or Set if need rewrite indices
        /// </summary>
        public bool needUpdateIndices
        {
            get { return (updateflags & UpdateFlags.Indices) != 0; }
            set { if (value) updateflags |= UpdateFlags.Indices; else updateflags &= ~UpdateFlags.Indices; }
        }
        /// <summary>
        /// Get or Set if need set a new transform or recalculate vertices
        /// </summary>
        public bool needUpdateTransform
        {
            get { return (updateflags & UpdateFlags.Transform) != 0; }
            set { if (value) updateflags |= UpdateFlags.Transform; else updateflags &= ~UpdateFlags.Transform; }
        }
        /// <summary>
        /// Get or Set if need change material id
        /// </summary>
        public bool needUpdateMaterial
        {
            get { return (updateflags & UpdateFlags.Material) != 0; }
            set { if (value) updateflags |= UpdateFlags.Material; else updateflags &= ~UpdateFlags.Material; }
        }
        /// <summary>
        /// Get or Set if need a render state change
        /// </summary>
        public bool needUpdateAll
        {
            get { return (updateflags & UpdateFlags.RenderState) != 0; }
            set { if (value) updateflags |= UpdateFlags.RenderState; else updateflags &= ~UpdateFlags.RenderState; }
        }
        public EngineCoreFlags coreflags = EngineCoreFlags.None;
        /// <summary>
        /// Get or Set the animation optimization of node, if true the node are stored into dynamic buffer type
        /// </summary>
        public bool isAnimable
        {
            get { return (coreflags & EngineCoreFlags.IsDynamic) != 0; }
            set
            {
                if (value) coreflags |= EngineCoreFlags.IsDynamic;
                else coreflags &= ~EngineCoreFlags.IsDynamic;
            }
        }
        /// <summary>
        /// Get or Set if node in nodeRemove list in the EngineGroup
        /// </summary>
        public bool isMarkAsRemove
        {
            get { return (coreflags & EngineCoreFlags.MarkRemove) != 0; }
            set { if (value) coreflags |= EngineCoreFlags.MarkRemove; else coreflags &= ~EngineCoreFlags.MarkRemove; }
        }


        public void Reset()
        {
            coreflags = EngineCoreFlags.None;
            updateflags = UpdateFlags.None;
            m_group = null;
            ResetFlags();
        }

        public virtual void ResetFlags()
        {
        }

        public override string ToString()
        {
            return string.Format("  updateflags : {0} \r\n  coreflags : {1} \r\n", updateflags.ToString() ,  coreflags.ToString());
        }
    }

    public class NodeBatchProperties : NodeCoreProperties
    {
        public NodeBatchProperties(NodeCoreProperties expandprop)
        {
            this.m_group = expandprop.m_group;
            this.updateflags= expandprop.updateflags;
            this.coreflags = expandprop.coreflags;
        }

        /// <summary>
        /// The cache there is the node
        /// </summary>
        public BatchCache m_cache = null;
        /// <summary>
        /// index of chunk in bufferschunks list
        /// </summary>
        public int m_ichunk = -1;
        /// <summary>
        /// Get or Set all flags.isInBuffer/flags.isInWrite/flags.isInRemove/flags.isInOverwrite falgs all in one
        /// </summary>
        public BatchChunkBufferFlags chunkflags = BatchChunkBufferFlags.None;
        /// <summary>
        /// Get or Set flag if it's or not in the EngineCache's buffer
        /// </summary>
        public bool isInBuffer
        {
            get { return (chunkflags & BatchChunkBufferFlags.InBuffer) != 0; }
            set { if (value) chunkflags |= BatchChunkBufferFlags.InBuffer; else chunkflags &= ~BatchChunkBufferFlags.InBuffer; }
        }
        /// <summary>
        /// Get or Set flag if it's or not in the EngineGroup's write list
        /// </summary>
        public bool isInWrite
        {
            get { return (chunkflags & BatchChunkBufferFlags.InWrite) != 0; }
            set { if (value) chunkflags |= BatchChunkBufferFlags.InWrite; else chunkflags &= ~BatchChunkBufferFlags.InWrite; }
        }
        /// <summary>
        /// Get or Set flag if it's or not in the EngineGroup's remove list
        /// </summary>
        public bool isInRemove
        {
            get { return (chunkflags & BatchChunkBufferFlags.InRemove) != 0; }
            set { if (value) chunkflags |= BatchChunkBufferFlags.InRemove; else chunkflags &= ~BatchChunkBufferFlags.InRemove; }
        }
        /// <summary>
        /// Get or Set flag if it's or not in the EngineGroup's update list
        /// </summary>
        public bool isInOverwrite
        {
            get { return (chunkflags & BatchChunkBufferFlags.InUpdate) != 0; }
            set { if (value) chunkflags |= BatchChunkBufferFlags.InUpdate; else chunkflags &= ~BatchChunkBufferFlags.InUpdate; }
        }
        /// <summary>
        /// Before add a new node or After node are deleted from renderer need to reset its flags
        /// chunkIndex -1 ; cacheKey 0 ; flags.chunkflags NONE
        /// </summary>
        public void ResetCacheFlags()
        {
            m_ichunk = -1;
            m_cache = null;
            chunkflags = BatchChunkBufferFlags.None;
        }    
        /// <summary>
        /// The comparing function for all node used in a batch system, they are sorted by chunk index to improve algorithm
        /// </summary>
        public static int CompareByChunkPosition(NodeBatchProperties node1, NodeBatchProperties node2)
        {
            return node1.m_ichunk.CompareTo(node2.m_ichunk);
        }

        public override void ResetFlags()
        {
            ResetCacheFlags();
        }

        public override string ToString()
        {
            return base.ToString() + string.Format("  engineflag: {0} \r\n  chunkPos  : {1} \r\n  groupPos  : {2} \r\n",chunkflags.ToString(),m_ichunk,m_group.GetType().ToString());
        }
    }

    /// <summary>
    /// Gemetry data in Local Coordinate System
    /// </summary>
    [DebuggerDisplay("{name}_{Key}")]
    public abstract class Node : IKey, ITrasformable //, INodeMaterial
    {
        public uint m_key { get; set; }
        public bool m_removed { get; set; }

        //public virtual NodeMaterial m_material { get { return null; } set { } }
        //public virtual uint m_material_key { get { return 0; } }

        struct NodeGeometryProperties
        {
            public Matrix4 globalcoord;
            public Matrix4 globalcoord_inv;
        }

        public NodeCoreProperties engineProp = new NodeCoreProperties();
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        
        NodeGeometryProperties geometryProp;
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
         VertexFormat vformat = VertexFormat.Position;
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        protected PrimitiveType m_primitive = PrimitiveType.Instance;

        /// <summary>
        /// you can set a string to debug
        /// </summary>
        public string name { get; set; }

        #region Geometry
 
        /// <summary>
        /// Get the type of primitive used for this node, if is instance the primitive must be search in the parent node
        /// </summary>
        public PrimitiveType primitive
        {
            get { return m_primitive; }
            set { m_primitive = value; }
        }
        /// <summary>
        /// Get or Set how geometry are displayed when rendered. The implemented struct formats are : 
        /// Position , PositionColor , PositionNormalColor , PositionNormalTexture.
        /// </summary>
        public VertexFormat vertexFormat
        {
            get { return vformat; }
            set { vformat = value; }
        }
        /// <summary>
        /// Is the transformation of this node from 3d world root, this because the renderer set the world matrix to identity.
        /// </summary>
        public Matrix4 transform
        {
            get { return geometryProp.globalcoord; }
            set { geometryProp.globalcoord = value; geometryProp.globalcoord_inv = Matrix4.Inverse(value); }
        }
        /// <summary>
        /// When necessary is usefull to have a inverse matrix calculated only when necessary
        /// </summary>
        public Matrix4 transform_inv
        {
            get { return geometryProp.globalcoord_inv; }
        }
        /// <summary>
        /// need a basic volume information example to understand che center of mesh, the value are in LOCAL space.
        /// The only remark if for instance nodes, the bounding sphere is stored in the main node
        /// </summary>
        public BoundarySphere boundSphere = BoundarySphere.NaN;

        /// <summary>
        /// changing the transfrom matrix without affect vertices position in the world space. The vertices
        /// and normals are trasformed in world space and re-trasformed in the new local space
        /// </summary>
        public abstract void changeTransform(Matrix4 newtransform);

        /// <summary>
        /// Get the vertex stream ready to write into buffer, the type of returned array was defined by
        /// VertexDeclaration. If geometry not contain some elements the array will be filled with default
        /// value (Vector3.Zero, Vector2.Zero , Color.Black, ecc...)
        /// </summary>
        /// <param name="globalCoorSys">if true, the vertices and normals will be transformed using node transform</param>
        public abstract Array GetVerticesStream(bool globalCoorSys, VertexDeclaration declaration);
        /// <summary>
        /// Get the indices stream ready to write into buffer, the type or returned array was Uint or Ushort
        /// </summary>
        /// <param name="index_offset">precalculate a offset, if index + offset > IT.maxvalue genereate a exception</param>
        /// <remarks>
        /// (ushort.maxvalue + int.maxvalue) are always &lt; uint.maxvalue. But if you "use32bit" a exception occurs when
        /// (index_offset + face.maxindex) &gt; ushort.maxvalue
        /// </remarks>
        public abstract Array GetIndicesStream(int index_offset, bool use32bit);
        /// <summary>
        /// The number of total indices that will be used in the rendered, if is a not-indexed geometry the value are calculated using primitive count
        /// </summary>
        public abstract int numIndices { get; }
        /// <summary>
        /// The number of total vertices that will be used in the rendered
        /// </summary>
        public abstract int numVertices { get; }
        /// <summary>
        /// The number of total primitives that will be used in the rendered, can't be 0
        /// </summary>
        public abstract int numPrimitives { get; }
        #endregion

        public Node()
        {
            name = "";
            vertexFormat = VertexFormat.None;
            m_primitive = PrimitiveType.Instance;
            //m_material = null;
            m_removed = false;
            m_key = 0;

            geometryProp = new NodeGeometryProperties
            {
                globalcoord = Matrix4.Identity,
                globalcoord_inv = Matrix4.Identity,
            };
        }
        ~Node()
        {
            // if was added into engine core (handleID != 0), we need to delte it
        }

        /// <summary>
        /// return the index of primitive selected (triangle , segment or point), if not
        /// intersect return -1.
        /// </summary>
        /// <param name="ray">input ray</param>
        /// <param name="t">parametric value of ray at the intersection point</param>
        /// <param name="intersection">intersection point</param>
        public virtual int PrimitiveIntersection(Ray ray, out float t, out Vector3 intersection)
        {
            intersection = Vector3.Zero;
            t = 0;
            return -1;
        }


        public override string ToString()
        {
            StringBuilder str = new StringBuilder("DxNode info :\r\n");
            str.AppendLine(string.Format("  classtype : {0}", (this.GetType()).ToString()));
            str.AppendLine(string.Format("  name      : {0}", name));
            str.AppendLine(string.Format("  handleid  : 0x{0}", m_key.ToString("X4")));
            str.AppendLine(string.Format("  rendermode: {0}", vertexFormat.ToString()));
            str.AppendLine(engineProp.ToString());
            return str.ToString();
        }


    }
}