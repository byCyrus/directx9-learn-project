﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;

namespace Engine.Graphics
{
    /// <summary>
    /// interface to access to main camera's values
    /// </summary>
    public interface ICamera
    {
        Vector3 Targhet { get; }
        Vector3 Eye { get; }
        Matrix4 View { get; }
        Matrix4 Projection { get; }
        Viewport Viewport { get; }
    }

    /// <summary>
    /// The struct what contain all camera values necessary to update it
    /// </summary>
    public struct Camera
    {
        #region View Components
        public enum eViewComponent { Right, Up, Look, Eye }


        public Vector3 up, eye, targhet;
        public Matrix4 view, iview;

        public Vector3 ViewRight
        {
            get { return new Vector3(view.m00, view.m01, view.m02); }
        }
        public Vector3 ViewUp
        {
            get { return new Vector3(view.m10, view.m11, view.m12); }
        }
        /// <summary>
        /// the direction from LooAt --> Eye, carefull
        /// </summary>
        public Vector3 ViewLook
        {
            get { return new Vector3(view.m20, view.m21, view.m22); }
        }
        public Vector3 ViewEye
        {
            get { return new Vector3(iview.m03, iview.m13, iview.m23); }
        }

        public static Vector3 GetViewComponent(Matrix4 View, eViewComponent component)
        {
            switch (component)
            {
                case eViewComponent.Right: return new Vector3(View.m00, View.m01, View.m02);
                case eViewComponent.Up: return new Vector3(View.m10, View.m11, View.m12);
                case eViewComponent.Look: return new Vector3(View.m20, View.m21, View.m22);
                default: return Vector3.Zero;
            }
        }


        public void UpdateView()
        {
            //Console.WriteLine("update <{0}> <{1}> <{2}>", eye.ToString(), targhet.ToString(), up.ToString());
            view = Matrix4.MakeViewLH(eye, targhet, up);
            iview = Matrix4.Inverse(view);
            //Console.WriteLine("result <{0}> <{1}> <{2}>", view_look.ToString(), view_right.ToString(), view_up.ToString());
            up = ViewUp;
        }

        #endregion

        #region Project Components
        public enum ProjectionType
        {
            Orthogonal,
            Prospective
        }
        public Viewport viewport;
        public Matrix4 proj;
        public ProjectionType projType;
        public float fovy, fovx, near, far;

        public void UpdateProj()
        {
            switch (projType)
            {
                case ProjectionType.Orthogonal:
                    proj = Matrix4.MakeOrthoLH(fovy, viewport, near, far);
                    break;
                case ProjectionType.Prospective:
                    proj = Matrix4.MakeProjectionLH(fovy, viewport, near, far);
                    break;
                default: throw new NotImplementedException();
            }
        }

        public static Camera Default
        {
            get
            {
                Camera cam = new Camera();
                cam.up = Vector3.UnitY;
                cam.eye = new Vector3(10, 10, 10);
                cam.targhet = Vector3.Zero;
                cam.viewport = new Viewport(800, 640, 0, 0);
                cam.projType = ProjectionType.Prospective;
                cam.fovx = cam.fovy = MathUtils.DegreeToRadian(45);
                cam.near = 0.1f;
                cam.far = 1000.0f;
                cam.UpdateView();
                cam.UpdateProj();
                return cam;
            }
        }
        #endregion
    }

    /// <summary>
    /// A class to manage the control events
    /// </summary>
    public abstract class CameraController
    {
        protected Control panel;
        protected MouseJob mousing;
        protected ChangeMethod moveaffect;
        protected Point mouseStart;

        public CameraController(Control owner)
        {
            Enabled = true;
            panel = owner;
            panel.MouseDown += new MouseEventHandler(OnMouseDown);
            panel.MouseMove += new MouseEventHandler(OnMouseMove);
            panel.MouseUp += new MouseEventHandler(OnMouseUp);
            panel.MouseWheel += new MouseEventHandler(OnMouseWheel);
            panel.Resize += new EventHandler(OnResize);
            panel.Paint += new PaintEventHandler(OnPaint);

            mousing = MouseJob.None;
            moveaffect = ChangeMethod.Test;
            mouseStart = Point.Empty;
        }

        ~CameraController()
        {
            if (panel != null)
            {
                panel.MouseDown -= OnMouseDown;
                panel.MouseMove -= OnMouseMove;
                panel.MouseUp -= OnMouseUp;
                panel.MouseWheel -= OnMouseWheel;
                panel.Resize -= OnResize;
            }
        }
        /// <summary>
        /// Enable or disable the mouse events, the OnResize work always to ensure a correct projection matrix
        /// </summary>
        public bool Enabled { get; set; }

        protected abstract void OnResize(object sender, EventArgs e);
        protected abstract void OnMouseDown(object sender, MouseEventArgs e);
        protected abstract void OnMouseMove(object sender, MouseEventArgs e);
        protected abstract void OnMouseUp(object sender, MouseEventArgs e);
        protected abstract void OnMouseWheel(object sender, MouseEventArgs e);
        protected abstract void OnPaint(object sender, PaintEventArgs e);

        protected enum MouseJob
        {
            // mouse was not pressed : view or proj matrix is fixed -> avoid constanly update
            None,
            // left mouse pressed
            Traslation,
            // right mouse pressed
            Rotating,
            // wheel mouse rotating
            Zooming,
            // middle (wheel) mouse pressed
            Turning
        }
        protected enum ChangeMethod
        {
            // add constantly for each MouseMove call a movement.
            // Issue : the metric error are added each time so if mouse go in the previous position the value can be different
            Incremental,

            // store last postion or rotation, for each MouseMove calculate the rotation.
            // Pro : avoid metric error, the same previous mouse position return the same rotation
            PreservePrev,

            Test,
        }
    }

    /// <summary>
    /// My previouse implementation
    /// </summary>
    public class TrackBallCamera : CameraController, ICamera
    {
        Camera camera = Camera.Default;

        Matrix4 rotation = Matrix4.Identity;
        Vector3 traslation = Vector3.Zero;
        float planeWidth = 40.0f;
        float floatXpixel = 1.0f;
        float defaultZoom;
        float adjustWidth { get { return 1.0f / ((camera.viewport.Width - 1.0f) * 0.5f); } }
        float adjustHeight { get { return 1.0f / ((camera.viewport.Height - 1.0f) * 0.5f); } }
        float ballRadius { get { return min(camera.viewport.Width / 2, camera.viewport.Height / 2); } }
        int min(int a, int b) { return a < b ? a : b; }


        public TrackBallCamera(Control panel, Vector3 eye, Vector3 targhet, Vector3 up, float near, float far)
            : base(panel)
        {
            defaultZoom = Vector3.Distance(eye, targhet);

            if (defaultZoom < 1e-6)
                throw new ArgumentException("Please use a not degenerated vector");

            //update with new values
            camera.viewport = new Viewport(panel.ClientSize.Width, panel.ClientSize.Height, 0, 0);
            camera.eye = eye;
            camera.targhet = targhet;
            camera.up = up;
            camera.near = near;
            camera.far = far;
            camera.UpdateView();
            camera.UpdateProj();
        }

        protected override void OnPaint(object sender, PaintEventArgs e)
        {
        }

        protected override void OnResize(object sender, EventArgs e)
        {
            camera.viewport = new Viewport(panel.ClientSize.Width, panel.ClientSize.Height, 0, 0);
            camera.UpdateProj();
        }

        protected override void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (!Enabled) return;

            mousing = MouseJob.None;
            mouseStart.X = e.X;
            mouseStart.Y = e.Y;
            mousing = traslate(e.Button);

            if (mousing == MouseJob.Traslation)
            {
                Vector3 vect = -camera.ViewLook;
                float Height = (float)panel.ClientSize.Height;
                float Width = (float)panel.ClientSize.Width;

                switch (camera.projType)
                {
                    // get the length in units / lenght of screen for stardard projection
                    case Camera.ProjectionType.Prospective:
                        floatXpixel = vect.Length * 1.5f * (float)Math.Tan(45.0 * 0.5f) / (Height > Width ? Height : Width);

                        break;
                    // get the length in units / lenght of screen for ortogonal projection
                    case Camera.ProjectionType.Orthogonal:
                        floatXpixel = planeWidth / Width;
                        break;
                }
            }
        }

        protected override void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (Enabled && mousing != MouseJob.None)
            {
                // horizontal draw
                float dx = this.mouseStart.X - e.X;
                // vertical draw
                float dy = this.mouseStart.Y - e.Y;
                // importance of draw
                float distance = Vector3.GetLength(camera.eye - camera.targhet);
                // world coordinate conversion of draw
                Vector3 spin = camera.ViewRight * dx - camera.ViewUp * dy;

                switch (mousing)
                {
                    // TRASLATION
                    case MouseJob.Traslation:
                        this.mouseStart.X = e.X;
                        this.mouseStart.Y = e.Y;
                        Vector3 move = spin * floatXpixel * distance * 1.9f;
                        camera.eye += move;
                        camera.targhet += move;
                        camera.UpdateView();
                        break;

                    // TRACKBALL ROTATION
                    case MouseJob.Rotating:
                        float deltay = dy / panel.ClientSize.Height * 2.0f;
                        float deltax = dx / panel.ClientSize.Width * 2.0f;

                        if (moveaffect == ChangeMethod.Incremental)
                        {
                            rotation *= Matrix4.RotationYawPitchRoll(deltay, deltax, 0);
                            this.mouseStart.X = e.X;
                            this.mouseStart.Y = e.Y;
                        }
                        else if (moveaffect == ChangeMethod.PreservePrev)
                        {
                            rotation = Matrix4.RotationYawPitchRoll(deltay, deltax, 0);
                        }
                        else
                        {
                            Vector3 prev = arcballvect(mouseStart.X, mouseStart.Y);
                            Vector3 curr = arcballvect(e.Y, e.Y);
                            prev.Normalize();
                            curr.Normalize();

                            if (Vector3.Distance(prev, curr) > 1e-6)
                            {
                                Vector3 rotaxis = Vector3.Cross(prev, curr);
                                rotaxis.Normalize();
                                float ang = Vector3.Dot(prev, curr);
                                rotation = new Quaternion(rotaxis.x, rotaxis.y, rotaxis.z, ang);
                                rotation = Matrix4.Inverse(rotation);
                            }

                        }

                        break;

                    //TRACKBALL TURNING
                    case MouseJob.Turning:
                        spin.Normalize();
                        spin *= 0.01f; // set velocity
                        camera.up += spin;
                        camera.UpdateView();
                        break;
                }
            }
        }

        protected override void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (Enabled && mousing != MouseJob.None)
            {
                rotation = Matrix4.Inverse(rotation);

                // eye and up change, targhet remain fixed because trackball do this
                Vector3 eye = rotation * (camera.eye - camera.targhet) + traslation + camera.targhet;
                Vector3 up = rotation * camera.up;
                camera.eye = eye;
                camera.up = up;

                camera.UpdateView();
                camera.UpdateProj();

                rotation = Matrix4.Identity;
                traslation = Vector3.Zero;

                mousing = MouseJob.None;
            }
        }

        protected override void OnMouseWheel(object sender, MouseEventArgs e)
        {
            if (!Enabled) return;

            switch (camera.projType)
            {
                // get the length in units / lenght of screen for stardard projection
                case Camera.ProjectionType.Prospective:
                    Vector3 rayView = camera.targhet - camera.eye;
                    Vector3 move = -camera.ViewLook * (rayView.Length * e.Delta / 600.0f);
                    camera.eye += move;
                    camera.UpdateView();
                    break;
                // get the length in units / lenght of screen for ortogonal projection
                case Camera.ProjectionType.Orthogonal:
                    planeWidth *= e.Delta > 0 ? 1.1f : 0.9f;
                    camera.UpdateProj();
                    break;
            }
        }

        public Vector3 Targhet
        {
            get
            {
                if (mousing == MouseJob.None)
                    return camera.targhet;
                else
                    return traslation + camera.targhet;
            }
            set { camera.targhet = value; camera.UpdateView(); }
        }
        public Vector3 Eye
        {
            get
            {
                if (mousing == MouseJob.None)
                {
                    return camera.eye;
                }
                else
                {
                    return Matrix4.Inverse(rotation) * (camera.eye - camera.targhet) + traslation + camera.targhet;
                }
            }
            set { camera.eye = value; camera.UpdateView(); }
        }
        public Matrix4 View
        {
            get
            {
                // when mousing, return a temporaney value and preserve the camera struct's values example if you abort
                if (mousing == MouseJob.None)
                    return camera.view;
                else
                {
                    return camera.view * rotation;

                    // eye and up change, targhet remain fixed because trackball do this
                    Vector3 eye = rotation * (camera.eye - camera.targhet) + traslation + camera.targhet;
                    Vector3 up = rotation * camera.up;
                    return Matrix4.MakeViewLH(eye, camera.targhet, up);
                }
            }
        }
        public Matrix4 Projection
        {
            get { return camera.proj; }
        }
        public Viewport Viewport
        {
            get { return camera.viewport; }
            set { camera.viewport = value; camera.UpdateProj(); }
        }

        MouseJob traslate(MouseButtons mouse)
        {
            switch (mouse)
            {
                case MouseButtons.Left: return MouseJob.Traslation;
                case MouseButtons.Right: return MouseJob.Rotating;
                case MouseButtons.Middle: return MouseJob.Turning;
            }
            return MouseJob.None;
        }

        Vector3 point2world(Point p, float zdeph)
        {
            return Vector3.Unproject(p.X, p.Y, zdeph, camera.viewport, camera.proj, camera.view, Matrix4.Identity);
        }

        //map to sphere
        Vector3 arcballvect(int X, int Y)
        {
            //Adjust point coords and scale down to range of [-1,1]
            float x = X * adjustWidth - 1.0f;
            float y = 1.0f - adjustHeight * Y;
            float d = x * x + y * y;
            float r_sq = ballRadius * ballRadius;

            Vector3 P = new Vector3(x, y, 0);

            if (d > 1.0f)
            {
                P.Normalize();
            }
            else
            {
                P.z = (float)Math.Sqrt(1 - d);
            }

            return P;
        }


    }

    /// <summary>
    /// My new implementation using arcball math, but i found a lot of difficult to adapt its
    /// </summary>
    public class TrackBallCamera_new : CameraController, ICamera
    {
        Camera camera = Camera.Default;

        Matrix4 rotation = Matrix4.Identity;
        Matrix4 traslation = Matrix4.Identity;
        Mousing operation = Mousing.None;
        Point start, end;
        float zoom;

        Matrix4 arcballWorld;

        public TrackBallCamera_new(Control panel, Vector3 CameraEye, Vector3 CameraTarghet, Vector3 CameraUp, float near, float far)
            : base(panel)
        {
            //update with new values
            camera.viewport = new Viewport(panel.ClientSize.Width, panel.ClientSize.Height, 0, 0);
            camera.near = near;
            camera.far = far;
            camera.UpdateProj();

            this.zoom = Vector3.Distance(CameraEye, CameraTarghet);

            camera.view = Matrix4.MakeViewLH(CameraEye, CameraTarghet, CameraUp);

            // TODO : understand this corrrection because this it the only matrix that work correctly with rotation.
            // my idea to fix orientation is assign the defautl view matrix and rotate it to match with input data
            arcballWorld = Matrix4.Identity;

            arcballWorld.m00 = camera.view.m00;
            arcballWorld.m01 = camera.view.m01;
            arcballWorld.m02 = camera.view.m02;

            arcballWorld.m10 = camera.view.m10;
            arcballWorld.m11 = camera.view.m11;
            arcballWorld.m12 = camera.view.m12;

            arcballWorld.m20 = camera.view.m20;
            arcballWorld.m21 = camera.view.m21;
            arcballWorld.m22 = camera.view.m22;

            // to match with virtual 2D ball of screen
            camera.eye = new Vector3(0, 0, -zoom);
            camera.targhet = new Vector3(0, 0, 0);
            camera.up = new Vector3(0, 1, 0);
            camera.UpdateView();

            rotation = arcballWorld;
            traslation = Matrix4.Translating(-CameraTarghet);
        }

        int min(int a, int b) { return a < b ? a : b; }


        int Width
        {
            get { return panel != null ? panel.ClientSize.Width : 0; }
        }
        int Height
        {
            get { return panel != null ? panel.ClientSize.Height : 0; }
        }

        public int ArcBallRadius
        {
            get { return (int)(min(Width, Height) / 2 * 0.8f); }
        }

        public Matrix4 View
        {
            get { return camera.view * rotation * traslation; }
        }
        public Matrix4 InvView
        {
            get { return camera.iview * rotation * traslation; }
        }
        public Vector3 Eye
        {
            get { return Vector3.TransformCoordinate(camera.eye, Matrix4.Inverse(rotation * traslation)); }
        }
        public Vector3 Targhet
        {
            get { return Vector3.TransformCoordinate(camera.targhet, Matrix4.Inverse(rotation * traslation)); }
        }
        public Matrix4 Projection
        {
            get { return camera.proj; }
        }
        public Viewport Viewport
        {
            get { return camera.viewport; }
            set { camera.viewport = value; camera.UpdateProj(); }
        }
        protected override void OnPaint(object sender, PaintEventArgs e)
        {
            if (!Enabled) return;

            int centerx = this.Width / 2;
            int centery = this.Height / 2;
            int arcradius = this.ArcBallRadius;

            using (System.Drawing.Graphics g = e.Graphics)
            {
                // to dispose grafic at the end
                g.DrawEllipse(Pens.Yellow, centerx - arcradius, centery - arcradius, arcradius * 2, arcradius * 2);
            }
        }
        protected override void OnResize(object sender, EventArgs e)
        {
            camera.viewport = new Viewport(panel.ClientSize.Width, panel.ClientSize.Height, 0, 0);
            camera.UpdateProj();
        }
        protected override void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (!Enabled) return;

            mousing = MouseJob.None;
            if (e.Button == MouseButtons.Left)
            {
                operation = Mousing.Traslation;
                mousing = MouseJob.Traslation;
            }
            else if (e.Button == MouseButtons.Right)
            {
                operation = Mousing.Rotation;
                mousing = MouseJob.Rotating;
            }
            else
            {
                operation = Mousing.None;
                mousing = MouseJob.None;
            }
            start = e.Location;
            // else wheel don't work
            panel.Focus();
        }
        protected override void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!Enabled) return;

            if (operation != Mousing.None)
            {
                end = e.Location;

                if (operation == Mousing.Traslation)
                {
                    int dx = end.X - start.X;
                    int dy = start.Y - end.Y;

                    Matrix4 CurrView = View;
                    Vector3 vRight = new Vector3(View.m00, View.m01, View.m02);
                    Vector3 vUp = new Vector3(View.m10, View.m11, View.m12);
                    Vector3 move = (dx * vRight + dy * vUp) * (0.001f * zoom);

                    traslation = Matrix4.Translating(move) * traslation;
                }
                else if (operation == Mousing.Rotation)
                {
                    Vector3 p0 = arcballPoint(start);
                    Vector3 p1 = arcballPoint(end);

                    if ((p1 - p0).Length > 0.001f)
                    {
                        //Quaternion q0 = new Quaternion(p0.X, p0.Y, p0.Z, 0);
                        //Quaternion q1 = new Quaternion(p1.X, p1.Y, p1.Z, 0);
                        //rotation = Matrix.RotationQuaternion(q0 * q1) * rotation;
                        rotation = rotFromVectors(p0, p1) * rotation;
                    }
                }
                start = end;
            }
        }
        protected override void OnMouseUp(object sender, MouseEventArgs e)
        {
            operation = Mousing.None;
            mousing = MouseJob.None;
        }
        protected override void OnMouseWheel(object sender, MouseEventArgs e)
        {
            if (!Enabled) return;

            operation = Mousing.Zooming;

            if (e.Delta > 0) zoom *= 0.9f;
            else zoom *= 1.1f;

            camera.eye = new Vector3(0, 0, -zoom);
            camera.UpdateView();

            operation = Mousing.None;
        }

        /// <summary>
        /// Convert a screen point to the screen virtual hemisphere
        /// </summary>
        Vector3 arcballPoint(Point P)
        {
            Vector3 p = new Vector3();

            // adjust to center screen 
            p.x = P.X - Width / 2;
            p.y = Height / 2 - P.Y;

            // distance from radius
            float radiusSq = ArcBallRadius * ArcBallRadius;
            float distSq = p.x * p.x + p.y * p.y;

            //project it to sphere surface
            if (distSq > radiusSq)
            {
                // the point is outside ball (on XY plane), is sufficent scale the vector
                p.z = 0;
            }
            else
            {
                // the point is inside ball, (on hemisphere surface) remember that z(x,y) : z^2 = r^2 - x^2 - y^2
                p.z = (float)Math.Sqrt(radiusSq - distSq);
            }

            // unknow operation
            //p.x *= -1;
            //p.y *= -1;
            p.z *= -1;

            p.Normalize();


            return p;
        }


        Matrix4 rotFromVectors(Vector3 from, Vector3 to)
        {
            float dot = Vector3.Dot(from, to);
            Vector3 axe = Vector3.Cross(from, to);
            return (Matrix4)(new Quaternion(axe.x, axe.y, axe.z, dot));
            //return Matrix.RotationQuaternion(new Quaternion(axe.X, axe.Y, axe.Z, dot));
        }

        enum Mousing
        {
            None,
            Traslation,
            Rotation,
            Zooming
        }
    }
}
