﻿using System;
using System.Collections.Generic;
using System.Text;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;

namespace Engine.Renderer
{
    /// <summary>
    /// All renderable object must have a draw function
    /// </summary>
    public interface IDrawable : ITrasformable
    {
        /// <summary>
        /// only drawprimitive implementation
        /// </summary>
        void Draw();
        /// <summary>
        /// <seealso cref="Draw"/> with material techinc
        /// </summary>
        void Render(MaterialBase material);

        void SetToDevice();
    }

    public abstract class DrawObjects : IDrawable
    {
        protected GraphicDevice device;
        public VertexBuffer vertexbuffer;
        public VertexDeclaration vertexdecl;
        public IndexBuffer indexbuffer;
        public PrimitiveType primitive;
        public Matrix4 modelcoord;
        public MaterialBase material;
        public int numvertices, numindices, numprimitives;


        public DrawObjects(GraphicDevice device, VertexDeclaration declaration)
        {
            this.device = device;
            this.vertexdecl = declaration;
        }

        public Matrix4 transform
        {
            get { return modelcoord; }
            set { modelcoord = value; }
        }

        public void SetToDevice()
        {
            vertexdecl.SetToDevice();
            vertexbuffer.SetToDevice();
            if (indexbuffer != null) indexbuffer.SetToDevice();
        }

        public void Draw()
        {
            if (indexbuffer != null) device.DrawIndexedPrimitives(primitive, 0, 0, numvertices, 0, numprimitives);
            else device.DrawPrimitives(primitive, 0, numprimitives);
        }

        public void Render(MaterialBase material)
        {
            int count = material.Begin();
            for (int pass = 0; pass < count; pass++)
            {
                material.BeginPass(pass);
                Draw();
                material.EndPass();
            }
            material.End();
        }
    }

    public class TrianglesRenderer : DrawObjects
    {
        BaseTriGeometry geometry;

        public TrianglesRenderer(GraphicDevice device, VertexDeclaration declaration, BaseTriGeometry geometry):base(device,declaration)
        {
            this.geometry = geometry;
            this.primitive = geometry.primitive;
            this.numvertices = geometry.numVertices;
            this.numprimitives = geometry.numPrimitives;
            this.numindices = geometry.numIndices;
            this.modelcoord = geometry.transform;

            vertexbuffer = new VertexBuffer(device, declaration.Format, numvertices, false, false);
            VertexStream vdata = vertexbuffer.OpenStream(0, numvertices, false);        
            geometry.WriteAttibutes(vdata);
            vertexbuffer.CloseStream();

            if (geometry.IsIndexed)
            {
                indexbuffer = new IndexBuffer(device, IndexInfo.GetIndexFormat(typeof(ushort)), numindices, false, false);
                IndexStream idata = indexbuffer.OpenStream();
                geometry.WriteAttributesIndex(idata);
                indexbuffer.CloseStream();
            }
            else
            {
                indexbuffer = null;
            }
        }
    }
}
