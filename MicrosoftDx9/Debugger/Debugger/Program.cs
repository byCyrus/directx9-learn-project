﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;

namespace Debugger
{
    public class DX9Form : Form
    {
        bool SHADERMODE = true;

        Effect effect;
        Effect terraineffect;

        Device device = null;
        bool mousing = false;
        Point ptLastMousePosit;
        Point ptCurrentMousePosit;
        int spinX;
        int spinY;
        Microsoft.DirectX.Direct3D.Font text;

        VolumeTexture texture;
        VertexBuffer shaderbuffer;
        VertexDeclaration shaderdecl;

        VertexBuffer terrainB;
        VertexBuffer terrainH;
        VertexDeclaration terrainDecl;

        Matrix model;
        Matrix view;
        Matrix proj;


        static float elapsedTime;
        static DateTime currentTime;
        static DateTime lastTime;

        #region Form
        static void Main()
        {
            using (DX9Form frm = new DX9Form())
            {
                frm.Show();
                frm.Init();
                Application.Run(frm);
            }
        }
        public DX9Form()
        {
            this.ClientSize = new Size(640, 480);
            this.Text = "Direct3D (DX9/C#) - Multiple Vertex Buffers";
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.Opaque, true);


            Console.WriteLine("size1_0 " + (int)VertexTextureCoordinate.Size1(1));
            Console.WriteLine("size3_0 " + (int)VertexTextureCoordinate.Size3(1));
            Console.WriteLine("size4_0 " + (int)VertexTextureCoordinate.Size4(1));

        }
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            currentTime = DateTime.Now;
            TimeSpan elapsedTimeSpan = currentTime.Subtract(lastTime);
            elapsedTime = (float)elapsedTimeSpan.Milliseconds * 0.001f;
            lastTime = currentTime;

            this.Render();
            this.Invalidate();
        }
        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            ptLastMousePosit = ptCurrentMousePosit = PointToScreen(new Point(e.X, e.Y));
            mousing = true;
        }
        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            mousing = false;
        }
        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            ptCurrentMousePosit = PointToScreen(new Point(e.X, e.Y));

            if (mousing)
            {
                spinX -= (ptCurrentMousePosit.X - ptLastMousePosit.X);
                spinY -= (ptCurrentMousePosit.Y - ptLastMousePosit.Y);
            }

            ptLastMousePosit = ptCurrentMousePosit;
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        #endregion

        private void LoadTexture()
        {
            Bitmap[] maps = new Bitmap[]
            {
                Properties.Resources.bmp1,
                Properties.Resources.bmp2,
                Properties.Resources.bmp3,
                Properties.Resources.bmp4
            };

            int depth = maps.Length;
            int width = maps[0].Width;
            int height = maps[0].Height;

            texture = new VolumeTexture(device, width, height, depth, 1, Usage.None, Format.A8R8G8B8, Pool.Managed);

            int[, ,] pixelColor = (int[, ,])texture.LockBox(typeof(int), 0, LockFlags.None, new int[] { depth, height, width });

            for (int k = 0; k < depth; k++)
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                        pixelColor[k, y, x] = maps[k].GetPixel(x, y).ToArgb();
            
            texture.UnlockBox(0);


            device.SamplerState[0].MinFilter = TextureFilter.Linear;
            device.SamplerState[0].MagFilter = TextureFilter.Linear;
        }

        /// <summary>
        /// This method basically creates and initialize the Direct3D device and
        /// anything else that doens't need to be recreated after a device 
        /// reset.
        /// </summary>
        private void Init()
        {
            //
            // Do we support hardware vertex processing? If so, use it. 
            // If not, downgrade to software.
            //

            Caps caps = Manager.GetDeviceCaps(Manager.Adapters.Default.Adapter,
                                               DeviceType.Hardware);
            CreateFlags flags;

            if (caps.DeviceCaps.SupportsHardwareTransformAndLight)
                flags = CreateFlags.HardwareVertexProcessing;
            else
                flags = CreateFlags.SoftwareVertexProcessing;

            //
            // Everything checks out - create a simple, windowed device.
            //

            PresentParameters d3dpp = new PresentParameters();

            d3dpp.BackBufferFormat = Format.Unknown;
            d3dpp.SwapEffect = SwapEffect.Discard;
            d3dpp.Windowed = true;
            d3dpp.EnableAutoDepthStencil = true;
            d3dpp.AutoDepthStencilFormat = DepthFormat.D16;
            d3dpp.PresentationInterval = PresentInterval.Immediate;

            device = new Device(0, DeviceType.Hardware, this, flags, d3dpp);

            // Register an event-handler for DeviceReset and call it to continue
            // our setup.
            device.DeviceReset += new System.EventHandler(this.OnResetDevice);
            OnResetDevice(device, null);

            effect = Effect.FromString(device, Properties.Resources.Simple, null, ShaderFlags.None, null);
            effect.Technique = "Simple";


            terraineffect = Effect.FromString(device, Properties.Resources.Terrain, null, ShaderFlags.None, null);
            terraineffect.Technique = "Terrain";

        }

        /// <summary>
        /// This event-handler is a good place to create and initialize any 
        /// Direct3D related objects, which may become invalid during a 
        /// device reset.
        /// </summary>
        public void OnResetDevice(object sender, EventArgs e)
        {
            Device m_device = (Device)sender;

            System.Drawing.Font systemfont = new System.Drawing.Font("Arial", 12f, FontStyle.Regular);
            text = new Microsoft.DirectX.Direct3D.Font(device, systemfont);


            device.Lights[0].Diffuse = Color.White;
            device.Lights[0].Direction = new Vector3(-1, -1, -1);
            device.Lights[0].Enabled = true;
            device.Lights[0].Position = new Vector3(100, 100, 100);
            device.Lights[0].Type = LightType.Directional;

            
            proj = Matrix.PerspectiveFovLH(Geometry.DegreeToRadian(45.0f),(float)this.ClientSize.Width / this.ClientSize.Height,0.1f, 100.0f);
            view = Matrix.LookAtLH(new Vector3(3, 3, 3), Vector3.Empty, new Vector3(0, 1, 0));

            m_device.RenderState.ZBufferEnable = true;
            m_device.RenderState.Lighting = true;

            
            PositionColor[] vertices = OpenCube.vertexstream;
            shaderbuffer = new VertexBuffer(typeof(PositionColor), vertices.Length, m_device, Usage.None, PositionColor.format, Pool.Managed);
            shaderbuffer.SetData(vertices, 0, LockFlags.None);
            shaderdecl = new VertexDeclaration(m_device, PositionColor.elements);
           
            /*
            vertexBuffer = new VertexBuffer(typeof(Engine.Maths.Vector3),OpenCube.vertices.Length, m_device,Usage.Dynamic | Usage.WriteOnly,VertexFormats.Position,Pool.Default);
            GraphicsStream gStream = vertexBuffer.Lock(0, 0, LockFlags.None);
            gStream.Write(OpenCube.vertices);
            vertexBuffer.Unlock();


            colorBuffer = new VertexBuffer(typeof(Engine.Maths.Color32), OpenCube.colors.Length, m_device, Usage.Dynamic | Usage.WriteOnly, VertexFormats.Diffuse, Pool.Default);
            gStream = null;
            gStream = colorBuffer.Lock(0, 0, LockFlags.None);
            gStream.Write(OpenCube.colors);
            colorBuffer.Unlock();


            texCoordBuffer = new VertexBuffer(typeof(Engine.Maths.Vector2), OpenCube.textures.Length, m_device, Usage.Dynamic | Usage.WriteOnly, VertexFormats.Texture1, Pool.Default);
            gStream = null;
            gStream = texCoordBuffer.Lock(0, 0, LockFlags.None);
            gStream.Write(OpenCube.textures);
            texCoordBuffer.Unlock();


            VertexElement[] elements = new VertexElement[]
            {
                //		        Stream  Offset        Type                    Method                 Usage                      Usage Index
                new VertexElement( 0,     0,  DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position,          0),
                new VertexElement( 1,     0,  DeclarationType.Color,  DeclarationMethod.Default, DeclarationUsage.Color,             0),
                new VertexElement( 2,     0,  DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 0),
                VertexElement.VertexDeclarationEnd 
            };
            m_device.VertexDeclaration = new VertexDeclaration(m_device, elements);
            */

            LoadTexture();

            WriteTerrain();
        }


        float zphase = 0;
        float xphase = 0;

        public void WriteTerrain()
        {
            VertexElement[] elements = new VertexElement[]
            {
                new VertexElement(0,0,DeclarationType.Float2,DeclarationMethod.Default,DeclarationUsage.Position,0),
                new VertexElement(1,0,DeclarationType.Float1,DeclarationMethod.Default,DeclarationUsage.Position,1),
                VertexElement.VertexDeclarationEnd
            };

            terrainDecl = new VertexDeclaration(device, elements);

            Vector2[] grid = new Vector2[]
            {
                new Vector2(0,0),
                new Vector2(1,0),
                new Vector2(0,1),
                new Vector2(0,1),
                new Vector2(1,0),
                new Vector2(1,1)
            };
            float[] height = new float[] { 0, .5f, .5f, .5f, .5f, 0 };

            terrainB = new VertexBuffer(typeof(Vector2), grid.Length, device, Usage.None,VertexFormats.Position, Pool.Managed);
            terrainB.SetData(grid, 0, LockFlags.None);

            terrainH = new VertexBuffer(typeof(float), grid.Length, device, Usage.None, VertexFormats.Position, Pool.Managed);
            terrainH.SetData(height, 0, LockFlags.None);

        }

        public void UpdateWave()
        {
            float[] height = new float[]
            { 
                zphase % 1,
                xphase % 1,
                xphase % 1,
                zphase % 1,
                zphase % 1,
                xphase % 1
            };

            zphase += 0.1f;
            xphase += 0.1f;

            terrainH.SetData(height, 0, LockFlags.None);
        }

        int Frames = 0;
        int LastTickCount = 0;
        float LastFrameRate = 0;
        
        void UpdateFramerate()
        {
            Frames++;
            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                LastFrameRate = (float)Frames * 1000 / Math.Abs(Environment.TickCount - LastTickCount);
                LastTickCount = Environment.TickCount;
                Frames = 0;
            }
            text.DrawText(null, string.Format("Framerate : {0:0.00} fps", LastFrameRate), new Point(10, 430), Color.Red);
        }
        /// <summary>
        /// This method is dedicated completely to rendering our 3D scene and is
        /// is called by the OnPaint() event-handler.
        /// </summary>
        private void Render()
        {
            System.Threading.Thread.Sleep(100);
            model = Matrix.RotationYawPitchRoll(Geometry.DegreeToRadian(spinX), Geometry.DegreeToRadian(spinY), 0.0f);
           
            device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, Color.CornflowerBlue, 1.0f, 0);

            device.BeginScene();

            device.VertexDeclaration = shaderdecl;
            device.SetStreamSource(0, shaderbuffer, 0);

            //device.SetStreamSource(0, vertexBuffer, 0);
            //device.SetStreamSource(1, colorBuffer, 0);
            //device.SetStreamSource(2, texCoordBuffer, 0);
            //d3dDevice.SetTexture(0, texture);

            if (SHADERMODE)
            {
                device.RenderState.FillMode = FillMode.WireFrame;
                device.RenderState.CullMode = Cull.None;

                UpdateWave();

                device.SetStreamSource(0, terrainB, 0);
                device.SetStreamSource(1, terrainH, 0);
                device.VertexDeclaration = terrainDecl;

                terraineffect.SetValue("WorldViewProj", model * view * proj);
                int numpasses = terraineffect.Begin(0);
                for (int i = 0; i < numpasses; i++)
                {
                    terraineffect.BeginPass(i);
                    device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                    terraineffect.EndPass();
                }
                terraineffect.End();


                /*
                effect.SetValue("xWorldViewProjection", model * view * proj);
                effect.SetValue("xColoredTexture", texture);
                effect.SetValue("xLightPos", new Vector4(0,10,0,1));
                effect.SetValue("xLightPower", 2.0f);
                effect.SetValue("xRot", Matrix.Identity);
                effect.CommitChanges();

                int numpasses = effect.Begin(0);
                for (int i = 0; i < numpasses; i++)
                {
                    effect.BeginPass(i);
                    device.DrawPrimitives(PrimitiveType.TriangleStrip, 0, 2);
                    device.DrawPrimitives(PrimitiveType.TriangleStrip, 4, 2);
                    device.DrawPrimitives(PrimitiveType.TriangleStrip, 8, 2);
                    device.DrawPrimitives(PrimitiveType.TriangleStrip, 12, 2);
                    device.DrawPrimitives(PrimitiveType.TriangleStrip, 16, 2);
                    device.DrawPrimitives(PrimitiveType.TriangleStrip, 20, 2);
                    effect.EndPass();
                }
                effect.End();
                */
            }
            else
            {
                device.Transform.Projection = proj;
                device.Transform.View = view;
                device.Transform.World = model;

                device.DrawPrimitives(PrimitiveType.TriangleStrip, 0, 2);
                device.DrawPrimitives(PrimitiveType.TriangleStrip, 4, 2);
                device.DrawPrimitives(PrimitiveType.TriangleStrip, 8, 2);
                device.DrawPrimitives(PrimitiveType.TriangleStrip, 12, 2);
                device.DrawPrimitives(PrimitiveType.TriangleStrip, 16, 2);
                device.DrawPrimitives(PrimitiveType.TriangleStrip, 20, 2);
            }

            UpdateFramerate();

            device.EndScene();
            device.Present();
        }
    }
}
