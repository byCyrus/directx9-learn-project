﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;

using DX = Microsoft.DirectX;
using D3D = Microsoft.DirectX.Direct3D;

namespace Engine.Graphics
{
    /// <summary>
    /// Microsoft Directx : Enumerate all graphic card capacities, only for First default adaptes
    /// </summary>
    public static class DX9Enumerations
    {
        const int adapter = 0;
        static D3D.Caps caps = D3D.Manager.GetDeviceCaps(adapter, D3D.DeviceType.Hardware);

        #region utilities
        private enum DepthFormat
        {
            Unknown = 0,
            D16Lockable = 70,
            D32 = 71,
            D15S1 = 73,
            D24S8 = 75,
            D24X8 = 77,
            D24X4S4 = 79,
            D16 = 80,
            L16 = 81,
            D32SingleLockable = 82,
            D24SingleS8 = 83,
        }

        static bool isDepthFormat(Format format)
        {
            switch (format)
            {
                case Format.Unknown:
                case Format.D16Lockable:
                case Format.D32:
                case Format.D15S1:
                case Format.D24S8:
                case Format.D24X8:
                case Format.D24X4S4:
                case Format.D16:
                case Format.L16:
                case Format.D32SingleLockable:
                case Format.D24SingleS8: return true;
                default: return false;
            }
        }
        #endregion

        /// <summary>
        /// Check the screen format, old card can support only 16bit pixels
        /// </summary>
        public static bool CheckDeviceType(Format DisplayFormat,Format BackBufferFormat,bool windowed)
        {
            int result;
            bool test = D3D.Manager.CheckDeviceType(adapter, D3D.DeviceType.Hardware, (D3D.Format)DisplayFormat, (D3D.Format)BackBufferFormat, windowed, out result);
            if (result!=0) test = false;
            return test;
        }
        /// <summary>
        /// </summary>
        public static bool CheckDepthStencil(Format DisplayFormat, Format DepthStencilFormat)
        {
            if (!isDepthFormat(DepthStencilFormat))
                throw new ArgumentException("Isn't a depth format...");
            return D3D.Manager.CheckDepthStencilMatch(adapter, D3D.DeviceType.Hardware, (D3D.Format)DisplayFormat, (D3D.Format)DisplayFormat, (D3D.DepthFormat)DepthStencilFormat);
        }
        /// <summary>
        /// </summary>
        public static Version PixelShaderVersion
        {
            get { return caps.PixelShaderVersion; }
        }
        /// <summary>
        /// </summary>
        public static Version VertexShaderVersion
        {
            get { return caps.VertexShaderVersion; }
        }
        public static int MaxVertexShaderConst
        {
            get {  return caps.MaxVertexShaderConst;}
        }
        /// <summary>
        /// check if a resolution is supported
        /// </summary>
        public static bool CheckDisplayMode(int width, int height, Format format)
        {
            //restituisce un enumeratore con tutte le risoluzioni supportate
            IEnumerator enumerator = D3D.Manager.Adapters[adapter].SupportedDisplayModes.GetEnumerator();
            while (enumerator.MoveNext())
            {
                D3D.DisplayMode d = (D3D.DisplayMode)enumerator.Current;
                if (d.Width == width & d.Height == height & d.Format.Equals(format))
                    return true;
            }
            return false;
        }


        public static List<DisplayMode> GetViewsMode(Format format)
        {
            IEnumerator enumerator = D3D.Manager.Adapters[adapter].SupportedDisplayModes.GetEnumerator();
            List<DisplayMode> list = new List<DisplayMode>();
            while (enumerator.MoveNext())
            {
                D3D.DisplayMode d = (D3D.DisplayMode)enumerator.Current;
                list.Add(new DisplayMode { width = d.Width, height = d.Height, refreshRate = d.RefreshRate, format = (Format)d.Format });
            }
            return list;
        }

        /// <summary>
        /// Some check of max size, squareonly and pow2 capacities
        /// </summary>
        public static bool SupportTextureSize(Size size)
        {
            if (caps.MaxTextureWidth < size.Width || caps.MaxTextureHeight < size.Height) return false;

            if (caps.TextureCaps.SupportsSquareOnly && size.Height != size.Width) return false;

            if (caps.TextureCaps.SupportsPower2)
            {
                List<int> pow2 = new List<int> { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };
                if (!pow2.Contains(size.Width) || !pow2.Contains(size.Height)) return false;
            }
            return true;
        }
        /// <summary>
        /// Max simultaneous textures, Directx9 can support max 8 textures
        /// </summary>
        public static int MaxActiveTexture
        {
            get { return caps.MaxSimultaneousTextures; }
        }
        /// <summary>
        /// Used to know if i can use uint instead ushort indices format, remember that 0xFFFF for ushort is used
        /// for reset primitives counter example in Triangle.Fan
        /// </summary>
        public static int MaxVertexIndex
        {
            get { return caps.MaxVertexIndex; }
        }

        public static int MaxPrimitiveCount
        {
            get { return caps.MaxPrimitiveCount; }
        }
        /// <summary>
        /// Directx9 constant = 8
        /// </summary>
        public static int MaxLights
        {
            get { return caps.MaxActiveLights; }
        }
        public static bool SupportsHardwareTransformAndLight
        {
            get { return caps.DeviceCaps.SupportsHardwareTransformAndLight; }
        }
        public static bool SupportsPureDevice
        {
            get { return caps.DeviceCaps.SupportsPureDevice; }
        }
        public static DisplayMode CurrentDisplayMode
        {
            get
            {
                D3D.DisplayMode d = D3D.Manager.Adapters[adapter].CurrentDisplayMode;
                return new DisplayMode { width = d.Width, height = d.Height, refreshRate = d.RefreshRate, format = (Format)d.Format };
            }
        }
    }
}
