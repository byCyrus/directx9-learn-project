﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// NOT-INDEXED
    /// The dynamic caches, optimized for Read and Write buffer, store in managed memory pool
    /// </summary>
    public abstract class BatchCacheDynamic : BatchCache
    {
        protected VertexBuffer m_VB;

        protected const int chunkSizeV = ushort.MaxValue;

        protected BatchCacheDynamic(
            GraphicDevice device,
            VertexDeclaration verticesDeclaration,
            PrimitiveType primitive,
            EngineCore Core,
            bool useBatchMethod,
            bool useLocalTransform,
            bool indexed)
            : base(device, verticesDeclaration, primitive, indexed, Core, useBatchMethod, useLocalTransform)
        {
            // dynamic buffer use managed resource, so not need to link with device events
        }



        public BatchCacheDynamic(
            GraphicDevice device,
            VertexDeclaration verticesDeclaration,
            PrimitiveType primitive,
            EngineCore Core,
            bool useBatchMethod,
            bool useLocalTransform)
            : base(device, verticesDeclaration, primitive,false, Core, useBatchMethod, useLocalTransform)
        {
            // dynamic buffer use managed resource, so not need to link with device events
        }

        /// <summary>
        /// </summary>
        ~BatchCacheDynamic()
        {
            Destroy();
        }

        /// <summary>
        /// Destroing a cache remove all bufferchunks, dispose the buffer, remove from device events and mark as removed
        /// </summary>
        public override void Destroy()
        {
            Clear();
            m_removed = true;
        }
        
        /// <summary>
        /// TODO : cut unused space to decrease the buffer memory. The best way is trimbuffer only after 
        /// WriteAll() process , this because if you trimbuffer after RemoveAll() is possible you
        /// need to resize another time the buffer.
        /// </summary>
        public override void CleanBuffers()
        {
        }
        /// <summary>
        /// optimize the size of buffer to look, if resize need rewrite old data without use node to recalculate them
        /// </summary>
        /// <param name="GPUvertices">the vertex buffer stream </param>
        /// <param name="vlockoffset">the offset used when open the buffer</param>
        protected void PrepareVertexBuffer(out BufferStream GPUvertices, out int vlockoffset)
        {
            //Console.WriteLine("Write all in cache " + handle);

            // nReservedIndis and nReservedVerts are the finally size of
            // buffer. The DxNode must not change so is fondamental what
            // WriteAll are called immediatly after Add process

            // check if need resize buffer : 
            // if nReserved > Stream.Capacity mean size too small
            // if nReserved < GetMinCapacity(nReserved) mean size too big

            // get new size
            int newCapacity = nRequiredVerts + nRequiredVerts % chunkSizeV;
            if (newCapacity > MAXVERTS) newCapacity = MAXVERTS;

            // resize test : if capacity is too small or is too big
            bool isVerticesResize = m_VB==null || nRequiredVerts > m_VB.Capacity || newCapacity < m_VB.Capacity;

            // minimum offset for locking buffer
            int nvertices = vlockoffset = m_VB.Count;

            if (isVerticesResize)
            {
                // need to write from the beginning
                vlockoffset = 0;

                if (m_VB.Count > 0)
                {
                    m_VB.Open();
                    Array tmp_vertices = m_VB.Open.Read();

                   // Array tmp_vertices = m_VB.Open(0, m_VB.Count).Read(0, m_VB.Count);
                    m_VB.Close();
                    m_VB.Initialize(newCapacity);
                    GPUvertices = m_VB.Open(0, nRequiredVerts);
                    GPUvertices.Write(tmp_vertices, 0);
                }
                else
                {
                    m_VB.Initialize(newCapacity);
                    GPUvertices = m_VB.Open(0, nRequiredVerts);
                }
            }
            else
            {
                GPUvertices = m_VB.Open(vlockoffset, nRequiredVerts - vlockoffset);
            }
            m_VB.Count = nvertices;
        }
        /// <summary>
        /// Removing all chunks data and push all value at the beginning of buffers, no trimbuffer implementation
        /// to ensure a big buffer in WriteAll()
        /// </summary>
        public override void DeleteAll()
        {
            if (ready2remove.Count == 0) return;

            //Console.WriteLine("Delelte all in cache " + handle);

            int vminChunkIndex = BufferChunks.Count;
            int vlockoffset = 0;


            // if all data are removed do a quick erase
            if (ready2remove.Count == BufferChunks.Count)
            {
                foreach (Node node in ready2remove)
                {
                    // reset node's flags
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                    prop.isInBuffer = false;
                    prop.m_ichunk = -1;
                    prop.m_cache = null;
                    prop.stackoverflowcounter_Remove = 0;
                }

                BufferChunks.Clear();
                ready2remove.Clear();
                vminChunkIndex = 0;
                m_VB.Count = 0;

                // if buffer will not utilized int next process is a good idea destory buffers
                if (ready2write.Count == 0)
                {

                }
                return;
            }

            foreach (Node node in ready2remove)
            {
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                int n = prop.m_ichunk;
                if (n < vminChunkIndex) vminChunkIndex = n;

                BatchChunkInfo chunk = BufferChunks[n];

                if (chunk.node != node)
                    throw new Exception("something wrong... buffer's chunk don't match");

                vlockoffset = chunk.voffset;

                // mark as deleted
                chunk.node = null;
                prop.isInBuffer = false;
                prop.stackoverflowcounter_Remove = 0;
            }

            // remove all chunks marked, start from end to use correcly the index
            for (int i = BufferChunks.Count - 1; i >= vminChunkIndex; i--)
                if (BufferChunks[i].node == null)
                    BufferChunks.RemoveAt(i);


            BufferStream GPUvertices = m_VB.Open(vlockoffset, m_VB.Count - vlockoffset);

            int vv = 0;
            // number of previous vertices and indices count
            int nvertices = vlockoffset;

            // push all isolated indices to the beginning of buffer
            for (int n = vminChunkIndex; n < BufferChunks.Count; n++)
            {
                BatchChunkInfo chunk = BufferChunks[n];
                NodeBatchProperties prop = (NodeBatchProperties)chunk.node.engineProp;

                // start reading from "j" but use a correction offset "ilockoffset"
                int j = 0;

                j = chunk.voffset - vlockoffset;

                Array tmp_vertices = GPUvertices.Read(vv, chunk.nverts);
                GPUvertices.Write(tmp_vertices, j);
                j += tmp_vertices.GetUpperBound(0);

                chunk.voffset = vlockoffset + nvertices;
                nvertices += chunk.nverts;

                prop.m_ichunk = n;
            }

            m_VB.Close();
            m_VB.Count = nvertices;

            vminChunkIndex = BufferChunks.Count;

            ready2remove.Clear();
        }
        /// <summary>
        /// All new node are added at end of buffer (Append method)
        /// A trimBuffer implementation ensure the minimum size of buffer
        /// </summary>
        public override void WriteAll()
        {
            if (ready2write.Count == 0) return;

            BufferStream GPUvertices;
            int vlockoffset;
            PrepareVertexBuffer(out GPUvertices, out vlockoffset);
            int nvertices = m_VB.Count;

            //------------------------------
            // Append new data
            //------------------------------
            base.AppendNewNodes(ready2write, ref nvertices, vlockoffset, m_VB);

            if (nRequiredVerts != nvertices)
                throw new Exception("some DxNode change their data during Flush process... ");

            m_VB.Count = nRequiredVerts;
            m_VB.Close();

            ready2write.Clear();
        }

        protected void OverwriteVertices()
        {
            //----------------
            // Only Vertices
            //----------------
            // find first and last chunk index to use
            int vmin = 0;
            int vmax = ready2overwrite.Count - 1;
            while (vmin < ready2overwrite.Count && !((NodeBatchProperties)ready2overwrite[vmin].engineProp).needUpdateVertices) vmin++;
            while (vmax > vmin && !((NodeBatchProperties)ready2overwrite[vmax].engineProp).needUpdateVertices) vmax--;

            if (vmin < ready2overwrite.Count)
            {
                BatchChunkInfo first = (BatchChunkInfo)BufferChunks[((NodeBatchProperties)ready2overwrite[vmin].engineProp).m_ichunk];
                BatchChunkInfo last = (BatchChunkInfo)BufferChunks[((NodeBatchProperties)ready2overwrite[vmax].engineProp).m_ichunk];
                int vlockoffset = first.voffset;
                int nverts = last.voffset + last.nverts - vlockoffset;

                BufferStream GPUvertices = m_VB.Open(vlockoffset, nverts);

                for (int i = 0; i < ready2overwrite.Count; i++)
                {
                    Node node = ready2overwrite[i];
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;

                    // overwrite only necessary nodes
                    if (prop.needUpdateVertices)
                    {
                        BatchChunkInfo chunk = BufferChunks[prop.m_ichunk];
                        Array meshVertices = node.GetVerticesStream(!base.UseLocal, base.m_Vdeclaration);
                        GPUvertices.Write(meshVertices, chunk.voffset - vlockoffset);
                    }
                    prop.stackoverflowcounter_Update = 0;
                }
            }
            m_VB.Close();        
        }

        /// <summary>
        /// Overwriting method try to lock a smaller buffer size, the wrost case is when you update two nodes
        /// in first and in last position
        /// </summary>
        public override void OverwriteAll()
        {
            if (ready2overwrite.Count == 0) return;

            // sort node using bufferchunk order
            ready2overwrite.Sort();
            OverwriteVertices();

            ready2overwrite.Clear();
        }
        /// <summary>
        /// </summary>
        public override bool DebugFlushAllProcess()
        {
            if (m_VB != null && m_VB.Count != nRequiredVerts)
                throw new ArgumentOutOfRangeException("incoerent value : " + this.m_VB.Count + " != " + nRequiredVerts);
            return true;
        }
        public override string ToString()
        {
            return base.ToString() +
                "\r\nVBUFFER : " + m_VB.ToString() + "\r\n";
        }
        public Array CacheVertices()
        {
            BufferStream stream = m_VB.Open(0, m_VB.Count);
            m_VB.Close();
            return stream.Read(0, m_VB.Count);
        }
    }


    /// <summary>
    /// INDEXED
    /// The dynamic caches, optimized for Read and Write buffer, store in managed memory pool
    /// </summary>
    public abstract class BatchCacheDynamicIndexed : BatchCacheDynamic
    {
        protected DxIndexBuffer IndexBuffer;

        public BatchCacheDynamicIndexed(
            GraphicDevice device,
            VertexDeclaration verticesDeclaration,
            PrimitiveType primitive,
            EngineCore Core,
            bool useBatchMethod,
            bool useLocalTransform)
            : base(device, verticesDeclaration, primitive, Core, useBatchMethod, useLocalTransform,true)
        {
            // dynamic buffer use managed resource, so not need to link with device events
            InitializeIB();
        }

        /// <summary></summary>
        protected void InitializeIB()
        {
            IndexBuffer = new DxIndexBuffer(device.m_device, base.indices32bit, base.MaxIndices, BufferUsage.Dynamic, true);
            IndexBuffer.Count = 0;
            IndexBuffer.ChunkSize = m_VB.ChunkSize * 6;

        }
        /// <summary></summary>
        public override void CleanBuffers()
        {

        }
        /// <summary>
        /// optimize the size of buffer to look, if resize need rewrite old data without use node to recalculate them
        /// </summary>
        /// <param name="GPUvertices">the vertex buffer stream </param>
        /// <param name="vlockoffset">the offset used when open the buffer</param>
        protected void PrepareIndexBuffer(out BufferStream GPUindices, out int ilockoffset)
        { 
            if (IndexBuffer == null) InitializeIB();
            // get new size
            int newIndisCapacity = IndexBuffer.GetSizeUsingChunk(nRequiredIndis);
            
            // resize test : if capacity is too small or is too big
            bool isIndicesResize = nRequiredIndis > IndexBuffer.Capacity || newIndisCapacity < IndexBuffer.Capacity;
            
            // minimum offset for locking buffer
            ilockoffset = IndexBuffer.Count;

            int nindices = IndexBuffer.Count;

            // if need resize, the tmp_indices is a copy of GPU buffer 
            if (isIndicesResize)
            {
                // need to write from the beginning
                ilockoffset = 0;

                if (IndexBuffer.Count > 0)
                {
                    Array tmp_indices;

                    if (base.indices32bit) tmp_indices = IndexBuffer.GetData<uint>(0, IndexBuffer.Count);
                    else tmp_indices = IndexBuffer.GetData<ushort>(0, IndexBuffer.Count);

                    IndexBuffer.Initialize(newIndisCapacity);
                    // indices is the GPU buffer
                    GPUindices = IndexBuffer.Open(0, nRequiredIndis);
                    GPUindices.Write(tmp_indices, 0);
                }
                else
                {
                    IndexBuffer.Initialize(newIndisCapacity);
                    // indices is the GPU buffer
                    GPUindices = IndexBuffer.Open(0, nRequiredIndis);
                }

            }
            else
            {
                GPUindices = IndexBuffer.Open(ilockoffset, nRequiredIndis - ilockoffset);
            }

            IndexBuffer.Count = nindices;
        }

        /// <summary></summary>
        public override void DeleteAll()
        {
            if (ready2remove.Count == 0) return;

            //Console.WriteLine("Delelte all in cache " + handle);

            int iminChunkIndex = BufferChunks.Count;
            int vminChunkIndex = BufferChunks.Count;
            int ilockoffset = 0;
            int vlockoffset = 0;

            // if all data are removed do a quick erase
            if (ready2remove.Count == BufferChunks.Count)
            {
                foreach (Node node in ready2remove)
                {
                    // reset node's flags
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                    prop.isInBuffer = false;
                    prop.m_ichunk = -1;
                    prop.m_cache = null;
                    prop.stackoverflowcounter_Remove = 0;
                }

                BufferChunks.Clear();
                ready2remove.Clear();
                iminChunkIndex = vminChunkIndex = 0;
                m_VB.Count = 0;
                IndexBuffer.Count = 0;

                // if buffer will not utilized in next process is a good idea destory buffers
                if (ready2write.Count == 0)
                {

                }

                return;
            }

            foreach (Node node in ready2remove)
            {
                NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                int n = prop.m_ichunk;
                if (n < vminChunkIndex) iminChunkIndex = vminChunkIndex = n;
                
                BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[n];

                if (chunk.node != node)
                    throw new Exception("something wrong... buffer's chunk don't match");

                ilockoffset = chunk.ioffset;
                vlockoffset = chunk.voffset;

                // mark as removed
                chunk.node = null;
                prop.m_ichunk = -1;
                prop.m_cache = null;
                prop.isInBuffer = false;
                prop.stackoverflowcounter_Remove = 0;
            }

            // remove all chunks marked, start from end to use correcly the index
            for (int i = BufferChunks.Count - 1; i >= vminChunkIndex; i--)
                if (BufferChunks[i].node == null)
                    BufferChunks.RemoveAt(i);


            BufferStream GPUindices = IndexBuffer.Open(ilockoffset, IndexBuffer.Count - ilockoffset);
            BufferStream GPUvertices = m_VB.Open(vlockoffset, m_VB.Count - vlockoffset);

            int ii = 0;
            int vv = 0;
            // number of previous vertices and indices count
            int nvertices = vlockoffset;
            int nindices = ilockoffset;

            // push all isolated indices to the beginning of buffer
            for (int n = vminChunkIndex; n < BufferChunks.Count; n++)
            {
                BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[n];
                NodeBatchProperties prop = (NodeBatchProperties)chunk.node.engineProp;

                // start reading from "j" but use a correction offset "ilockoffset"
                int j = 0;


                j = chunk.ioffset - ilockoffset;

                // sumvalue can be negative
                int sumvalue = (base.UseBatch ? nvertices : 0) - chunk.voffset;

                // if you move indices you must recalculate its offset and use uint 
                // because uint.MaxValue is the maximum index implementable                 
                if (indices32bit)
                {
                    UInt32[] tmp_indices = (UInt32[])GPUindices.Read(j, chunk.nindis);
                    for (int m = 0; m < chunk.nindis; m++)
                    {
                        long I = tmp_indices[m] + sumvalue;
                        tmp_indices[m] = (UInt32)I;
                    }
                    // move item from "j" to "i" position
                    GPUindices.Write(tmp_indices, ii);
                    ii += tmp_indices.GetUpperBound(0);
                }
                else
                {
                    UInt16[] tmp_indices = (UInt16[])GPUindices.Read(j, chunk.nindis);
                    for (int m = 0; m < chunk.nindis; m++)
                    {
                        long I = tmp_indices[m] + sumvalue;
                        tmp_indices[m] = (UInt16)I;
                    }
                    // move item from "j" to "i" position
                    GPUindices.Write(tmp_indices, ii);
                    ii += tmp_indices.GetUpperBound(0);
                }
                chunk.ioffset = ilockoffset + nindices;


                j = chunk.voffset - vlockoffset;

                Array tmp_vertices = GPUvertices.Read(vv, chunk.nverts);
                GPUvertices.Write(tmp_vertices, j);
                j += tmp_vertices.GetUpperBound(0);

                chunk.voffset = vlockoffset + nvertices;

                nindices += chunk.nindis;
                nvertices += chunk.nverts;

                prop.m_ichunk = n;
            }

            IndexBuffer.Close();
            IndexBuffer.Count = nindices;

            m_VB.Close();
            m_VB.Count = nvertices;

            iminChunkIndex = vminChunkIndex = BufferChunks.Count;

            ready2remove.Clear();
        }
        /// <summary></summary>
        public override void WriteAll()
        {
            if (ready2write.Count == 0) return;

            BufferStream GPUvertices, GPUindices;
            int vlockoffset, ilockoffset;

            PrepareVertexBuffer(out GPUvertices, out vlockoffset);
            PrepareIndexBuffer(out GPUindices, out ilockoffset);

            int nvertices = m_VB.Count;
            int nindices = IndexBuffer.Count;
            //------------------------------
            // Append new data
            //------------------------------
            base.AppendNewNodes(ready2write, ref nvertices, vlockoffset, GPUvertices, ref nindices, ilockoffset, GPUindices);

            if (nRequiredVerts != nvertices || nRequiredIndis != nindices)
                throw new Exception("some DxNode change their data during Flush process... ");

            IndexBuffer.Count = nRequiredIndis;
            m_VB.Count = nRequiredVerts;
            
            m_VB.Close();
            IndexBuffer.Close();
            
            ready2write.Clear();
        }

        /// <summary></summary>
        protected void OverwriteIndices()
        {
            //----------------
            // Only Indices
            //----------------
            int imin = 0;
            int imax = ready2overwrite.Count - 1;
            while (imin < ready2overwrite.Count && !((NodeBatchProperties)ready2overwrite[imin].engineProp).needUpdateIndices) imin++;
            while (imax > imin && !((NodeBatchProperties)ready2overwrite[imax].engineProp).needUpdateIndices) imax--;

            if (imin < ready2overwrite.Count)
            {
                BatchChunkInfoIndexed first = (BatchChunkInfoIndexed)BufferChunks[((NodeBatchProperties)ready2overwrite[imin].engineProp).m_ichunk];
                BatchChunkInfoIndexed last = (BatchChunkInfoIndexed)BufferChunks[((NodeBatchProperties)ready2overwrite[imax].engineProp).m_ichunk];
                int ilockoffset = first.ioffset;
                int nindis = last.ioffset + last.nindis - ilockoffset;

                BufferStream GPUindices = IndexBuffer.Open(ilockoffset, nindis);

                for (int i = imin; i <= imax; i++)
                {
                    Node node = ready2overwrite[i];
                    NodeBatchProperties prop = (NodeBatchProperties)node.engineProp;
                    
                    // overwrite only necessary nodes
                    if (prop.needUpdateIndices)
                    {
                        BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[prop.m_ichunk];
                        Array meshIndices = node.GetIndicesStream(base.UseBatch ? chunk.voffset : 0, base.indices32bit);
                        GPUindices.Write(meshIndices, chunk.ioffset - ilockoffset);
                    }
                    prop.stackoverflowcounter_Update = 0;
                }
            }
            IndexBuffer.Close();
        }

        /// <summary></summary>
        public override void OverwriteAll()
        {
            if (ready2overwrite.Count == 0) return;

            // sort node using bufferchunk order
            ready2overwrite.Sort();
            OverwriteVertices();
            OverwriteIndices();

            ready2overwrite.Clear();
        }
        
        /// <summary></summary>
        public override bool DebugFlushAllProcess()
        {
            if (IndexBuffer != null)
                if (IndexBuffer.Count != nRequiredIndis)
                    throw new ArgumentOutOfRangeException("incoerent value : " + this.IndexBuffer.Count + " != " + nRequiredIndis);
            if (m_VB != null)
                if (m_VB.Count != nRequiredVerts)
                    throw new ArgumentOutOfRangeException("incoerent value : " + this.m_VB.Count + " != " + nRequiredVerts);
            return true;
        }

        /// <summary></summary>
        public override string ToString()
        {
            return base.ToString() + "\r\nIBUFFER : " + IndexBuffer.ToString() + "\r\n";
        }

        public Array CacheIndices()
        {
            BufferStream stream = IndexBuffer.Open(0, IndexBuffer.Count);
            IndexBuffer.Close();
            return stream.Read(0, IndexBuffer.Count);
        }

    }
}
