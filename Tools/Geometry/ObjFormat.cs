﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Graphics;
using System.Globalization;

namespace Engine.Geometry
{
    public class ObjFormat
    {
        [Flags]
        enum FaceFormat
        {
            None = 0,
            useVerts = 1,
            useTexCoord = 2,
            useNormals = 4,
        }

        public BoundaryAABB bounding;
        public List<Vector3> vertices = new List<Vector3>();
        public List<Vector2> texcoords = new List<Vector2>();
        public List<Vector3> normals = new List<Vector3>();
        public List<Face16> faces = new List<Face16>();

        string[] SpaceSeparator = new string[] { " " };
        string[] SlashSeparator = new string[] { "/" };


        float[] SplitFloat(string values,int count)
        {
            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            float[] array = new float[count];
            for (int i = 0; i < count; i++) array[i] = float.Parse(str[i],CultureInfo.InvariantCulture);
            return array;
        }
        int[] SplitInteger(string values, int count)
        {
            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            int[] array = new int[count];
            for (int i = 0; i < count; i++) array[i] = int.Parse(str[i]);
            return array;
        }
        int[] SplitIntegerSlash(string values, int count)
        {
            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            int[] array = new int[count];
            for (int i = 0; i < count; i++) array[i] = int.Parse(str[i]);
            return array;
        }

        void AddVertex(string values)
        {
            float[] f = SplitFloat(values, 3);
            vertices.Add(new Vector3(f[0], f[1], f[2]));
        }
        void AddTexCoord(string values)
        {
            float[] f = SplitFloat(values, 3);
            texcoords.Add(new Vector2(f[0], f[1]));
        }
        void AddNormal(string values)
        {
            float[] f = SplitFloat(values, 3);
            normals.Add(new Vector3(f[0], f[1], f[2]));
        }

        void AddFace(string values)
        {
            int[] idx = new int[] { 0, 0, 0 };

            string[] str = values.Split(SpaceSeparator, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < 3; i++)
            {
                string[] istr = str[i].Split(SlashSeparator, StringSplitOptions.RemoveEmptyEntries);
                switch (istr.Length)
                {
                    case 1: idx[i] = int.Parse(istr[0]) - 1; break;
                    case 2: idx[i] = int.Parse(istr[0]) - 1; break;
                    case 3: idx[i] = int.Parse(istr[0]) - 1; break;
                }
            }
            faces.Add(new Face16(idx[0], idx[1], idx[2]));
        }

        void SplitLine(string line, out string keyword, out string arguments)
        {
            int idx = line.IndexOf(' ');
            if (idx < 0)
            {
                keyword = line;
                arguments = null;
                return;
            }
            keyword = line.Substring(0, idx);
            arguments = line.Substring(idx + 1);
        }

        public ObjFormat(string filepath)
        {
            if (!File.Exists(filepath)) return;
            FileStream file = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read);
            StreamReader reader = new StreamReader(file);

            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (line == null) break;

                line = line.Trim();

                if (line.StartsWith("#") || line.Length == 0) continue;

                string keyword, values;
                SplitLine(line, out keyword, out values);

                switch (keyword.ToLower())
                {
                    // Vertex data
                    case "v": AddVertex(values); break;// geometric vertices
                    case "vt": AddTexCoord(values); break;// texture vertices
                    case "vn": AddNormal(values); break;// vertex normals
                    case "vp": // parameter space vertices
                    case "cstype": // rational or non-rational forms of curve or surface type: basis matrix, Bezier, B-spline, Cardinal, Taylor
                    case "degree": // degree
                    case "bmat": // basis matrix
                    case "step": // step size
                        break;

                    // Elements
                    case "f": this.AddFace(values); break;// face
                    case "p": // point
                    case "l": // line
                    case "curv": // curve
                    case "curv2": // 2D curve
                    case "surf": // surface
                        break;

                    // Free-form curve/surface body statements
                    case "parm": // parameter name
                    case "trim": // outer trimming loop (trim)
                    case "hole": // inner trimming loop (hole)
                    case "scrv": // special curve (scrv)
                    case "sp":  // special point (sp)
                    case "end": // end statement (end)
                        break;

                    // Connectivity between free-form surfaces
                    case "con": // connect
                        break;

                    // Grouping
                    case "g":// group name
                    case "s": // smoothing group
                    case "mg": // merging group
                    case "o": // object name
                        break;

                    // Display/render attributes
                    case "mtllib": // material library
                    case "usemtl": // material name
                    case "usemap": // texture map name
                    case "bevel": // bevel interpolation
                    case "c_interp": // color interpolation
                    case "d_interp": // dissolve interpolation
                    case "lod": // level of detail
                    case "shadow_obj": // shadow casting
                    case "trace_obj": // ray tracing
                    case "ctech": // curve approximation technique
                    case "stech": // surface approximation technique
                        break;
                }
            }

            bounding = BoundaryAABB.FromData(vertices);
        }
    }
}
