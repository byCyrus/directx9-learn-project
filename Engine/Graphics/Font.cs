﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using Engine.Tools;

namespace Engine.Graphics
{
    /// <summary>
    /// My managed directx Font objects , in this case graphicdevice can dispose and restore automatically
    /// the resource because i only store fontstyle and fontsize
    /// </summary>
    public class Font : DisposableResource, IResettable
    {
        public enum FontName
        {
            Calibri,
            Arial
        }
        GraphicDevice device;
        DxFont m_font;
        FontName fontstyle;
        int fontsize;

        public Font(GraphicDevice device, FontName fontstyle, int fontsize)
        {
            this.device = device;
            this.fontstyle = fontstyle;
            this.fontsize = fontsize;

            Restore();

            device.resourcesToDispose.Add(this);
            device.resourcesToRestore.Add(this);
        }

        /// <summary>
        /// the dispose are necessary else visual studio generate a NULLEXCEPTION event when close form
        /// </summary>
        ~Font()
        {
            device.resourcesToDispose.Remove(this);
            device.resourcesToRestore.Remove(this);
        }
        /// <summary>
        /// Set to device the texture, the method check if current class are initialized.
        /// If texture null or Empty the static int prevHandle will be set to 0;
        /// </summary>
        /// <param name="device"></param>
        public void Draw(string text , int x , int y , Color color)
        {
            m_font.DrawString(text, x, y, color);
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed) m_font.Dispose();
            disposed = true;
        }

        public void Restore()
        {
            Dispose();
            m_font = new DxFont(device.m_device, fontstyle.ToString(), fontsize);
            disposed = false;
        }
    }
}