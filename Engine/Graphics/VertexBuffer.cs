﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Engine.Graphics
{
    /// <summary>
    /// If buffer is not managed, you can assign the rewrite delegate function to automatize.
    /// If GraphicDevice is null, the buffer work in emulator mode, the data are stored in a shadow array
    /// </summary>
    public class VertexBuffer : BaseBuffer
    {
        VertexStream m_datastream = null;
        VertexInfo m_declaration;  
        DX9VertexBuffer m_buffer;
        int m_stream = 0;

        /// <summary>
        /// Emulator mode, generate a Array that simulate the directx buffer
        /// </summary>
        public VertexBuffer(Type vertextype, VertexInfo vertexDeclaration, int size, bool readable)
            : base(null, true, readable, true, true)
        {
            /* EXAMPLE
            VertexBuffer shadow = new VertexBuffer(tcvertex_decl, mesh.numVertices, true);
            shadow.Open();
            shadow.Write<Vector3>(mesh.vertices.data,NTVERTEX.m_elements[0], mesh.numVertices, 0);
            shadow.Write<Vector3>(mesh.normals.data,NTVERTEX.m_elements[1] ,mesh.numVertices, 0);
            shadow.Write<Vector2>(mesh.textures.data,NTVERTEX.m_elements[2], mesh.numVertices, 0);
            shadow.Close();
            shadow.Count = mesh.numVertices;
            */
            m_declaration = vertexDeclaration;
            m_shadowtype = vertextype;
            m_typeSize = vertexDeclaration.bytesize;

            if (!m_shadowtype.IsValueType || m_shadowtype.IsEnum)
                throw new ArgumentException("type must be a struct with fized size");

            if (m_declaration.bytesize != Marshal.SizeOf(m_shadowtype))
                throw new ArgumentException("size of type " + m_shadowtype.ToString() + " don't match with declaration");

            InitBuffer(size);
        }

        /// <summary>
        /// If buffer is not managed, you can assign the rewrite delegate function to automatize
        /// </summary>
        /// <param name="declaration">vertex type information</param>
        /// <param name="size">capacity of vertices</param>
        /// <param name="managed">if not managed need to dispose before device reseting and all data must be rewritten</param>
        /// <param name="dynamic">improve for many updating</param>
        /// <param name="readable">use buffer as readable, not very usefull</param>
        public VertexBuffer(GraphicDevice device, VertexInfo vertexDeclaration, int size, bool dynamic, bool managed, bool readable = false)
            : base(device, dynamic, readable, managed)
        {
            m_declaration = vertexDeclaration;
            m_typeSize = m_declaration.bytesize;
            InitBuffer(size);
        }

        /// <summary>
        /// Get or Set the stream used by this buffer
        /// </summary>
        public int StreamID
        {
            get { return m_stream; }
            set { m_stream = value; }
        }

        /// <summary>
        /// </summary>
        public override void SetToDevice()
        {
            if (disposed) throw new Exception("buffer not restored");
            if (m_buffer != null) m_buffer.SetStreamSource(m_stream, m_typeSize);
        }
        /// <summary>
        /// Used for Hardware instancing, require shared 3.0 or greater, use StreamSource.Reset to reseting frequency
        /// </summary>
        public void SetFrequency(int frequency, StreamSource source)
        {
            if (m_device != null) m_device.m_device.SetStreamSourceFrequency(m_stream, frequency, source);
        }
        public VertexInfo Format 
        {
            get { return m_declaration; }
        }

        /// <summary>
        /// Open stream
        /// </summary>
        public VertexStream OpenStream(int Offset, int Count , bool ReadOnly)
        {
            if (Offset > 0 && m_shadowEnabled) throw new NotSupportedException("offset not implemented in emulator mode");
            if (m_datastream != null) throw new Exception("vertex buffer already open, please use the same data stream instance");

            IntPtr ptr = base.Open(Offset, Count, ReadOnly);         
            m_datastream = new VertexStream(ptr, m_declaration, Count, base.IsReadable);          
            return m_datastream;
        }

        /// <summary>
        /// Open in writeonly mode
        /// </summary>
        public VertexStream OpenStream(int Offset, int Count)
        {
            return OpenStream(Offset, Count, false);
        }
        /// <summary>
        /// Open whole in writeonly mode
        /// </summary>
        public VertexStream OpenStream()
        {
            return OpenStream(0, m_size / m_typeSize, false);
        }

        /// <summary>
        /// Recreate a empty vertex buffer but with different size, Count will be set to zero
        /// </summary>
        /// <param name="count">capacity of buffer in currect elements type size, not in bytes</param>
        protected override void InitBuffer(int count)
        {
            if (count < 1) throw new Exception("Directx9 can't initialize a buffer with zero bytes");
            if (!disposed) Dispose(true);
            m_size = count * m_typeSize;
            m_count = 0;
            m_incrementoffset = 0;

            if (m_shadowEnabled)
            {
                m_shadowbuffer = Array.CreateInstance(m_shadowtype, count);
            }
            else
            {
                m_buffer = new DX9VertexBuffer(m_device.m_device, VertexFormat.None, m_size, m_usage, m_memory);
            }
            disposed = false;
        }

        protected override IntPtr Lock(int boffset, int bsize, LockFlags mode)
        {
            if (disposed) throw new Exception("buffer not restored");
            m_incrementoffset = 0;
            m_flushbytes = 0;

            if (m_buffer != null)
            {
                return m_buffer.Lock(boffset, bsize, mode);
            }
            else if (m_shadowbuffer != null)
            {
                // emulator mode
                m_shadowhandle = GCHandle.Alloc(m_shadowbuffer, GCHandleType.Pinned);
                return m_shadowhandle.AddrOfPinnedObject();
            }
            else
            {
                return IntPtr.Zero;
            }
        }

        protected override void UnLock()
        {
            if (disposed) throw new Exception("buffer not restored");
            m_incrementoffset = 0;

            m_datastream.Close();
            m_datastream = null;
            
            if (m_buffer != null)
            {
                m_buffer.UnLock();
            }
            if (m_shadowbuffer != null && m_shadowhandle.IsAllocated)
            {
                m_shadowhandle.Free();
            }
        }

        protected override void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (m_buffer != null)
                    {
                        m_buffer.Dispose();
                        m_buffer = null;
                    }
                    else if (m_shadowbuffer != null)
                    {
                        m_shadowbuffer = null;
                    }
                    m_count = 0;
                    m_incrementoffset = 0;
                }
                disposed = true;
            }
        }
    }
}
