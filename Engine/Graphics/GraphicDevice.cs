﻿// by johnwhile
using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Renderer;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Graphics
{
    public class GraphicDevice : DeviceResource
    {
        internal DX9Device m_device;

        Control m_control;
        /// <summary>
        /// Check if device can be used to draw
        /// </summary>
        DeviceState current_state = DeviceState.InvalidDevice;
        /// <summary>
        /// What device can and can't do
        /// </summary>
        public static Capabilities capabilities = new Capabilities();
        /// <summary>
        /// Render states of device, need to minimize the changes
        /// </summary>
        public DeviceStates renderstates { get; private set; }
        /// <summary>
        /// Lights list, this class auto-check the ligth limit
        /// </summary>
        public List<Light> lights = new List<Light>();

        /// <summary>
        /// Resource list what must be rebuilded after device reseting, example resources not managed, if not you can see it on screen
        /// </summary>
        internal List<IResettable> resourcesToRestore = new List<IResettable>();
        /// <summary>
        /// Resource list what must be disposed before device reseting, example resources not managed, if not the device crash
        /// </summary>
        internal List<IDisposable> resourcesToDispose = new List<IDisposable>();
        /// <summary>
        /// Resource list that require device but don't be affected by device reseting, before close device it is a good idea dispose all possible resources
        /// </summary>
        internal List<IDisposable> resourcesToClose = new List<IDisposable>();

        /// <summary>
        /// The size and position of render target
        /// </summary>
        public Viewport viewport
        {
            get { return m_device.viewport; }
            set { m_device.viewport = value; }
        }
        /// <summary>
        /// The GPU class, they use a deph stencil buffer and try to use a puredevice flag.
        /// </summary>
        public GraphicDevice(Control control)
        {
            Trace.WriteLine("Initialize Directx9 Graphic Class...");

            m_control = control;
            m_device = new DX9Device(control, true, true, true);

            renderstates = new DeviceStates(m_device.renderstates);

            renderstates.world = Matrix4.Identity;
            renderstates.projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45.0f), (float)m_device.BackBufferWidth / (float)m_device.BackBufferHeight, 0.1f, 1000.0f);
            renderstates.view = Matrix4.MakeViewLH(new Vector3(10, 10, 10), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

            m_device.SetRendersStates();

            Trace.Indent();
            Trace.WriteLine("device info      : " + m_device.ToString());
            Trace.Unindent();
            Trace.Flush();
        }

        public void DrawIndexedPrimitives(PrimitiveType primitive, int baseVertex, int minVertex, int numVertices, int startIndex, int numPrimitives)
        {
            Debug.Assert(current_state == DeviceState.OK,"you need checking device.Begin() after all device operations");
            m_device.DrawIndexedPrimitives(primitive, baseVertex, minVertex, numVertices, startIndex, numPrimitives);
        }
        public void DrawPrimitives(PrimitiveType primitive, int startIndex, int numPrimitives)
        {
            Debug.Assert(current_state == DeviceState.OK, "you need checking device.Begin() after all device operations");
            m_device.DrawPrimitives(primitive, startIndex, numPrimitives);
        }
        public void DrawUserPrimitives<T>(PrimitiveType type, int numPrimitives, T[] array) where T : struct
        {
            Debug.Assert(current_state == DeviceState.OK, "you need checking device.Begin() after all device operations");
            m_device.DrawUserPrimitives(type, numPrimitives, array);
        }
        
        public void Clear(Color background)
        {
            m_device.Clear(background);
        }

        public void RemoveTexture()
        {
            m_device.Texture = null;
            if (TextureBase.prevtexture != null) TextureBase.prevtexture = null;
        }
        /// <summary>
        /// If you use SwapChain render target, the device use them, to draw with original 
        /// buffers need to re-set them
        /// </summary>
        public void SetRenderTarghet()
        {
            m_device.SetRenderTarghet();
        }   
        /// <summary>
        /// Inform device that it's time to draw, return false if can't. 
        /// </summary>
        public bool BeginDraw()
        {
            current_state = m_device.Begin();
            
            switch(current_state)
            {
                case DeviceState.OK:
                    //Console.WriteLine("DeviceState.OK");
                    // light must be set evary frames
                    for (int i = 0; i < lights.Count; i++)
                        m_device.SetLight(lights[i], i);

                    return true;

                case DeviceState.DeviceLost:
                    Console.WriteLine("DeviceState.DeviceLost");
                    //Can't Reset yet, wait for a bit
                    System.Threading.Thread.Sleep(500);
                    return false;

                case DeviceState.DeviceNotReset:
                    Console.WriteLine("DeviceState.DeviceNotReset");
                    System.Threading.Thread.Sleep(100);
                    this.Reset();
                    return false;

                case DeviceState.InvalidCall:
                    throw new Exception("Device crash");

                default: return false;
            }
        }
        /// <summary>
        /// Inform device to finisch draw
        /// </summary>
        public void EndDraw()
        {
            m_device.End();
        }
        /// <summary>
        /// Draw to control the result
        /// </summary>
        public void PresentDraw()
        {
            m_device.Present();
        }
        /// <summary>
        /// When windows handles linked to device are resized, if you not resize the image
        /// will have a wrong aspect ratio
        /// </summary>
        public void Resize(Size backbuffersize)
        {
            DisplayMode screen = capabilities.Current;

            int width = backbuffersize.Width;
            int height = backbuffersize.Height;

            // why resize if is the same ?
            if (m_device.BackBufferHeight == height && m_device.BackBufferWidth == width) return;

            if (height > screen.height) height = screen.height;
            if (width > screen.width) width = screen.width;

            m_device.BackBufferHeight = height > 1 ? height : 1;
            m_device.BackBufferWidth = width > 1 ? width : 1;

            Console.WriteLine(m_device.BackBufferWidth + " x " + m_device.BackBufferHeight);

            this.Reset();

            m_device.viewport = new Viewport
            {
                Height = height,
                Width = width,
                MaxDepth = 1,
                MinDepth = 0,
                X = 0,
                Y = 0
            };

        }
        /// <summary>
        /// Reset device, dispose and recreate all managed resources
        /// </summary>
        public void Reset()
        {
            RemoveTexture();

            foreach (IDisposable res in resourcesToDispose)
                res.Dispose();

            m_device.Reset();

            foreach (IResettable res in resourcesToRestore)
                res.Restore();

            // tell to texture class that device texture are lost
            NodeMaterial.RemoveFromDevice(this);
        }

        protected override void Dispose(bool disposing)
        {
            foreach (IDisposable res in resourcesToDispose) res.Dispose();
            foreach (IDisposable res in resourcesToClose) res.Dispose();
            m_device.Dispose();
        }

        public override string ToString()
        {
            return DX9Device.graphicVersion.ToString();
        }

        public struct Capabilities
        {
            public int MaxActiveTexture { get { return DX9Enumerations.MaxActiveTexture; } }
            public int MaxVertexIndex { get { return DX9Enumerations.MaxVertexIndex; } }
            public int MaxPrimitiveCount { get { return DX9Enumerations.MaxPrimitiveCount; } }
            public bool SupportTextureSize(Size size) { return DX9Enumerations.SupportTextureSize(size); }
            public int MaxLights { get { return DX9Enumerations.MaxLights; } }

            public DisplayMode Current { get { return DX9Enumerations.CurrentDisplayMode; } }
            
            public void Check(Format format)
            {
                List<DisplayMode> modes = DX9Enumerations.GetViewsMode(format);
            }
        }

        public void SetVertexShaderNULL()
        {
            m_device.SetVertexShaderNULL();
        }
    }
    /// <summary>
    /// Manage the know renderstates , transformstates, etc...
    /// </summary>
    public class DeviceStates
    {
        DX9RenderStatesManager state;
        internal DeviceStates(DX9RenderStatesManager renderstates)
        {
            state = renderstates;
        }
        public Material material { get { return state.material; } set { state.material = value; } }
        public Matrix4 world{ get { return state.world; } set { state.world = value; } }
        public Matrix4 projection{ get { return state.projection; } set { state.projection = value; } }
        public Matrix4 view{ get { return state.view; } set { state.view = value; } }
        public bool AlphaBlendEnable{ get { return state.AlphaBlendEnable; } set { state.AlphaBlendEnable = value; } }
        public Compare AlphaFunction{ get { return state.AlphaFunction; } set { state.AlphaFunction = value; } }
        public bool AlphaTestEnable{ get { return state.AlphaTestEnable; } set { state.AlphaTestEnable = value; } }
        public Cull cullMode{ get { return state.cullMode; } set { state.cullMode = value; } }
        public float depth{ get { return state.depth; } set { state.depth = value; } }
        public FillMode fillMode{ get { return state.fillMode; } set { state.fillMode = value; } }
        public bool lightEnable{ get { return state.lightEnable; } set { state.lightEnable = value; } }
        public bool NormalizeNormals{ get { return state.NormalizeNormals; } set { state.NormalizeNormals = value; } }
        public bool ZBufferEnable{ get { return state.ZBufferEnable; } set { state.ZBufferEnable = value; } }
        public Compare ZBufferFunction{ get { return state.ZBufferFunction; } set { state.ZBufferFunction = value; } }
        public bool ZBufferWriteEnable{ get { return state.ZBufferWriteEnable; } set { state.ZBufferWriteEnable = value; } }
    }

}