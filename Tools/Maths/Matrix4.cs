﻿/*
Axiom Graphics Engine Library
Copyright � 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

using System;
using System.Text;
using System.Runtime.InteropServices;

using Engine.Tools;
using Engine.Graphics;

namespace Engine.Maths
{
    /// <summary>
    /// Directx Matrix4x4 is a trasposed matrix and don't match with the mathematical notation,
    /// but in this case i'll use always che classic math notation
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Matrix4
    {
        //     1  0  0  Tx
        //     0  1  0  Ty
        //     0  0  1  Tz
        //     0  0  0  1

        public float m00, m01, m02, m03;
        public float m10, m11, m12, m13;
        public float m20, m21, m22, m23;
        public float m30, m31, m32, m33;

        public static readonly Matrix4 Zero = new Matrix4(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        public static readonly Matrix4 Identity = new Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

        public Matrix4(
            float m00, float m01, float m02, float m03,
            float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23,
            float m30, float m31, float m32, float m33)
        {
            this.m00 = m00;
            this.m01 = m01;
            this.m02 = m02;
            this.m03 = m03;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;
            this.m13 = m13;
            this.m20 = m20;
            this.m21 = m21;
            this.m22 = m22;
            this.m23 = m23;
            this.m30 = m30;
            this.m31 = m31;
            this.m32 = m32;
            this.m33 = m33;
        }

        static void swap(ref float a, ref float b)
        {
            float tmp = a;
            a = b;
            b = tmp;
        }

        /// <summary>
        /// Get the i-th Row
        /// </summary>
        public Vector4 getRow(int i)
        {
            switch (i)
            {
                case 0: return new Vector4(m00, m01, m02, m03);
                case 1: return new Vector4(m10, m11, m12, m13);
                case 2: return new Vector4(m20, m21, m22, m23);
                case 3: return new Vector4(m30, m31, m32, m33);
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }
        /// <summary>
        /// Set the i-th Row
        /// </summary>
        public void setRow(int i, Vector4 val)
        {
            switch (i)
            {
                case 0: m00 = val.x; m01 = val.y; m02 = val.z; m03 = val.w; break;
                case 1: m10 = val.x; m11 = val.y; m12 = val.z; m13 = val.w; break;
                case 2: m20 = val.x; m21 = val.y; m22 = val.z; m23 = val.w; break;
                case 3: m30 = val.x; m31 = val.y; m32 = val.z; m33 = val.w; break;
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }
        /// <summary>
        /// Get the i-th Column
        /// </summary>
        public Vector4 getCol(int i)
        {
            switch (i)
            {
                case 0: return new Vector4(m00, m10, m20, m30);
                case 1: return new Vector4(m01, m11, m21, m31);
                case 2: return new Vector4(m02, m12, m22, m32);
                case 3: return new Vector4(m03, m13, m23, m33);
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }
        /// <summary>
        /// Set the i-th Column
        /// </summary>
        public void setCol(int i, Vector4 val)
        {
            switch (i)
            {
                case 0: m00 = val.x; m10 = val.y; m20 = val.z; m30 = val.w; break;
                case 1: m01 = val.x; m11 = val.y; m21 = val.z; m31 = val.w; break;
                case 2: m02 = val.x; m12 = val.y; m22 = val.z; m32 = val.w; break;
                case 3: m03 = val.x; m13 = val.y; m23 = val.z; m33 = val.w; break;
                default: throw new IndexOutOfRangeException("index not in the range 0-3");
            }
        }
        /// <summary>
        ///    Allows the Matrix to be accessed like a 2d array (i.e. matrix[2,3])
        /// </summary>
        /// <remarks>
        ///    This indexer is only provided as a convenience, and is <b>not</b> recommended for use in
        ///    intensive applications.  
        /// </remarks>
        public float this[int row, int col]
        {
            get
            {
#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: return m00;
                            case 1: return m01;
                            case 2: return m02;
                            case 3: return m03;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: return m10;
                            case 1: return m11;
                            case 2: return m12;
                            case 3: return m13;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: return m20;
                            case 1: return m21;
                            case 2: return m22;
                            case 3: return m23;
                        }
                        break;
                    case 3:
                        switch (col)
                        {
                            case 0: return m30;
                            case 1: return m31;
                            case 2: return m32;
                            case 3: return m33;
                        }
                        break;
                }
                throw new IndexOutOfRangeException("Attempt to access Matrix4 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        return *(pM + ((4 * row) + col));
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert((row >= 0 && row < 4) && (col >= 0 && col < 4), "Attempt to access Matrix4 indexer out of bounds.");

#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: m00 = value; break;
                            case 1: m01 = value; break;
                            case 2: m02 = value; break;
                            case 3: m03 = value; break;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: m10 = value; break;
                            case 1: m11 = value; break;
                            case 2: m12 = value; break;
                            case 3: m13 = value; break;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: m20 = value; break;
                            case 1: m21 = value; break;
                            case 2: m22 = value; break;
                            case 3: m23 = value; break;
                        }
                        break;
                    case 3:
                        switch (col)
                        {
                            case 0: m30 = value; break;
                            case 1: m31 = value; break;
                            case 2: m32 = value; break;
                            case 3: m33 = value; break;
                        }
                        break;
                }
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        *(pM + ((4 * row) + col)) = value;
                    }
                }
#endif
            }
        }

        /// <summary>
        ///		Allows the Matrix to be accessed linearly (m[0] -> m[15]).  
        /// </summary>
        /// <remarks>
        ///    This indexer is only provided as a convenience, and is <b>not</b> recommended for use in
        ///    intensive applications.  
        /// </remarks>
        public float this[int index]
        {
            get
            {
                //Debug.Assert(index >= 0 && index < 16, "Attempt to access Matrix4 linear indexer out of bounds.");

#if !UNSAFE
                switch (index)
                {
                    case 0: return m00;
                    case 1: return m01;
                    case 2: return m02;
                    case 3: return m03;
                    case 4: return m10;
                    case 5: return m11;
                    case 6: return m12;
                    case 7: return m13;
                    case 8: return m20;
                    case 9: return m21;
                    case 10: return m22;
                    case 11: return m23;
                    case 12: return m30;
                    case 13: return m31;
                    case 14: return m32;
                    case 15: return m33;
                }
                throw new IndexOutOfRangeException("Attempt to access Matrix4 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        return *(pMatrix + index);
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert(index >= 0 && index < 16, "Attempt to access Matrix4 linear indexer out of bounds.");

#if !UNSAFE
                switch (index)
                {
                    case 0: m00 = value; break;
                    case 1: m01 = value; break;
                    case 2: m02 = value; break;
                    case 3: m03 = value; break;
                    case 4: m10 = value; break;
                    case 5: m11 = value; break;
                    case 6: m12 = value; break;
                    case 7: m13 = value; break;
                    case 8: m20 = value; break;
                    case 9: m21 = value; break;
                    case 10: m22 = value; break;
                    case 11: m23 = value; break;
                    case 12: m30 = value; break;
                    case 13: m31 = value; break;
                    case 14: m32 = value; break;
                    case 15: m33 = value; break;
                }
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        *(pMatrix + index) = value;
                    }
                }
#endif
            }
        }

        public static readonly int sizeinbyte = sizeof(float) * 16;

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < 4; i++)
                str.Append(String.Format("{0,4} {1,4} {2,4} {3,4}\n", this[i, 0], this[i, 1], this[i, 2], this[i, 3]));
            return str.ToString();
        }


        /// <summary>
        ///	 Get or Set the translation value of the matrix using math notation
        ///		| 0 0 0 Tx |
        ///		| 0 0 0 Ty |
        ///		| 0 0 0 Tz |
        ///		| 0 0 0  1 |
        /// </summary>
        public Vector3 TranslationComponent
        {
            get { return new Vector3(this.m03, this.m13, this.m23); }
            set { this.m03 = value.x; this.m13 = value.y; this.m23 = value.z; }
        }
        /// <summary>
        /// do a movement
        /// </summary>
        /// <param name="vector"></param>
        public void Translate(Vector3 vector)
        {
            this.m03 += vector.x;
            this.m13 += vector.y;
            this.m23 += vector.z;
        }

        /// <summary>
        ///  Get or Set the scale value of the matrix. Not match when matrix is rotated
        ///		|Sx 0  0  0 |
        ///		| 0 Sy 0  0 |
        ///		| 0  0 Sz 0 |
        ///		| 0  0  0 1 |
        /// </summary>
        public Vector3 DiagonalComponent
        {
            get
            {
                return new Vector3(this.m00, this.m11, this.m22);
            }
            set
            {
                this.m00 = value.x;
                this.m11 = value.y;
                this.m22 = value.z;
            }
        }
        /// <summary>
        /// Get the determinant of matrix.
        /// </summary>
        public float Determinant
        {
            get
            {
                float result = this.m00 *
                             (this.m11 * (this.m22 * this.m33 - this.m32 * this.m23) -
                               this.m12 * (this.m21 * this.m33 - this.m31 * this.m23) +
                               this.m13 * (this.m21 * this.m32 - this.m31 * this.m22)) -
                             this.m01 *
                             (this.m10 * (this.m22 * this.m33 - this.m32 * this.m23) -
                               this.m12 * (this.m20 * this.m33 - this.m30 * this.m23) +
                               this.m13 * (this.m20 * this.m32 - this.m30 * this.m22)) +
                             this.m02 *
                             (this.m10 * (this.m21 * this.m33 - this.m31 * this.m23) -
                               this.m11 * (this.m20 * this.m33 - this.m30 * this.m23) +
                               this.m13 * (this.m20 * this.m31 - this.m30 * this.m21)) -
                             this.m03 *
                             (this.m10 * (this.m21 * this.m32 - this.m31 * this.m22) -
                               this.m11 * (this.m20 * this.m32 - this.m30 * this.m22) +
                               this.m12 * (this.m20 * this.m31 - this.m30 * this.m21));

                return result;
            }
        }

        /// <summary>
        /// Extract the 3x3 matrix representing the current rotation. 
        /// </summary>
        public Matrix3 ExtractRotation()
        {
            Vector3 axis = new Vector3();
            Matrix3 rotation = Matrix3.Identity;

            axis.x = this.m00;
            axis.y = this.m10;
            axis.z = this.m20;
            axis.Normalize();
            rotation.m00 = axis.x;
            rotation.m10 = axis.y;
            rotation.m20 = axis.z;

            axis.x = this.m01;
            axis.y = this.m11;
            axis.z = this.m21;
            axis.Normalize();
            rotation.m01 = axis.x;
            rotation.m11 = axis.y;
            rotation.m21 = axis.z;

            axis.x = this.m02;
            axis.y = this.m12;
            axis.z = this.m22;
            axis.Normalize();
            rotation.m02 = axis.x;
            rotation.m12 = axis.y;
            rotation.m22 = axis.z;

            return rotation;
        }
        /// <summary>
        /// Extract scaling information.
        /// </summary>
        public Vector3 ExtractScale()
        {
            var scale = new Vector3(1, 1, 1);
            var axis = new Vector3(0, 0, 0);

            axis.x = this.m00;
            axis.y = this.m10;
            axis.z = this.m20;
            scale.x = axis.Length;

            axis.x = this.m01;
            axis.y = this.m11;
            axis.z = this.m21;
            scale.y = axis.Length;

            axis.x = this.m02;
            axis.y = this.m12;
            axis.z = this.m22;
            scale.z = axis.Length;

            return scale;
        }

        /// <summary>
        /// Decompose the matrix.
        /// </summary>
        public void Decompose(out Vector3 translation, out Vector3 scale, out Quaternion orientation)
        {
            scale = new Vector3(1, 1, 1);
            var rotation = Matrix3.Identity;
            var axis = Vector3.Zero;

            axis.x = this.m00;
            axis.y = this.m10;
            axis.z = this.m20;
            scale.x = axis.Normalize(); // Normalize() returns the vector's length before it was normalized
            rotation.m00 = axis.x;
            rotation.m10 = axis.y;
            rotation.m20 = axis.z;

            axis.x = this.m01;
            axis.y = this.m11;
            axis.z = this.m21;
            scale.y = axis.Normalize();
            rotation.m01 = axis.x;
            rotation.m11 = axis.y;
            rotation.m21 = axis.z;

            axis.x = this.m02;
            axis.y = this.m12;
            axis.z = this.m22;
            scale.z = axis.Normalize();
            rotation.m02 = axis.x;
            rotation.m12 = axis.y;
            rotation.m22 = axis.z;

            /* http://www.robertblum.com/articles/2005/02/14/decomposing-matrices check to support transforms with negative scaling */
            //thanks sebj for the info
            if (rotation.Determinant < 0)
            {
                rotation.m00 = -rotation.m00;
                rotation.m10 = -rotation.m10;
                rotation.m20 = -rotation.m20;
                scale.x = -scale.x;
            }

            orientation = Quaternion.FromRotationMatrix(rotation);
            translation = this.TranslationComponent;
        }


        /// <summary>
        /// NOT TESTED
        /// </summary>
        public static Matrix4 Orthogonalize(Matrix4 matrix)
        {
            Vector3 axisX = new Vector3();
            Vector3 axisY = new Vector3();
            Vector3 axisZ = new Vector3();
            Vector3 scale = new Vector3(1, 1, 1);

            Matrix4 result = Matrix4.Identity;

            axisX.x = matrix.m00;
            axisX.y = matrix.m10;
            axisX.z = matrix.m20;
            scale.x = axisX.Length;


            axisY.x = matrix.m01;
            axisY.y = matrix.m11;
            axisY.z = matrix.m21;
            scale.y = axisY.Length;

            axisZ.x = matrix.m02;
            axisZ.y = matrix.m12;
            axisZ.z = matrix.m22;
            scale.z = axisZ.Length;

            axisY = Vector3.Cross(axisX, axisZ);
            axisZ = Vector3.Cross(axisX, axisY);

            axisX.Normalize();
            axisY.Normalize();
            axisZ.Normalize();

            result.m00 = axisX.x;
            result.m10 = axisX.y;
            result.m20 = axisX.z;
            result.m01 = axisY.x;
            result.m11 = axisY.y;
            result.m21 = axisY.z;
            result.m02 = axisZ.x;
            result.m12 = axisZ.y;
            result.m22 = axisZ.z;

            result.TranslationComponent = matrix.TranslationComponent;

            result *= Matrix4.Scaling(scale.x, scale.y, scale.z);

            return result;
        }

        /// <summary>
        ///  Returns an inverted matrix.
        /// </summary>
        public Matrix4 Inverse()
        {
            float d = Determinant;
            if (d > -float.Epsilon && d < float.Epsilon)
                return this;
            else
                return Adjoint() * (1f / d);
        }
        public static Matrix4 Inverse(Matrix4 matrix)
        {
            return matrix.Inverse();
        }

        /// <summary>
        ///  Used to generate the adjoint of this matrix.
        /// </summary>
        Matrix4 Adjoint()
        {
            // note: this is an expanded version of the Ogre adjoint() method, to give better performance in C#. Generated using a script
            float val0 = this.m11 * (this.m22 * this.m33 - this.m32 * this.m23) -
                       this.m12 * (this.m21 * this.m33 - this.m31 * this.m23) +
                       this.m13 * (this.m21 * this.m32 - this.m31 * this.m22);

            float val1 = -(this.m01 * (this.m22 * this.m33 - this.m32 * this.m23) -
                         this.m02 * (this.m21 * this.m33 - this.m31 * this.m23) +
                         this.m03 * (this.m21 * this.m32 - this.m31 * this.m22));

            float val2 = this.m01 * (this.m12 * this.m33 - this.m32 * this.m13) -
                         this.m02 * (this.m11 * this.m33 - this.m31 * this.m13) +
                         this.m03 * (this.m11 * this.m32 - this.m31 * this.m12);

            float val3 = -(this.m01 * (this.m12 * this.m23 - this.m22 * this.m13) -
                          this.m02 * (this.m11 * this.m23 - this.m21 * this.m13) +
                          this.m03 * (this.m11 * this.m22 - this.m21 * this.m12));

            float val4 = -(this.m10 * (this.m22 * this.m33 - this.m32 * this.m23) -
                          this.m12 * (this.m20 * this.m33 - this.m30 * this.m23) +
                          this.m13 * (this.m20 * this.m32 - this.m30 * this.m22));

            float val5 = this.m00 * (this.m22 * this.m33 - this.m32 * this.m23) -
                         this.m02 * (this.m20 * this.m33 - this.m30 * this.m23) +
                         this.m03 * (this.m20 * this.m32 - this.m30 * this.m22);

            float val6 = -(this.m00 * (this.m12 * this.m33 - this.m32 * this.m13) -
                           this.m02 * (this.m10 * this.m33 - this.m30 * this.m13) +
                           this.m03 * (this.m10 * this.m32 - this.m30 * this.m12));

            float val7 = this.m00 * (this.m12 * this.m23 - this.m22 * this.m13) -
                         this.m02 * (this.m10 * this.m23 - this.m20 * this.m13) +
                         this.m03 * (this.m10 * this.m22 - this.m20 * this.m12);

            float val8 = this.m10 * (this.m21 * this.m33 - this.m31 * this.m23) -
                         this.m11 * (this.m20 * this.m33 - this.m30 * this.m23) +
                         this.m13 * (this.m20 * this.m31 - this.m30 * this.m21);

            float val9 = -(this.m00 * (this.m21 * this.m33 - this.m31 * this.m23) -
                           this.m01 * (this.m20 * this.m33 - this.m30 * this.m23) +
                           this.m03 * (this.m20 * this.m31 - this.m30 * this.m21));

            float val10 = this.m00 * (this.m11 * this.m33 - this.m31 * this.m13) -
                          this.m01 * (this.m10 * this.m33 - this.m30 * this.m13) +
                         this.m03 * (this.m10 * this.m31 - this.m30 * this.m11);

            float val11 = -(this.m00 * (this.m11 * this.m23 - this.m21 * this.m13) -
                            this.m01 * (this.m10 * this.m23 - this.m20 * this.m13) +
                            this.m03 * (this.m10 * this.m21 - this.m20 * this.m11));

            float val12 = -(this.m10 * (this.m21 * this.m32 - this.m31 * this.m22) -
                            this.m11 * (this.m20 * this.m32 - this.m30 * this.m22) +
                            this.m12 * (this.m20 * this.m31 - this.m30 * this.m21));

            float val13 = this.m00 * (this.m21 * this.m32 - this.m31 * this.m22) -
                          this.m01 * (this.m20 * this.m32 - this.m30 * this.m22) +
                          this.m02 * (this.m20 * this.m31 - this.m30 * this.m21);

            float val14 = -(this.m00 * (this.m11 * this.m32 - this.m31 * this.m12) -
                            this.m01 * (this.m10 * this.m32 - this.m30 * this.m12) +
                            this.m02 * (this.m10 * this.m31 - this.m30 * this.m11));

            float val15 = this.m00 * (this.m11 * this.m22 - this.m21 * this.m12) -
                          this.m01 * (this.m10 * this.m22 - this.m20 * this.m12) +
                          this.m02 * (this.m10 * this.m21 - this.m20 * this.m11);

            return new Matrix4(val0, val1, val2, val3, val4, val5, val6, val7, val8, val9, val10, val11, val12, val13, val14, val15);
        }


        #region Operators
        public static implicit operator Matrix3(Matrix4 matrix)
        {
            Matrix3 m = new Matrix3();
            m.m00 = matrix.m00;
            m.m01 = matrix.m01;
            m.m02 = matrix.m02;

            m.m10 = matrix.m10;
            m.m11 = matrix.m11;
            m.m12 = matrix.m12;

            m.m20 = matrix.m20;
            m.m21 = matrix.m21;
            m.m22 = matrix.m22;

            return m;
        }

        /// <summary>
        ///          __R
        ///          \
        /// M[r,c] = /_i  A[r,i] * B[i,c]
        ///          
        /// </summary>
        public static Matrix4 operator *(Matrix4 left, Matrix4 right)
        {
            Matrix4 result = new Matrix4();

            result.m00 = left.m00 * right.m00 + left.m01 * right.m10 + left.m02 * right.m20 + left.m03 * right.m30;
            result.m01 = left.m00 * right.m01 + left.m01 * right.m11 + left.m02 * right.m21 + left.m03 * right.m31;
            result.m02 = left.m00 * right.m02 + left.m01 * right.m12 + left.m02 * right.m22 + left.m03 * right.m32;
            result.m03 = left.m00 * right.m03 + left.m01 * right.m13 + left.m02 * right.m23 + left.m03 * right.m33;

            result.m10 = left.m10 * right.m00 + left.m11 * right.m10 + left.m12 * right.m20 + left.m13 * right.m30;
            result.m11 = left.m10 * right.m01 + left.m11 * right.m11 + left.m12 * right.m21 + left.m13 * right.m31;
            result.m12 = left.m10 * right.m02 + left.m11 * right.m12 + left.m12 * right.m22 + left.m13 * right.m32;
            result.m13 = left.m10 * right.m03 + left.m11 * right.m13 + left.m12 * right.m23 + left.m13 * right.m33;

            result.m20 = left.m20 * right.m00 + left.m21 * right.m10 + left.m22 * right.m20 + left.m23 * right.m30;
            result.m21 = left.m20 * right.m01 + left.m21 * right.m11 + left.m22 * right.m21 + left.m23 * right.m31;
            result.m22 = left.m20 * right.m02 + left.m21 * right.m12 + left.m22 * right.m22 + left.m23 * right.m32;
            result.m23 = left.m20 * right.m03 + left.m21 * right.m13 + left.m22 * right.m23 + left.m23 * right.m33;

            result.m30 = left.m30 * right.m00 + left.m31 * right.m10 + left.m32 * right.m20 + left.m33 * right.m30;
            result.m31 = left.m30 * right.m01 + left.m31 * right.m11 + left.m32 * right.m21 + left.m33 * right.m31;
            result.m32 = left.m30 * right.m02 + left.m31 * right.m12 + left.m32 * right.m22 + left.m33 * right.m32;
            result.m33 = left.m30 * right.m03 + left.m31 * right.m13 + left.m32 * right.m23 + left.m33 * right.m33;

            return result;
        }

        /// <summary>
        /// M x V
        ///	Transforms the given 3-D vector by the matrix, projecting the 
        ///	result back into <i>w</i> = 1.
        ///	<p/>
        ///	This means that the initial <i>w</i> is considered to be 1.0,
        ///	and then all the tree elements of the resulting 3-D vector are
        ///	divided by the resulting <i>w</i>.
        /// </summary>
        /// <remarks>
        /// ABij = Sk(Aik,Bkj)
        /// </remarks>
        public static Vector3 operator *(Matrix4 matrix, Vector3 vector)
        {
            Vector3 result = new Vector3();
            float inverseW = 1.0f / (matrix.m30 * vector.x + matrix.m31 * vector.y + matrix.m32 * vector.z + matrix.m33 * 1.0f);

            result.x = ((matrix.m00 * vector.x) + (matrix.m01 * vector.y) + (matrix.m02 * vector.z) + matrix.m03) * inverseW;
            result.y = ((matrix.m10 * vector.x) + (matrix.m11 * vector.y) + (matrix.m12 * vector.z) + matrix.m13) * inverseW;
            result.z = ((matrix.m20 * vector.x) + (matrix.m21 * vector.y) + (matrix.m22 * vector.z) + matrix.m23) * inverseW;

            return result;
        }

        /// <summary>
        ///	Used to multiply a Matrix4 object by a scalar value..
        /// </summary>
        public static Matrix4 operator *(Matrix4 left, float scalar)
        {
            Matrix4 result = new Matrix4();

            result.m00 = left.m00 * scalar;
            result.m01 = left.m01 * scalar;
            result.m02 = left.m02 * scalar;
            result.m03 = left.m03 * scalar;

            result.m10 = left.m10 * scalar;
            result.m11 = left.m11 * scalar;
            result.m12 = left.m12 * scalar;
            result.m13 = left.m13 * scalar;

            result.m20 = left.m20 * scalar;
            result.m21 = left.m21 * scalar;
            result.m22 = left.m22 * scalar;
            result.m23 = left.m23 * scalar;

            result.m30 = left.m30 * scalar;
            result.m31 = left.m31 * scalar;
            result.m32 = left.m32 * scalar;
            result.m33 = left.m33 * scalar;

            return result;
        }
        public static Matrix4 operator +(Matrix4 left, Matrix4 right)
        {
            Matrix4 result = new Matrix4();

            result.m00 = left.m00 + right.m00;
            result.m01 = left.m01 + right.m01;
            result.m02 = left.m02 + right.m02;
            result.m03 = left.m03 + right.m03;

            result.m10 = left.m10 + right.m10;
            result.m11 = left.m11 + right.m11;
            result.m12 = left.m12 + right.m12;
            result.m13 = left.m13 + right.m13;

            result.m20 = left.m20 + right.m20;
            result.m21 = left.m21 + right.m21;
            result.m22 = left.m22 + right.m22;
            result.m23 = left.m23 + right.m23;

            result.m30 = left.m30 + right.m30;
            result.m31 = left.m31 + right.m31;
            result.m32 = left.m32 + right.m32;
            result.m33 = left.m33 + right.m33;

            return result;
        }
        public static Matrix4 operator -(Matrix4 left, Matrix4 right)
        {
            Matrix4 result = new Matrix4();

            result.m00 = left.m00 - right.m00;
            result.m01 = left.m01 - right.m01;
            result.m02 = left.m02 - right.m02;
            result.m03 = left.m03 - right.m03;

            result.m10 = left.m10 - right.m10;
            result.m11 = left.m11 - right.m11;
            result.m12 = left.m12 - right.m12;
            result.m13 = left.m13 - right.m13;

            result.m20 = left.m20 - right.m20;
            result.m21 = left.m21 - right.m21;
            result.m22 = left.m22 - right.m22;
            result.m23 = left.m23 - right.m23;

            result.m30 = left.m30 - right.m30;
            result.m31 = left.m31 - right.m31;
            result.m32 = left.m32 - right.m32;
            result.m33 = left.m33 - right.m33;

            return result;
        }
        #endregion

        #region Render

        #region NOT TESTED

        /// <summary>
        /// Semplified inverse for projection matrix
        /// </summary>
        public static Matrix4 InverseProjection(Matrix4 proj)
        {
            Matrix4 inv = new Matrix4();

            //traspose rotation matrix
            inv.m00 = proj.m00;
            inv.m01 = proj.m10;
            inv.m02 = proj.m20;

            inv.m10 = proj.m01;
            inv.m11 = proj.m11;
            inv.m12 = proj.m21;

            inv.m20 = proj.m02;
            inv.m21 = proj.m12;
            inv.m22 = proj.m22;

            // fix col4
            inv.m03 = inv.m13 = inv.m23 = 0; inv.m33 = 1;

            //traslate position -p.r  -p.u -p.l
            inv.m30 = -proj.m03 * proj.m00 - proj.m13 * proj.m10 - proj.m23 * proj.m20;
            inv.m31 = -proj.m03 * proj.m01 - proj.m13 * proj.m11 - proj.m23 * proj.m21;
            inv.m32 = -proj.m03 * proj.m02 - proj.m13 * proj.m12 - proj.m23 * proj.m22;

            return inv;
        }

        /// <summary>
        /// Make a Left-Handle othogonal projection matrix
        /// </summary>
        /// <remarks>NOT TESTED</remarks>
        public static Matrix4 MakeOrthoLH(float fovy, Viewport viewport, float near, float far)
        {
            float Width = viewport.Width;
            float Height = viewport.Height;

            float thetaY = fovy / 2.0f;
            float tanThetaY = (float)Math.Tan(thetaY);
            float tanThetaX = tanThetaY * Width / Height;

            float half_w = tanThetaX * near;
            float half_h = tanThetaY * near;

            float iw = 1.0f / (half_w);
            float ih = 1.0f / (half_h);
            float q = 0.0f;

            if (far != 0)
            {
                q = 1.0f / (far - near);
            }

            Matrix4 mat = Matrix4.Zero;
            mat.m00 = iw;
            mat.m11 = ih;
            mat.m22 = q;
            mat.m23 = -near / (far - near);
            mat.m33 = 1;

            return Matrix4.Traspose(mat);
        }
        /// <summary>
        /// Make a Left-Handle othogonal projection matrix
        /// </summary>
        /// <remarks>NOT TESTED</remarks>
        public static Matrix4 MakeOrthoLH(float left, float right, float top, float bottom, float near, float far)
        {
            Matrix4 mat = Matrix4.Zero;
            mat.m00 = 2 / (right - left);
            mat.m11 = 2 / (top - bottom);
            mat.m22 = -1 / (far - near);
            mat.m30 = -(right + left) / (right - left);
            mat.m31 = -(top + bottom) / (top - bottom);
            mat.m32 = -near / (far - near);
            mat.m33 = 1;

            return Matrix4.Traspose(mat);
        }
        /// <summary>
        /// Make a Left-Handle prospective projection matrix
        /// </summary>
        /// <remarks>NOT TESTED</remarks>
        public static Matrix4 MakeProjectionLH(float left, float right, float top, float bottom, float near, float far)
        {
            Matrix4 mat = Matrix4.Zero;
            mat.m00 = 2 * near / (right - left);
            mat.m11 = 2 * near / (top - bottom);
            mat.m20 = (right + left) / (right - left);
            mat.m21 = (top + bottom) / (top - bottom);
            mat.m22 = -(far + near) / (far - near);
            mat.m23 = -1;
            mat.m32 = -2 * far * near / (far - near);

            return Matrix4.Traspose(mat);
        }
        #endregion
        /// <summary>
        /// Make a Left-Handle prospective projection matrix
        /// </summary>
        /// <param name="fovy">vertical(H) field-of-view in radians</param>
        public static Matrix4 MakeProjectionLH(float fovy, Viewport viewport, float near, float far)
        {
            // | w   0   0   0 |  s = 1/tan(fovy/2) 
            // | 0   h   0   0 |  q = f/(f-n)
            // | 0   0   Q   1 |
            // | 0   0  -Q*n 0 |

            float Width = viewport.Width;
            float Height = viewport.Height;
            float h = 1.0f / (float)Math.Tan(fovy * 0.5);
            float w = h / Width * Height; // h / aspectration
            float Q = far / (far - near);

            Matrix4 mat = Matrix4.Zero;
            mat.m00 = w;
            mat.m11 = h;
            mat.m22 = Q;
            mat.m32 = -Q * near;
            mat.m23 = 1;

            mat.Traspose();

            return mat;
        }
        /// <summary>
        /// Make a Left-Handle prospective projection matrix
        /// </summary>
        /// <param name="fovx">horizontal(W) field-of-view in radians</param>
        /// <param name="fovy">vertical(H) field-of-view in radians</param>
        public static Matrix4 MakeProjectionLH(float fovx, float fovy, float near, float far)
        {
            float h = 1.0f / (float)Math.Tan(fovy * 0.5);
            float w = 1.0f / (float)Math.Tan(fovx * 0.5);
            float Q = far / (far - near);

            Matrix4 mat = Matrix4.Zero;
            mat.m00 = w;
            mat.m11 = h;
            mat.m22 = Q;
            mat.m32 = -Q * near;
            mat.m23 = 1;

            // i want traspose here because will be caotic understand internet's tutorial using directx notation
            mat.Traspose();
            return mat;
        }
        /// <summary>
        /// Make a Left-Handle view matrix.
        /// </summary>
        /// <remarks>
        /// The "up" vector by default is "Y" but will be ortogonalized in the matrix so the "up" component will be different.
        /// Carefull when eye-target axes is vertical, the up vector can't be "Y" otherwise the cross product return a ramndom rotation
        /// With all default parameters you obtain identiy.
        /// </remarks>
        /// <param name="eye">where camera is, default is 0,0,0 </param>
        /// <param name="target">where camera look at, default is 0,0,1</param>
        /// <param name="up">rotation of camara to eye-look axes, default is 0,1,0 </param>
        public static Matrix4 MakeViewLH(Vector3 eye, Vector3 target, Vector3 up)
        {
            // | r   u   d   r.e |   right , up , direction , eye vectors
            // | r   u   d   u.e |
            // | r   u   d   d.e |
            // | 0   0   0    1  |

            // you pass a zero vector
            if (up.Normalize() < 1e-6) up = Vector3.UnitY;

            // targhet and eye have same coordinates
            Vector3 vz = target - eye;
            if (vz.Normalize() < 1e-6) vz = Vector3.UnitZ;

            // z vector is parallel to up vector , the cross == (0,0,0)
            Vector3 vx = Vector3.Cross(up, vz);
            if (vx.Normalize() < 1e-6) vx = Vector3.UnitX;

            // y vector is parallel to x vector, the cross == (0,0,0)
            Vector3 vy = Vector3.Cross(vz, vx);
            if (vy.Normalize() < 1e-6) vy = Vector3.UnitY;

            return new Matrix4(vx.x, vx.y, vx.z, -Vector3.Dot(vx, eye),
                               vy.x, vy.y, vy.z, -Vector3.Dot(vy, eye),
                               vz.x, vz.y, vz.z, -Vector3.Dot(vz, eye),
                               0, 0, 0, 1);
        }
        
        public static Matrix4 RotationX(float radians)
        {
            //  0  0  0  0
            //  0  c -s  0
            //  0  s  c  0
            //  0  0  0  1
            Matrix4 matrix = Matrix4.Identity;
            matrix.m11 = (float)Math.Cos(radians);
            matrix.m12 = -(float)Math.Sin(radians);
            matrix.m21 = (float)Math.Sin(radians);
            matrix.m22 = (float)Math.Cos(radians);
            return matrix;
        }
        public static Matrix4 RotationY(float radians)
        {
            //  c  0  s  0
            //  0  0  0  0
            // -s  0  c  0
            //  0  0  0  1
            Matrix4 matrix = Matrix4.Identity;
            matrix.m00 = (float)Math.Cos(radians);
            matrix.m02 = (float)Math.Sin(radians);
            matrix.m20 = -(float)Math.Sin(radians);
            matrix.m22 = (float)Math.Cos(radians);
            return matrix;
        }
        public static Matrix4 RotationZ(float radians)
        {
            //  c -s  0  0
            //  s  c  0  0
            //  0  0  0  0
            //  0  0  0  1

            Matrix4 matrix = Matrix4.Identity;
            matrix.m00 = (float)Math.Cos(radians);
            matrix.m01 = -(float)Math.Sin(radians);
            matrix.m10 = (float)Math.Sin(radians);
            matrix.m11 = (float)Math.Cos(radians);
            return matrix;
        }
        public static Matrix4 Scaling(Vector3 scale)
        {
            return Matrix4.Scaling(scale.x, scale.y, scale.z);
        }
        public static Matrix4 Scaling(float x, float y, float z)
        {
            Matrix4 matrix = Matrix4.Identity;
            matrix.m00 = x;
            matrix.m11 = y;
            matrix.m22 = z;
            return matrix;
        }
        public static Matrix4 Translating(Vector3 vector)
        {
            return Matrix4.Translating(vector.x, vector.y, vector.z);
        }
        public static Matrix4 Translating(float x, float y, float z)
        {
            Matrix4 matrix = Matrix4.Identity;
            matrix.m03 = x;
            matrix.m13 = y;
            matrix.m23 = z;
            return matrix;
        }

        public void Traspose()
        {
            // diagonals are the same
            swap(ref m01, ref m10);
            swap(ref m02, ref m20);
            swap(ref m03, ref m30);
            swap(ref m12, ref m21);
            swap(ref m13, ref m31);
            swap(ref m23, ref m32);
        }
        /// <summary>
        /// Return a new Trasposed matrix, remember the properties (ABC)^T = (C^T)(B^T)(A^T) when you 
        /// are working with WorldViewProjection trasformation in shader code
        /// </summary>
        public static Matrix4 Traspose(Matrix4 matrix)
        {
            Matrix4 m = new Matrix4();
            m.m00 = matrix.m00;
            m.m01 = matrix.m10;
            m.m02 = matrix.m20;
            m.m03 = matrix.m30;

            m.m10 = matrix.m01;
            m.m11 = matrix.m11;
            m.m12 = matrix.m21;
            m.m13 = matrix.m31;

            m.m20 = matrix.m02;
            m.m21 = matrix.m12;
            m.m22 = matrix.m22;
            m.m23 = matrix.m32;

            m.m30 = matrix.m03;
            m.m31 = matrix.m13;
            m.m32 = matrix.m23;
            m.m33 = matrix.m33;

            return m;
        }

        /// <summary>
        /// Composed rotation for z y x axis
        /// </summary>
        /// <param name="roll">counterclockwise rotation about the x axis</param>
        /// <param name="pitch">counterclockwise rotation about the y axis</param>
        /// <param name="yaw">counterclockwise rotation about the z axis</param>
        /// <returns></returns>
        public static Matrix4 RotationYawPitchRoll(float yaw, float pitch, float roll)
        {
            Matrix4 matrix = Matrix4.Identity;

            float a = yaw;
            float b = pitch;
            float c = roll;

            matrix.m00 = (float)(Math.Cos(a) * Math.Cos(b));
            matrix.m01 = (float)(Math.Cos(a) * Math.Sin(b) * Math.Sin(c) - Math.Sin(a) * Math.Cos(c));
            matrix.m02 = (float)(Math.Cos(a) * Math.Sin(b) * Math.Cos(c) + Math.Sin(a) * Math.Sin(c));

            matrix.m10 = (float)(Math.Sin(a) * Math.Cos(b));
            matrix.m11 = (float)(Math.Sin(a) * Math.Sin(b) * Math.Sin(c) + Math.Cos(a) * Math.Cos(c));
            matrix.m12 = (float)(Math.Sin(a) * Math.Sin(b) * Math.Cos(c) - Math.Cos(a) * Math.Sin(c));

            matrix.m20 = (float)(-Math.Sin(b));
            matrix.m21 = (float)(Math.Cos(b) * Math.Sin(c));
            matrix.m22 = (float)(Math.Cos(b) * Math.Cos(c));

            // TO DO : make direclty in math notation
            matrix.Traspose();

            return matrix;
        }

        public static implicit operator Matrix4(Matrix3 right)
        {
            Matrix4 result = Matrix4.Identity;

            result.m00 = right.m00;
            result.m01 = right.m01;
            result.m02 = right.m02;
            result.m10 = right.m10;
            result.m11 = right.m11;
            result.m12 = right.m12;
            result.m20 = right.m20;
            result.m21 = right.m21;
            result.m22 = right.m22;

            return result;
        }

        public static implicit operator Matrix4(Quaternion q)
        {
            Matrix4 matrix = Matrix4.Identity;

            matrix.m00 = 1 - 2 * q.y * q.y - 2 * q.z * q.z;
            matrix.m01 = 2 * q.x * q.y - 2 * q.z * q.w;
            matrix.m02 = 2 * q.x * q.z + 2 * q.y * q.w;

            matrix.m10 = 2 * q.x * q.y + 2 * q.z * q.w;
            matrix.m11 = 1 - 2 * q.x * q.x - 2 * q.z * q.z;
            matrix.m12 = 2 * q.y * q.z - 2 * q.x * q.w;

            matrix.m20 = 2 * q.x * q.z - 2 * q.y * q.w;
            matrix.m21 = 2 * q.y * q.z + 2 * q.x * q.w;
            matrix.m22 = 1 - 2 * q.x * q.x - 2 * q.y * q.y;

            return matrix;
        }
        #endregion

    }
}