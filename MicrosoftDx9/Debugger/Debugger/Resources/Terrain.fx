struct VertexShaderInput
{
    float2 Grid    : POSITION0;
    float  Height  : POSITION1;
};

struct VertexShaderOutput 
{
	float4 Position : POSITION;
    float4 Color    : COLOR0;
};

float4x4 WorldViewProj;

VertexShaderOutput  VertexShaderFunction(VertexShaderInput Input)
{
    VertexShaderOutput Output = (VertexShaderOutput)0;
    Output.Position = mul(float4(Input.Grid.x, Input.Height, Input.Grid.y, 1), WorldViewProj);
    return Output;    
}

float4 PixelShaderFunction(VertexShaderOutput Input) : COLOR0
{
    return float4(1, 0, 0, 0);
}

technique Terrain
{
    pass Pass0
    {        
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
