﻿namespace Engine.Test
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.viewport1 = new Engine.Renderer.GraphicPanel();
            this.viewport3 = new Engine.Renderer.GraphicPanel();
            this.viewport4 = new Engine.Renderer.GraphicPanel();
            this.viewport2 = new Engine.Renderer.GraphicPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.viewport1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.viewport3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.viewport4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.viewport2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1105, 562);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // viewport1
            // 
            this.viewport1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.viewport1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewport1.cameraEvents = false;
            this.viewport1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.viewport1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewport1.Location = new System.Drawing.Point(3, 3);
            this.viewport1.MaximumSize = new System.Drawing.Size(2000, 2000);
            this.viewport1.MinimumSize = new System.Drawing.Size(2, 2);
            this.viewport1.Name = "viewport1";
            this.viewport1.Size = new System.Drawing.Size(546, 275);
            this.viewport1.TabIndex = 0;
            // 
            // viewport3
            // 
            this.viewport3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.viewport3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewport3.cameraEvents = false;
            this.viewport3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.viewport3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewport3.Location = new System.Drawing.Point(3, 284);
            this.viewport3.MaximumSize = new System.Drawing.Size(2000, 2000);
            this.viewport3.MinimumSize = new System.Drawing.Size(2, 2);
            this.viewport3.Name = "viewport3";
            this.viewport3.Size = new System.Drawing.Size(546, 275);
            this.viewport3.TabIndex = 2;
            // 
            // viewport4
            // 
            this.viewport4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.viewport4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewport4.cameraEvents = false;
            this.viewport4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.viewport4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewport4.Location = new System.Drawing.Point(555, 284);
            this.viewport4.MaximumSize = new System.Drawing.Size(2000, 2000);
            this.viewport4.MinimumSize = new System.Drawing.Size(2, 2);
            this.viewport4.Name = "viewport4";
            this.viewport4.Size = new System.Drawing.Size(547, 275);
            this.viewport4.TabIndex = 3;
            // 
            // viewport2
            // 
            this.viewport2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.viewport2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewport2.cameraEvents = false;
            this.viewport2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.viewport2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewport2.Location = new System.Drawing.Point(555, 3);
            this.viewport2.MaximumSize = new System.Drawing.Size(2000, 2000);
            this.viewport2.MinimumSize = new System.Drawing.Size(2, 2);
            this.viewport2.Name = "viewport2";
            this.viewport2.Size = new System.Drawing.Size(547, 275);
            this.viewport2.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1105, 562);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Renderer.GraphicPanel viewport1;
        private Renderer.GraphicPanel viewport2;
        private Renderer.GraphicPanel viewport3;
        private Renderer.GraphicPanel viewport4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

    }
}

