float4x4 xWorldViewProj;
float4   xColor;

struct VS_INPUT
{
    float4  Position  : POSITION;
};

struct VS_OUTPUT
{
    float4  Position  : POSITION;    
    float4  Color     : COLOR0;
};

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	output.Position = mul(input.Position, xWorldViewProj);
	output.Color = xColor;
	return output;
}


float4 PShader(VS_OUTPUT input): COLOR0
{
	return input.Color;
}
float4 PShader_black(VS_OUTPUT input): COLOR0
{
	return xColor;
}
//////////////////////////////////////
// Techniques specs follow
//////////////////////////////////////
technique Default
{
    pass solidpass
    {
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShader();
        PixelShader =  compile ps_3_0 PShader();
    }
	pass wireframepass
    {
		FillMode = Wireframe;
        VertexShader = compile vs_3_0 VShader();
        PixelShader =  compile ps_3_0 PShader_black();
    }
}