struct Material
{
	float4 ambient;
	float4 diffuse;
	float4 emissive;
	float4 specular;
	float shininess;
};
//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 WorldViewProj : WORLDVIEWPROJ;
Material material;


//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------

// base vertex shader input, accept all buffers with DeclarationUsage.Position defined.
// can accept also a float3 value
struct VS_INPUT
{
    float4  Position  : POSITION;
};
// base vertex shader ouput, must contain COLOR0 and POSITION semantic to tell device position and color of pixel
struct VS_OUTPUT
{   
	float4  Position  : POSITION;
    float4  Color     : COLOR0;
};

VS_OUTPUT VShaderA(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	input.Position.w = 1;
	output.Position = mul(input.Position, WorldViewProj);
	output.Color = material.diffuse;
	return output;
}
VS_OUTPUT VShaderB(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	input.Position.w = 1;
	output.Position = mul(input.Position, WorldViewProj);
	output.Color = material.ambient;
	return output;
}

float4 PShaderA(VS_OUTPUT input) : COLOR0
{
	return input.Color;
}

//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique DefaultTechique
{
    pass p0
    {
		CullMode = CW;
        VertexShader = compile vs_3_0 VShaderA();
		PixelShader =  compile ps_3_0 PShaderA();
    }
}
technique AlphaTechique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShaderA();
		PixelShader =  compile ps_3_0 PShaderA();
    }
}
technique WireframeTechique1
{
	// color used from material
    pass p0
    {
        VertexShader = compile vs_3_0 VShaderA();
		PixelShader =  compile ps_3_0 PShaderA();
    }
	// for wireframe color
	pass p1
    {
        VertexShader = compile vs_3_0 VShaderB();
        PixelShader =  compile ps_3_0 PShaderA();
    }
}
technique WireframeTechique2
{
    pass p0
    {
		ZWriteEnable = TRUE;
		ZEnable = TRUE;
		AlphaBlendEnable = FALSE;
		AlphaTestEnable = FALSE;
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShaderA();
		PixelShader =  compile ps_3_0 PShaderA();
    }
	pass p1
    {
		FillMode = Wireframe;
        VertexShader = compile vs_3_0 VShaderB();
		PixelShader =  compile ps_3_0 PShaderA();
    }
}