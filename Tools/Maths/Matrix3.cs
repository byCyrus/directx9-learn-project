﻿/*
Axiom Graphics Engine Library
Copyright � 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code 
contained within this library is a derivative of the open source Object Oriented 
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.  
Many thanks to the OGRE team for maintaining such a high quality project.

The math library included in this project, in addition to being a derivative of
the works of Ogre, also include derivative work of the free portion of the 
Wild Magic mathematics source code that is distributed with the excellent
book Game Engine Design.
http://www.wild-magic.com/

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Engine.Maths
{
    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct Matrix3
    {
        public float m00, m01, m02;
        public float m10, m11, m12;
        public float m20, m21, m22;

        public static readonly Matrix3 Identity = new Matrix3(1, 0, 0, 0, 1, 0, 0, 0, 1);
        public static readonly Matrix3 Zero = new Matrix3(0, 0, 0, 0, 0, 0, 0, 0, 0);


        public Matrix3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22)
        {
            this.m00 = m00;
            this.m01 = m01;
            this.m02 = m02;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;
            this.m20 = m20;
            this.m21 = m21;
            this.m22 = m22;
        }
        public Matrix3(Vector3 xAxis, Vector3 yAxis, Vector3 zAxis)
        {
            this.m00 = xAxis.x;
            this.m01 = yAxis.x;
            this.m02 = zAxis.x;
            this.m10 = xAxis.y;
            this.m11 = yAxis.y;
            this.m12 = zAxis.y;
            this.m20 = xAxis.z;
            this.m21 = yAxis.z;
            this.m22 = zAxis.z;
        }

        public static readonly int sizeinbyte = sizeof(float) * 9;

        /// <summary>
        /// Indexer for accessing the matrix like a 2d array (i.e. matrix[2,3]).
        /// </summary>
        public float this[int row, int col]
        {
            get
            {
                //Debug.Assert((row >= 0 && row < 3) && (col >= 0 && col < 3), "Attempt to access Matrix3 indexer out of bounds.");

#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: return m00;
                            case 1: return m01;
                            case 2: return m02;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: return m10;
                            case 1: return m11;
                            case 2: return m12;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: return m20;
                            case 1: return m21;
                            case 2: return m22;
                        }
                        break;
                }
			    throw new IndexOutOfRangeException("Attempt to access Matrix3 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        return *(pM + ((3 * row) + col));
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert((row >= 0 && row < 3) && (col >= 0 && col < 3), "Attempt to access Matrix3 indexer out of bounds.");

#if !UNSAFE
                switch (row)
                {
                    case 0:
                        switch (col)
                        {
                            case 0: m00 = value; break;
                            case 1: m01 = value; break;
                            case 2: m02 = value; break;
                        }
                        break;
                    case 1:
                        switch (col)
                        {
                            case 0: m10 = value; break;
                            case 1: m11 = value; break;
                            case 2: m12 = value; break;
                        }
                        break;
                    case 2:
                        switch (col)
                        {
                            case 0: m20 = value; break;
                            case 1: m21 = value; break;
                            case 2: m22 = value; break;
                        }
                        break;
                }
#else
                unsafe
                {
                    fixed (float* pM = &this.m00)
                    {
                        *(pM + ((3 * row) + col)) = value;
                    }
                }
#endif
            }
        }

        /// <summary>
        ///		Allows the Matrix to be accessed linearly (m[0] -> m[8]).  
        /// </summary>
        public float this[int index]
        {
            get
            {
                //Debug.Assert(index >= 0 && index <= 8, "Attempt to access Matrix3 linear indexer out of bounds.");

#if !UNSAFE
                switch (index)
                {
                    case 0: return m00;
                    case 1: return m01;
                    case 2: return m02;
                    case 3: return m10;
                    case 4: return m11;
                    case 5: return m12;
                    case 6: return m20;
                    case 7: return m21;
                    case 8: return m22;                    
                }
                throw new IndexOutOfRangeException("Attempt to access Matrix3 indexer out of bounds.");
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        return *(pMatrix + index);
                    }
                }
#endif
            }
            set
            {
                //Debug.Assert(index >= 0 && index <= 8, "Attempt to access Matrix3 linear indexer out of bounds.");

#if !UNSAFE
                switch (index)
                {
                    case 0: m00 = value; break;
                    case 1: m01 = value; break;
                    case 2: m02 = value; break;
                    case 3: m10 = value; break;
                    case 4: m11 = value; break;
                    case 5: m12 = value; break;
                    case 6: m20 = value; break;
                    case 7: m21 = value; break;
                    case 8: m22 = value; break;
                }
#else
                unsafe
                {
                    fixed (float* pMatrix = &this.m00)
                    {
                        *(pMatrix + index) = value;
                    }
                }
#endif
            }
        }

        /// <summary>
        /// Get the determinant of matrix.
        /// </summary>
        public float Determinant
        {
            get
            {
                float cofactor00 = this.m11 * this.m22 - this.m12 * this.m21;
                float cofactor10 = this.m12 * this.m20 - this.m10 * this.m22;
                float cofactor20 = this.m10 * this.m21 - this.m11 * this.m20;

                float result = this.m00 * cofactor00 + this.m01 * cofactor10 + this.m02 * cofactor20;

                return result;
            }
        }

        public void FromEulerAnglesXYZ(float yaw, float pitch, float roll)
        {
            float cos = (float)Math.Cos(yaw);
            float sin = (float)Math.Sin(yaw);
            Matrix3 xMat = new Matrix3(1, 0, 0, 0, cos, -sin, 0, sin, cos);

            cos = (float)Math.Cos(pitch);
            sin = (float)Math.Sin(pitch);
            Matrix3 yMat = new Matrix3(cos, 0, sin, 0, 1, 0, -sin, 0, cos);

            cos = (float)Math.Cos(roll);
            sin = (float)Math.Sin(roll);
            Matrix3 zMat = new Matrix3(cos, -sin, 0, sin, cos, 0, 0, 0, 1);

            this = xMat * (yMat * zMat);
        }

        public Vector3 ToEulerAnglesXYZ()
        {
            double yAngle;
            double rAngle;
            double pAngle;

            pAngle = Math.Asin(this.m01);
            if (pAngle < Math.PI / 2)
            {
                if (pAngle > -Math.PI / 2)
                {
                    yAngle = Math.Atan2(this.m21, this.m11);
                    rAngle = Math.Atan2(this.m02, this.m00);
                }
                else
                {
                    // WARNING. Not a unique solution.
                    var fRmY = (float)Math.Atan2(-this.m20, this.m22);
                    rAngle = 0.0f; // any angle works
                    yAngle = rAngle - fRmY;
                }
            }
            else
            {
                // WARNING. Not a unique solution.
                var fRpY = Math.Atan2(-this.m20, this.m22);
                rAngle = 0.0f; // any angle works
                yAngle = fRpY - rAngle;
            }

            return new Vector3((float)yAngle, (float)rAngle, (float)pAngle);
        }
        /// <summary>
        ///	 Get or Set the translation value of the matrix using math notation
        ///		| 1 0 Tx |
        ///		| 0 1 Ty |
        ///		| 0 0  1 |
        /// </summary>
        public Vector2 TranslationComponent
        {
            get { return new Vector2(this.m02, this.m12); }
            set { this.m02 = value.x; this.m12 = value.y;}
        }

        /// <summary>
        /// Get a rotation matrix
        /// </summary>
        /// <param name="radians"></param>
        /// <returns></returns>
        public static Matrix3 Rotation(float radians)
        {
            Matrix3 matrix = new Matrix3();
            matrix.m00 = (float)Math.Cos(radians);
            matrix.m01 = (float)Math.Sin(radians);
            matrix.m10 = -(float)Math.Sin(radians);
            matrix.m11 = (float)Math.Cos(radians);
            return matrix;
        }

        #region operators
        public static Matrix3 operator *(Matrix3 left, Matrix3 right)
        {
            Matrix3 result = new Matrix3();

            result.m00 = left.m00 * right.m00 + left.m01 * right.m10 + left.m02 * right.m20;
            result.m01 = left.m00 * right.m01 + left.m01 * right.m11 + left.m02 * right.m21;
            result.m02 = left.m00 * right.m02 + left.m01 * right.m12 + left.m02 * right.m22;

            result.m10 = left.m10 * right.m00 + left.m11 * right.m10 + left.m12 * right.m20;
            result.m11 = left.m10 * right.m01 + left.m11 * right.m11 + left.m12 * right.m21;
            result.m12 = left.m10 * right.m02 + left.m11 * right.m12 + left.m12 * right.m22;

            result.m20 = left.m20 * right.m00 + left.m21 * right.m10 + left.m22 * right.m20;
            result.m21 = left.m20 * right.m01 + left.m21 * right.m11 + left.m22 * right.m21;
            result.m22 = left.m20 * right.m02 + left.m21 * right.m12 + left.m22 * right.m22;

            return result;
        }

        /// <summary>
        ///  vector * matrix [1x3 * 3x3 = 1x3]
        /// </summary>
        public static Vector3 operator *(Vector3 vector, Matrix3 matrix)
        {
            var product = new Vector3();

            product.x = matrix.m00 * vector.x + matrix.m01 * vector.y + matrix.m02 * vector.z;
            product.y = matrix.m10 * vector.x + matrix.m11 * vector.y + matrix.m12 * vector.z;
            product.z = matrix.m20 * vector.x + matrix.m21 * vector.y + matrix.m22 * vector.z;

            return product;
        }
        /// <summary>
        ///  matrix * vector [3x3 * 3x1 = 3x1]
        /// </summary>
        public static Vector3 operator *(Matrix3 matrix, Vector3 vector)
        {
            var product = new Vector3();

            product.x = matrix.m00 * vector.x + matrix.m01 * vector.y + matrix.m02 * vector.z;
            product.y = matrix.m10 * vector.x + matrix.m11 * vector.y + matrix.m12 * vector.z;
            product.z = matrix.m20 * vector.x + matrix.m21 * vector.y + matrix.m22 * vector.z;

            return product;
        }

        /// <summary>
        /// Negates all the items in the Matrix.
        /// </summary>
        public static Matrix3 operator -(Matrix3 matrix)
        {
            Matrix3 result = new Matrix3();

            result.m00 = -matrix.m00;
            result.m01 = -matrix.m01;
            result.m02 = -matrix.m02;
            result.m10 = -matrix.m10;
            result.m11 = -matrix.m11;
            result.m12 = -matrix.m12;
            result.m20 = -matrix.m20;
            result.m21 = -matrix.m21;
            result.m22 = -matrix.m22;

            return result;
        }
        /// <summary>
        ///  Used to subtract two matrices.
        /// </summary>
        public static Matrix3 operator -(Matrix3 left, Matrix3 right)
        {
            Matrix3 result = new Matrix3();

            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    result[row, col] = left[row, col] - right[row, col];
                }
            }

            return result;
        }
        #endregion

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < 3; i++)
                str.Append(String.Format("{0,4} {1,4} {2,4}\n", this[i, 0], this[i, 1], this[i, 2]));
            return str.ToString();
        }
    }
}