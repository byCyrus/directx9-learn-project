﻿#region LGPL License
/*
Axiom Graphics Engine Library
Copyright © 2003-2011 Axiom Project Team

The overall design, and a majority of the core engine and rendering code
contained within this library is a derivative of the open source Object Oriented
Graphics Engine OGRE, which can be found at http://ogre.sourceforge.net.
Many thanks to the OGRE team for maintaining such a high quality project.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
#endregion

using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Tools;

namespace Engine.Graphics
{
    /// <summary>
    /// The common buffer base class, if memory pool used is default, this class will be automatically insert in
    /// the GraphicDevice events for a correct disposing. The Resetable implementation can't be called by device becuase i want not
    /// store also a shaddow copy of data, you must implement your custum restorable function.
    /// </summary>
    public abstract class BaseBuffer : DeviceResource, IResettable
    {
        public delegate void RewriteBufferDelegate();
        /// <summary>
        /// Function used to write to buffer each time device call resetting event
        /// </summary> 
        public RewriteBufferDelegate RewriteBufferFunction;

        bool isLocked;
        bool managed;
        bool readable;
        bool dynamic;
        protected int m_size;       // size in bytes of whole buffer
        protected int m_count;      // num of elements(as m_type) you are using, must be <= capacity
        protected int m_typeSize;   // size in bytes of one buffer's element
        protected IntPtr m_bufferPtr; // Managed Pointer to gpu data stream, if is close is set to zero
        protected int m_lockSize;   // the size in bytes locked
        protected int m_lockOffset; // the offset in bytes used to lock buffer  
        protected int m_incrementoffset = 0;// use to increment offset when write singular value, in bytes        
        protected int m_flushbytes = 0; // debug the bytes flushed in or out to gpu buss
        
        protected Pool m_memory;
        protected Usage m_usage;
        protected GraphicDevice m_device;

        // can't pinned a portion of array, so i need a offset and i will use the m_lockOffset also when write or read
        protected bool m_shadowEnabled = false;
        public Array m_shadowbuffer; // shadow buffer emulate the graphic buffer, used in debug mode without inizialize device
        protected Type m_shadowtype;        //tipe of buffer's elements
        protected GCHandle m_shadowhandle;


        protected BaseBuffer(GraphicDevice device, bool dynamic, bool readable, bool managed)
            : this(device, dynamic, readable, managed, false) { }
        
        /// <summary>
        /// If buffer is not managed, you can assign the rewrite delegate function to automatize
        /// </summary>
        /// <param name="dynamic">if true the buffer is optimized for many update</param>
        /// <param name="readable">default is false, not usefull</param>
        /// <param name="managed">if true the device store a copy in the Ram for device lost and reseting</param>
        protected BaseBuffer(GraphicDevice device, bool dynamic, bool readable, bool managed,bool emulatormode)
        {
            this.m_count = 0;
            this.m_size = 0;
            this.dynamic = dynamic;
            this.m_bufferPtr = IntPtr.Zero;
            this.isLocked = false;
            this.disposed = false;
            this.managed = managed;
            this.readable = readable;
            
            this.m_usage = Usage.None;
            this.m_memory = managed ? Pool.Managed : Pool.Default;

            if (!emulatormode)
            {
                if (device == null)
                    throw new ArgumentNullException("need the device");

                this.m_device = device;
                if (!managed)
                {
                    if (!readable) m_usage |= Usage.WriteOnly;
                    if (dynamic) m_usage |= Usage.Dynamic;

                    if (device != null)
                    {
                        device.resourcesToDispose.Add(this);
                        device.resourcesToRestore.Add(this);
                    }
                }
            }
            else
            {
                m_shadowEnabled = true;
                this.m_device = null;
            }

        }

        ~BaseBuffer()
        {
            if (!managed && m_device != null)
            {
                m_device.resourcesToDispose.Remove(this);
                m_device.resourcesToRestore.Remove(this);
            }
        }

        /// <summary>
        /// Info about bytes flush in and out Gpu
        /// </summary>
        public int BytesToGpu { get { return m_flushbytes; } }
        /// <summary>
        /// Get or Set the elements position where start writing or reading
        /// </summary>
        public int Position
        {
            get { return m_incrementoffset * m_typeSize; }
            set { m_incrementoffset = value * m_typeSize; if (m_incrementoffset > m_size) m_incrementoffset = m_size; }
        }
        /// <summary>
        /// Get or Set the number of elements to use in the buffer, if exed the size will be set to maximum value
        /// After device reseting and the pool is "default", the Count value was set to zero because all data are
        /// invalidated.
        /// </summary>
        /// <remarks>
        /// Count mean : use the elements from 0 to Count, the buffer can contain velid elements from Count to Capacity
        /// but the idea is to discard them during process
        /// </remarks>
        public int Count
        {
            get { return m_count; }
            set 
            {
                int current = m_size / m_typeSize;
                m_count = (value > current) ? current : value; 
            }
        }
        /// <summary>
        /// Get the maximum number of elements that buffer can store, if you need to increase it the only solution is create a new buffer
        /// CAREFULL , increase or decrease size do a completly re-initialization and is slow
        /// </summary>
        public int Capacity { get { return m_size / m_typeSize; } }
        /// <summary>
        /// Get if buffer can be read.
        /// </summary>
        public bool IsReadable { get { return readable; } }
        /// <summary>
        /// Return the gpu buffer managed pointer
        /// </summary>
        protected abstract IntPtr Lock(int offset, int size, LockFlags mode);
        /// <summary>
        /// Release the gpu buffer pointer
        /// </summary>
        protected abstract void UnLock();

        /// <summary>
        /// Open the buffer. The offset and count value are relative to Type used
        /// </summary>
        /// <param name="onlyread">if you want only read from buffer, example to return the elements array when coping</param>
        /// <remarks>
        /// to use onlyread mode, ensure buffer is readable, overwise directx return always a zero buffer, so isn't usefull.
        /// </remarks>
        protected IntPtr Open(int offset, int count, bool onlyread)
        {
            m_flushbytes = 0;
            m_lockSize = count * m_typeSize;
            m_lockOffset = offset * m_typeSize;

            if (m_lockSize + m_lockOffset > m_size)
                throw new ArgumentOutOfRangeException("you try to open a incorrect size");

            if (!readable && onlyread)
                throw new Exception("Can't readonly this buffer, is writeonly");

            // only read case
            LockFlags mode = onlyread ? LockFlags.ReadOnly : LockFlags.Normal;

            // only write case
            if (!managed && !readable)
            {
                if ((m_usage & Usage.Dynamic) != 0)
                {
                    // only if dynamic and writeonly, you can discard if you are open  
                    // whole stored data. if not writeonly, use LockFlags.None
                    if (count >= m_count)
                        mode |= LockFlags.Discard;
                    else
                        mode |= LockFlags.NoOverwrite;
                }
            }
            m_bufferPtr = Lock(m_lockOffset, m_lockSize, mode);
            isLocked = m_bufferPtr != IntPtr.Zero;
            return m_bufferPtr;
        }
        /// <summary>
        /// Close buffer
        /// </summary>
        public void CloseStream()
        {
            if (isLocked) UnLock();
            m_bufferPtr = IntPtr.Zero;
            m_lockOffset = m_lockSize = 0;
        }
        /// <summary>
        /// Simple set to device, auto assign vertex declaration
        /// </summary>
        public abstract void SetToDevice();
        
        protected abstract void InitBuffer(int size);
        /// <summary>
        /// Call when device reseting , when resources are not managed and when RewriteBufferFunction is assigned
        /// </summary>
        public void Restore()
        {
            if (disposed)
                InitBuffer(m_size / m_typeSize);
            if (RewriteBufferFunction != null) 
                RewriteBufferFunction();
        }


        #region Read buffer
        /// <summary>
        /// Read the buffer. The startidx and count value are relative to Type used
        /// </summary>
        /// <returns>the array is in type value</returns>
        public Array Read(int startidx, int count)
        {
            if (!isLocked) throw new Exception("Graphic Buffer not open");


            if ((startidx + count) * m_typeSize > m_size) throw new ArgumentOutOfRangeException("Try to write a stream too big");
            
            m_flushbytes += m_typeSize * count;

            Array dst = Array.CreateInstance(m_shadowtype, count);

            // pin the array so we can get a pointer to it
            GCHandle destHandle = GCHandle.Alloc(dst, GCHandleType.Pinned);
            unsafe
            {
                byte* p_buffer = (byte*)m_bufferPtr.ToPointer();
                byte* trgPtr = (byte*)destHandle.AddrOfPinnedObject().ToPointer();

                int bstart = startidx * m_typeSize;
                for (int i = bstart, j = 0; j < count * m_typeSize; i++, j++)
                {
                    trgPtr[j] = p_buffer[i];
                }
            }
            destHandle.Free();
            return dst;
            //return m_datastream.Read(startidx, count);
        }
        public Array Read()
        {
            return Read(0, (int)m_count);
        } 
        #endregion

        /*
        public unsafe void writeadvance(float f)
        {
            byte* pdest = (byte*)m_bufferPtr.ToPointer() + m_incrementoffset;

            *(float*)pdest = f;
            m_incrementoffset += 4;
        }
        public unsafe void writeadvance(Vector3 v)
        {
            byte* pdest = (byte*)m_bufferPtr.ToPointer() + m_incrementoffset;

            *(Vector3*)pdest = v;
            m_incrementoffset += 12;
        }
        public unsafe void writeadvance(int i)
        {
            byte* pdest = (byte*)m_bufferPtr.ToPointer() + m_incrementoffset;

            *(int*)pdest = i;
            m_incrementoffset += 4;
        }
        public unsafe void writeadvance(CVERTEX v)
        {
            byte* pdest = (byte*)m_bufferPtr.ToPointer() + m_incrementoffset;

            *(CVERTEX*)pdest = v;
            m_incrementoffset += (ulong)sizeof(CVERTEX);
        }
        public unsafe void writeadvance(NTVERTEX v)
        {
            byte* pdest = (byte*)m_bufferPtr.ToPointer() + m_incrementoffset;

            *(NTVERTEX*)pdest = v;
            m_incrementoffset += (ulong)sizeof(NTVERTEX);
        }
        */
    }

}
