﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Tools;
using Engine.Maths;
using Engine.Renderer;
using Engine.Resources;
using Engine.Graphics;

namespace Engine.Scene
{
    /// <summary>
    /// A scene node that rappresent the gizmo, don't add in nodecollector because isn't
    /// a world objects
    /// </summary>
    public class SceneSelectionGizmo : ISceneNode
    {
        Matrix4 localCoord = Matrix4.Identity;

        /// <summary>
        /// Esternal values
        /// </summary>
        public enum GizmoMode
        {
            Movement,
            Rotation,
            Scaling
        }

        /// <summary>
        /// Get the current working axis's component, is a flag so can be more than one
        /// </summary>
        eAxis workingAxis = eAxis.None;

        ObjCone coneX, coneY, coneZ;
        ObjLine axisX, axisY, axisZ;

        List<ISceneNode> nodes = new List<ISceneNode>();

        GizmoMode mode = GizmoMode.Movement;

        BoundarySphere bSphere = BoundarySphere.Empty; // used for collision
        BoundaryAABB bAABB = BoundaryAABB.Empty; // used for spatial partitioning

        public string name { get { return "SceneGizmo"; } set { } }

        public Matrix4 transform
        {
            get { return localCoord; }
            set { throw new NotImplementedException(); }
        }
        public Vector3 position
        {
            get { return localCoord.TranslationComponent; }
            set { localCoord.TranslationComponent = value; }
        }
        /// <summary>
        /// Traslate the entire scene node (position change have less operations than other transformations)
        /// </summary>
        public void Traslate(Vector3 vector)
        {
            localCoord.Translate(vector);

            foreach (Node node in geometries)
            {
                Matrix4 mat = node.transform * Matrix4.Translating(vector.x,vector.y,vector.z);
 
                node.transform = mat;
            }

            bSphere.center += vector;
            bAABB.center += vector;
        }


        public Node[] geometries
        {
            get
            {
                switch (mode)
                {
                    case GizmoMode.Movement: return new Node[] { coneX, axisX, coneY, axisY, coneZ, axisZ };
                    case GizmoMode.Rotation: return new Node[] { };
                    case GizmoMode.Scaling: return new Node[] { };
                    default: return new Node[0];
                }
            }
        }
        public BoundarySphere boundSphere { get { return bSphere; } }
        public BoundaryAABB boundAABB { get { return bAABB; } }

        /// <summary>
        /// The intersect Geometries define also the working axis
        /// </summary>
        public bool IntersectGeometries(Ray ray, out float t)
        {
            t = 0;
            Vector3 v;
            bool x, y, z;

            switch (mode)
            {
                case GizmoMode.Movement:
                    x = axisX.PrimitiveIntersection(ray, out t, out v) > -1;
                    y = axisY.PrimitiveIntersection(ray, out t, out v) > -1;
                    z = axisZ.PrimitiveIntersection(ray, out t, out v) > -1;
                    break;
                case GizmoMode.Rotation:
                    return false;
                case GizmoMode.Scaling:
                    return false;
                default: return false;
            }
            workingAxis = eAxis.None;
            if (x) workingAxis |= eAxis.X;
            if (y) workingAxis |= eAxis.Y;
            if (z) workingAxis |= eAxis.Z;

            return (x || y || z);
        }

        public bool Selected { get; set; }
        public bool Hide { get { return false; } set { } }
        public uint handle { get; set; }


        public GizmoMode TransformOperation
        {
            get { return mode; }
            set { mode = value; }
        }
        public eAxis WorkingAxis
        {
            get { return workingAxis; }
        }

        public List<ISceneNode> Selection
        {
            get { return nodes; }
            set { nodes = value; localCoord.TranslationComponent = GetCenter(); }
        }

        Vector3 GetCenter()
        {
            Vector3 center = new Vector3(0, 0, 0);
            if (nodes.Count > 0)
            {
                foreach (ISceneNode node in nodes)
                    center += node.boundSphere.center;
                center *= 1.0f / (float)nodes.Count;
            }
            return center;
        }


        public SceneSelectionGizmo()
        {
            Vector3 center = GetCenter();
            bSphere = new BoundarySphere(center, 1.0f);
            bAABB = new BoundaryAABB(center + new Vector3(1, 1, 1), center - new Vector3(1, 1, 1));

            coneX = new ObjCone(8, Color.Red);
            coneY = new ObjCone(8, Color.Green);
            coneZ = new ObjCone(8, Color.Blue);
            axisX = new ObjLine(Color.Red);
            axisY = new ObjLine(Color.Green);
            axisZ = new ObjLine(Color.Blue);


            // safety check
            coneX.vertexFormat = coneY.vertexFormat = coneZ.vertexFormat =
            axisX.vertexFormat = axisY.vertexFormat = axisZ.vertexFormat = VertexFormat.Position | VertexFormat.Diffuse;

            // fix size
            Vector3 coneScaling = new Vector3(0.2f, 0.8f, 0.2f);
            coneX.transform = Matrix4.Scaling(coneScaling.x, coneScaling.y, coneScaling.z) * Matrix4.RotationZ(-(float)Math.PI / 2.0f) * Matrix4.Translating(1, 0, 0);
            coneY.transform = Matrix4.Scaling(coneScaling.x, coneScaling.y, coneScaling.z) * Matrix4.Translating(0, 1, 0);
            coneZ.transform = Matrix4.Scaling(coneScaling.x, coneScaling.y, coneScaling.z) * Matrix4.RotationX((float)Math.PI / 2.0f) * Matrix4.Translating(0, 0, 1);

            axisX.transform = Matrix4.RotationZ(-(float)Math.PI / 2.0f) * Matrix4.Translating(1, 0, 0);
            axisZ.transform = Matrix4.RotationX((float)Math.PI / 2.0f) * Matrix4.Translating(0, 0, 1);

            // set the only updating mode
            //coneX.updateMode = coneY.updateMode = coneZ.updateMode = axisX.updateMode = axisY.updateMode = axisZ.updateMode = UpdateModeEnum.OnlyVertices;

        }
        
        
        public void MoveSelection(Vector3 movement)
        {

        }
        public void RotateSelection(Quaternion rotation)
        {

        }
        public void ScaleSelection(Vector3 scale)
        {

        }
        public void OnMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
        }
        public void OnMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
        }
        public void OnMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
        }
        /// <summary>
        /// Like animation, need a update function each time camera change values or for each viewports
        /// </summary>
        public void Update(TrackballCamera camera)
        {
            // simply change the local coordinate system
            Vector3 viewDirection = Vector3.GetNormal(camera.eye - camera.look);

            switch (mode)
            {
                case GizmoMode.Movement:
                    break;
                case GizmoMode.Rotation:
                    break;
                case GizmoMode.Scaling:
                    break;
            }
        }

        public override string ToString()
        {
            return "Gizmo " + workingAxis.ToString() + " " + TransformOperation.ToString();
        }
    }
}
