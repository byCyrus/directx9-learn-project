﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;

using Engine.Maths;
using Engine.Tools;

namespace Engine.Graphics
{
    /// <summary>
    /// Manager of vertices array fragmented by DeclarationUsage
    /// </summary>
    public class VertexAttribute<T> : IEnumerable<T> where T : struct
    {
        GCHandle handle; 
        int elementSize;
        public T[] data;

        /// <summary>
        /// Usage of this array, match with semantic in shader version. It'll be used also to understand the usage in fragmented buffer
        /// when you need to fill vertices in the buffer
        /// </summary>
        public DeclarationUsage Usage { get; set; }
        /// <summary>
        /// Index for different semantic with same Usage
        /// </summary>
        public int UsageIdx { get; set; }


        public int Count
        {
            get { return data.GetUpperBound(0) + 1; }
        }

        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, int count, T initialvalue , int usageidx = 0)
        {
            elementSize = Marshal.SizeOf(typeof(T));
            Usage = usage;
            UsageIdx = usageidx;
            data = new T[count];
            for (int i = 0; i < count; i++) data[i] = initialvalue;
        }
        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, int count, int usageidx = 0)
            : this(usage, count, default(T))
        { }
        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, ref T[] source, int usageidx = 0)
        {
            elementSize = Marshal.SizeOf(typeof(T));
            Usage = usage;
            UsageIdx = usageidx;
            data = source;
        }
        /// <summary>
        /// </summary>
        public VertexAttribute(DeclarationUsage usage, IList<T> source, int usageidx = 0)
        {
            elementSize = Marshal.SizeOf(typeof(T));
            Usage = usage;
            UsageIdx = usageidx;
            int i = 0;
            data = new T[source.Count];
            foreach (T item in source) data[i++] = item;
        }

        ~VertexAttribute()
        {
            FreePointer();
        }
        /// <summary>
        /// Create a copy
        /// </summary>
        public VertexAttribute<T> Clone()
        {
            return new VertexAttribute<T>(this.Usage, (T[])data.Clone());
        }

        public T this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public IntPtr GetPointer()
        {
            handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            return handle.AddrOfPinnedObject();
        }
        public void FreePointer()
        {
            if (handle.IsAllocated) handle.Free();
        }
        /// <summary>
        /// Write entire attribute to a graphic buffer
        /// </summary>
        /// <param name="buffer">destination buffer</param>
        /// <param name="bufferInfo">buffer's element descriptor, used to know where write</param>
        /// <param name="bufferSize">buffer size in bytes,</param>
        /// <param name="bufferOffset">offset in bytes in buffer where write beginning</param>
        /// <returns></returns>
        public bool WriteToBuffers(IntPtr buffer, VertexInfo bufferInfo,int bufferSize, int bufferOffset )
        {
            VertexElement element;

            if (bufferInfo.GetElement(Usage,UsageIdx, out element))
            {
                if (elementSize != element.bytesize)
                    throw new ArgumentException("wrong size");

                IntPtr source = GetPointer();
                int sourcesize = Count * element.bytesize;
                int start = element.offset;
                int end = bufferInfo.bytesize - element.bytesize - element.offset;
                MemoryTool.WriteFragmentStruct(buffer, bufferSize, bufferOffset, source, sourcesize, 0, start, element.bytesize, end);
                FreePointer();
                return true;
            }
            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return (IEnumerator<T>)data.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    /// <summary>
    /// Manager of indices array, don't exist fragmented version, the difference is only 32bit or 16bit
    /// </summary>
    /// <remarks>when write to buffer the best way is split into base numberic uint or ushort</remarks>
    public class IndexAttribute<T> : IEnumerable<T> where T : struct
    {
        GCHandle handle;
        public T[] data;
        // TODO: usefull to enable uint->ushort casting
        uint maxIndex;
        
        public IndexInfo format { get; private set; }

        /// <summary>
        /// </summary>
        public IndexAttribute(int count, T initialValue)
            : this(count)
        {
            for (int i = 0; i < count; i++) data[i] = initialValue;
        }
                /// <summary>
        /// </summary>
        public IndexAttribute(int count)
        {
            format = IndexInfo.GetIndexFormat(typeof(T));
            if (Marshal.SizeOf(typeof(T)) != format.bytesize)
                throw new Exception("wrong struct size");
            data = new T[count];
        }
        /// <summary>
        /// </summary>
        public IndexAttribute(ref T[] source)
        {
            format = IndexInfo.GetIndexFormat(typeof(T));
            data = source;
        }
        /// <summary>
        /// </summary>
        public IndexAttribute(IList<T> source)
            : this(source.Count)
        {
            int i = 0;
            foreach (T item in source) data[i++] = item;
        }
        ~IndexAttribute()
        {
            FreePointer();
        }
        public int Count
        {
            get { return data.GetUpperBound(0) + 1; }
        }
        /// <summary>
        /// Create a copy
        /// </summary>
        public IndexAttribute<T> Clone()
        {
            IndexAttribute<T> copy = new IndexAttribute<T>((T[])data.Clone());
            copy.format = this.format;
            copy.maxIndex = this.maxIndex;
            return copy;
        }

        public T this[int index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }
        public IntPtr GetPointer()
        {
            handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            return handle.AddrOfPinnedObject();
        }
        public void FreePointer()
        {
            if (handle.IsAllocated) handle.Free();
        }
        /// <summary>
        /// Cast operation, work only if targhet buffer use a type greater than source, so is not possible
        /// copy from Uint32 to Uint16
        /// </summary>
        /// <typeparam name="K">the structure to cast, sizeof must be equal or less than currect struct</typeparam>
        public IndexAttribute<K> ConverterTo<K>() where K : struct
        {
            IndexInfo destFormat = IndexInfo.GetIndexFormat(typeof(K));

            // destination
            int destCount = format.numOfIndis * Count / destFormat.numOfIndis;
            IndexAttribute<K> dest = new IndexAttribute<K>(destCount);

            IntPtr destBuffer = dest.GetPointer();
            IntPtr srcBuffer = this.GetPointer();
            MemoryTool.WriteIndex(destBuffer, destFormat, destCount, 0, srcBuffer, this.format, 0, Count, 0);
            dest.FreePointer();
            this.FreePointer();

            dest.maxIndex = maxIndex;
            return dest;
        }

        /// <summary>
        /// Write entire attribute to a graphic buffer
        /// </summary>
        /// <param name="buffer">destination buffer</param>
        /// <param name="bufferInfo">buffer's element descriptor, used to know where write</param>
        /// <param name="bufferSize">buffer size in bytes,</param>
        /// <param name="bufferOffset">offset in bytes in buffer where write beginning</param>
        /// <param name="IndexOffset">a value to sum to each indices, usefull when use batch algorithm</param>
        /// <returns></returns>
        public bool WriteToBuffers(IntPtr buffer, IndexInfo bufferInfo, int bufferSize, int bufferOffset,int IndexOffset)
        {
            if (Count > 0)
            {
                IntPtr source = GetPointer();
                int size = format.bytesize * Count;
                MemoryTool.WriteIndex(buffer, bufferInfo, bufferSize, bufferOffset, source, format, size, 0, IndexOffset);
                FreePointer();
                return true;
            }
            return false;
        }
        public IEnumerator<T> GetEnumerator()
        {
            return (IEnumerator<T>)data.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
