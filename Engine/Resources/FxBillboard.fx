﻿//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
#define  ALPHAVALUE   200

float4x4 xWorldViewProj;
float3   xEye;
float3   xTarghet;
float3   xAllowedRotDir;
float2   xWind;


Texture xBillboardTexture;

sampler textureSampler = sampler_state 
{ 
	texture = <xBillboardTexture> ;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter=LINEAR;
	AddressU = CLAMP; 
	AddressV = CLAMP;
};

//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
	//stream0 : the billboard geometry
	float2  TexCoord   : TEXCOORD0;
	float2  Scalar     : TEXCOORD1;
	//stream1 : the instance buffer
	float2  Windf      : TEXCOORD2;
	float4  Traslation : POSITION;
	float4  Color      : COLOR0;
};
struct VS_OUTPUT
{   
	float4  Position  : POSITION;
    float4  Color     : COLOR0;
	float2  TexCoord  : TEXCOORD0;
};

float4 billboardvertex(VS_INPUT vertex , float3 vup , float3 vright)
{
	// vlook  |
	//   \    |
	//    \   |vup
	//     \  |
	//      \ |
    //       \|- - - - > vright

	float3 pos = vup * vertex.Scalar.x + vright * vertex.Scalar.y;
	
	float posL = length(pos);
	//float posL = sqrt(scalar.x * scalar.x + scalar.y * scalar.y);

	float fblend = vertex.Scalar.x;
	fblend = fblend+1;
	fblend = fblend*fblend;
	fblend = fblend*fblend - fblend;

	pos.x = pos.x + xWind.x * fblend * vertex.Windf.x;
	pos.z = pos.z + xWind.y * fblend * vertex.Windf.y;
	pos = normalize(pos) * posL;
	
	pos += vertex.Traslation;
	return mul(float4(pos,1), xWorldViewProj);
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// The billboard are alligned to eye and turn arond a vector
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
VS_OUTPUT VShader_cyl(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
    float3 vLook  = normalize(input.Traslation.xyz - xEye);
    float3 vUp    = normalize(xAllowedRotDir);
    float3 vRight = normalize(cross(vUp, vLook)); //remember cross in LH coordsystem
	
	output.Position = billboardvertex(input, vUp, vRight);
	output.Color    = input.Color;
	output.TexCoord = input.TexCoord;
    return output;
}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// The billboard are aligned to eye-targhet direction
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
VS_OUTPUT VShader_dir(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
    float3 vLook  = normalize(xTarghet - xEye);
    float3 vUp    = normalize(xAllowedRotDir);
    float3 vRight = normalize(cross(vUp, vLook));
	
	output.Position = billboardvertex(input, vUp, vRight);
	output.Color    = input.Color;
	output.TexCoord = input.TexCoord;
    return output;
}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// The billboard are always alligned to eye
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
VS_OUTPUT VShader_eye(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
    float3 vLook  = input.Traslation.xyz - xEye;
    float3 vRight = cross(xAllowedRotDir, vLook);
	float3 vUp    = cross(vLook, vRight);

	vLook  = normalize(vLook);
	vRight = normalize(vRight);
	vUp    = normalize(vUp);
	
	output.Position = billboardvertex(input, vUp, vRight);
	output.Color    = input.Color;
	output.TexCoord = input.TexCoord;
    return output;
}
//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader_vcolor(VS_OUTPUT Input) : COLOR0
{
	return Input.Color;
}
float4 PShader_texture(VS_OUTPUT Input) : COLOR0
{
	 return tex2D(textureSampler, Input.TexCoord);
}
float4 PShader_black(VS_OUTPUT Input) : COLOR0
{
	return float4(0,0,0,1);
}
//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// cylinder billboard
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
technique CylBillboardTechique
{
    pass p0
    {
		CullMode = CCW;
        VertexShader = compile vs_3_0 VShader_cyl();
		PixelShader =  compile ps_3_0 PShader_vcolor();
    }
}
technique CylBillboardTextureTechique
{
    pass p0
    {
		CullMode = CCW;
		ZWriteEnable = TRUE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = GREATEREQUAL;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = FALSE;
		
        VertexShader = compile vs_3_0 VShader_cyl();
		PixelShader =  compile ps_3_0 PShader_texture();
    }
	pass p1
    {
		ZWriteEnable    = FALSE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = LESS;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = TRUE;
		SrcBlend  = SRCALPHA;
		DestBlend = INVSRCALPHA;

        VertexShader = compile vs_3_0 VShader_cyl();
		PixelShader =  compile ps_3_0 PShader_texture();
    }
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// direction billboard
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
technique ParBillboardTechique
{
    pass p0
    {
		CullMode = CCW;
        VertexShader = compile vs_3_0 VShader_dir();
		PixelShader =  compile ps_3_0 PShader_vcolor();
    }
}
technique ParBillboardTextureTechique
{
    pass p0
    {
		CullMode = CCW;
		ZWriteEnable = TRUE;
		FillMode = Solid;

		AlphaTestEnable = TRUE;
		AlphaFunc = GREATER;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = FALSE;

        VertexShader = compile vs_3_0 VShader_dir();
		PixelShader =  compile ps_3_0 PShader_texture();
    }

	/*
    pass p0
    {
		CullMode = CCW;
		ZWriteEnable = TRUE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = GREATER;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = FALSE;

        VertexShader = compile vs_3_0 VShader_dir();
		PixelShader =  compile ps_3_0 PShader_texture();
    }

    pass p1
    {
		ZWriteEnable    = FALSE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = LESSEQUAL;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = TRUE;
		SrcBlend  = SRCALPHA;
		DestBlend = INVSRCALPHA;

        VertexShader = compile vs_3_0 VShader_dir();
		PixelShader =  compile ps_3_0 PShader_texture();
    }
	*/
}
technique ParBillboardTextureWireTechique
{
    pass p0
    {
		DepthBias = 0;
		CullMode = CCW;
		FillMode = SOLID;
		ZWriteEnable = TRUE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = GREATEREQUAL;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = FALSE;

        VertexShader = compile vs_3_0 VShader_dir();
		PixelShader = compile ps_3_0 PShader_texture();
    }
    pass p1
    {
		ZWriteEnable    = FALSE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = LESS;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = TRUE;
		SrcBlend  = SRCALPHA;
		DestBlend = INVSRCALPHA;

        VertexShader = compile vs_3_0 VShader_dir();
		PixelShader =  compile ps_3_0 PShader_texture();

    }
	pass p3
    {	
		DepthBias = -0.00001f;
		ZWriteEnable = TRUE;
		AlphaTestEnable = FALSE;
		FillMode = WIREFRAME;

        VertexShader = compile vs_3_0 VShader_dir();
		PixelShader =  compile ps_3_0 PShader_black();
    }
}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// spheric billboard
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
technique SphBillboardTechique
{
    pass p0
    {
		CullMode = CCW;
        VertexShader = compile vs_3_0 VShader_eye();
		PixelShader = compile ps_3_0 PShader_vcolor();
    }
}
technique SphBillboardTextureTechique
{
    pass p0
    {
		CullMode = CCW;
		ZWriteEnable = TRUE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = GREATEREQUAL;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = FALSE;
        VertexShader = compile vs_3_0 VShader_eye();
		PixelShader = compile ps_3_0 PShader_texture();
    }

	pass p1
    {
		ZWriteEnable    = FALSE;
		
		AlphaTestEnable = TRUE;
		AlphaFunc = LESS;
		AlphaRef  = ALPHAVALUE;
		
		AlphaBlendEnable = TRUE;
		SrcBlend  = SRCALPHA;
		DestBlend = INVSRCALPHA;

        VertexShader = compile vs_3_0 VShader_eye();
		PixelShader = compile ps_3_0 PShader_texture();
    }
}