﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Renderer
{
    /// <summary>
    /// Byte : usefull flags to avoid the very slow function Dictionary.Contains
    /// </summary>
    [Flags]
    public enum BatchChunkBufferFlags : byte
    {
        None = 0x0,
        InWrite = 0x1,
        InRemove = 0x2,
        InUpdate = 0x4,
        InBuffer = 0x8
    }
    /// <summary>
    /// Byte : usefull flags for EngineCore processes
    /// </summary>
    [Flags]
    public enum EngineCoreFlags : byte
    {
        None = 0,
        /// <summary>
        /// flag to evaluate if you want use a dynamic or static cache for this node
        /// </summary>
        IsDynamic = 1,
        /// <summary>
        /// flag to tell EngineCore that this node will be completly remove from main list
        /// </summary>
        MarkRemove = 2,
    }

    /// <summary>
    /// byte : flag to get the change that node done
    /// </summary>
    [Flags]
    public enum UpdateFlags : byte // max flag : 128 = 0x10000000
    {
        /// <summary> 000000 : No changes, objects are invariate</summary>
        None = 0,
        /// <summary> 000001 : Change only vertices, need rewrite them to buffer</summary>
        Vertices = 1,  
        /// <summary> 000011 : Change vertices count, need rewrite them to buffer and increase chunk size</summary>
        /// <remarks> Remark that this flag set true also Vertices Flag for coherence</remarks>
        VerticesCount = 3,
        /// <summary> 000100 : Change only indices, need rewrite them to buffer</summary>
        Indices = 4,
        /// <summary> 000110 : Change indices count, need rewrite them to buffer and increase chunk size</summary>
        /// <remarks> Remark that this flag set true also Indices Flag for coherence</remarks>
        IndicesCount = 6,     
        /// <summary> 001000 : Change only texture, need change cache or update the cache.textureid</summary>
        Material = 8,
        /// <summary> 010000 : Change only transformation matrix, need rewrite vertices</summary>
        /// <remarks>
        /// if not use local coordsys will convert in "UpdateFlags.Vertices" to apply trasformation to match objects 
        /// to a device.world.Matrix4.Identiy coordinate system
        /// </remarks>
        Transform = 16,
        /// <summary> 111111 : Change some render states, need change group </summary>
        /// <remarks>
        /// example when change texture, rendermode, count of vertice or indices, animable ecc... generate an Removing + Adding in the renderer
        /// This flag have priority to all other flag so all set to true for coherence
        /// </remarks>
        RenderState = 63,
    }
    /// <summary>
    /// Int : Use some internal check , if less than 0 is all ok, error start from 0 value
    /// </summary>
    public enum RESULT : int
    {
        //positive match
        CONTINUE = -2,
        OK = -1,
        //negative match
        ERR = 0,
        NOT_FOUND = 1,
        INCOMPATIBLE = 2,
        OUT_OF_MEMORY = 3
    }
}
