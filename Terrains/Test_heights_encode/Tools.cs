﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

namespace Test_heights_encode
{
    public class AxisObj
    {
        public MaterialFX_VCOLOR material = null;
        GraphicDevice device;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public Vector3[] axe = new Vector3[3];

        public AxisObj(GraphicDevice device)
        {
            this.device = device;
            this.cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(50,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,20,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,50, Color.Blue)
            };

            axe[0] = verts[1].position;
            axe[1] = verts[3].position;
            axe[2] = verts[5].position;

            axis = new VertexBuffer(device, cvertex_decl.Format, verts.Length, false, true);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public void Draw(ICamera camera)
        {
            cvertex_decl.SetToDevice();
            axis.SetToDevice();

            if (material != null)
            {
                material.ProjViewWorld = camera.Projection * camera.View * Matrix4.Identity;
                int count = material.Begin();
                for (int pass = 0; pass < count; pass++)
                {
                    material.BeginPass(pass);
                    device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                    material.EndPass();
                }
                material.End();
            }
            device.renderstates.projection = camera.Projection;
            device.renderstates.view = camera.View;
            device.renderstates.world = Matrix4.Identity;
            device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
        }
    }



    public class HieghtShader : MaterialFX
    {
        public HieghtShader(GraphicDevice device)
            : base(device, Test_heights_encode.Properties.Resources.QuadMap)
        {
            /*
            float3   xPatchOrigin; // the min(x,y,z) position in global coord system of current patch
            float2   xPatchScale;  // the scale factor of patch's vertices in (dx,dy) format
            float4   xPatchColor;  // debug patch with a custum color if diffuse texture aren't available
            float4   xTerrainSize  // global size of terrain in (minx,minz,maxx,maxz) format
            */
            effect.Parameters.Add("Technique", new EffectAttributeTechnique("WireframeTechique2"));
            effect.Parameters.Add("Transform", new EffectAttributeMatrix(Matrix4.Identity, "WorldViewProj"));
            effect.Parameters.Add("PatchOrigin", new EffectAttributeVector3("xPatchOrigin"));
            effect.Parameters.Add("PatchScale", new EffectAttributeVector3("xPatchScale"));
            effect.Parameters.Add("PatchDimension", new EffectAttributeVector2("xPatchDim"));
            effect.Parameters.Add("PatchColor", new EffectAttributeColor(Color32.Write, "xPatchColor"));

            effect.Parameters.Add("HeightMap", new EffectAttributeTexture(null, "xHeightMap"));
            effect.Parameters.Add("NormalMap", new EffectAttributeTexture(null, "xNormalMap"));
            effect.Parameters.Add("DiffuseMap", new EffectAttributeTexture(null, "xColorMap"));
            effect.Parameters.Add("RockMap", new EffectAttributeTexture(null, "xRockMap"));

            effect.Parameters.Add("TerrainMin", new EffectAttributeVector3("xTerrainMin"));
            effect.Parameters.Add("TerrainMax", new EffectAttributeVector3("xTerrainMax"));
            effect.Parameters.Add("LightDirection", new EffectAttributeVector3("xLightDir"));
            effect.Parameters.Add("CameraEye", new EffectAttributeVector3("xEye"));

            effect.Parameters.Add("Morph", new EffectAttributeVector2("xMorph"));

        }

        public bool WireframeMode
        {
            get { return (string)effect.Parameters["Technique"] == "WireframeTechique2"; }
            set { effect.Parameters["Technique"] = value ? "WireframeTechique2" : "DefaultTechique"; }
        }
        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
        public Texture HeightMap
        {
            get { return (Texture)effect.Parameters["HeightMap"]; }
            set { effect.Parameters["HeightMap"] = (Texture)value; }
        }
        public Texture DiffuseMap
        {
            get { return (Texture)effect.Parameters["DiffuseMap"]; }
            set { effect.Parameters["DiffuseMap"] = (Texture)value; }
        }
        public Texture NormalMap
        {
            get { return (Texture)effect.Parameters["NormalMap"]; }
            set { effect.Parameters["NormalMap"] = (Texture)value; }
        }
        public Texture RockMap
        {
            get { return (Texture)effect.Parameters["RockMap"]; }
            set { effect.Parameters["RockMap"] = (Texture)value; }
        }
        public Vector3 TerrainMin
        {
            get { return (Vector3)effect.Parameters["TerrainMin"]; }
            set { effect.Parameters["TerrainMin"] = (Vector3)value; }
        }
        public Vector3 TerrainMax
        {
            get { return (Vector3)effect.Parameters["TerrainMax"]; }
            set { effect.Parameters["TerrainMax"] = (Vector3)value; }
        }



        public Vector2 Morph
        {
            get { return (Vector2)effect.Parameters["Morph"]; }
            set { effect.Parameters["Morph"] = (Vector2)value; }
        }

        /// <summary>
        /// In [minX,minY,minZ] format
        /// </summary>
        public Vector3 PatchOrigin
        {
            get { return (Vector3)effect.Parameters["PatchOrigin"]; }
            set { effect.Parameters["PatchOrigin"] = (Vector3)value; }
        }
        public Vector2 PatchDimension
        {
            get { return (Vector2)effect.Parameters["PatchDimension"]; }
            set { effect.Parameters["PatchDimension"] = (Vector2)value; }
        }
        public Vector3 PatchScale
        {
            get { return (Vector3)effect.Parameters["PatchScale"]; }
            set { effect.Parameters["PatchScale"] = (Vector3)value; }
        }
        public Vector3 LightDirection
        {
            get { return (Vector3)effect.Parameters["LightDirection"]; }
            set { effect.Parameters["LightDirection"] = (Vector3)value; }
        }
        public Vector3 CameraEye
        {
            get { return (Vector3)effect.Parameters["CameraEye"]; }
            set { effect.Parameters["CameraEye"] = (Vector3)value; }
        }
        public Color32 PatchColor
        {
            get { return (Color32)effect.Parameters["PatchColor"]; }
            set { effect.Parameters["PatchColor"] = (Color32)value; }
        }

        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
    }

    public class Terrain
    {
        GraphicDevice device;
        HieghtShader shader;


        public Terrain(GraphicDevice device)
        {
            this.device = device;
            this.shader = new HieghtShader(device);
        }
        public void Draw(ICamera camera)
        {

        }
    }
}
