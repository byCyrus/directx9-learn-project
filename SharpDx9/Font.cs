﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;

namespace Engine.Graphics
{
    /// <summary>
    /// SharpDx
    /// </summary>
    public class DxFont : IDisposable
    {
        DX9Device device;
        internal D3D.Font m_font;
  
        /// <summary>
        /// Default fontName = "arial" and fontSize = 8
        /// </summary>
        /// <param name="device"></param>
        public DxFont(DX9Device device) : this(device, "arial", 12) { }       
       
        public DxFont(DX9Device device, string fontName, int fontSize)
        {
            this.device = device;
            m_font = new D3D.Font(device.m_device, new Font(fontName, fontSize, FontStyle.Bold));

            m_font.Disposing += new EventHandler<EventArgs>(Disposing);
        }

        void Disposing(object sender, EventArgs args)
        {
            Console.WriteLine("Font disposing" + sender.ToString() + " " + args.ToString());
        }

        public void DrawString(string text, int x, int y, Color color)
        {
            if (!String.IsNullOrEmpty(text))
                m_font.DrawText(null, text, x, y, DX.ColorBGRA.FromBgra(color.ToArgb()));
        }

        public void Dispose()
        {
            if (m_font!=null) m_font.Dispose();
        }
    }
}
