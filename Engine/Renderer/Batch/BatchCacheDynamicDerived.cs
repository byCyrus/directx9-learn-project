﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using Engine.Tools;
using Engine.Graphics;
using Engine.Maths;

namespace Engine.Renderer
{
    /// <summary>
    /// Cache used for indexed triangles, triangle.strip and triangle.fan will be converted as indexed triangle.list
    /// so the "isCompatible()" function return true. Optimized for Read and Write buffer, store in managed memory pool
    /// </summary>
    public class BatchCacheDynamic_TriMesh : BatchCacheDynamicIndexed, INodeMaterial
    {
        public NodeMaterial m_material { get; set; }
        public uint m_material_key { get { return m_material != null ? m_material.m_key : 0; } }

        /// <summary>
        /// If UseBatch = false don't use a indices offset so , theoretically , the MaxVertsCount can be greater than index.MaxValue
        /// but need to test
        /// </summary>
        /// <param name="MaxVertsCount">greater vertices index</param>
        /// <param name="UseBatch">use a offset for each indices</param>
        public BatchCacheDynamic_TriMesh(GraphicDevice mdevice, VertexDeclaration declaration, NodeMaterial material, EngineCore Core, bool UseBatch, bool UseLocalCoord)
            : base(mdevice, declaration, PrimitiveType.TriangleList, Core, UseBatch, UseLocalCoord)
        {
            if (material!=null)
            {
                // not in the main collector
                if (material.m_key == 0)
                {
                    core.Add(material);
                }
                m_material = material;
                m_material.numassignment++;
            }
            else
            {
                m_material = null;
            }
        }
        /// <summary>
        /// ... Decrease also the material counter
        /// </summary>
        public override void Destroy()
        {
            base.Destroy();
            if (m_material != null) m_material.numassignment--;
        }
        /// <summary>
        /// ... Sorting using material is a good idea
        /// </summary>
        public override int CompareTo(object obj)
        {
            return NodeMaterial.CompareByTexture(this, (INodeMaterial)obj);
        } 

        public override bool isCompatible(Node node)
        {
            //check if is a triangle primitive
            if (!(node is PrimitiveTriangle)) return false;
            PrimitiveTriangle mesh = (PrimitiveTriangle)node;

            //check if it using the same vertex format
            return (mesh.vertexFormat == this.vertexFormat) && (mesh.m_material_key == m_material_key) && mesh.engineProp.isAnimable;
        }

        protected override void DrawImplementation()
        {
            if (m_VB == null || m_VB.Count == 0) return;

            // Set material
            if (NodeMaterial.SetToDevice(device, m_material, m_Vdeclaration.Contain(VertexFormat.Texture1)))
                core.nTotTextureSet++;

            IndexBuffer.SetToDevice(device.m_device);
            m_VB.SetToDevice(device.m_device);

            //Array verts = base.CacheVertices();
            //Array indis = base.CacheIndices();
            if (base.UseBatch)
            {
                device.renderstates.world = globalCoordSystem;
                device.m_device.DrawIndexedPrimitives(base.cachePrimitive, 0, 0, m_VB.Count, 0, IndexBuffer.Count / 3);
                core.nTotDrawCall++;
            }
            else
            {
                if (base.UseLocal)
                {
                    for (int i = 0; i < BufferChunks.Count; i++)
                    {
                        BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                        if (!chunk.show) continue;

                        // to use correctly the local matrix need to add also globalCoordSystem
                        device.renderstates.world = chunk.node.transform * globalCoordSystem;

                        /* Offset to add to each vertex index in the index buffer */
                        int baseVertex = chunk.voffset; // batching not done
                        /* Minimum vertex index for vertices used during the call. The minVertexIndex parameter and
                         * all of the indices in the index stream are relative to the baseVertex parameter */
                        int minVertexIndex = 0;
                        /* Location in the index array at which to start reading vertices */
                        int startIndex = chunk.ioffset;

                        device.m_device.DrawIndexedPrimitives(base.cachePrimitive, baseVertex, minVertexIndex, chunk.nverts, startIndex, chunk.nindis / 3);
                        core.nTotDrawCall++;
                    }
                }
                else
                {
                    for (int i = 0; i < BufferChunks.Count; i++)
                    {
                        BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                        if (!chunk.show) continue;
                        int baseVertex = chunk.voffset; // batching not done
                        int minVertexIndex = 0;
                        int startIndex = chunk.ioffset;
                        device.m_device.DrawIndexedPrimitives(PrimitiveType.TriangleList, baseVertex, minVertexIndex, chunk.nverts, startIndex, chunk.nindis / 3);
                        core.nTotDrawCall++;
                    }
                }
            }
        }

    }

    /// <summary>
    /// Cache used for indexed lines, line.strip will be converted as indexed line.list so the 
    /// "isCompatible()" function return true. Optimized for Read and Write buffer, store in managed memory pool
    /// </summary>
    public class BatchCacheDynamic_Spline : BatchCacheDynamicIndexed
    {
        /// <summary>
        /// If UseBatch = false don't use a indices offset so , theoretically , the MaxVertsCount can be greater than index.MaxValue
        /// but need to test
        /// </summary>
        /// <param name="MaxVertsCount">greater vertices index</param>
        /// <param name="UseBatch">use a offset for each indices</param>
        public BatchCacheDynamic_Spline(GraphicDevice mdevice, VertexDeclaration declaration, EngineCore Core, bool UseBatch, bool UseLocalCoord)
            : base(mdevice, declaration, PrimitiveType.LineList, Core, UseBatch, UseLocalCoord)
        {
            // lines don't use texture
        }

        public override bool isCompatible(Node node)
        {
            //check if is a line primitive
            if (!(node is PrimitiveLine)) return false;
            PrimitiveLine shape = (PrimitiveLine)node;

            //check if it using the same vertex format
            return (shape.vertexFormat == this.vertexFormat) && shape.engineProp.isAnimable;
        }

        protected override void DrawImplementation()
        {
            if (m_VB == null || m_VB.Count == 0) return;
            
            DxDevice m_device = device.m_device;
            
            NodeMaterial.RemoveFromDevice(device);

            IndexBuffer.SetToDevice(device.m_device);
            m_VB.SetToDevice(device.m_device);

            if (base.UseBatch)
            {
                device.m_device.DrawIndexedPrimitives(base.cachePrimitive, 0, 0, m_VB.Count, 0, IndexBuffer.Count / 2);
                core.nTotDrawCall++;
            }
            else
            {
                if (base.UseLocal)
                    for (int i = 0; i < BufferChunks.Count; i++)
                    {
                        BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                        if (!chunk.show) continue;
                        device.renderstates.world = chunk.node.transform * globalCoordSystem;
                        // Offset to add to each vertex index in the index buffer
                        int baseVertex = chunk.voffset; // batching not done
                        // Minimum vertex index for vertices used during the call. The minVertexIndex parameter and
                        // all of the indices in the index stream are relative to the baseVertex parameter
                        int minVertexIndex = 0;
                        // Location in the index array at which to start reading vertices
                        int startIndex = chunk.ioffset;
                        device.m_device.DrawIndexedPrimitives(base.cachePrimitive, baseVertex, minVertexIndex, chunk.nverts, startIndex, chunk.nindis / 2);
                        core.nTotDrawCall++;
                    }
                else
                    for (int i = 0; i < BufferChunks.Count; i++)
                    {
                        BatchChunkInfoIndexed chunk = (BatchChunkInfoIndexed)BufferChunks[i];
                        if (!chunk.show) continue;
                        int baseVertex = chunk.voffset; // batching not done
                        int minVertexIndex = 0;
                        int startIndex = chunk.ioffset;
                        m_device.DrawIndexedPrimitives(base.cachePrimitive, baseVertex, minVertexIndex, chunk.nverts, startIndex, chunk.nindis / 2);
                        core.nTotDrawCall++;
                    }
            }
        }

    }

    /// <summary>
    /// Cache used only for not indexed line.list to eliminate unused indices. 
    /// Optimized for Read and Write buffer, store in managed memory pool
    /// </summary>
    public class BatchCacheDynamic_Line : BatchCacheDynamic
    {
        /// <summary>
        /// If UseBatch = false don't use a indices offset so , theoretically , the MaxVertsCount can be greater than index.MaxValue
        /// but need to test
        /// </summary>
        /// <param name="MaxVertsCount">greater vertices index</param>
        /// <param name="UseBatch">use a offset for each indices</param>
        public BatchCacheDynamic_Line(GraphicDevice mdevice, VertexDeclaration declaration, EngineCore Core, bool UseBatch, bool UseLocalCoord)
            : base(mdevice, declaration, PrimitiveType.LineList, Core, UseBatch, UseLocalCoord)
        {
        }

        public override bool isCompatible(Node node)
        {
            //check if is a not indexed line.list primitive
            if (node.primitive!= PrimitiveType.LineList && !(node is Line) ) return false;
            Line shape = (Line)node;

            //check if it using the same vertex format
            return (shape.vertexFormat == this.vertexFormat) && shape.engineProp.isAnimable;
        }

        protected override void DrawImplementation()
        {
            if (m_VB == null || m_VB.Count == 0) return;
            //Console.WriteLine(">> cache" + ID + " render");

            DxDevice m_device = device.m_device;

            NodeMaterial.RemoveFromDevice(device);

            m_VB.SetToDevice(m_device);

            if (base.UseBatch)
            {
                m_device.DrawPrimitives(PrimitiveType.LineList, 0, m_VB.Count / 2);
                core.nTotDrawCall++;
            }
            else
            {
                if (base.UseLocal)
                    for (int i = 0; i < BufferChunks.Count; i++)
                    {
                        BatchChunkInfo chunk = BufferChunks[i];
                        if (!chunk.show) continue;
                        device.renderstates.world = chunk.node.transform * globalCoordSystem;
                        // Offset to add to each vertex index in the index buffer
                        int startVertex = chunk.voffset; // batching not done

                        m_device.DrawPrimitives(PrimitiveType.LineList, startVertex, chunk.nverts / 2);
                        core.nTotDrawCall++;
                    }
                else
                    for (int i = 0; i < BufferChunks.Count; i++)
                    {
                        BatchChunkInfo chunk = BufferChunks[i];
                        if (!chunk.show) continue;
                        int startVertex = chunk.voffset;
                        m_device.DrawPrimitives(PrimitiveType.LineList, startVertex, chunk.nverts / 2);
                        core.nTotDrawCall++;
                    }
            }
        }

    }
}
