﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Engine.Graphics;
using Engine.Renderer;
using Engine.Resources;
using Engine.Tools;
using Engine.Maths;


namespace Engine.Renderer
{
    using Font = Engine.Graphics.Font;

    /// <summary>
    /// Awesome Super Mega Grafic Engine 0.0
    /// </summary>
    [DebuggerDisplay("TotalNodes = {m_nodes.Count}")]
    public class EngineCore : IResettable
    {
        #region singleton
        static NodeMaterial tmp0 = null;
        static NodeMaterial tmp1 = null;
        static int getcount = 0;
        
        public NodeMaterial DefaultTextureNode
        {
            get
            {
                getcount++;
                if (getcount % 2 == 0)
                {
                    if (tmp0 == null) tmp0 = NodeMaterial.TextureFromImage(m_device,Engine.Properties.Resources.empytexture0);
                    return tmp0;
                }
                else
                {
                    if (tmp1 == null) tmp1 = NodeMaterial.TextureFromImage(m_device,Engine.Properties.Resources.empytexture1);
                    return tmp1;
                }
            }
        }
        #endregion

        internal GraphicDevice m_device = null;
        internal Font m_font = null;
        internal UnsortedCollectionManager<Node> m_nodes = new UnsortedCollectionManager<Node>(true);
        internal UnsortedCollectionManager<NodeMaterial> m_materials = new UnsortedCollectionManager<NodeMaterial>(true);
        internal BatchGroup[] m_batchgroups;
        /// <summary>
        /// Node what need to remove completly from m_nodes collector
        /// </summary>
        List<Node> nodeFullRemove = new List<Node>();
        /// <summary>
        /// When you need to adding/removing a lot of nodes you can disable render, then enable it at the end of process.
        /// This avoid all buffer operations for each render call and is very fast.
        /// </summary>
        public bool RedrawEnabled { get; set; }
        /// <summary>
        /// Indices count currently used
        /// </summary>
        public int nTotIndices
        {
            get
            {
                int count = 0;
                foreach (BatchGroup group in m_batchgroups) count += group.nTotIndices;
                return count;
            }
        }
        /// <summary>
        /// Vertices count currently used
        /// </summary>
        public int nTotVerts
        {
            get
            {
                int count = 0;
                foreach (BatchGroup group in m_batchgroups) count += group.nTotVerts;
                return count;
            }
        }      
        /// <summary>
        /// Number of working Nodes, if &lt; nTotNodeCount is possible that not all node are removed
        /// </summary>
        public int nTotNodes
        {
            get
            {
                int count = 0;
                foreach (BatchGroup group in m_batchgroups) count += group.nTotNodes;
                return count;
            }
        }
        /// <summary>
        /// Number of node managed
        /// </summary>
        public int nTotNodeCount
        {
            get { return m_nodes.Count; }
        }
        /// <summary>
        /// Num of meshes batchs
        /// </summary>
        public int nTotCache
        {
            get
            {
                int count = 0;
                foreach (BatchGroup group in m_batchgroups) count += group.nTotCache;
                return count;
            }
        }
        /// <summary>
        /// Debug draw call counter
        /// </summary>
        public int nTotDrawCall { get; set; }
        /// <summary>
        /// Debug texture setting counter
        /// </summary>
        public int nTotTextureSet { get; set; }

        int LastTickCount = 1; // ms
        int Frames = 0;
        float LastFrameRate = 0;
        bool haveJobToDo = false; // check if you need rebuild internal resources

        public EngineCore(GraphicDevice device)
        {
            throw new NotImplementedException("before continue need to complete a basic renderer engine");

            this.m_device = device;
            nTotDrawCall = 0;
            nTotTextureSet = 0;
            RedrawEnabled = true;
            Trace.WriteLine("Device support " + (GraphicDevice.capabilities.MaxVertexIndex > ushort.MaxValue - 1 ? "32" : "16") + "bit indices, maxvertexindex: " + GraphicDevice.capabilities.MaxVertexIndex);


            m_batchgroups = new BatchGroup[]
            {
                new BatchGroupTriangle(device,this),
                new BatchGroupLine(device,this),
                new BatchGroupPoint(device,this)
            };

            m_font = new Font(device, Font.FontName.Arial, 8);
            
            //LineAxisInstance.Init(device.m_device);

            // Tell engineCore to restore some not managed resources...
            // not add to ResourcesToDispose because isn't allowed
            device.resourcesToRestore.Add(this);
        }
        ~EngineCore()
        {
            this.Destroy();
        }
        /// <summary>
        /// Add a node into engine. These operations will accumulate with some temporary list until it's called CompleteJobs()
        /// </summary>
        public bool Add(Node node)
        {
            //Console.WriteLine("ADD " + node.name);
            // insert to dictionary
            if (node.m_key != 0)
            {
                Console.WriteLine("node already added");
                if (node.engineProp.stackoverflowcounter_Add++ > 2)
                {
                    throw new StackOverflowException("Node already added , prevent your code try to add more time the same node example in a infinite loop...");
                }
                return false;
            }

            m_nodes.Add(node);
            //Console.WriteLine("Add node " + node.handle.ToString());

            // insert into best group
            int i = 0;
            switch (node.primitive)
            {
                case PrimitiveType.TriangleStrip:
                case PrimitiveType.TriangleList:
                case PrimitiveType.TriangleFan: i = 0; break;
                case PrimitiveType.LineList:
                case PrimitiveType.LineStrip: i = 1; break;

                case PrimitiveType.PointList: i = 2; break;

                case PrimitiveType.Instance: throw new NotImplementedException("instancing method not implemented");
            }
            node.engineProp = new NodeBatchProperties(node.engineProp);
            
            // if node is a candidate to be remove completly, unflag it
            if (node.engineProp.isMarkAsRemove)
            {
                node.engineProp.isMarkAsRemove = false;
                nodeFullRemove.Remove(node);
            }
            node.engineProp.m_group = m_batchgroups[i];      
            bool result = m_batchgroups[i].AddNode(node);
            haveJobToDo |= result;
            return result;
        }
        /// <summary>
        /// Remove node from engine. These operations will accumulate with some temporary list until it's called CompleteJobs()
        /// </summary>
        public bool Remove(Node node)
        {
            //Console.WriteLine("DEL " + node.m_key.ToString());
            
            if (node.m_key == 0)
            {
                Debug.WriteLine("Node non inizialized");
                return false;
            }
            // now node is a candidate to be removed completly
            if (!node.engineProp.isMarkAsRemove)
            {
                nodeFullRemove.Add(node);
                node.engineProp.isMarkAsRemove = true;
            }

            if (node.engineProp.m_group!=null)
            {
                if (!node.engineProp.m_group.Remove(node))
                {
                    if (node.engineProp.stackoverflowcounter_Remove++ > 2)
                        throw new StackOverflowException("try to remove more than one time... exception for a safety test of infinite loop");
                    return false;
                }
            }
            else
            {
                Debug.WriteLine("Not find group of node");
                return false;
            }
            haveJobToDo = true;
            return true;
        }
        /// <summary>
        /// Update a node in engine. These operations will accumulate with some temporary list until it's called CompleteJobs()
        /// </summary>
        /// <returns>
        /// return false if node are already in update process or aren't in buffer
        /// </returns>
        public bool Update(Node node)
        {
            //Console.WriteLine("UP " + node.ToString()); 
            if (node.m_key == 0)
            {
                Debug.WriteLine("Node non inizialized");
                return false;
            }        
            if (node.engineProp.m_group != null)
            {
                if (node.engineProp.stackoverflowcounter_Update++>10)
                    throw new StackOverflowException("try to update more than one time... exception for a safety test of infinite loop");
                
                bool result = node.engineProp.m_group.UpdateNode(node);
                haveJobToDo |= result;
                return result;
            }
            else
            {
                throw new ArgumentNullException("node " + node.m_key.ToString("X4") + ", not implemented in a group");
            }
        }
       
        public bool Add(NodeMaterial material)
        {
            // insert to dictionary
            if (material.m_key != 0)
            {
                Debug.WriteLine("Texture already added");
                return false;
            }

            m_materials.Add(material);
            //Console.WriteLine("Add node " + node.handle.ToString());
            haveJobToDo = true;
            return true;
        }
        public bool Remove(NodeMaterial material)
        {
            if (material.m_key == 0)
            {
                Debug.WriteLine("Texture non inizialized");
                return false;
            }
            haveJobToDo = true;
            m_materials.Remove(material);
            return true;
        }

        /// <summary>
        /// Remove all (Nodes and Textures) used in this renderer and reset their engine flags to ensure a reutilize.
        /// Dispose all vertex and index buffer.
        /// </summary>
        public void Destroy()
        {
            foreach (BatchGroup group in m_batchgroups)
                group.Destroy();

            foreach (Node node in m_nodes)
            {
                node.engineProp.stackoverflowcounter_Add = 0;
                node.engineProp.stackoverflowcounter_Remove = 0;
                node.engineProp.stackoverflowcounter_Update = 0;
                node.engineProp.ResetFlags();
                node.engineProp.m_group = null;
            }

            List<NodeMaterial> tmp = m_materials.ValueList;
            m_materials.Clear();


            m_nodes.Clear();
            nodeFullRemove.Clear();
        }
        /// <summary>
        /// Complete all calculation and temporary job before render
        /// </summary>
        void CompleteJobs()
        {
            List<Node> nodeToEliminate = new List<Node>();
            
            foreach (BatchGroup group in m_batchgroups)
                group.FlushAll(ref nodeToEliminate);

            Clean();
            haveJobToDo = false;
        }
        /// <summary>
        /// Complete internal arrangement if need and draw all objects
        /// </summary>
        public bool Draw()
        {
            if (haveJobToDo && RedrawEnabled)
                CompleteJobs();

            // increase Frames until are passed one second , then reset frames
            // FPS = N Frames / 1 seconds
            Frames++;
            if (Math.Abs(Environment.TickCount - LastTickCount) > 1000)
            {
                LastFrameRate = (float)Frames * 1000 / Math.Abs(Environment.TickCount - LastTickCount);
                LastTickCount = Environment.TickCount;
                Frames = 0;
            }
            m_device.renderstates.lightEnable = true;


            foreach (BatchGroup group in m_batchgroups)
                group.Draw();


            //m_device.renderstates.lightEnable = false;

            //LineAxisInstance.SetBuffer(m_device.m_device);
            //LineAxisInstance.DrawIstance(m_device.m_device, Matrix4.Identity);

            if (m_font != null)
            {
                m_font.Draw("FPS: " + (int)LastFrameRate, 10, 20, Color.Yellow);
                m_font.Draw("numVertices: " + nTotVerts.ToString("N0", System.Globalization.CultureInfo.InvariantCulture), 10, 30, Color.Yellow);
                m_font.Draw("numIndices : " + nTotIndices.ToString("N0", System.Globalization.CultureInfo.InvariantCulture), 10, 40, Color.Yellow);
                m_font.Draw("numDrawCall: " + nTotDrawCall, 10, 50, Color.Yellow);
                m_font.Draw("numTexture : " + nTotTextureSet, 10, 60, Color.Yellow);
                m_font.Draw("numNodes   : " + m_nodes.Count.ToString("N0", System.Globalization.CultureInfo.InvariantCulture), 10, 70, Color.Yellow);
                m_font.Draw(String.Format("backbuffer:    {0}x{1}", m_device.m_device.BackBufferWidth, m_device.m_device.BackBufferHeight), 10, 80, Color.Yellow);
                m_font.Draw(String.Format("library   :    {0}", m_device.ToString()), 10, 90, Color.Yellow);
            }

            nTotDrawCall = 0;
            nTotTextureSet = 0;
            
            return true;
        }
        /// <summary>
        /// Internal check if all data and references are correct
        /// </summary>
        public bool DebugCheck()
        {
            try
            {
                Debug.WriteLine("\n##### DEBUG #####");

                foreach (Node node in m_nodes)
                {
                    if (((NodeBatchProperties)node.engineProp).chunkflags == BatchChunkBufferFlags.None)
                    {
                        throw new Exception("Not rendering node but collector contain it");
                    }
                    Debug.WriteLine("node " + node.m_key + " debug : " + (Contain(node) ? "OK" : "ERR" + " need remove : " + (((NodeBatchProperties)node.engineProp).chunkflags == BatchChunkBufferFlags.None)));
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
            return true;
        }
        /// <summary>
        /// Tell engine that device is reseting and need restore not managed resources
        /// </summary>
        /// <remarks>
        /// The device's reset event call "Restore()" for all unmanaged memory "BatchCaches", to complete rewriting of
        /// static buffer need to call "WriteAll()" function so need to set "haveJobToDo" = true.
        /// If you don't set to true "haveJobToDo" the renderer draw an empty scene untill you add or remove something
        /// </remarks>
        public void Restore()
        {
            haveJobToDo = true;
        }
        /// <summary>
        /// Remove unused resources
        /// </summary>
        public void Clean()
        {
            // if a node was removed from all group, can be removed also from main collection
            foreach (Node node in nodeFullRemove)
            {
                if (m_nodes.Remove(node))
                {
                    node.engineProp.Reset();
                }
                else
                {
                    throw new Exception("i want nodeFullRemove work correctly");
                }
            }
            nodeFullRemove.Clear();

            foreach (NodeMaterial mat in m_materials)
            {
                if (mat.numassignment == 0) mat.m_removed = true;
            }
            m_materials.Clean();

        }

        public void removeallnodesref()
        {
            foreach (BatchGroup group in m_batchgroups)
                group.removeallnodesref();
            m_nodes.Clear();
        }
                  

        public bool Contain(Node node)
        {
            if (node.engineProp.m_group !=null)
                return node.engineProp.m_group.Contain(node);
            else
                return false;
        }
        
        public bool Contain(NodeMaterial material)
        {
            return m_materials.Contain(material);
        }
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("### ENGINE CORE ###");
            str.AppendLine("tot verts : " + nTotVerts);
            str.AppendLine("tot indis : " + nTotIndices);
            str.AppendLine("ntot obj  : " + nTotNodes);
            str.AppendLine("caches    : " + nTotCache);
            str.AppendLine(m_nodes.ToString());

            foreach (BatchGroup group in m_batchgroups)
                str.AppendLine(group.ToString());
            return str.ToString();
        }
    }
}
