﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

using Engine.Maths;
using Engine.Renderer;

namespace Engine.Graphics
{
    public abstract class MaterialBase
    {
        protected static MaterialBase prevmaterial = null;

        protected GraphicDevice device; 
        protected Material material;

        public MaterialBase(GraphicDevice device)
        {
            this.device = device;
        }

        /// <summary>
        /// Restore all properties\parameters before Begin() start, will be used example when you are using a new Material
        /// and previous properties must be updated.
        /// </summary>
        public abstract void ApplyParams();
        /// <summary>
        /// Start using this material and return number of passage to do for current technique
        /// </summary>
        public abstract int Begin();
        public abstract void BeginPass(int pass);
        public abstract void EndPass();
        public abstract void End();
    }

    /// <summary>
    /// FFP Material, for fixed function pipeline 
    /// </summary>
    public class MaterialDF : MaterialBase
    {
        protected bool prelight;
        protected float predepth;
        protected Matrix4 proj, view, world;

        public bool SolidWireframe { get; set; }

        public MaterialDF(GraphicDevice device)
            : base(device)
        {
            material = Material.Default;
            SolidWireframe = false;
        }
        
        public virtual Matrix4 Projection
        {
            get { return proj; }
            set { proj = value;}
        }
        public virtual Matrix4 View
        {
            get { return view; }
            set { view = value;} 
        }
        public virtual Matrix4 World
        {
            get { return world; }
            set { world = value; }
        }

        public override int Begin()
        {
            if (prevmaterial != this) ApplyParams();
            prevmaterial = this;
            return SolidWireframe ? 2 : 1;
        }

        public override void ApplyParams()
        {
            if (prevmaterial is MaterialFX) device.SetVertexShaderNULL();
            prelight = device.renderstates.lightEnable;
            predepth = device.renderstates.depth;
        }

        public override void BeginPass(int pass)
        {  
            device.renderstates.projection = proj;
            device.renderstates.view = view;
            device.renderstates.world = world;
            
            if (pass == 0)
            {
                device.renderstates.material = material;
                device.renderstates.depth = 0;
                device.renderstates.fillMode = FillMode.Solid;
            }
            else
            {

                device.renderstates.material = new Material { Diffuse = Color.Black };
                device.renderstates.lightEnable = true;
                device.renderstates.depth = -0.01f;
                device.renderstates.fillMode = FillMode.WireFrame;
            }
        }
        public override void EndPass()
        {

        }
        public override void End()
        {
            device.renderstates.lightEnable = prelight;
            device.renderstates.depth = predepth;
        }

    }

    /// <summary>
    /// FFP Material, using one texture
    /// </summary>
    public class MaterialDFTextured : MaterialDF
    {
        Texture texture;

        public MaterialDFTextured(GraphicDevice device, Texture texture)
            : base(device)
        {
            this.texture = texture;
        }

        public override void BeginPass(int pass)
        {
            device.renderstates.projection = proj;
            device.renderstates.view = view;
            device.renderstates.world = world;

            if (pass == 0)
            {
                texture.SetToDevice();
                device.renderstates.material = material;
                device.renderstates.depth = 0;
                device.renderstates.fillMode = FillMode.Solid;
            }
            else
            {
                device.RemoveTexture();
                device.renderstates.material = new Material { Diffuse = Color.Black };
                device.renderstates.lightEnable = true;
                device.renderstates.depth = -0.01f;
                device.renderstates.fillMode = FillMode.WireFrame;          
            }
        }
    }

    /// <summary>
    /// Effect Material, with shader code , will be the standard
    /// </summary>
    public abstract class MaterialFX : MaterialBase
    {
        protected Effect effect;

        public MaterialFX(GraphicDevice device, string shadercode)
            : base(device)
        {
            effect = new Effect(device, shadercode);
        }

        public override int Begin()
        {
            //Console.WriteLine("Begin " + this.ToString());
            if (prevmaterial != this)
            {
                //update all parameters
                //Console.WriteLine("ApplyParams");
                ApplyParams();   
            }
            effect.Parameters.UpdateParams(true);
            prevmaterial = this;
            return effect.Begin();
        }

        public override void BeginPass(int pass)
        {
            effect.BeginPass(pass);
        }
        
        public override void EndPass()
        {
            effect.EndPass();
        }

        public override void End()
        {
            //Console.WriteLine("End");
            effect.End();
        }

        public override void ApplyParams()
        {
            effect.Parameters.UpdateParams(true);
        }
    }

    /// <summary>
    /// Require only POSITION semantic in vertex buffer
    /// </summary>
    public class MaterialFX_POSITION : MaterialFX
    {
        public MaterialFX_POSITION(GraphicDevice device)
            : base(device,Engine.Properties.Resources.FxSimple)
        {
            Version vshaderVer = DX9Enumerations.VertexShaderVersion;
            int constants = DX9Enumerations.MaxVertexShaderConst;
            if (vshaderVer.Major < 1 || constants < 2) throw new Exception("This graphic card don't support this material");

            effect.Parameters.Add("Diffuse", new EffectAttributeColor(Color.Blue, "material.diffuse"));
            effect.Parameters.Add("Wireframe", new EffectAttributeColor(Color.Black,"material.ambient"));
            effect.Parameters.Add("Transform", new EffectAttributeMatrix(Matrix4.Identity, "WorldViewProj"));
            effect.Parameters.Add("Technique", new EffectAttributeTechnique("DefaultTechique"));
            //effect.Technique = "WireframeTechique2";
            SolidWireframe = false;
        }

        public bool SolidWireframe
        {
            get { return (string)(effect.Parameters["Technique"]) == "WireframeTechique2"; }
            set { effect.Parameters["Technique"] = value ? "WireframeTechique2" : "DefaultTechique"; }
        }
        public Color32 Diffuse
        {
            get { return (Color32)effect.Parameters["Diffuse"]; }
            set { effect.Parameters["Diffuse"] = (Color32)value; }
        }
        public Color32 WireframeColor
        {
            get { return (Color32)effect.Parameters["Wireframe"]; }
            set { effect.Parameters["Wireframe"] = (Color32)value; }
        }
        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
    }
    /// <summary>
    /// Require POSITION and COLOR semantic in vertex buffer, draw using vertex color
    /// </summary>
    public class MaterialFX_VCOLOR : MaterialFX
    {
        public MaterialFX_VCOLOR(GraphicDevice device)
            : base(device, Engine.Properties.Resources.FxVertexColor)
        {
            Version vshaderVer = DX9Enumerations.VertexShaderVersion;
            if (vshaderVer.Major < 2) throw new Exception("This graphic card don't support shader 2.0");

            effect.Parameters.Add("Technique", new EffectAttributeTechnique("DefaultTechique"));
            effect.Parameters.Add("Transform", new EffectAttributeMatrix(Matrix4.Identity, "WorldViewProj"));
            effect.Parameters.Add("Wireframe", new EffectAttributeColor((Color32)Color.Black, "wireframecolor"));
            SolidWireframe = false;
        }
        public Color32 WireframeColor
        {
            get { return (Color32)effect.Parameters["Wireframe"]; }
            set { effect.Parameters["Wireframe"] = value; }
        }
        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = value; }
        }
        public bool SolidWireframe
        {
            get { return (string)(effect.Parameters["Technique"]) == "WireframeTechique2"; }
            set { effect.Parameters["Technique"] = value ? "WireframeTechique2" : "DefaultTechique"; }
        } 
    }

    /// <summary>
    /// Require POSITION and TEXCOORD(float3) for traslation semantic in vertex buffer
    /// </summary>
    public class MaterialFX_INSTANCING : MaterialFX
    {
        public MaterialFX_INSTANCING(GraphicDevice device)
            : base(device, Engine.Properties.Resources.FxInstancing)
        {
            if (DX9Enumerations.VertexShaderVersion.Major < 3) throw new Exception("This graphic card don't support shader 3.0");

            effect.Parameters.Add("Technique", new EffectAttributeTechnique("InstancingTechique"));
            effect.Parameters.Add("Diffuse", new EffectAttributeColor((Color32)Color.Blue, "material.ambient"));
            effect.Parameters.Add("Wireframe", new EffectAttributeColor((Color32)Color.Black, "material.diffuse"));
            effect.Parameters.Add("Transform", new EffectAttributeMatrix(Matrix4.Identity, "WorldViewProj"));
            SolidWireframe = false;
        }

        public Color32 WireframeColor
        {
            get { return (Color32)effect.Parameters["Wireframe"]; }
            set { effect.Parameters["Wireframe"] = value; }
        }
        public Color32 Diffuse
        {
            get { return (Color32)effect.Parameters["Diffuse"]; }
            set { effect.Parameters["Diffuse"] = value; }
        }
        public bool SolidWireframe
        {
            get { return (string)(effect.Parameters["Technique"]) == "WireframeTechique2"; }
            set { effect.Parameters["Technique"] = value ? "WireframeTechique2" : "InstancingTechique"; }
        } 
        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = value; }
        }
    }
    
    
    public class MaterialFX_BILLBOARD : MaterialFX
    {
        Techniques current;
        public enum Techniques
        {
            CylinderBillboard,
            ParallelBillboard,
            SphericBillboard,

            CylinderBillboardWithTexture,
            ParallelBillboardWithTexture,
            SphericBillboardWithTexture,

            ParallelBillboardWithWireframeTexture
        }

        public MaterialFX_BILLBOARD(GraphicDevice device)
            : base(device, Engine.Properties.Resources.FxBillboard)
        {
            if (DX9Enumerations.VertexShaderVersion.Major < 1) throw new Exception("This graphic card don't support shader 3.0");

            effect.Parameters.Add("Technique", new EffectAttributeTechnique("CylBillboardTechique"));
            effect.Parameters.Add("Transform", new EffectAttributeMatrix("xWorldViewProj"));
            effect.Parameters.Add("CameraPos", new EffectAttributeVector3("xEye"));
            effect.Parameters.Add("CameraTarghet", new EffectAttributeVector3("xTarghet"));
            effect.Parameters.Add("WindXZ", new EffectAttributeVector2(new Vector2(1,1), "xWind"));
            effect.Parameters.Add("AllowedDir", new EffectAttributeVector3(Vector3.UnitY, "xAllowedRotDir"));
            effect.Parameters.Add("Texture", new EffectAttributeTexture(null, "xBillboardTexture"));

            Method = Techniques.ParallelBillboard;
        }
        public Techniques Method
        {
            get { return current; }
            set
            {
                string tech = "CylBillboardTechique";
                switch (value)
                {
                    case Techniques.CylinderBillboard: tech = "CylBillboardTechique"; break;
                    case Techniques.ParallelBillboard: tech = "ParBillboardTechique"; break;
                    case Techniques.SphericBillboard: tech = "SphBillboardTechique"; break;

                    case Techniques.CylinderBillboardWithTexture: tech = "CylBillboardTextureTechique"; break;
                    case Techniques.ParallelBillboardWithTexture: tech = "ParBillboardTextureTechique"; break;
                    case Techniques.SphericBillboardWithTexture: tech = "SphBillboardTextureTechique"; break;

                    case Techniques.ParallelBillboardWithWireframeTexture: tech = "ParBillboardTextureWireTechique"; break;
                    default: throw new ArgumentException();
                }
                effect.Parameters["Technique"] = tech;
                current = value;
            }
        }

        public Matrix4 ProjViewWorld
        {
            get { return (Matrix4)effect.Parameters["Transform"]; }
            set { effect.Parameters["Transform"] = (Matrix4)value; }
        }
        public Vector3 Eye
        {
            get { return (Vector3)effect.Parameters["CameraPos"]; }
            set { effect.Parameters["CameraPos"] = (Vector3)value; }
        }
        public Vector3 Targhet
        {
            get { return (Vector3)effect.Parameters["CameraTarghet"]; }
            set { effect.Parameters["CameraTarghet"] = (Vector3)value; }
        }
        public Vector3 AllowedDir
        {
            get { return (Vector3)effect.Parameters["AllowedDir"]; }
            set { effect.Parameters["AllowedDir"] = (Vector3)value; }
        }
        public Texture DiffuseMap
        {
            get { return (Texture)effect.Parameters["Texture"]; }
            set { effect.Parameters["Texture"] = (Texture)value; }
        }
        public Vector2 WindXZ
        {
            get { return (Vector2)effect.Parameters["WindXZ"]; }
            set { effect.Parameters["WindXZ"] = (Vector2)value; }
        }
    }

    public class MaterialFX_Bump : MaterialFX
    {
        public MaterialFX_Bump(GraphicDevice device, Texture diffuse, Texture bump)
            : base(device,"")
        {

        }
    }
}
