﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Maths;
using Font = Engine.Graphics.Font;

namespace Test_heights_encode
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Form1 form = new Form1();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        GraphicDevice device;
        public static MaterialFX_VCOLOR materialVC;
        public static MaterialFX_POSITION materialP;

        TrackBallCamera2 camera;
        Terrain terrain;
        AxisObj axis;
        Font font;

        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();

            device = new GraphicDevice(this);
            font = new Font(device, Engine.Graphics.Font.FontName.Arial, 8);
            materialVC = new MaterialFX_VCOLOR(device);
            materialP = new MaterialFX_POSITION(device);

            axis = new AxisObj(device);
            axis.material = materialVC;

            camera = new TrackBallCamera2(this, new Vector3(-70, 40, -70), Vector3.Zero, Vector3.UnitY, 0.1f, 1000.0f);

            device.lights.Add(Light.Sun);

            terrain = new Terrain(device);

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (device.BeginDraw())
            {
                device.SetRenderTarghet();
                device.Clear(Color.CornflowerBlue);

                axis.Draw(camera);
                terrain.Draw(camera);


                Vector3 pos;
                pos = Vector3.Project(axis.axe[0], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("X", (int)pos.x, (int)pos.y, Color.Red);
                pos = Vector3.Project(axis.axe[1], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("Y", (int)pos.x, (int)pos.y, Color.Green);
                pos = Vector3.Project(axis.axe[2], camera.Viewport, camera.Projection, camera.View, Matrix4.Identity);
                font.Draw("Z", (int)pos.x, (int)pos.y, Color.Blue);


                device.EndDraw();
                device.PresentDraw();
            }

        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (device == null) return;
            device.Resize(this.ClientSize);
        }

        #region GAME LOOP
        bool draw = true;
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {
                System.Threading.Thread.Sleep(10); //reduce CPU usage 
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion
    }
}
