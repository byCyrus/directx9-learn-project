﻿/* Framework created by RobyDx for Managed DirectX 9.0c for .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * You are free to use this framework 
 * but please, make reference to me or to my website
 * 
 * Framework creato da RobyDx per Managed DirectX 9.0c per .Net 2.0
 * (www.robydx.135.it, RobbyDx@Fastwebnet.it )
 * Siete liberi di usare questo framework
 * ma per favore fate riferimento a me o al mio sito
*/
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.IO;
using DX = SharpDX;
using D3D = SharpDX.Direct3D9;

using Engine.Tools;

namespace Engine.Graphics
{
    public class DX9TextureState
    {
        // default set
        public TextureFilter MinFilter = TextureFilter.None;
        public TextureFilter MagFilter = TextureFilter.None;

        internal void SetStates(D3D.Device m_device, int stage)
        {
            m_device.SetSamplerState(stage, D3D.SamplerState.MinFilter, (D3D.TextureFilter)MinFilter);
            m_device.SetSamplerState(stage, D3D.SamplerState.MagFilter, (D3D.TextureFilter)MagFilter);

            m_device.SetTextureStageState(stage, D3D.TextureStage.ColorOperation, (D3D.TextureOperation)ColorOperation);
            m_device.SetTextureStageState(stage, D3D.TextureStage.ColorArg1, (D3D.TextureArgument)ColorArgument1);
            m_device.SetTextureStageState(stage, D3D.TextureStage.ColorArg2, (D3D.TextureArgument)ColorArgument2);
            m_device.SetTextureStageState(stage, D3D.TextureStage.AlphaOperation, (D3D.TextureOperation)AlphaOperation);
            m_device.SetTextureStageState(stage, D3D.TextureStage.AlphaArg1, (D3D.TextureArgument)AlphaArgument1);

            m_device.SetTextureStageState(stage, D3D.TextureStage.TexCoordIndex, TexCoordIndex);

            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat00, BumpEnvironmentMat[0, 0]);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat01, BumpEnvironmentMat[0, 1]);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat10, BumpEnvironmentMat[1, 0]);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentMat11, BumpEnvironmentMat[1, 1]);

            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentLOffset, BumpEnvironmentLOffset);
            m_device.SetTextureStageState(stage, D3D.TextureStage.BumpEnvironmentLScale, BumpEnvironmentLScale);

        }

#pragma warning disable
        #region default
        //Per-stage constant color. Use packed integer colors to set this state.
        int Constant = 0;
        //Setting to select the destination register for the result of this stage.
        TextureArgument ResultArg = TextureArgument.Current;
        //Settings for the third alpha operand for triadic operations. 
        TextureArgument AlphaArg0 = TextureArgument.Current;
        //Settings for the third color operand for triadic operations.
        TextureArgument ColorArg0 = TextureArgument.Current;
        //Specifies transformation options for texture coordinates.
        TextureTransform TextureTransformFlags = TextureTransform.Disable;
        //Floating-point offset value for bump-map luminance
        public float BumpEnvironmentLOffset = 0.0f;
        //Floating-point scale value for bump-map luminance
        public float BumpEnvironmentLScale = 0.0f;
        //Index of the texture coordinate set to use with this texture stage.
        //You can specify up to eight sets of texture coordinates per vertex.
        //If a vertex does not include a set of texture coordinates at the specified index, the system defaults to the u and v coordinates (0,0).
        public int TexCoordIndex = 0;
        //coefficient in a bump-mapping matrix
        public float[,] BumpEnvironmentMat = new float[,] { { 0, 0 }, { 0, 0 } };

        //Texture-stage state is the second alpha argument for the stage
        TextureArgument AlphaArg2 = TextureArgument.Current;
        //Texture-stage state is the first alpha argument for the stage
        public TextureArgument AlphaArgument1 = TextureArgument.Texture;
        //Texture-stage state is a texture alpha blending operation. D3D.TextureOperation.Disable for stage > 0
        public TextureOperation AlphaOperation = TextureOperation.SelectArg1;
        //Texture-stage state is the second color argument for the stage
        public TextureArgument ColorArgument2 = TextureArgument.Current;
        //Texture-stage state is the first color argument for the stage
        public TextureArgument ColorArgument1 = TextureArgument.Texture;
        //Texture-stage state is a texture color blending operation D3D.TextureOperation.Disable for stage > 0
        public TextureOperation ColorOperation = TextureOperation.Modulate;
        #endregion
#pragma warning restore
        /// <summary>
        /// A stage > 0 use different setting
        /// </summary>
        public static DX9TextureState GetDefaultSetting(int stage)
        {
            DX9TextureState states = new DX9TextureState();
            if (stage > 0)
            {
                states.TexCoordIndex = 0;
                states.AlphaOperation = TextureOperation.Disable;
                states.ColorOperation = TextureOperation.Disable;
            }
            return states;
        }
    }
    /// <summary>
    /// SharpDX texture
    /// </summary>
    public class DX9Texture : IDisposable
    {
        D3D.Device m_device;
        public D3D.Texture m_texture;
        public DX9TextureState m_state;
        public int m_width, m_height;
        public Format m_pixelformat;

        private DX9Texture(DX9Device device,int stage)
        {
            m_device = device.m_device;
            m_state = DX9TextureState.GetDefaultSetting(0);
            m_pixelformat = Format.A8R8G8B8;
        }

        public static DX9Texture FromEmpty(DX9Device device, Size size, Usage usage, Pool memory, int stage)
        {
            DX9Texture text = new DX9Texture(device, stage);
            text.m_texture = new D3D.Texture(device.m_device, size.Width, size.Height, 0, (D3D.Usage)usage, D3D.Format.A8R8G8B8, (D3D.Pool)memory);
            text.m_width = size.Width;
            text.m_height = size.Height;
            text.m_pixelformat = Format.A8R8G8B8;
            return text;
        }

        public static DX9Texture FromBitmap(DX9Device device, Bitmap bitmap, Usage usage, Pool memory,int stage)
        {
            if (usage != Usage.None && usage != Usage.RenderTarget)
                throw new ArgumentException("usage not allow");

            DX9Texture text = new DX9Texture(device,stage);

            ImageConverter converter = new ImageConverter();
            byte[] buffer = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));

            text.m_texture = D3D.Texture.FromMemory(device.m_device, buffer, (D3D.Usage)usage, (D3D.Pool)memory);
            text.m_width = bitmap.Width;
            text.m_height = bitmap.Height;
            return text;
        }
        public static DX9Texture FromFilename(DX9Device device, string filename, Usage usage, Pool memory, int stage)
        {
            if (usage != Usage.None && usage != Usage.RenderTarget)
                throw new ArgumentException("usage not allow");

            DX9Texture text = new DX9Texture(device, stage);

            text.m_texture = D3D.Texture.FromFile(device.m_device, filename, (D3D.Usage)usage, (D3D.Pool)memory);
            
            D3D.Surface surface = text.m_texture.GetSurfaceLevel(0);
            text.m_width = surface.Description.Width;
            text.m_height = surface.Description.Height;
            return text;
        }
        public static DX9Texture FromBitmap(DX9Device device, Bitmap bitmap, Usage usage, Pool memory)
        {
            return DX9Texture.FromBitmap(device, bitmap, usage, memory, 0);
        }
        
        public IntPtr Lock()
        {
            DX.DataRectangle ret = m_texture.LockRectangle(0, D3D.LockFlags.None);
            return ret.DataPointer;
        }
        public void UnLock()
        {
            m_texture.UnlockRectangle(0);
        }
        public void SetToDevice(int stage)
        {
            m_state.SetStates(m_device, stage);
            m_device.SetTexture(stage, (D3D.BaseTexture)m_texture);
        }
        public void Dispose()
        {
           m_texture.Dispose();
        }
    }

    public class DX9VolumeTexture : IDisposable
    {
        D3D.Device m_device;
       public int m_width, m_height, m_depth;
        public DX9TextureState m_state;
        public D3D.VolumeTexture m_texture;

        public DX9VolumeTexture(DX9Device device, int width, int height, int depth, Usage usage, Pool memory)
        {
            m_device = device.m_device;
            m_state = DX9TextureState.GetDefaultSetting(0);

            if (usage != Usage.None && usage != Usage.RenderTarget)
                throw new ArgumentException("usage not allow");

            m_width = width;
            m_height = height;
            m_depth = depth;

            if (depth > 8) throw new ArgumentException("can use more than 8 textures");

            m_texture = new D3D.VolumeTexture(m_device, width, height, depth, 1, (D3D.Usage)usage, D3D.Format.X8R8G8B8, (D3D.Pool)memory);
        }

        /// <summary>
        /// Lock entire buffer of all texture TODO : lock only some
        /// </summary>
        public IntPtr Lock(LockFlags mode)
        {
            DX.DataBox data = m_texture.LockBox(0, (D3D.LockFlags)mode);
            return data.DataPointer;
        }
        public void UnLock()
        {
            m_texture.UnlockBox(0);
        }
        public void SetToDevice()
        {
            m_state.SetStates(m_device, 0);
            m_device.SetTexture(0, m_texture);
        }

        public void Dispose()
        {
            m_texture.Dispose();
        }
    }
    
    
    /// <summary>
    /// SharpDX texture implementations
    /// </summary>
    public class DxTexture : DisposableResource, IResettable
    {
        public Pool MemoryPool { get { return memoryPool; } }

        DX9Device device = null;
        byte[] m_buffer;
        string m_filename = "";
        Pool memoryPool;
        Usage usage;

        internal D3D.BaseTexture m_texture;

        /// <summary>
        /// Null texture
        /// </summary>
        public DxTexture()
        {
            device = null;
            m_texture = null;
            m_filename = null;
        }
        /// <summary>
        /// From original texture
        /// </summary>
        internal DxTexture(DX9Device device, D3D.BaseTexture texture)
        {
            this.device = device;
            m_texture = texture;
            m_filename = null;
            memoryPool = Pool.Managed;

        }
        /// <summary>
        /// From file name
        /// </summary>
        public DxTexture(DX9Device device, string filename)
        {
            this.device = device;
            this.m_filename = filename;
            Bitmap bmp = new Bitmap(filename);
            Initialize(device, bmp, Pool.Managed);
        }
        /// <summary>
        /// Texture from image
        /// </summary>
        public DxTexture(DX9Device device, Bitmap bitmap)
        {
            if (!DX9Enumerations.SupportTextureSize(bitmap.Size))
                throw new Exception("Device not support this image");

            Initialize(device, bitmap, Pool.Managed);
        }

        void Initialize(DX9Device device, Bitmap bitmap, Pool pool)
        {
            ImageConverter converter = new ImageConverter();
            byte[] buffer = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
            Initialize(device, buffer, pool);
        }
        void Initialize(DX9Device device, MemoryStream stream, Pool pool)
        {
            this.device = device;
            this.memoryPool = pool;
            usage = pool == Pool.Managed ? Usage.None : Usage.WriteOnly;
            m_texture = D3D.Texture.FromStream(device.m_device, (Stream)stream, (D3D.Usage)usage, (D3D.Pool)memoryPool);
            disposed = false;
        }
        void Initialize(DX9Device device, byte[] buffer, Pool pool)
        {
            this.device = device;
            this.memoryPool = pool;
            if (pool != Pool.Managed)
                this.m_buffer = buffer;
            usage = pool == Pool.Managed ? Usage.None : Usage.WriteOnly;

            m_texture = D3D.Texture.FromMemory(device.m_device, buffer, (D3D.Usage)usage, (D3D.Pool)memoryPool);
            disposed = false;
        }
        void Initialize(DX9Device device, string filename, Pool pool)
        {
            usage = pool == Pool.Managed ? Usage.None : Usage.WriteOnly;
            m_texture = D3D.Texture.FromFile(device.m_device, m_filename, (D3D.Usage)usage, (D3D.Pool)memoryPool);
            disposed = false;
        }

        protected override void Dispose(bool disposing)
        {
            // not dispose if it's use managed memory
            if (!disposed && memoryPool != Pool.Managed)
            {
                m_texture.Dispose();
                disposed = true;
            }
        }
        public void Restore()
        {
            // not restore if it's use managed memory
            if (memoryPool == Pool.Managed) return;
            
            Dispose();

            if (m_buffer != null)
            {
                Initialize(device, m_buffer, memoryPool);
            }
            else if (!String.IsNullOrEmpty(m_filename))
            {
                Initialize(device, m_filename, memoryPool);
            }
            throw new ArgumentNullException("not found a memorized image");
        }

    }
}