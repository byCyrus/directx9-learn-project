﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Engine.Maths
{
    /// <summary>
    /// Axis Aligned Bounding Box
    /// </summary>
    public struct BoundaryAABB
    {
        public Vector3 max, min;

        void safetyCheck()
        {
            if (max.x < min.x) Tools.Tool.Swap<float>(ref max.x, ref min.x);
            if (max.y < min.y) Tools.Tool.Swap<float>(ref max.y, ref min.y);
            if (max.z < min.z) Tools.Tool.Swap<float>(ref max.z, ref min.z);
        }
        public Vector3 center
        {
            get { return (max + min) * 0.5f; }
        }

        public BoundaryAABB(Vector3 Max, Vector3 Min)
        {
            max = Max;
            min = Min;
            safetyCheck();
        }
        public BoundaryAABB(Vector3 center, float Length, float Height, float Width)
        {
            max = new Vector3(center.x + Length / 2.0f, center.y + Height / 2.0f, center.z + Width / 2.0f);
            min = new Vector3(center.x - Length / 2.0f, center.y - Height / 2.0f, center.z - Width / 2.0f);
            safetyCheck();
        }
        
        public bool Intersect(Ray ray , out float t)
        {
            return BoundaryAABB.BoundaryIntersectRay(this, ray, out t);
        }
        /// <summary>
        /// Return the intersection with sphere using QRI algorithm (quick rejections intertwined)
        /// </summary>
        public bool Intersect(float centerx, float centery, float centerz, float radius,out float d)
        {
            // TODO : access vector3 by index is slow than access each element xyz
            float e;
            d = 0;
            #region X
            if ((e = centerx - min.x) < 0)
            {
                if (e < -radius) return false;
                d += e * e;
            }
            else if ((e = centerx - max.x) > 0)
            {
                if (e > radius) return false;
                d += e * e;
            }
            
            #endregion
            #region Y
            if ((e = centery - min.y) < 0)
            {
                if (e < -radius) return false;
                d += e * e;
            }
            else if ((e = centery - max.y) > 0)
            {
                if (e > radius) return false;
                d += e * e;
            }
            #endregion
            #region Z
            if ((e = centerz - min.z) < 0)
            {
                if (e < -radius) return false;
                d += e * e;
            }
            else if ((e = centerz - max.z) > 0)
            {
                if (e > radius) return false;
                d += e * e;
            }
            #endregion
            return d <= radius * radius;
        }

        public bool isPointInside(Vector3 p)
        {
            return isPointInside(p.x, p.y, p.z);
        }
        public bool isPointInside(float x, float y, float z)
        {
            return x <= max.x &&
                   y <= max.y && 
                   z <= max.z &&
                   x >= min.x && 
                   y >= min.y && 
                   z >= min.z;
        }

        public static bool BoundaryIntersectRay(Vector3 Max, Vector3 Min, Ray ray, out float t)
        {
            float tx0 = (Min.x - ray.orig.x) * ray.invdir.x;
            float ty0 = (Min.y - ray.orig.y) * ray.invdir.y;
            float tz0 = (Min.z - ray.orig.z) * ray.invdir.z;

            float tx1 = (Max.x - ray.orig.x) * ray.invdir.x;
            float ty1 = (Max.y - ray.orig.y) * ray.invdir.y;
            float tz1 = (Max.z - ray.orig.z) * ray.invdir.z;

            float tmin = Math.Max(Math.Max(Math.Min(tx0, tx1), Math.Min(ty0, ty1)), Math.Min(tz0, tz1));
            float tmax = Math.Min(Math.Min(Math.Max(tx0, tx1), Math.Max(ty0, ty1)), Math.Max(tz0, tz1));

            if (tmax < 0)
            {
                t = tmax;
                return false;
            }
            if (tmin > tmax)
            {
                t = tmax;
                return false;
            }
            t = tmin;
            return true;
        }
        public static bool BoundaryIntersectRay(BoundaryAABB aabb, Ray ray, out float t)
        {
            return BoundaryIntersectRay(aabb.max, aabb.min, ray, out t);
        }
        

        public static BoundaryAABB NaN
        {
            get
            {
                BoundaryAABB empty = new BoundaryAABB();
                // without safety check
                empty.max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
                empty.min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                return empty;
            }
        }
        public bool isNaN
        {
            get
            {
                return
                    max.x < min.x ||
                    max.y < min.y ||
                    max.z < min.z ||
                    max.isNaN ||
                    min.isNaN;
            }
        }
        /// <summary>
        /// Get the Axis aligned boundary from a vertices array
        /// </summary>
        public static BoundaryAABB FromData(IList<Vector3> Points)
        {
            BoundaryAABB aabb = BoundaryAABB.NaN;
            int count = Points.Count;

            if (Points == null || count == 0)
                return aabb;
            for (int i = 0; i < count; i++)
            {
                if (Points[i].x > aabb.max.x) aabb.max.x = Points[i].x;
                if (Points[i].y > aabb.max.y) aabb.max.y = Points[i].y;
                if (Points[i].z > aabb.max.z) aabb.max.z = Points[i].z;
                if (Points[i].x < aabb.min.x) aabb.min.x = Points[i].x;
                if (Points[i].y < aabb.min.y) aabb.min.y = Points[i].y;
                if (Points[i].z < aabb.min.z) aabb.min.z = Points[i].z;
            }
            return aabb;
        }
        
        /// <summary>
        /// Get the Axis aligned boundary from a sphere
        /// </summary>
        public static BoundaryAABB FromSphere(BoundarySphere bsphere)
        {
            BoundaryAABB aabb = BoundaryAABB.NaN;
            aabb.max.x = bsphere.center.x + bsphere.radius;
            aabb.max.y = bsphere.center.y + bsphere.radius;
            aabb.max.z = bsphere.center.z + bsphere.radius;
            aabb.min.x = bsphere.center.x - bsphere.radius;
            aabb.min.y = bsphere.center.y - bsphere.radius;
            aabb.min.z = bsphere.center.z - bsphere.radius;
            aabb.safetyCheck();
            return aabb;
        }
        
        /// <summary>
        /// Merge two Bounding Box, 
        /// </summary>
        public static BoundaryAABB operator +(BoundaryAABB a, BoundaryAABB b)
        {
            // if a == "Empty" will not be added because a.max.X are always < than b.max.X
            // if a and b == "Empty" the result is a Empty box

            Vector3 max = new Vector3(Math.Max(a.max.x, b.max.x), Math.Max(a.max.y, b.max.y), Math.Max(a.max.z, b.max.z));
            Vector3 min = new Vector3(Math.Min(a.min.x, b.min.x), Math.Min(a.min.y, b.min.y), Math.Min(a.min.z, b.min.z));
            return new BoundaryAABB(max, min);
        }

        public override string ToString()
        {
            if (this.isNaN) return "NULL_AABB";

            StringBuilder str = new StringBuilder();
            str.Append(string.Format("MAX : {0,4} {1,4} {2,4}\n", max.x, max.y, max.z));
            str.Append(string.Format("Min : {0,4} {1,4} {2,4}\n", min.x, min.y, min.z));

            return str.ToString();
        }
    }

    /// <summary>
    /// Oriented Bounding Box , world matrix can be used as transformation
    /// </summary>
    public struct BoundaryOBB
    {
        Matrix4 _world;
        float _x, _y, _z; // the semi-lenght , if < 0 are "NaN"

        /// <summary> orientation and center position</summary>
        public Matrix4 world { get { return _world; } set { _world = value; } }
        /// <summary> delta X , NOT the semi-lenght</summary>
        public float length { get { return _x * 2; } set { _x = value / 2.0f; } }
        /// <summary> delta Y , NOT the semi-height</summary>
        public float height { get { return _y * 2; } set { _y = value / 2.0f; } }
        /// <summary> delta Z , NOT the semi-width</summary>
        public float width { get { return _z * 2; } set { _z = value / 2.0f; } }
        
        /// <summary> 
        /// Get or Set the center value, is the position vector of <see cref="world"/>
        /// use it to move boundary : Vector3 Move; center += move
        /// </summary>
        public Vector3 center
        {
            get { return _world.TranslationComponent; }
            set { _world.TranslationComponent = value; }
        }
        
        public BoundaryOBB(Matrix4 World, float Length, float Height, float Width)
        {
            _world = World;
            _x = Length / 2.0f;
            _y = Height / 2.0f;
            _z = Width / 2.0f;
        }
        
        public bool Intersect(Ray ray, out float t)
        {
            return BoundaryOBB.BoundaryIntersectRay(this, ray, out t);
        }

        public static bool BoundaryIntersectRay(BoundaryOBB obb, Ray ray, out float t)
        {
            throw new NotImplementedException();
        }

        public static BoundaryOBB NaN
        {
            get { return new BoundaryOBB(Matrix4.Identity, -1, -1, -1); }
        }
        public bool isNaN 
        {
            get { return _x < 0 || _y < 0 || _z < 0; } 
        }
        /// <summary>
        /// Get the Axis aligned boundary from a Vertices array, the method
        /// try to find the best orientation to minimize volume
        /// </summary>
        public static BoundaryOBB GetMinimumEnclosedBB(IList<Vector3> Points)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// calculate the volume using a local coord system, else use Identity
        /// </summary>
        public static BoundaryOBB FromData(IList<Vector3> Points, Matrix4 incoord)
        {
            if (Points == null || Points.Count == 0)
                return BoundaryOBB.NaN;

            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            
            foreach (Vector3 V in Points)
            {
                Vector3 v = Vector3.TransformCoordinate(V, incoord);
                if (v.x > max.x) max.x = v.x;
                if (v.y > max.y) max.y = v.y;
                if (v.z > max.z) max.z = v.z;

                if (v.x < min.x) min.x = v.x;
                if (v.y < min.y) min.y = v.y;
                if (v.z < min.z) min.z = v.z;
            }
            return new BoundaryOBB(incoord, max.x - min.x, max.y - min.y, max.z - min.z);
        }
        /// <summary>
        /// Get the AABB from OBB, the orientation will be lost
        /// </summary>
        public static explicit operator BoundaryAABB(BoundaryOBB obb)
        {
            Vector3 c = obb.center;
            Vector3[] Points = new Vector3[8];
            Points[0] = new Vector3(c.x - obb.length, c.y - obb.height, c.z - obb.width);
            Points[1] = new Vector3(c.x - obb.length, c.y - obb.height, c.z + obb.width);
            Points[2] = new Vector3(c.x - obb.length, c.y + obb.height, c.z - obb.width);
            Points[3] = new Vector3(c.x - obb.length, c.y + obb.height, c.z + obb.width);
            Points[4] = new Vector3(c.x + obb.length, c.y - obb.height, c.z - obb.width);
            Points[5] = new Vector3(c.x + obb.length, c.y - obb.height, c.z + obb.width);
            Points[6] = new Vector3(c.x + obb.length, c.y + obb.height, c.z - obb.width);
            Points[7] = new Vector3(c.x + obb.length, c.y + obb.height, c.z + obb.width);
            Matrix4 world = obb.world;
            for (int i = 0; i < 8; i++)
                Points[i] = world * Points[i];

            return BoundaryAABB.FromData(Points);

        }
        
        public override string ToString()
        {
            if (this.isNaN) return "NULL_OBB";

            StringBuilder str = new StringBuilder();
            str.Append(string.Format("Center : {0,4} {1,4} {2,4}\n", center.x, center.y, center.z));
            str.Append(string.Format("Dimension : {0,4} {1,4} {2,4}\n", length, height, width));

            return str.ToString();
        }
    }

    /// <summary>
    /// Bounding Sphere
    /// </summary>
    public struct BoundarySphere
    {
        Vector3 _center;
        float _radius; // if radius < 0 is "Empty"

        public Vector3 center { get { return _center; } set { _center = value; } }
        public float radius { get { return _radius; } set { _radius = value; } }

        public BoundarySphere(Vector3 Center, float Radius)
        {
            _center = Center;
            _radius = Radius;
        }

        public bool Intersect(Ray ray, out float t0, out float t1)
        {
            return BoundarySphere.BoundaryIntersectRay(this, ray, out t0, out t1);
        }
        /// <summary> Sphere-Ray intersection, usefull know that if t0&lt0 the ray start inside sphere </summary>
        /// <param name="t0">enter intersection at ray(t0)</param>
        /// <param name="t1">exit intersection at ray(t1)</param>
        /// <returns></returns>
        public static bool BoundaryIntersectRay(Vector3 Center, float Radius, Ray ray, out float t0, out float t1)
        {
            t0 = 0;
            t1 = 0;
            if (Radius < 0) return false;

            Vector3 p = ray.orig - Center;
            float a = Vector3.Dot(ray.dir, ray.dir);
            float b = 2.0f * Vector3.Dot(p, ray.dir);
            float c = Vector3.Dot(p, p) - (Radius * Radius);
            float d = (b * b) - (4 * a * c);

            // if discriminant is negative the picking ray missed the sphere, otherwise it intersected the sphere.
            if (d < 0.0f) return false;

            d = (float)Math.Sqrt(d);
            t0 = 0.5f * (-b + d) / a;
            t1 = 0.5f * (-b - d) / a;

            if (t1 < t0) Tools.Tool.Swap<float>(ref t1, ref t0);

            return true;
        }
        /// <summary>
        /// avoid the calculation of T0 and T1 , return true also if Ray Origin is inside sphere
        /// </summary>
        public static bool BoundaryIntersectRay(Vector3 Center, float Radius, Ray ray)
        {
            if (Radius < 0) return false;

            Vector3 p = ray.orig - Center;
            float a = Vector3.Dot(ray.dir, ray.dir);
            float b = 2.0f * Vector3.Dot(p, ray.dir);
            float c = Vector3.Dot(p, p) - (Radius * Radius);
            float d = (b * b) - (4 * a * c);

            // if discriminant is negative the picking ray missed the sphere, otherwise it intersected the sphere.
            if (d < 0.0f) return false;
            return true;
        }
              
        public static bool BoundaryIntersectRay(BoundarySphere bs, Ray ray, out float t0, out float t1)
        {
            return BoundaryIntersectRay(bs.center, bs.radius, ray, out t0, out t1);
        }
        /// <summary>
        /// A void sphere radius = -1 , center = Zero, used example when you want never collisions 
        /// </summary>
        public static BoundarySphere Zero
        {
            get { return new BoundarySphere(Vector3.Zero, -1); }
        }
        /// <summary>
        /// A not usable sphere, not zero, used example when you want considerate the struct not processed or with some errors
        /// radius = -1 , center = NaN
        /// </summary>
        public static BoundarySphere NaN
        {
            get { return new BoundarySphere(Vector3.NaN, -1); }
        }
        public bool isNaN { get { return _radius < 0; } }
        /// <summary>
        /// http://softsurfer.com/Archive/algorithm_0107/algorithm_0107.htm
        /// Copyright 2001, softSurfer (www.softsurfer.com)
        /// This code may be freely used and modified for any purpose
        /// providing that this copyright notice is included with it.
        /// SoftSurfer makes no warranty for this code, and cannot be held
        /// liable for any real or imagined damage resulting from its use.
        /// Users of this code must verify correctness for their application.
        /// </summary>
        public static BoundarySphere FromDataFast(IList<Vector3> Points)
        {
            if (Points == null || Points.Count == 0)
                return BoundarySphere.NaN;
            int count = Points.Count;
            Vector3 C = Vector3.Zero;
            float R = 0;
            //////////////////////////////////////////////////////// 1
            //  get three pair of points
            //  P1 (minimum x) P2 (maximum x)
            //  Q1 (minimum y) Q2 (maximum y)
            //  R1 (minimum z) R2 (maximum z)
            //  find the pair with mamimum distance (using square distance)
            float xmin = Points[0].x;
            float xmax = Points[0].x;
            float ymin = Points[0].y;
            float ymax = Points[0].y;
            float zmin = Points[0].z;
            float zmax = Points[0].z;
            int Pxmin = 0;
            int Pxmax = 0;
            int Pymin = 0;
            int Pymax = 0;
            int Pzmin = 0;
            int Pzmax = 0;


            //find a large diameter to start with
            for (int i = 0; i < count; i++)
            {
                if (Points[i].x < xmin)
                {
                    xmin = Points[i].x;
                    Pxmin = i;
                }
                else if (Points[i].x > xmax)
                {
                    xmax = Points[i].x;
                    Pxmax = i;
                }
                else if (Points[i].y < ymin)
                {
                    ymin = Points[i].y;
                    Pymin = i;
                }
                else if (Points[i].y > ymax)
                {
                    ymax = Points[i].y;
                    Pymax = i;
                }
                else if (Points[i].z < zmin)
                {
                    zmin = Points[i].y;
                    Pzmin = i;
                }
                else if (Points[i].z > zmax)
                {
                    zmax = Points[i].y;
                    Pzmax = i;
                }
            }
            Vector3 dx = Points[Pxmax] - Points[Pxmin];
            Vector3 dy = Points[Pymax] - Points[Pymin];
            Vector3 dz = Points[Pzmax] - Points[Pzmin];

            float dx2 = Vector3.GetLengthSquared(dx);
            float dy2 = Vector3.GetLengthSquared(dy);
            float dz2 = Vector3.GetLengthSquared(dz);

            float rad2;
            // pair x win
            if (dx2 > dy2 && dx2 > dz2)
            {
                C = Points[Pxmin] + (dx * 0.5f); //Center = midpoint of extremes
                rad2 = Vector3.GetLengthSquared(Points[Pxmax] - C);//radius squared
            }
            // pair y win
            else if (dy2 > dx2 && dy2 > dz2)
            {
                C = Points[Pymin] + (dy * 0.5f);
                rad2 = Vector3.GetLengthSquared(Points[Pymax] - C);
            }
            // pair z win
            else
            {
                C = Points[Pzmin] + (dz * 0.5f);
                rad2 = Vector3.GetLengthSquared(Points[Pzmax] - C);
            }
            float rad = (float)Math.Sqrt(rad2);


            ///////////////////////////////////////////////////////////// 2
            //  now check that all points V[i] are in the ball
            //  and if not, expand the ball just enough to include them

            Vector3 dV = Vector3.Zero;
            float dist, dist2;

            for (int i = 0; i < count; i++)
            {
                dV = Points[i] - C;
                dist2 = Vector3.GetLengthSquared(dV);

                if (dist2 > rad2)
                {
                    //V[i] not in ball, so expand ball to include it
                    dist = (float)Math.Sqrt(dist2);
                    rad = (rad + dist) / 2.0f; //enlarge radius just enough
                    rad2 = rad * rad;
                    C = Points[i] - dV * (rad / dist);  //shift Center toward V[i]
                    //C = C + dV * ((rad - dist) / dist);  //shift Center toward V[i]
                    Console.Write("");
                }
            }
            R = rad;

            return new BoundarySphere(C, R);
        }
        /// <summary>
        /// calculate O(2n) Center = (Sum all vertices)/(num vertices) ,
        /// Radius = Max(dist(vertex i , center)).
        /// </summary>
        public static BoundarySphere FromDataBasic(IList<Vector3> Points)
        {
            Vector3 C = Vector3.Zero;
            float R = 0.0f;

            float rad2 = 0.0f;

            foreach (Vector3 v in Points) C += v;
            C = C * (1.0f / Points.Count);

            foreach (Vector3 v in Points)
            {
                float dist2 = Vector3.GetLengthSquared(v - C);
                if (dist2 > rad2)
                {
                    rad2 = dist2;
                }
            }
            R = (float)Math.Sqrt(rad2);

            return new BoundarySphere(C, R);
        }
        /// <summary>
        /// Merge two Sphere, if "Empty" aren't added
        /// </summary>
        public static BoundarySphere operator +(BoundarySphere a, BoundarySphere b)
        {
            Vector3 v = b._center - a._center;
            float vl = v.Length;

            // b is inside a, b always inside if is b=="Empty"
            if (a._radius >= vl + b._radius) return a;
            // a is inside b, a always inside if is a=="Empty"
            if (b._radius >= vl + a._radius) return b;
            // a + b , if a & b == "Empty" the sum return always a negative radius
            return new BoundarySphere(a._center + b._center + v * ((b._radius - a._radius) / vl) * 0.5f, (vl + a._radius + b._radius) * 0.5f);
        }

        public override string ToString()
        {
            if (this.isNaN) return "NULL_SPHERE";
            return string.Format("Center : {0,4} {1,4} {2,4} Radius : {3,4}\n", _center.x, _center.y, _center.z , _radius);
        }
    }
}
