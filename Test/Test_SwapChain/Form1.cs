﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Maths;
using Engine.Tools;
using Engine.Renderer;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        static Random Rnd = new Random();  
        GraphicDevice device;
        AxisObj axis;
        bool draw = true;

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 myForm = new Form1();
            Application.Idle += new EventHandler(myForm.Application_Idle);
            Application.Run(myForm);
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            InitializeComponent();
            InitializeGraphics();
            InitializeSwapChains();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            device.Dispose();
            base.OnClosing(e);
        }

        void InitializeSwapChains()
        {
            viewport1.InitializeSwapChain(device);
            viewport1.camera = new Viewports.TrackBallController(viewport1, new Vector3(100, 100, 100), Vector3.Zero, Vector3.UnitY);
            viewport1.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);

            viewport2.InitializeSwapChain(device);
            viewport2.camera = new Viewports.TrackBallController(viewport2, new Vector3(-100, 100, 100), Vector3.Zero, Vector3.UnitY);
            viewport2.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);
            
            viewport3.InitializeSwapChain(device);
            viewport3.camera = new Viewports.TrackBallController(viewport3, new Vector3(50, 50, -50), Vector3.Zero, Vector3.UnitY);
            viewport3.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);  
         
            viewport4.InitializeSwapChain(device);
            viewport4.camera = new Viewports.TrackBallController(viewport4, new Vector3(-50, 50, -50), Vector3.Zero, Vector3.UnitY);
            viewport4.RenderFunction = new GraphicPanel.RenderFunctionDelegate(Render);
        }

        void InitializeGraphics()
        {
            device = new GraphicDevice(this);
            //graphicDevice = viewport1.InitializeDevice();

            Light sun = new Light
            {
                Ambient = Color.White,
                Diffuse = Color.White,
                Specular = Color.Black,
                Direction = Vector3.GetNormal( new Vector3(-1, -1, -1)),
                Position = new Vector3(1000,1000,1000),
                Type = LightType.Directional,
                
            };

            Light ambient = new Light
            {
                Ambient = Color.Black,
                Diffuse = Color.FromArgb(100, 100, 100),
                Specular = Color.Black,
                Direction = Vector3.GetNormal(new Vector3(1, 1, 1)),
                Position = new Vector3(-1000, -1000, -1000),
                Type = LightType.Directional,
            };

            device.renderstates.ambient = Color.Black;
            device.renderstates.cull = Cull.None;
            device.renderstates.fillMode = FillMode.Solid;

            device.lights.Add(sun);
            device.lights.Add(ambient);

            axis = new AxisObj(device);
        }

        void Render(GraphicPanel panel, ViewportOption options, ICamera camera)
        {
            if (!draw) return;

            for (int i = 0; i < device.lights.Count; i++)
            {
                Light light = device.lights[i];
                Matrix4 rot = Matrix4.Identity;
                switch (Rnd.Next(3))
                {
                    case 0: rot = Matrix4.RotationX(0.02f); break;
                    case 1: rot = Matrix4.RotationY(0.01f); break;
                    case 2: rot = Matrix4.RotationZ(0.03f); break;
                }
                light.Position = Vector3.TransformCoordinate(light.Position, rot);
                light.Direction = -light.Position.Normal;
                device.lights[i] = light;
            }

            device.renderstates.world = camera.World;
            device.renderstates.projection = camera.Projection;
            device.renderstates.view = camera.View;

            axis.SetCamera(camera);
            axis.Draw();
        }



        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle && draw)
            {
                System.Threading.Thread.Sleep(10);
                this.viewport1.Invalidate();
                this.viewport2.Invalidate();
                this.viewport3.Invalidate();
                this.viewport4.Invalidate();

            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion

        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
        }
        
    }


    public class AxisObj : IDrawable
    {
        MaterialFX_VCOLOR material;
        GraphicDevice device;
        VertexBuffer axis;
        VertexDeclaration cvertex_decl;

        public AxisObj(GraphicDevice device)
        {
            this.material = new MaterialFX_VCOLOR(device);
            this.device = device;
            this.cvertex_decl = new VertexDeclaration(device, CVERTEX.m_elements);

            CVERTEX[] verts = new CVERTEX[]
            {
                new CVERTEX(0,0,0, Color.Red),
                new CVERTEX(10,0,0, Color.Red),
                new CVERTEX(0,0,0, Color.Green),
                new CVERTEX(0,10,0, Color.Green),
                new CVERTEX(0,0,0, Color.Blue),
                new CVERTEX(0,0,10, Color.Blue)
            };
            axis = new VertexBuffer(device, cvertex_decl.Format, verts.Length, false, true);

            VertexStream vdata = axis.OpenStream();
            vdata.WriteCollection<CVERTEX>(verts, 0, verts.Length, 0);
            axis.CloseStream();
            axis.Count = verts.Length;
        }

        public Matrix4 transform
        {
            get { return Matrix4.Identity; }
            set { }
        }
        public void SetCamera(ICamera camera)
        {
            material.ProjViewWorld = camera.Projection * camera.View * camera.World;
        }

        public void Draw()
        {
            cvertex_decl.SetToDevice();
            axis.SetToDevice();

            int count = material.Begin();
            for (int pass = 0; pass < count; pass++)
            {
                material.BeginPass(pass);
                device.DrawPrimitives(PrimitiveType.LineList, 0, axis.Count / 2);
                material.EndPass();
            }
            material.End();
        }

        public void Draw(MaterialBase material)
        {
            throw new NotImplementedException();
        }
        public void SetToDevice()
        {
            throw new NotImplementedException();
        }
        public void Render(MaterialBase material)
        {
            throw new NotImplementedException();
        }
    }

}
