﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Engine.Graphics;
using Engine.Tools;
using Engine.Maths;
using Engine.Renderer;

using Font = Engine.Graphics.Font;

namespace Engine.Test
{
    public partial class Form1 : Form
    {
        GraphicDevice device;
        Font font;

        MeshObj mesh;
        BillboardObj billboards;
        //InstanceObj instances;
        AxisObj axis;

        bool draw = true;
        int drawcall = 0;
        
        CameraValues camera;
        TrackBallCamera_new ballcamera;
        bool useballcamera = true;


        Vector3 eye, look;
        float yangle = MathUtils.DegreeToRadian(135);
        float l;
        Point start = new Point(0, 0);
        Point end = new Point(0, 0);

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Form1 form = new Form1();
            Application.Idle += new EventHandler(form.Application_Idle);
            Application.Run(form);
        }

        public Form1()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.Opaque, true);
            InitializeComponent();
            InitializeGraphics();

            Console.WriteLine(Marshal.SizeOf(typeof(Quaternion)));
        }

        void InitializeGraphics()
        {
            device = new GraphicDevice(this);
            font = new Font(device, Engine.Graphics.Font.FontName.Arial,12);


            Light sun = Light.Sun;
            sun.Position = new Vector3(-100, 100, -100);
            sun.Direction = Vector3.GetNormal(-sun.Position);
            device.lights.Add(sun);


            ////////////////////// mesh
            billboards = new BillboardObj(device);
            //instances = new InstanceObj(device, 100);
            axis = new AxisObj(device);
            mesh = new MeshObj(device);

            eye = new Vector3(-20, 20, -20);
            l = eye.Length;
            look = Vector3.Zero;

            ballcamera = new TrackBallCamera_new(this, eye, look,Vector3.UnitY, 0.1f, 1000.0f);

            UpdateCamera();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            device.Dispose();

        }
        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            draw = false;
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            draw = true;
            device.Resize(this.ClientSize);
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (!draw) return;
            unchecked { drawcall++; }

            UpdateCamera();
   
            bool state = device.BeginDraw();
            Debug.Assert(state, "Render Fail");
            if (state)
            {
              
                device.SetRenderTarghet();
                device.Clear(Color.CornflowerBlue);

                ICamera values;
                if (useballcamera)
                    values = ballcamera; 
                else values = camera;

                axis.SetCamera(values);
                axis.SetToDevice();
                axis.Draw();

                billboards.SetCamera(values);
                billboards.SetToDevice();
                billboards.Draw();

                mesh.SetCamera(values);
                mesh.SetToDevice();
                mesh.Draw();

                //instances.SetCamera(camera);
                //instances.SetToDevice();
                //instances.Draw();

                /*
                if (billboards != null)
                {
                    for (int i = 0; i < billboards.instance.Length; i++)
                    {
                        Vector3 basepos = billboards.instance[i].traslation;
                        Vector3 s = Vector3.Project(basepos, values.Viewport, values.Projection, values.View, values.World);
                        font.Draw(i.ToString(), (int)s.x, (int)s.y, Color.Black);
                    }
                }
                */
                

                device.EndDraw();
                device.PresentDraw();
            }
            
        }

        void swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }
        Vector3 neweye;

        void UpdateCamera()
        {
            neweye = eye;
            neweye = new Vector3(Math.Sin(yangle) * l, eye.y, Math.Cos(yangle) * l);
            yangle += 0.001f;

            camera.nearZ = 0.1f;
            camera.farZ = 500f;
            camera.Viewport = new Viewport(ClientSize.Width, ClientSize.Height, 0, 0);
            camera.Projection = Matrix4.MakeProjectionLH(MathUtils.DegreeToRadian(45), camera.Viewport, camera.nearZ, camera.farZ);
            camera.View = Matrix4.MakeViewLH(neweye, look, Vector3.UnitY);
            camera.inview = Matrix4.Inverse(camera.View);
            camera.World = Matrix4.Identity;
            camera.Eye = neweye;
        }

        #region GAME LOOP
        protected void Application_Idle(object sender, EventArgs e)
        {
            while (AppStillIdle)
            {
                System.Threading.Thread.Sleep(10);
                this.Invalidate();
            }
        }
        public bool AppStillIdle
        {
            get
            {
                PeekMsg msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }
        [System.Security.SuppressUnmanagedCodeSecurity]
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage(out PeekMsg msg, IntPtr hWnd,
                 uint messageFilterMin, uint messageFilterMax, uint flags);

        [StructLayout(LayoutKind.Sequential)]
        public struct PeekMsg
        {
            public IntPtr hWnd;
            public Message msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }
        #endregion


        private void checkbtn_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox btn = (CheckBox)sender;
            useballcamera = !btn.Checked;
        }
    }

    public struct CameraValues : ICamera
    {
        public float nearZ;
        public float farZ;

        public Vector3 Eye { get; set; }
        public Matrix4 World { get; set; }
        public Matrix4 View { get; set; }
        public Matrix4 inview { get; set; }
        public Matrix4 Projection { get; set; }
        /// <summary>
        /// BackBufferSize
        /// </summary>
        public Viewport Viewport { get; set; }

        public Vector3 Targhet
        {
            get { return Vector3.Zero; }
        }
    }


}
