﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;

using Engine.Maths;

namespace Engine.Graphics
{
    /// <summary>
    /// Defines the vertex data layout. Each vertex can contain one or more data types, and each data type is described by a vertex element.
    /// Vertex data is defined using an array of SharpDX.Direct3D9.VertexElement structures. Use D3DDECL_END to declare the last element in the declaration.
    /// </summary>
    public struct VertexElement : IEquatable<VertexElement>
    {
        /// <summary>
        /// Size in bytes of this vertex element
        /// </summary>
        public int bytesize;
        /// <summary>
        /// </summary>
        public DeclarationMethod method;
        /// <summary>
        /// Offset in bytes from the beginning of the vertex data to the data associated with the particular data type.
        /// </summary>
        public short offset;
        /// <summary>
        ///  Stream number
        /// </summary>
        public short stream;
        /// <summary>
        /// The data type, specified as a SharpDX.Direct3D9.DeclarationType. One of several predefined types that define the data size.
        /// Some methods have an implied type.
        /// </summary>
        public DeclarationType type;  
        /// <summary>
        ///  Defines what the data will be used for; that is, the interoperability between vertex data layouts and vertex shaders.
        ///  Each usage acts to bind a vertex declaration to a vertex shader.
        /// </summary>
        /// <remarks>
        /// In some cases, they have a special interpretation.
        /// For example, an element that specifies SharpDX.Direct3D9.DeclarationUsage.Normal or SharpDX.Direct3D9.DeclarationUsage.Position 
        /// is used by the N-patch tessellator to set up tessellation. See SharpDX.Direct3D9.DeclarationUsage for a list of the available semantics. 
        /// SharpDX.Direct3D9.DeclarationUsage.TextureCoordinate can be used for user-defined fields (which don't have an existing usage defined).
        /// </remarks>
        public DeclarationUsage usage;

        /// <summary>
        /// Modifies the usage data to allow the user to specify multiple usage types.
        /// </summary>
        public byte usageIndex; 
        /// <summary>
        /// Used for closing a VertexElement declaration.
        /// </summary>
        public static VertexElement VertexDeclarationEnd = new VertexElement(255, 0, DeclarationType.Unused, DeclarationMethod.Default, DeclarationUsage.Position, 0);

        public VertexElement(int stream, int offset, DeclarationType type, DeclarationMethod method, DeclarationUsage usage, int usageIndex)
            : this((short)stream, (short)offset, type, method, usage, (byte)usageIndex) { }

        public VertexElement(short stream, short offset, DeclarationType type, DeclarationMethod method, DeclarationUsage usage, byte usageIndex)
        {
            if (stream < 0 || offset < 0 || usageIndex < 0 || usageIndex > 7) 
                throw new ArgumentOutOfRangeException(); 

            this.method = method;
            this.stream = stream;
            this.offset = offset;
            this.type = type;
            this.usage = usage;
            this.usageIndex = usageIndex;

            switch (type)
            {
                case DeclarationType.Color: bytesize = sizeof(int); break;
                case DeclarationType.Float1: bytesize = sizeof(float); break;
                case DeclarationType.Float2: bytesize = sizeof(float) * 2; break;
                case DeclarationType.Float3: bytesize = sizeof(float) * 3; break;
                case DeclarationType.Float4: bytesize = sizeof(float) * 4; break;
                case DeclarationType.HalfFour: bytesize = sizeof(ushort) * 4; break;
                case DeclarationType.HalfTwo: bytesize = sizeof(ushort) * 2; break;
                case DeclarationType.Ubyte4: bytesize = sizeof(byte) * 4; break;
                case DeclarationType.Short2: bytesize = sizeof(short) * 2; break;
                case DeclarationType.Short4: bytesize = sizeof(short) * 4; break;
                default: bytesize = 0; break;
            }
        }

        /// <summary>
        /// Return a unique semantic name used in shader code, example for texture return "TEXCOORD0"
        /// </summary>
        public static string GetSemanticName(DeclarationUsage Usage,int UsageIdx)
        {
            return Usage.ToString().ToUpper() + UsageIdx.ToString();
        }
        public string SemanticName
        {
            get { return GetSemanticName(this.usage, this.usageIndex); }
        }

        public bool Equals(VertexElement other)
        {
            return other.stream == stream && other.type == type && other.method == method && other.usage == usage && other.usageIndex == usageIndex;
        }
        public override string ToString()
        {
            if (this.Equals(VertexElement.VertexDeclarationEnd))
                return "END";
            else
                return string.Format("stream{0} : size{1} , offset {2} , type{3}, usage{4}", stream, bytesize, offset, type, usage);
        }
    }



    public struct Material
    {
        public Color32 Ambient { get; set; }
        public Color32 Diffuse { get; set; }
        public Color32 Emissive { get; set; }
        public Color32 Specular { get; set; }
        /// <summary>Power</summary>
        public float SpecularSharpness { get; set; }

        public static readonly Material Default = new Material
        {
            Ambient = Color.Black,
            Diffuse = Color.White,
            Emissive = Color.Black,
            Specular = Color.Black,
            SpecularSharpness = 0.0f,
        };

        public static bool operator ==(Material mat1, Material mat2)
        {
            return (mat1.Ambient == mat2.Ambient) &&
                   (mat1.Diffuse == mat2.Diffuse) &&
                   (mat1.Emissive == mat2.Emissive) &&
                   (mat1.Specular == mat2.Specular) &&
                   (mat1.SpecularSharpness == mat2.SpecularSharpness); 
        }
        public static bool operator !=(Material mat1, Material mat2)
        {
            return !(mat1 == mat2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
    public struct Light
    {
        public Color32 Ambient { get; set; }
        public Color32 Diffuse { get; set; }
        public Color32 Specular { get; set; }
        public Vector3 Direction { get; set; }
        public Vector3 Position { get; set; }
        public LightType Type { get; set; }
        public float Attenuation0 { get; set; }
        public float Attenuation1 { get; set; }
        public float Attenuation2 { get; set; }
        public float Falloff { get; set; }
        public float OuterConeAngle { get; set; }
        public float InnerConeAngle { get; set; }
        public float Range { get; set; }

        public static readonly Light Sun = new Light
        {
            Ambient = Color.Black,
            Diffuse = Color.White,
            Specular = Color.Black,
            Type = LightType.Directional,
            Direction = Vector3.GetNormal(new Vector3(-1, -1, -1)),
            Position = new Vector3(1000, 1000, 1000),
            Range = 1000.0f
        };
    }

    /// <summary>
    /// Defines the window dimensions of a render target surface onto which a 3D volume projects.
    /// </summary>
    public struct Viewport
    {
        //     +-----> x (Width)
        //     |
        //     |
        //     y (Height)

        /// <summary>
        /// </summary>
        /// <param name="width">dX length</param>
        /// <param name="height">dY length</param>
        /// <param name="x">horizontal min</param>
        /// <param name="y">vertical min</param>
        public Viewport(int width, int height, int x, int y)
        {
            Height = height;
            MaxDepth = 1;
            MinDepth = 0;
            Width = width;
            X = x;
            Y = y;
        }
        /// <summary>
        /// Retrieves or sets the height(vertical y) dimension of the viewport on the render target surface, in pixels.
        /// </summary>
        public int Height;
        /// <summary>
        ///  Retrieves or sets the maximum value of the clip volume.
        /// </summary>
        public float MaxDepth;
        /// <summary>
        /// Retrieves or sets the minimum value of the clip volume.
        /// </summary>
        public float MinDepth;
        /// <summary>
        /// Retrieves or sets the width(horizontal x) dimension of the viewport on the render target surface, in pixels.
        /// </summary>
        public int Width;
        /// <summary>
        /// Retrieves or sets the pixel coordinate of the upper-left corner of the viewport on the render target surface.
        /// </summary>
        public int X;
        /// <summary>
        /// Retrieves or sets the pixel coordinate of the upper-left corner of the viewport on the render target surface.
        /// </summary>
        public int Y;
    }

    /// <summary>
    /// 
    /// </summary>
    public struct DisplayMode
    {
        public Format format;
        public int height;
        public int refreshRate;
        public int width;

        public override string ToString()
        {
            return string.Format("Monitor : {0}x{1} f: {2} Hz: {3}", width, height, format, refreshRate);
        }
    }
}
