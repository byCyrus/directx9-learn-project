//-----------------------------------------------------------------------------
// Globals.
//-----------------------------------------------------------------------------
float4x4 WorldViewProj : WORLDVIEWPROJ;
float4 wireframecolor;
//-----------------------------------------------------------------------------
// Vertex Shaders.
//-----------------------------------------------------------------------------
struct VS_INPUT
{
    float4  Position  : POSITION;
	float4  Color     : COLOR0;
};

struct VS_OUTPUT
{   
	float4  Position  : POSITION;
    float4  Color     : COLOR0;
};

VS_OUTPUT VShader(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	input.Position.w = 1;
	output.Position = mul(input.Position, WorldViewProj);
	output.Color = input.Color;
	return output;
}
VS_OUTPUT VShaderB(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	input.Position.w = 1;
	output.Position = mul(input.Position, WorldViewProj);
	output.Color = float4(1,1,1,0);
	return output;
}
VS_OUTPUT VShaderBlack(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 
	input.Position.w = 1;
	output.Position = mul(input.Position, WorldViewProj);
	output.Color = wireframecolor;
	return output;
}
//-----------------------------------------------------------------------------
// Pixel Shaders.
//-----------------------------------------------------------------------------
float4 PShader(VS_OUTPUT Input) : COLOR0
{
	return Input.Color;
}

//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique DefaultTechique
{
    pass p0
    {
        VertexShader = compile vs_3_0 VShader();
        PixelShader =  compile ps_3_0 PShader();
    }
}
technique WireframeTechique2
{
    pass p0
    {
		FillMode = Solid;
        VertexShader = compile vs_3_0 VShader();
        PixelShader =  compile ps_3_0 PShader();
    }
	pass p1
    {
		FillMode = Wireframe;
        VertexShader = compile vs_3_0 VShaderBlack();
        PixelShader =  compile ps_3_0 PShader();
    }
}
